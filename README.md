# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version 1.2
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration:
Windows + MinGW 5.3.0 (32bit) + Qt 5 (for Qt_GUI) + CMake >2.4

* Dependencies:
ABB libMASS library (check INCLUDEPATH in .pro file)

* Database configuration:

Qt_GUI:
The GUI was designed for standard spectra fitting tasks (Spectrum Plotter tab) as well as for spectra simulation (Spectra Sim tab). Inputs for Spectrum Plotter
are therefore disabled here; for Spectra Sim you need crossstack.ini, phys_consts.ini, enTUinputs.ini, AllBroadeningCoefficients.ini and input_stackPT.ini
in /MasterINI directory; crossstack.ini with additional peaks and phys_consts.ini should also be put in /FitINI directory.

Command line app:
Build the project using the top-level CMakeLists. Place /MasterINI, /FitINI in your top-level TestDir folder and create a whateverName folder in TestDir. Place
noiseConfig.txt together with two libraries: libSpcSim.dll and libMASS.dll, as well as spectraSimulator.exe in whateverName.

The resulting file tree of testing directory should be like this:

+ TestDir
|
+ = + MasterINI
|      |
|      + crossstack.ini
|      + phys_consts.ini
|      + input_stackPT.ini
|      + AllBroadeningCoefficients.ini
|      + eNTUinputs.ini
|
+ = + FitINI
|      |
|      + crossstack.ini
|      + phys_consts.ini
|
+ = + whateverName
|      |
|      + libMASS.dll
|      + libSpcSim.dll
|      + noiseConfig.txt
|      + spectraSimulator.exe
|
+ = + Qt_GUI (if needed)
       |
	   + libMASS.dll
	   + libSpcSim.dll
	   + SpectrumCalculator.exe
	   + additional Qt dll's

* How to run tests:
no automatic test yet, use UI or command line app

* Deployment instructions
Place the libSpcSim.dll (built in ProjectDirectory/Src with INCLUDEPATH in .pro file changed appropriately to point to your location of libMASS headers)
and the spectraSimulator.exe according to the file tree above. The executable file for command line testing is built in ProjectDirectory/Exec folder.

To deploy a standalone Qt_GUI application it is preferable to use the Qt Windows Deployment Tool (http://doc.qt.io/qt-5/windows-deployment.html),
which produces all the necessary Qt library files in the directory set as build directory for QT_GUI project. Then copy the SpectrumCalculator.exe
together with all the dlls and put them in your TestDir according to the file tree above.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines
In spcinterfaceimplementation header file there are definitions of spcInterface struct (a pure abstract interface class) and spcImplementation class which is
a singleton designed to create an instance of the interface. The inputs can for now only be read from INI files, development of I/O interface in progress.

### Who do I talk to? ###

* Repo owner or admin:
Bartek Kozłowiec (Mobica)

* Other community or team contact:
Dmitry Skvortsov (ABB) - libMASS source code