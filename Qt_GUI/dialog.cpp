#include "dialog.h"
#include "widget.h"
#include "ui_dialog.h"
#include "spcgenerator.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Dialog::Dialog(QWidget* parent) : QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
    setWindowTitle("Multiple Calculations Dialog");

    ui->groupBox_2->hide();
    layout()->setSizeConstraint(QLayout::SetFixedSize);

    ui->doubleSpinBox_1->setDisabled(true);
    ui->doubleSpinBox_2->setDisabled(true);
    ui->doubleSpinBox_3->setDisabled(true);

    Widget* parentPtr = qobject_cast<Widget*>(this->parent());

    currentVVindex = 0;

    connect(this, SIGNAL(accepted()), this, SLOT(deleteLater()));
    connect(this, SIGNAL(rejected()), this, SLOT(deleteLater()));

    connect(ui->comboBox,SIGNAL(activated(int)),this,SLOT(setVectorVariable(int)));
    connect(this,SIGNAL(vecVarChanged(int)),this,SLOT(disableSpinBox(int)));
    connect(ui->okButton,SIGNAL(clicked(bool)),this,SLOT(fillMsgStruct()));
    connect(this,SIGNAL(msgStructFilled(const multipleCalcsData&)),parentPtr,SLOT(receiveDialogMsg(multipleCalcsData)));

    ui->label->setStyleSheet("QLabel { color : red; }");
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Dialog::~Dialog()
{
    delete ui;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Dialog::setVectorVariable( int sigVar )
{
    currentVVindex = vectorVar(sigVar);
    emit vecVarChanged(currentVVindex);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Dialog::disableSpinBox( int sigVar )
{
    ui->doubleSpinBox_1->setDisabled(false);
    ui->doubleSpinBox_2->setDisabled(false);
    ui->doubleSpinBox_3->setDisabled(false);
    ui->doubleSpinBox_4->setDisabled(false);
    ui->doubleSpinBox_5->setDisabled(false);
    ui->doubleSpinBox_6->setDisabled(false);
    ui->doubleSpinBox_7->setDisabled(false);

    switch( sigVar )
    {
    case Concentration:
        ui->doubleSpinBox_4->setDisabled(true);
        ui->groupBox_2->setVisible(true);
        break;
    case PathLength:
        ui->doubleSpinBox_5->setDisabled(true);
        ui->groupBox_2->setVisible(true);
        break;
    case Pressure:
        ui->doubleSpinBox_6->setDisabled(true);
        ui->groupBox_2->setVisible(true);
        break;
    case Temperature:
        ui->doubleSpinBox_7->setDisabled(true);
        ui->groupBox_2->setVisible(true);
        break;
    case None:
        ui->doubleSpinBox_1->setDisabled(true);
        ui->doubleSpinBox_2->setDisabled(true);
        ui->doubleSpinBox_3->setDisabled(true);
    default:
        break;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Dialog::fillMsgStruct()
{
    msgStruct = multipleCalcsData();
    msgStruct.vecVarIndex = currentVVindex;

    if( ui->doubleSpinBox_4->isEnabled() )
        msgStruct.O2concen = percent2ppm( ui->doubleSpinBox_4->value() );

    if( ui->doubleSpinBox_5->isEnabled() )
        msgStruct.pathLen  = ui->doubleSpinBox_5->value();

    if( ui->doubleSpinBox_6->isEnabled() )
        msgStruct.pres     = bar2torr( ui->doubleSpinBox_6->value() );

    if( ui->doubleSpinBox_7->isEnabled() )
        msgStruct.temp     = ui->doubleSpinBox_7->value();

    if( ui->doubleSpinBox_1->isEnabled() && ui->doubleSpinBox_2->isEnabled() && ui->doubleSpinBox_3->isEnabled() )
    {
        if( ui->doubleSpinBox_2->value() < ui->doubleSpinBox_1->value() )
        {
            ui->label->setText("Maximum value lower than minimum value!");
            return;
        }

        if( ui->doubleSpinBox_3->value() == 0 )
        {
            ui->label->setText("Increment value is set to 0!");
            return;
        }

        double vSize    = (ui->doubleSpinBox_2->value() - ui->doubleSpinBox_1->value()) / ui->doubleSpinBox_3->value() + 1.0;
        //printf("Dialog.cpp: vecSize = %f\n",vSize);
        double curValue = ui->doubleSpinBox_1->value();
        double incValue = ui->doubleSpinBox_3->value();

        if( currentVVindex == 1 )
        {
            curValue = percent2ppm( curValue );
            incValue = percent2ppm( incValue );
        }
        else if( currentVVindex == 3 )
        {
            curValue = bar2torr( curValue );
            incValue = bar2torr( incValue );
        }

        for(unsigned i=0; i<unsigned(vSize); i++)
        {
            msgStruct.vecVariable.push_back(curValue);
            curValue += incValue;
        }
    }

    emit msgStructFilled(msgStruct);

    this->accept();
}
