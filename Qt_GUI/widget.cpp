// https://stackoverflow.com/questions/15774386/create-qt-thread-event-loop
// https://stackoverflow.com/questions/42679791/how-to-stop-a-qthread-in-qt
// https://stackoverflow.com/questions/12372301/sharing-data-across-qt-threads

#include "widget.h"
#include "ui_widget.h"
#include "qtspcplotter.h"
#include "qtspcgenerator.h"
#include "dialog.h"

#include <qwt_plot.h>
#include <qwt_plot_curve.h>
#include <qwt_plot_grid.h>
#include <qwt_symbol.h>
#include <qwt_legend.h>
#include <qwt_plot_magnifier.h>
#include <qwt_plot_zoomer.h>

#include <QFileDialog>
#include <QtConcurrent/QtConcurrent>
#include <QApplication>

#include <Spectra/baseline.h>
#include <cstdio>

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Widget::Widget(QWidget* parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    projectDir = QDir::current();
    qDebug("Hello");

    spcPtr = new qtSpcPlotter;
    plotterThread =   new qtPlotterThread(spcPtr);
    spcGen = new qtSpcGenerator;
    generatorThread = new qtGeneratorThread(spcGen);

    if( spcGen->getStackPTptr() )
        ui->spinBox->setValue(spcGen->getStackPTptr()->noiseIterationsNum);

    connect(plotterThread, SIGNAL(resultReady()), this, SLOT(drawPlotterOutput()));
    //connect(plotterThread, SIGNAL(finished()), plotterThread, SLOT(quit())); // only if a thread event loop would be needed
    connect(spcGen, SIGNAL(calculatedInitialEV(double)), ui->lcdNumber_1, SLOT(display(double)));
    connect(spcGen, SIGNAL(calculatedFittedEV(double)), ui->lcdNumber_2, SLOT(display(double)));
    connect(spcGen, SIGNAL(calculatedFittedMV(double)), ui->lcdNumber_3, SLOT(display(double)));
    connect(spcGen, SIGNAL(calculatedFittedSD(double)), ui->lcdNumber_4, SLOT(display(double)));
    connect(generatorThread, SIGNAL(resultReady()), this, SLOT(drawGeneratorOutput()));
    connect(generatorThread, SIGNAL(resultReady()), generatorThread, SIGNAL(finished()));
    connect(generatorThread, SIGNAL(resultReady()), this, SLOT(calculateAvailableOutput()));
    connect(generatorThread, SIGNAL(finished()), generatorThread, SLOT(quit()));
    //connect(generatorThread, SIGNAL(finished()), generatorThread, SLOT(deleteLater()));
    //createPlot();
    //drawBaseline();
    connect(ui->checkBox_1, SIGNAL(stateChanged(int)), spcGen, SLOT(turnWhiteNoiseOnOff(int)));
    connect(ui->checkBox_2, SIGNAL(stateChanged(int)), spcGen, SLOT(turnPressureNoiseOnOff(int)));
    connect(ui->checkBox_3, SIGNAL(stateChanged(int)), spcGen, SLOT(turnTemperatureNoiseOnOff(int)));
    connect(ui->checkBox_4, SIGNAL(stateChanged(int)), spcGen, SLOT(turnFreqShiftNoiseOnOff(int)));
    connect(ui->checkBox_5, SIGNAL(stateChanged(int)), spcGen, SLOT(turnRAMnoiseOnOff(int)));
    connect(ui->checkBox_6, SIGNAL(stateChanged(int)), spcGen, SLOT(turnTransmissionNoiseOnOff(int)));
    connect(ui->spinBox, SIGNAL(valueChanged(int)), spcGen, SLOT(changeNoiseIterationsNumber(int)));

    ui->checkBox_1->setChecked(spcGen->getNoiseGenerator().getWhiteNoiseStruct().turnedOn);
    ui->checkBox_2->setChecked(spcGen->getNoiseGenerator().getPressureNoiseStruct().turnedOn);
    ui->checkBox_3->setChecked(spcGen->getNoiseGenerator().getTemperatureNoiseStruct().turnedOn);
    ui->checkBox_4->setChecked(spcGen->getNoiseGenerator().getFreqShiftNoiseStruct().turnedOn);
    ui->checkBox_5->setChecked(spcGen->getNoiseGenerator().getRAMnoiseStruct().turnedOn);
    ui->checkBox_6->setChecked(spcGen->getNoiseGenerator().getTransmissionNoiseStruct().turnedOn);

    connect(ui->doubleSpinBox_1, SIGNAL(valueChanged(double)), spcGen, SLOT(setWhiteNoiseSD(double)));
    connect(ui->doubleSpinBox_2, SIGNAL(valueChanged(double)), spcGen, SLOT(setPressureNoiseSD(double)));
    connect(ui->doubleSpinBox_3, SIGNAL(valueChanged(double)), spcGen, SLOT(setTemperatureNoiseSD(double)));
    connect(ui->doubleSpinBox_4, SIGNAL(valueChanged(double)), spcGen, SLOT(setFreqShiftNoiseSD(double)));
    connect(ui->doubleSpinBox_5, SIGNAL(valueChanged(double)), spcGen, SLOT(setRAMnoiseAmplitude(double)));
    connect(ui->doubleSpinBox_6, SIGNAL(valueChanged(double)), spcGen, SLOT(setRAMnoiseFbase(double)));
    connect(ui->doubleSpinBox_7, SIGNAL(valueChanged(double)), spcGen, SLOT(setRAMnoiseFepsilon(double)));
    connect(ui->doubleSpinBox_8, SIGNAL(valueChanged(double)), spcGen, SLOT(setRAMnoisePhiEpsilon(double)));
    connect(ui->doubleSpinBox_9, SIGNAL(valueChanged(double)), spcGen, SLOT(setTransmissionSF(double)));

    ui->doubleSpinBox_1->setValue(spcGen->getNoiseGenerator().getWhiteNoiseStruct().stdDevNormalized);
    ui->doubleSpinBox_2->setValue(spcGen->getNoiseGenerator().getPressureNoiseStruct().stdDev);
    ui->doubleSpinBox_3->setValue(spcGen->getNoiseGenerator().getTemperatureNoiseStruct().stdDev);
    ui->doubleSpinBox_4->setValue(spcGen->getNoiseGenerator().getFreqShiftNoiseStruct().stdDev);
    ui->doubleSpinBox_5->setValue(spcGen->getNoiseGenerator().getRAMnoiseStruct().A);
    ui->doubleSpinBox_6->setValue(spcGen->getNoiseGenerator().getRAMnoiseStruct().fBase);
    ui->doubleSpinBox_7->setValue(spcGen->getNoiseGenerator().getRAMnoiseStruct().fEpsilon);
    ui->doubleSpinBox_8->setValue(spcGen->getNoiseGenerator().getRAMnoiseStruct().phiEpsilon);
    ui->doubleSpinBox_9->setValue(spcGen->getNoiseGenerator().getTransmissionNoiseStruct().scalingFactor);

    // Reading interface inputs
    ui->doubleSpinBox_09->setValue( ppm2percent(spcGen->getInputsStruct().concTyp1) );
    ui->doubleSpinBox_10->setValue( ppm2percent(spcGen->getInputsStruct().concMin1) );
    ui->doubleSpinBox_11->setValue( ppm2percent(spcGen->getInputsStruct().concMax1) );
    ui->doubleSpinBox_12->setValue( spcGen->getInputsStruct().range1 );
    ui->doubleSpinBox_13->setValue( ppm2percent(spcGen->getInputsStruct().concTyp2) );
    ui->doubleSpinBox_14->setValue( ppm2percent(spcGen->getInputsStruct().concMin2) );
    ui->doubleSpinBox_15->setValue( ppm2percent(spcGen->getInputsStruct().concMax2) );
    ui->doubleSpinBox_16->setValue( spcGen->getInputsStruct().range2 );
    ui->doubleSpinBox_17->setValue( spcGen->getInputsStruct().PTtyp );
    ui->doubleSpinBox_18->setValue( spcGen->getInputsStruct().PPtyp );
    ui->doubleSpinBox_19->setValue( ppm2percent(spcGen->getInputsStruct().H2OconcTyp) );
    ui->doubleSpinBox_20->setValue( spcGen->getInputsStruct().dustConcTyp );
    ui->doubleSpinBox_21->setValue( spcGen->getInputsStruct().velTyp );
    ui->doubleSpinBox_22->setValue( spcGen->getInputsStruct().L1 );
    ui->doubleSpinBox_23->setValue( spcGen->getInputsStruct().PTmin );
    ui->doubleSpinBox_24->setValue( spcGen->getInputsStruct().PPmin );
    ui->doubleSpinBox_25->setValue( ppm2percent(spcGen->getInputsStruct().H2OconcMin) );
    ui->doubleSpinBox_26->setValue( spcGen->getInputsStruct().dustConcMin );
    ui->doubleSpinBox_27->setValue( spcGen->getInputsStruct().velMin );
    ui->doubleSpinBox_28->setValue( spcGen->getInputsStruct().L2 );
    ui->doubleSpinBox_29->setValue( spcGen->getInputsStruct().PTmax );
    ui->doubleSpinBox_30->setValue( spcGen->getInputsStruct().PPmax );
    ui->doubleSpinBox_31->setValue( ppm2percent(spcGen->getInputsStruct().H2OconcMax) );
    ui->doubleSpinBox_32->setValue( spcGen->getInputsStruct().dustConcMax );
    ui->doubleSpinBox_33->setValue( spcGen->getInputsStruct().velMax );
    ui->doubleSpinBox_34->setValue( spcGen->getInputsStruct().L3 );
    ui->doubleSpinBox_35->setValue( ppm2percent(spcGen->getInputsStruct().purConc1) );
    ui->doubleSpinBox_36->setValue( ppm2percent(spcGen->getInputsStruct().purConc2) );
    ui->doubleSpinBox_37->setValue( ppm2percent(spcGen->getInputsStruct().purMatxConc1) );
    ui->doubleSpinBox_38->setValue( ppm2percent(spcGen->getInputsStruct().purMatxConc2) );
    ui->doubleSpinBox_39->setValue( spcGen->getInputsStruct().dataOutputRate );
    ui->doubleSpinBox_40->setValue( spcGen->getInputsStruct().ATtyp );
    ui->doubleSpinBox_41->setValue( spcGen->getInputsStruct().ATmin );
    ui->doubleSpinBox_42->setValue( spcGen->getInputsStruct().ATmax );
    ui->doubleSpinBox_43->setValue( spcGen->getGasConfigStruct().threshICI );
    ui->spinBox_2->setValue( spcGen->getInputsStruct().airPurge );
    ui->spinBox_3->setValue( spcGen->getInputsStruct().nitPurge );
    ui->spinBox_4->setValue( spcGen->getInputsStruct().custPurge );
    ui->spinBox_5->setValue( spcGen->getInputsStruct().extCell );
    ui->spinBox_6->setValue( spcGen->getInputsStruct().intCell );
    ui->spinBox_7->setValue( spcGen->getInputsStruct().isoFlange );
    ui->spinBox_8->setValue( spcGen->getInputsStruct().insTube );
    ui->lineEdit_1->setText( QString::fromStdString(spcGen->getInputsStruct().gas1) );
    ui->lineEdit_2->setText( QString::fromStdString(spcGen->getInputsStruct().gas2) );
    ui->lineEdit_3->setText( QString::fromStdString(spcGen->getInputsStruct().purMatxGas1) );
    ui->lineEdit_4->setText( QString::fromStdString(spcGen->getInputsStruct().purMatxGas2) );
    
    // Creating connections between widgets and inputs
    connect(ui->doubleSpinBox_09, SIGNAL(valueChanged(double)), spcGen, SLOT(changeInputConcTyp1(double)));
    connect(ui->doubleSpinBox_10, SIGNAL(valueChanged(double)), spcGen, SLOT(changeInputConcMin1(double)));
    connect(ui->doubleSpinBox_11, SIGNAL(valueChanged(double)), spcGen, SLOT(changeInputConcMax1(double)));
    connect(ui->doubleSpinBox_12, SIGNAL(valueChanged(double)), spcGen, SLOT(changeInputRange1(double)));
    connect(ui->doubleSpinBox_13, SIGNAL(valueChanged(double)), spcGen, SLOT(changeInputConcTyp2(double)));
    connect(ui->doubleSpinBox_14, SIGNAL(valueChanged(double)), spcGen, SLOT(changeInputConcMin2(double)));
    connect(ui->doubleSpinBox_15, SIGNAL(valueChanged(double)), spcGen, SLOT(changeInputConcMax2(double)));
    connect(ui->doubleSpinBox_16, SIGNAL(valueChanged(double)), spcGen, SLOT(changeInputRange2(double)));
    connect(ui->doubleSpinBox_17, SIGNAL(valueChanged(double)), spcGen, SLOT(changeInputPTtyp(double)));
    connect(ui->doubleSpinBox_18, SIGNAL(valueChanged(double)), spcGen, SLOT(changeInputPPtyp(double)));
    connect(ui->doubleSpinBox_19, SIGNAL(valueChanged(double)), spcGen, SLOT(changeInputH2OconcTyp(double)));
    connect(ui->doubleSpinBox_20, SIGNAL(valueChanged(double)), spcGen, SLOT(changeInputDustConcTyp(double)));
    connect(ui->doubleSpinBox_21, SIGNAL(valueChanged(double)), spcGen, SLOT(changeInputVelTyp(double)));
    connect(ui->doubleSpinBox_22, SIGNAL(valueChanged(double)), spcGen, SLOT(changeInputL1(double)));
    connect(ui->doubleSpinBox_23, SIGNAL(valueChanged(double)), spcGen, SLOT(changeInputPTmin(double)));
    connect(ui->doubleSpinBox_24, SIGNAL(valueChanged(double)), spcGen, SLOT(changeInputPPmin(double)));
    connect(ui->doubleSpinBox_25, SIGNAL(valueChanged(double)), spcGen, SLOT(changeInputH2OconcMin(double)));
    connect(ui->doubleSpinBox_26, SIGNAL(valueChanged(double)), spcGen, SLOT(changeInputDustConcMin(double)));
    connect(ui->doubleSpinBox_27, SIGNAL(valueChanged(double)), spcGen, SLOT(changeInputVelMin(double)));
    connect(ui->doubleSpinBox_28, SIGNAL(valueChanged(double)), spcGen, SLOT(changeInputL2(double)));
    connect(ui->doubleSpinBox_29, SIGNAL(valueChanged(double)), spcGen, SLOT(changeInputPTmax(double)));
    connect(ui->doubleSpinBox_30, SIGNAL(valueChanged(double)), spcGen, SLOT(changeInputPPmax(double)));
    connect(ui->doubleSpinBox_31, SIGNAL(valueChanged(double)), spcGen, SLOT(changeInputH2OconcMax(double)));
    connect(ui->doubleSpinBox_32, SIGNAL(valueChanged(double)), spcGen, SLOT(changeInputDustConcMax(double)));
    connect(ui->doubleSpinBox_33, SIGNAL(valueChanged(double)), spcGen, SLOT(changeInputVelMax(double)));
    connect(ui->doubleSpinBox_34, SIGNAL(valueChanged(double)), spcGen, SLOT(changeInputL3(double)));
    connect(ui->doubleSpinBox_35, SIGNAL(valueChanged(double)), spcGen, SLOT(changeInputPurConc1(double)));
    connect(ui->doubleSpinBox_36, SIGNAL(valueChanged(double)), spcGen, SLOT(changeInputPurConc2(double)));
    connect(ui->doubleSpinBox_37, SIGNAL(valueChanged(double)), spcGen, SLOT(changeInputPurMatxConc1(double)));
    connect(ui->doubleSpinBox_38, SIGNAL(valueChanged(double)), spcGen, SLOT(changeInputPurMatxConc2(double)));
    connect(ui->doubleSpinBox_39, SIGNAL(valueChanged(double)), spcGen, SLOT(changeInputDataOutRate(double)));
    connect(ui->doubleSpinBox_40, SIGNAL(valueChanged(double)), spcGen, SLOT(changeInputATtyp(double)));
    connect(ui->doubleSpinBox_41, SIGNAL(valueChanged(double)), spcGen, SLOT(changeInputATmin(double)));
    connect(ui->doubleSpinBox_42, SIGNAL(valueChanged(double)), spcGen, SLOT(changeInputATmax(double)));
    connect(ui->doubleSpinBox_43, SIGNAL(valueChanged(double)), spcGen, SLOT(changeGasConfICIthresh(double)));
    connect(ui->spinBox_2, SIGNAL(valueChanged(int)), spcGen, SLOT(changeInputAirPurge(int)));
    connect(ui->spinBox_3, SIGNAL(valueChanged(int)), spcGen, SLOT(changeInputNitroPurge(int)));
    connect(ui->spinBox_4, SIGNAL(valueChanged(int)), spcGen, SLOT(changeInputCustomPurge(int)));
    connect(ui->spinBox_5, SIGNAL(valueChanged(int)), spcGen, SLOT(changeInputExtCell(int)));
    connect(ui->spinBox_6, SIGNAL(valueChanged(int)), spcGen, SLOT(changeInputIntCell(int)));
    connect(ui->spinBox_7, SIGNAL(valueChanged(int)), spcGen, SLOT(changeInputIsoFlange(int)));
    connect(ui->spinBox_8, SIGNAL(valueChanged(int)), spcGen, SLOT(changeInputInsTube(int)));
    connect(ui->lineEdit_1, SIGNAL(textChanged(QString)), spcGen, SLOT(changeInputGas1(QString)));
    connect(ui->lineEdit_2, SIGNAL(textChanged(QString)), spcGen, SLOT(changeInputGas2(QString)));
    connect(ui->lineEdit_3, SIGNAL(textChanged(QString)), spcGen, SLOT(changeInputPurgeGas1(QString)));
    connect(ui->lineEdit_4, SIGNAL(textChanged(QString)), spcGen, SLOT(changeInputPurgeGas2(QString)));

    connect(ui->comboBox_1, SIGNAL(currentTextChanged(QString)), this, SLOT(if_comboBox1_changed(QString)));
    connect(ui->comboBox_2, SIGNAL(currentTextChanged(QString)), this, SLOT(if_comboBox2_changed(QString)));
    connect(ui->comboBox_3, SIGNAL(currentTextChanged(QString)), this, SLOT(if_comboBox3_changed(QString)));
    connect(ui->comboBox_4, SIGNAL(currentTextChanged(QString)), this, SLOT(if_comboBox4_changed(QString)));
    connect(ui->comboBox_41, SIGNAL(currentTextChanged(QString)), this, SLOT(if_comboBox41_changed(QString)));
    connect(ui->comboBox_42, SIGNAL(currentTextChanged(QString)), this, SLOT(if_comboBox42_changed(QString)));
    connect(ui->comboBox_43, SIGNAL(currentTextChanged(QString)), this, SLOT(if_comboBox43_changed(QString)));
    connect(ui->comboBox_21, SIGNAL(currentTextChanged(QString)), this, SLOT(if_comboBox21_changed(QString)));
    connect(ui->comboBox_26, SIGNAL(currentTextChanged(QString)), this, SLOT(if_comboBox26_changed(QString)));

    emit ui->comboBox_1->currentTextChanged("0");
    emit ui->comboBox_2->currentTextChanged("0");
    emit ui->comboBox_3->currentTextChanged("0");
    emit ui->comboBox_41->currentTextChanged("0");
    emit ui->comboBox_42->currentTextChanged("0");
    emit ui->comboBox_43->currentTextChanged("0");
    emit ui->comboBox_21->currentTextChanged("0");
    emit ui->comboBox_26->currentTextChanged("0");

    connect(spcGen, SIGNAL(LLtxrxOutputCalculated(QString)), ui->label_79, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(LLstackOutputCalculated(QString)), ui->label_80, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(LLflangeOutputCalculated(QString)), ui->label_81, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(LLextCellOutputCalculated(QString)), ui->label_91, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(LLtxrxOutputCalculated(QString)), spcGen, SLOT(checkIfLLmethodSettable()));
    connect(spcGen, SIGNAL(LLstackOutputCalculated(QString)), spcGen, SLOT(checkIfLLmethodSettable()));
    connect(spcGen, SIGNAL(LLflangeOutputCalculated(QString)), spcGen, SLOT(checkIfLLmethodSettable()));
    connect(spcGen, SIGNAL(LLextCellOutputCalculated(QString)), spcGen, SLOT(checkIfLLmethodSettable()));
    connect(spcGen, SIGNAL(LLmethodSet(QString)), ui->label_92, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(LLextCellLenCalculated(QString)), ui->label_94, SLOT(setText(QString)));

    connect(ui->radioButton_2, SIGNAL(toggled(bool)), spcGen, SLOT(launchGasConfigOption1(bool)));
    connect(ui->radioButton_3, SIGNAL(toggled(bool)), spcGen, SLOT(launchGasConfigOption2(bool)));
    connect(ui->radioButton_4, SIGNAL(toggled(bool)), spcGen, SLOT(launchGasConfigOption3(bool)));
    connect(ui->radioButton_5, SIGNAL(toggled(bool)), spcGen, SLOT(setDustLoadingToMin(bool)));
    connect(ui->radioButton_6, SIGNAL(toggled(bool)), spcGen, SLOT(setDustLoadingToMax(bool)));
    connect(ui->radioButton_7, SIGNAL(toggled(bool)), spcGen, SLOT(setDustLoadingToTyp(bool)));

    connect(spcGen, SIGNAL(collimationTypeSet(QString)), ui->label_96, SLOT(setText(QString)));
    connect(ui->comboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(if_comboBox_changed(int)));
    connect(ui->checkBox, SIGNAL(toggled(bool)), spcGen, SLOT(setFloatedPWmode(bool)));

    connect(spcGen, SIGNAL(ICI_1resultCalculated(QString)), ui->label_110, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(ICI_2resultCalculated(QString)), ui->label_111, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(ICI_3resultCalculated(QString)), ui->label_112, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(ICI_4resultCalculated(QString)), ui->label_113, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(ICI_5resultCalculated(QString)), ui->label_114, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(ICI_6resultCalculated(QString)), ui->label_115, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(ICI_7resultCalculated(QString)), ui->label_116, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(ICI_8resultCalculated(QString)), ui->label_117, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(ICI_9resultCalculated(QString)), ui->label_118, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(ICI_10resultCalculated(QString)), ui->label_119, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(ICI_11resultCalculated(QString)), ui->label_120, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(ICI_12resultCalculated(QString)), ui->label_121, SLOT(setText(QString)));

    connect(spcGen, SIGNAL(PD_1resultCalculated(QString)), ui->label_134, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(PD_2resultCalculated(QString)), ui->label_135, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(PD_3resultCalculated(QString)), ui->label_136, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(PD_4resultCalculated(QString)), ui->label_137, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(PD_5resultCalculated(QString)), ui->label_138, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(PD_6resultCalculated(QString)), ui->label_139, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(PD_7resultCalculated(QString)), ui->label_140, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(PD_8resultCalculated(QString)), ui->label_141, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(PD_9resultCalculated(QString)), ui->label_142, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(PD_10resultCalculated(QString)), ui->label_143, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(PD_11resultCalculated(QString)), ui->label_144, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(PD_12resultCalculated(QString)), ui->label_145, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(PsensorAirFixedChecked(QString)), this, SLOT(setPsensorAirFixedOutput(QString)));
    connect(spcGen, SIGNAL(PsensorN2FixedChecked(QString)), this, SLOT(setPsensorN2FixedOutput(QString)));
    connect(spcGen, SIGNAL(PsensorCustFixedChecked(QString)), this, SLOT(setPsensorCustFixedOutput(QString)));
    connect(spcGen, SIGNAL(PsensorAirFloatedChecked(QString)), this, SLOT(setPsensorAirFloatedOutput(QString)));
    connect(spcGen, SIGNAL(PsensorN2FloatedChecked(QString)), this, SLOT(setPsensorN2FloatedOutput(QString)));
    connect(spcGen, SIGNAL(PsensorCustFloatedChecked(QString)), this, SLOT(setPsensorCustFloatedOutput(QString)));

    connect(spcGen, SIGNAL(TD_1resultCalculated(QString)), ui->label_195, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(TD_2resultCalculated(QString)), ui->label_197, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(TD_3resultCalculated(QString)), ui->label_199, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(TD_4resultCalculated(QString)), ui->label_201, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(TD_5resultCalculated(QString)), ui->label_203, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(TD_6resultCalculated(QString)), ui->label_205, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(TD_7resultCalculated(QString)), ui->label_207, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(TD_8resultCalculated(QString)), ui->label_209, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(TD_9resultCalculated(QString)), ui->label_211, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(TD_10resultCalculated(QString)), ui->label_213, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(TD_11resultCalculated(QString)), ui->label_215, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(TD_12resultCalculated(QString)), ui->label_217, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(TsensorAirFixedChecked(QString)), this, SLOT(setTsensorAirFixedOutput(QString)));
    connect(spcGen, SIGNAL(TsensorN2FixedChecked(QString)), this, SLOT(setTsensorN2FixedOutput(QString)));
    connect(spcGen, SIGNAL(TsensorCustFixedChecked(QString)), this, SLOT(setTsensorCustFixedOutput(QString)));
    connect(spcGen, SIGNAL(TsensorAirFloatedChecked(QString)), this, SLOT(setTsensorAirFloatedOutput(QString)));
    connect(spcGen, SIGNAL(TsensorN2FloatedChecked(QString)), this, SLOT(setTsensorN2FloatedOutput(QString)));
    connect(spcGen, SIGNAL(TsensorCustFloatedChecked(QString)), this, SLOT(setTsensorCustFloatedOutput(QString)));

    connect(spcGen, SIGNAL(Prec1resultCalculated(QString)), ui->label_183, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(Prec2resultCalculated(QString)), ui->label_185, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(Prec3resultCalculated(QString)), ui->label_187, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(Prec4resultCalculated(QString)), ui->label_189, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(Prec5resultCalculated(QString)), ui->label_191, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(Prec6resultCalculated(QString)), ui->label_193, SLOT(setText(QString)));

    connect(spcGen, SIGNAL(LDL_1resultCalculated(QString)), ui->label_158, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(LDL_2resultCalculated(QString)), ui->label_159, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(LDL_3resultCalculated(QString)), ui->label_160, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(LDL_4resultCalculated(QString)), ui->label_161, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(LDL_5resultCalculated(QString)), ui->label_162, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(LDL_6resultCalculated(QString)), ui->label_163, SLOT(setText(QString)));

    connect(spcGen, SIGNAL(RAM_1resultCalculated(QString)), ui->label_171, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(RAM_2resultCalculated(QString)), ui->label_173, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(RAM_3resultCalculated(QString)), ui->label_175, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(RAM_4resultCalculated(QString)), ui->label_177, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(RAM_5resultCalculated(QString)), ui->label_179, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(RAM_6resultCalculated(QString)), ui->label_181, SLOT(setText(QString)));

    connect(spcGen, SIGNAL(VA_1resultCalculated(double)), ui->lcdNumber_5, SLOT(display(double)));
    connect(spcGen, SIGNAL(VA_2resultCalculated(double)), ui->lcdNumber_6, SLOT(display(double)));
    connect(spcGen, SIGNAL(VA_3resultCalculated(double)), ui->lcdNumber_7, SLOT(display(double)));

    for(int i=0; i<ui->gridLayout_10->count(); ++i)
    {
        if( QComboBox* combobox = qobject_cast<QComboBox*>(ui->gridLayout_10->itemAt(i)->widget()) )
            connect(combobox, SIGNAL(currentIndexChanged(int)), this, SLOT(turnSimConfigOff()));
    }

    for(int i=0; i<ui->gridLayout_12->count(); ++i)
    {
        if( QComboBox* combobox = qobject_cast<QComboBox*>(ui->gridLayout_12->itemAt(i)->widget()) )
            connect(combobox, SIGNAL(currentIndexChanged(int)), this, SLOT(turnSimConfigOff()));
    }

    connect(ui->radioButton_2, SIGNAL(toggled(bool)), this, SLOT(turnSimConfigOff()));
    connect(ui->radioButton_3, SIGNAL(toggled(bool)), this, SLOT(turnSimConfigOff()));
    connect(ui->radioButton_4, SIGNAL(toggled(bool)), this, SLOT(turnSimConfigOff()));
    connect(ui->radioButton_5, SIGNAL(toggled(bool)), this, SLOT(turnSimConfigOff()));
    connect(ui->radioButton_6, SIGNAL(toggled(bool)), this, SLOT(turnSimConfigOff()));
    connect(ui->radioButton_7, SIGNAL(toggled(bool)), this, SLOT(turnSimConfigOff()));

    connect(spcGen, SIGNAL(simIterationsNumChanged(int)), ui->spinBox, SLOT(setValue(int)));

    connect(spcGen, SIGNAL(withinLimitsSet(QString)), ui->label_165, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(limitsWarningSet(QString)), ui->label_237, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(insTubeLengthSet(QString)), ui->label_235, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(insTubeTminSet(QString)), ui->label_167, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(insTubeTtypSet(QString)), ui->label_168, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(insTubeTmaxSet(QString)), ui->label_233, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(IRfilterPresenceSet(QString)), ui->label_230, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(spectralScanRateSet(QString)), ui->label_232, SLOT(setText(QString)));

    connect(spcGen, SIGNAL(LLextCellLenCalculated(QString)), spcGen, SLOT(checkConditionsForNewExtCellLen()));

    connect(spcGen, SIGNAL(trOrigMinSet(QString)), ui->label_241, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(trOrigTypSet(QString)), ui->label_242, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(trOrigMaxSet(QString)), ui->label_243, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(trITminSet(QString)), ui->label_247, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(trITtypSet(QString)), ui->label_248, SLOT(setText(QString)));
    connect(spcGen, SIGNAL(trITmaxSet(QString)), ui->label_249, SLOT(setText(QString)));

    //plot->resize( 600, 400 );

    ui->pushButton_4->setDisabled(true);
    ui->pushButton_7->setDisabled(true);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Widget::~Widget()
{
    delete spcPtr;
    delete ui;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Widget::setXpoints()
{
    xStart = 0;
    xEnd   = spcPtr->getBaselineFit().size() - 1;
    howManyPts = spcPtr->getBaselineFit().size();

    //linspace(xPoints, xStart, xEnd, howManyPts);
    xPoints = spcPtr->getGhzPoints();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Widget::calcYpoints()
{
    unsigned index;

    for(index = 0; index < howManyPts; index++)
    {
        yPtsOld.push_back(blOld->f(xPoints[index],0));
        yPtsFitted.push_back(blFitted->f(xPoints[index],0));
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Widget::createPlotL()
{
    plotL = new QwtPlot();
    plotL->setAutoReplot(true);
    setupPlotZooming(plotL);
    ui->plotLayout_1->addWidget(plotL);

    plotL->setTitle( "Plot spectra" );
    plotL->setCanvasBackground( Qt::white );
    plotL->setAxisScale(QwtPlot::yLeft, 0.0, 1.0);
    plotL->setAxisScale(QwtPlot::xBottom, 0.0, 1.0);
    plotL->setAxisTitle(QwtPlot::Axis::yLeft,"Light intensity [-]");
    plotL->setAxisTitle(QwtPlot::Axis::xBottom,"Frequency [GHz]");
    plotL->insertLegend( new QwtLegend() );

    QwtPlotGrid *grid = new QwtPlotGrid();
    grid->attach( plotL );
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Widget::createPlotR()
{
    plotR = new QwtPlot();
    plotR->setAutoReplot(true);
    setupPlotZooming(plotR);
    ui->plotLayout_1->addWidget(plotR);

    plotR->setTitle( "Plot raw signal" );
    plotR->setCanvasBackground( Qt::white );
    plotR->setAxisScale(QwtPlot::yLeft, 0.0, 1.0);
    plotR->setAxisScale(QwtPlot::xBottom, 0.0, 1.0);
    plotR->setAxisTitle(QwtPlot::Axis::yLeft,"Current [A] ?");
    plotR->setAxisTitle(QwtPlot::Axis::xBottom,"Array Index [-]");
    plotR->insertLegend( new QwtLegend() );

    QwtPlotGrid *grid = new QwtPlotGrid();
    grid->attach( plotR );
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Widget::drawBaseline()
{
    plotL->setAxisAutoScale(QwtPlot::yLeft);
    plotL->setAxisAutoScale(QwtPlot::xBottom);

    QwtPlotCurve *curve1 = new QwtPlotCurve();
    curve1->setTitle( "Baseline (Original Params)" );
    curve1->setPen( Qt::blue, 4 ),
    curve1->setRenderHint( QwtPlotItem::RenderAntialiased, true );

    QwtPlotCurve *curve2 = new QwtPlotCurve();
    curve2->setTitle( "Baseline (Fitted Params)" );
    curve2->setPen( Qt::red, 3 ),
    curve2->setRenderHint( QwtPlotItem::RenderAntialiased, true );

    QwtPlotCurve *curve3 = new QwtPlotCurve();
    curve3->setTitle( "Baseline (from SPC)" );
    curve3->setPen( Qt::magenta, 2 ),
    curve3->setRenderHint( QwtPlotItem::RenderAntialiased, true );

    /*QwtSymbol *symbol = new QwtSymbol( QwtSymbol::Ellipse,
        QBrush( Qt::yellow ), QPen( Qt::red, 2 ), QSize( 8, 8 ) );
    curve->setSymbol( symbol );*/

    blOld = new Baseline;
    blOld->setParams(spcPtr->getOldBlParams());

    blFitted = new Baseline;
    blFitted->setParams(spcPtr->getNewBlParams());
    setXpoints();
    calcYpoints();

    QPolygonF points1, points2, points3;
    /*points << QPointF( 0.0, 4.4 ) << QPointF( 1.0, 3.0 )
        << QPointF( 2.0, 4.5 ) << QPointF( 3.0, 6.8 )
        << QPointF( 4.0, 7.9 ) << QPointF( 5.0, 7.1 );*/
    for(unsigned i = 0; i < spcPtr->getBaselineFit().size(); i++)
    {
        points1 << QPointF( xPoints[i], yPtsOld[i] );
        points2 << QPointF( xPoints[i], yPtsFitted[i] );
        points3 << QPointF( xPoints[i], (spcPtr->getBaselineFit())[i] );
    }

    curve1->setSamples( points1 );
    curve2->setSamples( points2 );
    curve3->setSamples( points3 );

    curve1->attach( plotL );
    curve2->attach( plotL );
    curve3->attach( plotL );
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Widget::drawPlotterOutputL()
{
    plotL->setAxisAutoScale(QwtPlot::yLeft);
    plotL->setAxisAutoScale(QwtPlot::xBottom);

    std::vector<double> xPts  = spcPtr->getGhzPoints();
    std::vector<double> yPts1 = spcPtr->getPeakFit();
    std::vector<double> yPts2 = spcPtr->getInitialGuessFit();
    std::vector<double> yPts3 = spcPtr->getMeasuredData();

    QwtPlotCurve *curve1 = new QwtPlotCurve();
    curve1->setTitle( "PeakFit Data" );
    curve1->setPen( Qt::red, 6 ),
    curve1->setRenderHint( QwtPlotItem::RenderAntialiased, true );

    QPolygonF points1;
    for(unsigned i = 0; i < spcPtr->getNumOfPoints(); i++)
        points1 << QPointF(xPts[i], yPts1[i]);

    QwtPlotCurve *curve2 = new QwtPlotCurve();
    curve2->setTitle( "InitialFit Data" );
    curve2->setPen( Qt::green, 3 ),
    curve2->setRenderHint( QwtPlotItem::RenderAntialiased, true );

    QPolygonF points2;
    for(unsigned i = 0; i < spcPtr->getNumOfPoints(); i++)
        points2 << QPointF(xPts[i], yPts2[i]);

    QwtPlotCurve *curve3 = new QwtPlotCurve();
    curve3->setTitle( "Measured Data" );
    curve3->setPen( Qt::blue, 4 ),
    curve3->setRenderHint( QwtPlotItem::RenderAntialiased, true );

    QPolygonF points3;
    for(unsigned i = 0; i < spcPtr->getNumOfPoints(); i++)
        points3 << QPointF(xPts[i], yPts3[i]);

    curve1->setSamples(points1);
    curve2->setSamples(points2);
    curve3->setSamples(points3);

    curve1->attach(plotL);
    curve2->attach(plotL);
    //curve3->attach(plotL);

    printf("hello from GUI thread %i",thread()->currentThreadId());
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Widget::drawPlotterOutputR()
{
    plotR->setAxisAutoScale(QwtPlot::yLeft);
    plotR->setAxisAutoScale(QwtPlot::xBottom);

    std::vector<double> yPts1 = spcPtr->getRawSignal();
    std::vector<double> yPts2 = spcPtr->getOriginalSig();

    QwtPlotCurve *curve1 = new QwtPlotCurve();
    curve1->setTitle( "Raw Signal" );
    curve1->setPen( Qt::green, 4 ),
    curve1->setRenderHint( QwtPlotItem::RenderAntialiased, true );

    QwtPlotCurve *curve2 = new QwtPlotCurve();
    curve2->setTitle( "Original Signal" );
    curve2->setPen( Qt::red, 2 ),
    curve2->setRenderHint( QwtPlotItem::RenderAntialiased, true );

    QPolygonF points1;
    for(unsigned i = 0; i < spcPtr->getRawSignal().size(); i++)
        points1 << QPointF(i, yPts1[i]);

    QPolygonF points2;
    for(unsigned i = 0; i < spcPtr->getOriginalSig().size(); i++)
        points2 << QPointF(i, yPts2[i]);

    curve1->setSamples(points1);
    curve2->setSamples(points2);
    curve1->attach(plotR);
    curve2->attach(plotR);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Widget::drawPlotterOutput()
{
    drawPlotterOutputL();
    drawPlotterOutputR();
    QApplication::restoreOverrideCursor();
    ui->pushButton_1->setDisabled(true);
    ui->pushButton_2->setDisabled(true);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void linspace( std::vector<double>& result, double min, double max, unsigned n )
{
    //vector<double> result;
    int iterator = 0;

    /*for_each( result.begin(), result.end(), [&]()
    {
        double temp = min + i*(max-min)/(floor((double)n) - 1);
        result.insert(result.begin() + iterator, temp);
        iterator++;
    });*/

    for (unsigned i = 0; i <= n-2; i++)
    {
        double temp = min + i*(max-min)/(floor((double)n) - 1);
        result.insert(result.begin() + iterator, temp);
        iterator++;
    }

    result.insert(result.begin() + iterator, max);
    //return result;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Widget::on_pushButton_1_clicked()
{
    QString FileName = QFileDialog::getOpenFileName(this, tr("Open a data file"), ".",
                                                    tr("Data files (*.txt)"));

    if (!FileName.isEmpty())
    {
        FileName = projectDir.relativeFilePath(FileName);
        fileName = FileName.toLocal8Bit().constData();

        ui->label->setText(FileName);

        if(!iniDirName.empty())
        {
            createPlotL();
            createPlotR();
            plotterThread->getPathStrings(fileName,iniDirName);
            plotterThread->start();
            QApplication::setOverrideCursor(Qt::WaitCursor);
        }
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Widget::on_pushButton_2_clicked()
{
    QString DirName = QFileDialog::getExistingDirectory(this, tr("Open an INI directory"),".");

    if (!DirName.isEmpty())
    {
        DirName = projectDir.relativeFilePath(DirName);
        iniDirName = DirName.toLocal8Bit().constData();

        ui->label_2->setText(DirName);

        if(!fileName.empty())
        {
            createPlotL();
            createPlotR();
            plotterThread->getPathStrings(fileName,iniDirName);
            plotterThread->start();
            QApplication::setOverrideCursor(Qt::WaitCursor);
            /*QFuture<void> futureComput = QtConcurrent::run(spcPtr, &spcObject::emulateSPCtool,
                                                           fileName, iniDirName);
            QFutureWatcher<void>* watcher = new QFutureWatcher<void>(spcPtr);
            connect(watcher, SIGNAL(finished()), this, SLOT(drawPlotterOutput()));
            connect(watcher, SIGNAL(finished()), watcher, SLOT(deleteLater()));
            watcher->setFuture(futureComput);*/
        }
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Widget::setupPlotZooming( QwtPlot* plot )
{
    QwtPlotMagnifier* zoomWithWheel = new QwtPlotMagnifier( plot->canvas() );
    QwtPlotZoomer* zoomByAreaSelect = new QwtPlotZoomer( plot->canvas() );
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Widget::colorLCDnums()
{
    if( spcGen->concentrationsDiffer() )
    {
        ui->lcdNumber_1->setPalette(Qt::red);
        ui->lcdNumber_2->setPalette(Qt::red);
    }
    else
    {
        ui->lcdNumber_1->setPalette(Qt::green);
        ui->lcdNumber_2->setPalette(Qt::green);
    }

    ui->lcdNumber_3->setPalette(Qt::black);
    ui->lcdNumber_4->setPalette(Qt::black);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Widget::on_pushButton_3_clicked()
{
    createPlotT3();
    QApplication::setOverrideCursor(Qt::WaitCursor);

    generatorThread->start();
    printf("hello from generator thread %i\n",generatorThread->currentThreadId()); fflush(stdout);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Widget::createPlotT3()
{
    if( spcGen->multipleCalcsPresent() )
    {
        plotT3.append(new QwtPlot());
        plotT3[0]->setAxisAutoScale(QwtPlot::yLeft);
        plotT3[0]->setAxisAutoScale(QwtPlot::xBottom);

        QwtPlotGrid *grid = new QwtPlotGrid;
        grid->attach( plotT3[0] );
    }
    else
    {
        plotT3.append(new QwtPlot());
        plotT3[0]->setAxisAutoScale(QwtPlot::yLeft);
        plotT3[0]->setAxisAutoScale(QwtPlot::xBottom);
        plotT3.append(new QwtPlot());
        plotT3[1]->setAxisAutoScale(QwtPlot::yLeft);
        plotT3[1]->setAxisAutoScale(QwtPlot::xBottom);

        QwtPlotGrid *grid1 = new QwtPlotGrid;
        grid1->attach( plotT3[0] );
        QwtPlotGrid *grid2 = new QwtPlotGrid;
        grid2->attach( plotT3[1] );
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Widget::drawGeneratorOutput()
{
    QApplication::restoreOverrideCursor();
    colorLCDnums();

    if( spcGen->multipleCalcsPresent() )
        drawGeneratorOutputMC();
    else
        drawGeneratorOutputSC();

    ui->pushButton_3->setDisabled(true);
    ui->pushButton_4->setDisabled(false);
    ui->pushButton_7->setDisabled(false);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Widget::drawGeneratorOutputSC()
{
    // Luminous intensity plot
    plotT3[0]->setAxisTitle(QwtPlot::Axis::yLeft,"Light intensity [-]");
    plotT3[0]->setAxisTitle(QwtPlot::Axis::xBottom,"Frequency [GHz]");

    std::vector<double> xPts  = spcGen->getGhzPoints();
    std::vector<double> yPts1 = spcGen->getNoisedVector();
    std::vector<double> yPts2 = spcGen->getFittedVector();
    std::vector<double> yPts3 = spcGen->getBaseLine();

    QwtPlotCurve *curve1 = new QwtPlotCurve();
    curve1->setTitle( "Initial Guess + Noise" );
    curve1->setPen( Qt::green, 4 ),
    curve1->setRenderHint( QwtPlotItem::RenderAntialiased, true );

    QwtPlotCurve *curve2 = new QwtPlotCurve();
    curve2->setTitle( "Fitted Spectrum" );
    curve2->setPen( Qt::red, 2 ),
    curve2->setRenderHint( QwtPlotItem::RenderAntialiased, true );

    QwtPlotCurve *curve3 = new QwtPlotCurve();
    curve3->setTitle( "Base Line" );
    curve3->setPen( Qt::blue, 1 ),
    curve3->setRenderHint( QwtPlotItem::RenderAntialiased, true );

    QPolygonF points1;
    for(unsigned i = 0; i < yPts1.size(); i++)
        points1 << QPointF(xPts[i], yPts1[i]);

    QPolygonF points2;
    for(unsigned i = 0; i < yPts2.size(); i++)
        points2 << QPointF(xPts[i], yPts2[i]);

    QPolygonF points3;
    for(unsigned i = 0; i < yPts3.size(); i++)
        points3 << QPointF(xPts[i], yPts3[i]);

    curve1->setSamples(points1);
    curve1->attach(plotT3[0]);

    curve2->setSamples(points2);
    curve2->attach(plotT3[0]);

    curve3->setSamples(points3);
    curve3->attach(plotT3[0]);

    plotT3[0]->insertLegend( new QwtLegend() );
    setupPlotZooming(plotT3[0]);

    ui->verticalLayout_2->addWidget(plotT3[0]);

    // Absorbance plot
    plotT3[1]->setAxisTitle(QwtPlot::Axis::yLeft,"Absorbance [-]");
    plotT3[1]->setAxisTitle(QwtPlot::Axis::xBottom,"Frequency [GHz]");

    std::vector<double> YPts1 = spcGen->getSimAbsorbance();
    std::vector<double> YPts2 = spcGen->getFitAbsorbance();

    QwtPlotCurve *Curve1 = new QwtPlotCurve();
    Curve1->setTitle( "Initial Guess + Noise" );
    Curve1->setPen( Qt::green, 4 ),
    Curve1->setRenderHint( QwtPlotItem::RenderAntialiased, true );

    QwtPlotCurve *Curve2 = new QwtPlotCurve();
    Curve2->setTitle( "Fitted Spectrum" );
    Curve2->setPen( Qt::red, 2 ),
    Curve2->setRenderHint( QwtPlotItem::RenderAntialiased, true );

    QPolygonF Points1;
    for(unsigned i = 0; i < YPts1.size(); i++)
        Points1 << QPointF(xPts[i], YPts1[i]);

    QPolygonF Points2;
    for(unsigned i = 0; i < YPts2.size(); i++)
        Points2 << QPointF(xPts[i], YPts2[i]);

    Curve1->setSamples(Points1);
    Curve1->attach(plotT3[1]);

    Curve2->setSamples(Points2);
    Curve2->attach(plotT3[1]);

    plotT3[1]->insertLegend( new QwtLegend() );
    setupPlotZooming(plotT3[1]);

    ui->verticalLayout_2->addWidget(plotT3[1]);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Widget::drawGeneratorOutputMC()
{
    std::vector<double> xPts  = spcGen->getMultipleCalcsData().vecVariable;
    std::vector<double> yPts1 = spcGen->getO2stdDevValues();

    plotT3[0]->setAxisTitle(QwtPlot::Axis::yLeft,"O2 Conc. Std Dev [ppm]");

    switch( spcGen->getMultipleCalcsData().vecVarIndex )
    {
    case 1:
        for_each(xPts.begin(), xPts.end(), [](double& val){ val = ppm2percent(val); });
        plotT3[0]->setAxisTitle(QwtPlot::Axis::xBottom,"O2 Concentration [%]");
        break;
    case 2:
        plotT3[0]->setAxisTitle(QwtPlot::Axis::xBottom,"Path Length [m]");
        break;
    case 3:
        for_each(xPts.begin(), xPts.end(), [](double& val){ val = torr2bar(val); });
        plotT3[0]->setAxisTitle(QwtPlot::Axis::xBottom,"Pressure [bar]");
        break;
    case 4:
        plotT3[0]->setAxisTitle(QwtPlot::Axis::xBottom,"Temperature [deg C]");
        break;
    case 0:
    default:
        return;
    }

    QwtPlotCurve *curve1 = new QwtPlotCurve();
    curve1->setPen( Qt::red, 3 ),
    curve1->setRenderHint( QwtPlotItem::RenderAntialiased, true );

    QPolygonF points1;
    for(unsigned i = 0; i < yPts1.size(); i++)
        points1 << QPointF(xPts[i], yPts1[i]);

    curve1->setSamples(points1);
    curve1->setSymbol( new QwtSymbol(QwtSymbol::Diamond, Qt::black, Qt::SolidLine, QSize(8,8)) );
    curve1->attach(plotT3[0]);

    setupPlotZooming(plotT3[0]);

    ui->verticalLayout_2->addWidget(plotT3[0]);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Widget::on_pushButton_4_clicked()
{
    for(unsigned i=plotT3.size(); i>0; i--)
    {
        ui->verticalLayout_2->removeWidget(plotT3[i-1]);
        QwtPlot* temp = plotT3[i-1];
        plotT3.pop_back();
        delete temp;
    }
    update();

    spcGen->clearVectors();
    spcGen->updateStackPTiters(ui->spinBox->value());
    ui->pushButton_4->setDisabled(true);
    ui->pushButton_3->setDisabled(false);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Widget::on_pushButton_5_clicked()
{
    Dialog* dialogWindow = new Dialog(this);
    dialogWindow->show();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Widget::receiveDialogMsg( multipleCalcsData msgStruct )
{
    spcGen->initializeMultipleCalcs(std::move(msgStruct));
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Widget::on_pushButton_6_clicked()
{
    MtxDialog* mtxDialogWindow = new MtxDialog(this);
    mtxDialogWindow->show();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Widget::on_pushButton_7_clicked()
{
    emit ui->pushButton_4->clicked(true);

    spcGen->setConfigsToDefaults();
    spcGen->initializePTfromINI();
    spcGen->reinitializeOutputs();
    spcGen->resetITCcounter();

    ui->label_79->clear();
    ui->label_80->clear();
    ui->label_81->clear();
    ui->label_91->clear();
    ui->label_92->clear();
    ui->label_94->clear();

    ui->label_110->clear();
    ui->label_111->clear();
    ui->label_112->clear();
    ui->label_113->clear();
    ui->label_114->clear();
    ui->label_115->clear();
    ui->label_116->clear();
    ui->label_117->clear();
    ui->label_118->clear();
    ui->label_119->clear();
    ui->label_120->clear();
    ui->label_121->clear();

    ui->label_183->clear();
    ui->label_185->clear();
    ui->label_187->clear();
    ui->label_189->clear();
    ui->label_191->clear();
    ui->label_193->clear();

    ui->label_171->clear();
    ui->label_173->clear();
    ui->label_175->clear();
    ui->label_177->clear();
    ui->label_179->clear();
    ui->label_181->clear();

    ui->label_134->clear();
    ui->label_135->clear();
    ui->label_136->clear();
    ui->label_137->clear();
    ui->label_138->clear();
    ui->label_139->clear();
    ui->label_140->clear();
    ui->label_141->clear();
    ui->label_142->clear();
    ui->label_143->clear();
    ui->label_144->clear();
    ui->label_145->clear();
    ui->label_152->clear();
    ui->label_153->clear();
    ui->label_154->clear();
    ui->label_155->clear();
    ui->label_156->clear();
    ui->label_157->clear();
    ui->label_158->clear();
    ui->label_159->clear();
    ui->label_160->clear();
    ui->label_161->clear();
    ui->label_162->clear();
    ui->label_163->clear();

    ui->label_195->clear();
    ui->label_197->clear();
    ui->label_199->clear();
    ui->label_201->clear();
    ui->label_203->clear();
    ui->label_205->clear();
    ui->label_207->clear();
    ui->label_209->clear();
    ui->label_211->clear();
    ui->label_213->clear();
    ui->label_215->clear();
    ui->label_217->clear();
    ui->label_219->clear();
    ui->label_221->clear();
    ui->label_223->clear();
    ui->label_225->clear();
    ui->label_227->clear();
    ui->label_229->clear();

    ui->label_96->clear();
    ui->label_165->clear();
    ui->label_167->clear();
    ui->label_168->clear();
    ui->label_168->clear();
    ui->label_230->clear();
    ui->label_232->clear();
    ui->label_233->clear();
    ui->label_235->clear();
    ui->label_237->clear();

    ui->label_241->clear();
    ui->label_242->clear();
    ui->label_243->clear();
    ui->label_247->clear();
    ui->label_248->clear();
    ui->label_249->clear();

    ui->lcdNumber_5->display(0);
    ui->lcdNumber_6->display(0);
    ui->lcdNumber_7->display(0);

    ui->pushButton_7->setDisabled(true);
    //emit ui->radioButton->toggle();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Widget::on_radioButton_toggled(bool checked)
{
    if( checked )
    {
        spcGen->getGCsimFitStruct().useSimFitStruct = true;
        setSimFitConfiguration();

        if( ui->comboBox_1->currentText() == "0" && ui->comboBox_2->currentText() == "0" &&
            ui->comboBox_3->currentText() == "0" && ui->comboBox_4->currentText() == "0" )
        {
            ui->label_73->setStyleSheet("QLabel { color : red; }");
            ui->label_73->setText("All chambers set to 0!");
        }
        else
        {
            ui->label_73->clear();
            ui->label_73->setStyleSheet("QLabel { color : green; }");
            ui->label_73->setText("New configuration set.");
        }

        ui->pushButton_5->setDisabled(true);
        ui->radioButton_2->toggle();

        if( !(ui->radioButton_5->isChecked() || ui->radioButton_6->isChecked() || ui->radioButton_7->isChecked()) )
            ui->radioButton_7->toggle();

        spcGen->calculateTransmissionSF();
        ui->doubleSpinBox_9->setValue(spcGen->getNoiseGenerator().getTransmissionNoise());
    }
    else
    {
        spcGen->getGCsimFitStruct().useSimFitStruct   = false;
        spcGen->getGCsimFitStruct().stackFittingLater = false;
        ui->label_73->clear();
        ui->pushButton_5->setEnabled(true);
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Widget::if_comboBox1_changed( QString name )
{
    if( name == "0" )
    {
        ui->comboBox_5->setDisabled(true);
        ui->comboBox_9->setDisabled(true);
        ui->comboBox_13->setDisabled(true);
        ui->comboBox_17->setDisabled(true);
    }
    else
    {
        ui->comboBox_5->setDisabled(false);
        ui->comboBox_9->setDisabled(false);
        ui->comboBox_13->setDisabled(false);
        ui->comboBox_17->setDisabled(false);
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Widget::if_comboBox2_changed( QString name )
{
    if( name == "0" )
    {
        ui->comboBox_6->setDisabled(true);
        ui->comboBox_10->setDisabled(true);
        ui->comboBox_14->setDisabled(true);
        ui->comboBox_18->setDisabled(true);
    }
    else
    {
        ui->comboBox_6->setDisabled(false);
        ui->comboBox_10->setDisabled(false);
        ui->comboBox_14->setDisabled(false);
        ui->comboBox_18->setDisabled(false);
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Widget::if_comboBox3_changed( QString name )
{
    if( name == "0" )
    {
        ui->comboBox_7->setDisabled(true);
        ui->comboBox_11->setDisabled(true);
        ui->comboBox_15->setDisabled(true);
        ui->comboBox_19->setDisabled(true);
    }
    else
    {
        ui->comboBox_7->setDisabled(false);
        ui->comboBox_11->setDisabled(false);
        ui->comboBox_15->setDisabled(false);
        ui->comboBox_19->setDisabled(false);
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Widget::if_comboBox4_changed( QString name )
{
    if( name == "0" )
    {
        ui->comboBox_8->setDisabled(true);
        ui->comboBox_12->setDisabled(true);
        ui->comboBox_16->setDisabled(true);
        ui->comboBox_20->setDisabled(true);
    }
    else
    {
        ui->comboBox_8->setDisabled(false);
        ui->comboBox_12->setDisabled(false);
        ui->comboBox_16->setDisabled(false);
        ui->comboBox_20->setDisabled(false);
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Widget::if_comboBox21_changed( QString name )
{
    if( name == "0" )
    {
        ui->comboBox_22->setDisabled(true);
        ui->comboBox_23->setDisabled(true);
        ui->comboBox_24->setDisabled(true);
        ui->comboBox_25->setDisabled(true);
    }
    else
    {
        ui->comboBox_22->setDisabled(false);
        ui->comboBox_23->setDisabled(false);
        ui->comboBox_24->setDisabled(false);
        ui->comboBox_25->setDisabled(false);
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Widget::if_comboBox26_changed( QString name )
{
    if( name == "0" )
    {
        ui->comboBox_27->setDisabled(true);
        ui->comboBox_28->setDisabled(true);
        ui->comboBox_29->setDisabled(true);
        ui->comboBox_30->setDisabled(true);
    }
    else
    {
        ui->comboBox_27->setDisabled(false);
        ui->comboBox_28->setDisabled(false);
        ui->comboBox_29->setDisabled(false);
        ui->comboBox_30->setDisabled(false);
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Widget::if_comboBox41_changed( QString name )
{
    if( name == "0" )
    {
        ui->comboBox_45->setDisabled(true);
        ui->comboBox_49->setDisabled(true);
        ui->comboBox_53->setDisabled(true);
        ui->comboBox_57->setDisabled(true);
    }
    else
    {
        ui->comboBox_45->setDisabled(false);
        ui->comboBox_49->setDisabled(false);
        ui->comboBox_53->setDisabled(false);
        ui->comboBox_57->setDisabled(false);
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Widget::if_comboBox42_changed( QString name )
{
    if( name == "0" )
    {
        ui->comboBox_46->setDisabled(true);
        ui->comboBox_50->setDisabled(true);
        ui->comboBox_54->setDisabled(true);
        ui->comboBox_58->setDisabled(true);
    }
    else
    {
        ui->comboBox_46->setDisabled(false);
        ui->comboBox_50->setDisabled(false);
        ui->comboBox_54->setDisabled(false);
        ui->comboBox_58->setDisabled(false);
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Widget::if_comboBox43_changed( QString name )
{
    if( name == "0" )
    {
        ui->comboBox_47->setDisabled(true);
        ui->comboBox_51->setDisabled(true);
        ui->comboBox_55->setDisabled(true);
        ui->comboBox_59->setDisabled(true);
    }
    else
    {
        ui->comboBox_47->setDisabled(false);
        ui->comboBox_51->setDisabled(false);
        ui->comboBox_55->setDisabled(false);
        ui->comboBox_59->setDisabled(false);
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Widget::setSimFitConfiguration()
{
    std::array<bool,2> greens = spcGen->calculateNonSimOutputs();

    if( greens[0] )
        ui->label_165->setStyleSheet("QLabel { color : green; }");
    else
        ui->label_165->setStyleSheet("QLabel { color : red; }");

    if( greens[1] )
        ui->label_230->setStyleSheet("QLabel { color : green; }");
    else
        ui->label_230->setStyleSheet("QLabel { color : red; }");

    GASCONFIG_SIM_FIT_STRUCT& simFitData = spcGen->getGCsimFitStruct();
    simFitData.clear();

    simFitData.nameX1 = spcGen->getInputsStruct().gas1;
    simFitData.nameX2 = spcGen->getInputsStruct().gas2;

    /************************************************* Simulation part ******************************************************/

    // STACK part
    if( ui->comboBox_4->currentText() == "0" )
        simFitData.simData.stack.X1 = 0.0;
    else if( ui->comboBox_4->currentText() == "conc_min_1" )
    {
        simFitData.simData.stack.X1 = spcGen->getInputsStruct().concMin1;
        simFitData.simData.stack.isEmpty = false;
    }
    else if( ui->comboBox_4->currentText() == "conc_typ_1" )
    {
        simFitData.simData.stack.X1 = spcGen->getInputsStruct().concTyp1;
        simFitData.simData.stack.isEmpty = false;
    }

    simFitData.simData.stack.X2 = spcGen->getInputsStruct().concTyp2; //0.0; // no second measurement component (for the time being)

    if( ui->comboBox_8->currentText() == "Air" )
    {
        simFitData.simData.stack.nameMatrX.push_back("N2");
        simFitData.simData.stack.matrX = spcGen->createMatrixConcentrations(true,true);
    }
    else if( ui->comboBox_8->currentText().contains("Matrix") )
    {
        simFitData.simData.stack.nameMatrX.push_back("N2");
        simFitData.simData.stack.nameMatrX.push_back("CO2");

        if( ui->comboBox_8->currentText().contains("minPW") )
            simFitData.simData.stack.mtxStyle = GASCONFIG_SIM_FIT_STRUCT::minPW;
        else if( ui->comboBox_8->currentText().contains("maxPW") )
            simFitData.simData.stack.mtxStyle = GASCONFIG_SIM_FIT_STRUCT::maxPW;

        simFitData.simData.stack.matrX = spcGen->createMatrixConcentrations(true,true);
    }

    if( ui->comboBox_12->currentText() == "PT_min" )
        simFitData.simData.stack.T = spcGen->getInputsStruct().PTmin;
    else if( ui->comboBox_12->currentText() == "PT_max" )
        simFitData.simData.stack.T = spcGen->getInputsStruct().PTmax;
    else if( ui->comboBox_12->currentText() == "PT_typ" )
        simFitData.simData.stack.T = spcGen->getInputsStruct().PTtyp;
    else if( ui->comboBox_12->currentText() == "AT_min" )
        simFitData.simData.stack.T = spcGen->getInputsStruct().ATmin;
    else if( ui->comboBox_12->currentText() == "AT_max" )
        simFitData.simData.stack.T = spcGen->getInputsStruct().ATmax;

    if( ui->comboBox_16->currentText() == "P_atm" )
        simFitData.simData.stack.P = spcGen->getGasConfigStruct().PresAtm;
    else if( ui->comboBox_16->currentText() == "PP_min" )
        simFitData.simData.stack.P = spcGen->getInputsStruct().PPmin;
    else if( ui->comboBox_16->currentText() == "PP_max" )
        simFitData.simData.stack.P = spcGen->getInputsStruct().PPmax;
    else if( ui->comboBox_16->currentText() == "PP_typ" )
        simFitData.simData.stack.P = spcGen->getInputsStruct().PPtyp;

    if( ui->comboBox_20->currentText() == "L2-L_ins_tube" )
        simFitData.simData.stack.L = spcGen->getInputsStruct().L2 - spcGen->getOutputInsTubeLen();
    else if( ui->comboBox_20->currentText() == "L_Tx+L_Rx" )
        simFitData.simData.stack.L = spcGen->getGasConfigStruct().TxLen + spcGen->getGasConfigStruct().RxLen;
    else if( ui->comboBox_20->currentText() == "L1+L3" )
        simFitData.simData.stack.L = spcGen->getInputsStruct().L1 + spcGen->getInputsStruct().L3;
    else if( ui->comboBox_20->currentText() == "L_ext_cell" )
        simFitData.simData.stack.L = spcGen->getGasConfigStruct().extCellLenOpt;

    // FLANGES part

    // Flange 0
    if( ui->comboBox_1->currentText() != "0" ) // flange defined
    {
        simFitData.simData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
        simFitData.simData.flanges.back().X2 = 0.0;

        if( ui->comboBox_1->currentText() == "X_Air_1" )
            simFitData.simData.flanges.back().X1 = spcGen->getGasConfigStruct().conc1xAir;
        else if( ui->comboBox_1->currentText() == "pur_conc_1" )
            simFitData.simData.flanges.back().X1 = spcGen->getInputsStruct().purConc1;

        if( ui->comboBox_5->currentText() == "Air" )
        {
            simFitData.simData.flanges.back().nameMatrX.push_back("N2");
            simFitData.simData.flanges.back().matrX = spcGen->createMatrixConcentrations(false,true);
        }
        else if( ui->comboBox_5->currentText() == "custom" )
        {
            simFitData.simData.flanges.back().nameMatrX.push_back(spcGen->getInputsStruct().purMatxGas1);
            simFitData.simData.flanges.back().nameMatrX.push_back(spcGen->getInputsStruct().purMatxGas2);
            simFitData.simData.flanges.back().matrX = spcGen->createMatrixConcentrations(false,true);
        }

        if( ui->comboBox_9->currentText() == "AT_min" )
            simFitData.simData.flanges.back().T = spcGen->getInputsStruct().ATmin;
        else if( ui->comboBox_9->currentText() == "AT_max" )
            simFitData.simData.flanges.back().T = spcGen->getInputsStruct().ATmax;
        else if( ui->comboBox_9->currentText() == "AT_typ" )
            simFitData.simData.flanges.back().T = spcGen->getInputsStruct().ATtyp;

        if( ui->comboBox_13->currentText() == "P_atm" )
            simFitData.simData.flanges.back().P = spcGen->getGasConfigStruct().PresAtm;
        else if( ui->comboBox_13->currentText() == "PP_min" )
            simFitData.simData.flanges.back().P = spcGen->getInputsStruct().PPmin;
        else if( ui->comboBox_13->currentText() == "PP_max" )
            simFitData.simData.flanges.back().P = spcGen->getInputsStruct().PPmax;

        if( ui->comboBox_17->currentText() == "L_Tx+L_Rx" )
            simFitData.simData.flanges.back().L = spcGen->getGasConfigStruct().TxLen + spcGen->getGasConfigStruct().RxLen;
        else if( ui->comboBox_17->currentText() == "L_Tx+L_Rx+L_ext_cell" )
            simFitData.simData.flanges.back().L = spcGen->getGasConfigStruct().TxLen + spcGen->getGasConfigStruct().RxLen + spcGen->getOutputsStruct().L_ext_cell;
    }

    // Flange 1
    if( ui->comboBox_2->currentText() != "0" ) // flange defined
    {
        simFitData.simData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
        simFitData.simData.flanges.back().X2 = 0.0;

        if( ui->comboBox_2->currentText() == "X_Air_1" )
            simFitData.simData.flanges.back().X1 = spcGen->getGasConfigStruct().conc1xAir;
        else if( ui->comboBox_2->currentText() == "pur_conc_1" )
            simFitData.simData.flanges.back().X1 = spcGen->getInputsStruct().purConc1;

        if( ui->comboBox_6->currentText() == "Air" )
        {
            simFitData.simData.flanges.back().nameMatrX.push_back("N2");
            simFitData.simData.flanges.back().matrX = spcGen->createMatrixConcentrations(false,true);
        }
        else if( ui->comboBox_6->currentText() == "custom" )
        {
            simFitData.simData.flanges.back().nameMatrX.push_back(spcGen->getInputsStruct().purMatxGas1);
            simFitData.simData.flanges.back().nameMatrX.push_back(spcGen->getInputsStruct().purMatxGas2);
            simFitData.simData.flanges.back().matrX = spcGen->createMatrixConcentrations(false,true);
        }

        if( ui->comboBox_10->currentText() == "AT_min" )
            simFitData.simData.flanges.back().T = spcGen->getInputsStruct().ATmin;
        else if( ui->comboBox_10->currentText() == "AT_max" )
            simFitData.simData.flanges.back().T = spcGen->getInputsStruct().ATmax;
        else if( ui->comboBox_10->currentText() == "AT_typ" )
            simFitData.simData.flanges.back().T = spcGen->getInputsStruct().ATtyp;

        if( ui->comboBox_14->currentText() == "P_atm" )
            simFitData.simData.flanges.back().P = spcGen->getGasConfigStruct().PresAtm;
        else if( ui->comboBox_14->currentText() == "PP_min" )
            simFitData.simData.flanges.back().P = spcGen->getInputsStruct().PPmin;
        else if( ui->comboBox_14->currentText() == "PP_max" )
            simFitData.simData.flanges.back().P = spcGen->getInputsStruct().PPmax;
        else if( ui->comboBox_14->currentText() == "PP_typ" )
            simFitData.simData.flanges.back().P = spcGen->getInputsStruct().PPtyp;

        if( ui->comboBox_18->currentText() == "L1+L3" )
            simFitData.simData.flanges.back().L = spcGen->getInputsStruct().L1 + spcGen->getInputsStruct().L3;
    }

    // Flange 2
    if( ui->comboBox_3->currentText() != "0" && ((ui->comboBox_19->currentText() == "L_ins_tube" && spcGen->getOutputInsTubeLen() > 0.0)
                                             || ui->comboBox_19->currentText() == "L_ext_cell") ) // flange defined
    {
        simFitData.simData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
        simFitData.simData.flanges.back().X2 = 0.0;

        if( ui->comboBox_3->currentText() == "X_Air_1" )
            simFitData.simData.flanges.back().X1 = spcGen->getGasConfigStruct().conc1xAir;
        else if( ui->comboBox_3->currentText() == "pur_conc_1" )
            simFitData.simData.flanges.back().X1 = spcGen->getInputsStruct().purConc1;

        if( ui->comboBox_7->currentText() == "Air" )
        {
            simFitData.simData.flanges.back().nameMatrX.push_back("N2");
            simFitData.simData.flanges.back().matrX = spcGen->createMatrixConcentrations(false,true);
        }
        else if( ui->comboBox_7->currentText() == "custom" )
        {
            simFitData.simData.flanges.back().nameMatrX.push_back(spcGen->getInputsStruct().purMatxGas1);
            simFitData.simData.flanges.back().nameMatrX.push_back(spcGen->getInputsStruct().purMatxGas2);
            simFitData.simData.flanges.back().matrX = spcGen->createMatrixConcentrations(false,true);
        }

        // This part must go before setting the flange's temperature
        if( ui->comboBox_19->currentText() == "L_ext_cell" )
            simFitData.simData.flanges.back().L = spcGen->getGasConfigStruct().extCellLenOpt; // spcGen->getOutputsStruct().L_ext_cell;
        else if( ui->comboBox_19->currentText() == "L_ins_tube" )
        {
            simFitData.simData.flanges.back().L = spcGen->getOutputInsTubeLen();
            simFitData.simData.flanges.back().isInsTube = true;
            spcGen->changeInsTubePurgeGas( ui->comboBox_7->currentText().toLocal8Bit().constData() );
            spcGen->proceedWithENTUmethod();
        }

        if( ui->comboBox_11->currentText() == "AT_min" )
            simFitData.simData.flanges.back().T = spcGen->getInputsStruct().ATmin;
        else if( ui->comboBox_11->currentText() == "AT_max" )
            simFitData.simData.flanges.back().T = spcGen->getInputsStruct().ATmax;
        else if( ui->comboBox_11->currentText() == "T_ins_typ" )
            simFitData.simData.flanges.back().T = spcGen->getOutputsStruct().T_ins_tube_typ; // spcGen->getInputsStruct().ATtyp; // !!!! do zmiany

        if( ui->comboBox_15->currentText() == "P_atm" )
            simFitData.simData.flanges.back().P = spcGen->getGasConfigStruct().PresAtm;
        else if( ui->comboBox_15->currentText() == "PP_min" )
            simFitData.simData.flanges.back().P = spcGen->getInputsStruct().PPmin;
        else if( ui->comboBox_15->currentText() == "PP_max" )
            simFitData.simData.flanges.back().P = spcGen->getInputsStruct().PPmax;
        else if( ui->comboBox_15->currentText() == "PP_typ" )
            simFitData.simData.flanges.back().P = spcGen->getInputsStruct().PPtyp;
    }

    // Flange 3
    if( ui->comboBox_21->currentText() != "0" ) // flange defined
    {
        simFitData.simData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
        simFitData.simData.flanges.back().X2 = 0.0;

        if( ui->comboBox_21->currentText() == "X_IntCell_1" )
            simFitData.simData.flanges.back().X1 = spcGen->getGasConfigStruct().conc1xIntCell;

        if( ui->comboBox_22->currentText() == "Air" )
        {
            simFitData.simData.flanges.back().nameMatrX.push_back("N2");
            simFitData.simData.flanges.back().matrX = spcGen->createMatrixConcentrations(false,true);
        }

        if( ui->comboBox_23->currentText() == "AT_typ" )
            simFitData.simData.flanges.back().T = spcGen->getInputsStruct().ATtyp;

        if( ui->comboBox_24->currentText() == "P_atm" )
            simFitData.simData.flanges.back().P = spcGen->getGasConfigStruct().PresAtm;

        if( ui->comboBox_25->currentText() == "L_int_cell_opt" )
            simFitData.simData.flanges.back().L = spcGen->getGasConfigStruct().intCellLenOpt;
    }

    /************************************************** Fitting part ******************************************************/

    // STACK part
    simFitData.fitData.stack.X2 = spcGen->getInputsStruct().concTyp2; //0.0;

    if( ui->comboBox_48->currentText() == "Air" )
    {
        simFitData.fitData.stack.nameMatrX.push_back("N2");

        if( ui->comboBox_44->currentText() == "X_meas" )
            simFitData.stackFittingLater = true;
        //else
        simFitData.fitData.stack.matrX = spcGen->createMatrixConcentrations(true,false);
    }
    else if( ui->comboBox_48->currentText().contains("Matrix") )
    {
        simFitData.fitData.stack.nameMatrX.push_back("N2");
        simFitData.fitData.stack.nameMatrX.push_back("CO2");

        if( ui->comboBox_48->currentText().contains("minPW") )
            simFitData.fitData.stack.mtxStyle = GASCONFIG_SIM_FIT_STRUCT::minPW;
        else if( ui->comboBox_48->currentText().contains("maxPW") )
            simFitData.fitData.stack.mtxStyle = GASCONFIG_SIM_FIT_STRUCT::maxPW;

        if( ui->comboBox_44->currentText() == "X_meas" )
            simFitData.stackFittingLater = true;
        //else
        simFitData.fitData.stack.matrX = spcGen->createMatrixConcentrations(true,false);
    }

    if( ui->comboBox_52->currentText() == "PT_min" )
        simFitData.fitData.stack.T = spcGen->getInputsStruct().PTmin;
    else if( ui->comboBox_52->currentText() == "PT_max" )
        simFitData.fitData.stack.T = spcGen->getInputsStruct().PTmax;
    else if( ui->comboBox_52->currentText() == "PT_typ" )
        simFitData.fitData.stack.T = spcGen->getInputsStruct().PTtyp;
    else if( ui->comboBox_52->currentText() == "AT_min" )
        simFitData.fitData.stack.T = spcGen->getInputsStruct().ATmin;
    else if( ui->comboBox_52->currentText() == "AT_max" )
        simFitData.fitData.stack.T = spcGen->getInputsStruct().ATmax;
    else if( ui->comboBox_52->currentText() == "AT_typ" )
        simFitData.fitData.stack.T = spcGen->getInputsStruct().ATtyp;

    if( ui->comboBox_56->currentText() == "P_atm" )
        simFitData.fitData.stack.P = spcGen->getGasConfigStruct().PresAtm;
    else if( ui->comboBox_56->currentText() == "PP_min" )
        simFitData.fitData.stack.P = spcGen->getInputsStruct().PPmin;
    else if( ui->comboBox_56->currentText() == "PP_max" )
        simFitData.fitData.stack.P = spcGen->getInputsStruct().PPmax;
    else if( ui->comboBox_56->currentText() == "PP_typ" )
        simFitData.fitData.stack.P = spcGen->getInputsStruct().PPtyp;

    if( ui->comboBox_60->currentText() == "L2-L_ins_tube" )
        simFitData.fitData.stack.L = spcGen->getInputsStruct().L2 - spcGen->getOutputInsTubeLen();
    else if( ui->comboBox_60->currentText() == "L_Tx+L_Rx" )
        simFitData.fitData.stack.L = spcGen->getGasConfigStruct().TxLen + spcGen->getGasConfigStruct().RxLen;
    else if( ui->comboBox_60->currentText() == "L1+L3" )
        simFitData.fitData.stack.L = spcGen->getInputsStruct().L1 + spcGen->getInputsStruct().L3;
    else if( ui->comboBox_60->currentText() == "L_ext_cell" )
        simFitData.fitData.stack.L = spcGen->getGasConfigStruct().extCellLenOpt; // spcGen->getOutputsStruct().L_ext_cell;
    else if( ui->comboBox_60->currentText() == "L_int_cell_opt" )
        simFitData.fitData.stack.L = spcGen->getGasConfigStruct().intCellLenOpt;

    // FLANGES part

    // Flange 0
    if( ui->comboBox_41->currentText() != "0" ) // flange not defined
    {
        simFitData.fitData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
        simFitData.fitData.flanges.back().X2 = 0.0;

        if( ui->comboBox_41->currentText() == "X_Air_1" )
            simFitData.fitData.flanges.back().X1 = spcGen->getGasConfigStruct().conc1xAir;
        else if( ui->comboBox_41->currentText() == "pur_conc_1" )
            simFitData.fitData.flanges.back().X1 = spcGen->getInputsStruct().purConc1;

        if( ui->comboBox_45->currentText() == "Air" )
        {
            simFitData.fitData.flanges.back().nameMatrX.push_back("N2");
            simFitData.fitData.flanges.back().matrX = spcGen->createMatrixConcentrations(false,false);
        }
        else if( ui->comboBox_45->currentText() == "custom" )
        {
            simFitData.fitData.flanges.back().nameMatrX.push_back(spcGen->getInputsStruct().purMatxGas1);
            simFitData.fitData.flanges.back().nameMatrX.push_back(spcGen->getInputsStruct().purMatxGas2);
            simFitData.fitData.flanges.back().matrX = spcGen->createMatrixConcentrations(false,false);
        }

        if( ui->comboBox_49->currentText() == "AT_min" )
            simFitData.fitData.flanges.back().T = spcGen->getInputsStruct().ATmin;
        else if( ui->comboBox_49->currentText() == "AT_max" )
            simFitData.fitData.flanges.back().T = spcGen->getInputsStruct().ATmax;
        else if( ui->comboBox_49->currentText() == "AT_typ" )
            simFitData.fitData.flanges.back().T = spcGen->getInputsStruct().ATtyp;

        if( ui->comboBox_53->currentText() == "P_atm" )
            simFitData.fitData.flanges.back().P = spcGen->getGasConfigStruct().PresAtm;
        else if( ui->comboBox_53->currentText() == "PP_min" )
            simFitData.fitData.flanges.back().P = spcGen->getInputsStruct().PPmin;
        else if( ui->comboBox_53->currentText() == "PP_max" )
            simFitData.fitData.flanges.back().P = spcGen->getInputsStruct().PPmax;

        if( ui->comboBox_57->currentText() == "L_Tx+L_Rx" )
            simFitData.fitData.flanges.back().L = spcGen->getGasConfigStruct().TxLen + spcGen->getGasConfigStruct().RxLen;
        else if( ui->comboBox_57->currentText() == "L_Tx+L_Rx+L_ext_cell" )
            simFitData.fitData.flanges.back().L = spcGen->getGasConfigStruct().TxLen + spcGen->getGasConfigStruct().RxLen + spcGen->getOutputsStruct().L_ext_cell; // spcGen->getGasConfigStruct().extCellLenOpt;
    }

    // Flange 1
    if( ui->comboBox_42->currentText() != "0" ) // flange not defined
    {
        simFitData.fitData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
        simFitData.fitData.flanges.back().X2 = 0.0;

        if( ui->comboBox_42->currentText() == "X_Air_1" )
            simFitData.fitData.flanges.back().X1 = spcGen->getGasConfigStruct().conc1xAir;
        else if( ui->comboBox_42->currentText() == "pur_conc_1" )
            simFitData.fitData.flanges.back().X1 = spcGen->getInputsStruct().purConc1;

        if( ui->comboBox_46->currentText() == "Air" )
        {
            simFitData.fitData.flanges.back().nameMatrX.push_back("N2");
            simFitData.fitData.flanges.back().matrX = spcGen->createMatrixConcentrations(false,false);
        }
        else if( ui->comboBox_46->currentText() == "custom" )
        {
            simFitData.fitData.flanges.back().nameMatrX.push_back(spcGen->getInputsStruct().purMatxGas1);
            simFitData.fitData.flanges.back().nameMatrX.push_back(spcGen->getInputsStruct().purMatxGas2);
            simFitData.fitData.flanges.back().matrX = spcGen->createMatrixConcentrations(false,false);
        }

        if( ui->comboBox_50->currentText() == "AT_min" )
            simFitData.fitData.flanges.back().T = spcGen->getInputsStruct().ATmin;
        else if( ui->comboBox_50->currentText() == "AT_max" )
            simFitData.fitData.flanges.back().T = spcGen->getInputsStruct().ATmax;
        else if( ui->comboBox_50->currentText() == "AT_typ" )
            simFitData.fitData.flanges.back().T = spcGen->getInputsStruct().ATtyp;

        if( ui->comboBox_54->currentText() == "P_atm" )
            simFitData.fitData.flanges.back().P = spcGen->getGasConfigStruct().PresAtm;
        else if( ui->comboBox_54->currentText() == "PP_min" )
            simFitData.fitData.flanges.back().P = spcGen->getInputsStruct().PPmin;
        else if( ui->comboBox_54->currentText() == "PP_max" )
            simFitData.fitData.flanges.back().P = spcGen->getInputsStruct().PPmax;
        else if( ui->comboBox_54->currentText() == "PP_typ" )
            simFitData.fitData.flanges.back().P = spcGen->getInputsStruct().PPtyp;

        if( ui->comboBox_58->currentText() == "L1+L3" )
            simFitData.fitData.flanges.back().L = spcGen->getInputsStruct().L1 + spcGen->getInputsStruct().L3;
    }

    // Flange 2
    if( ui->comboBox_43->currentText() != "0" && ui->comboBox_59->currentText() == "L_ins_tube" && spcGen->getOutputInsTubeLen() > 0.0 ) // flange not defined
    {
        simFitData.fitData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
        simFitData.fitData.flanges.back().X2 = 0.0;

        if( ui->comboBox_43->currentText() == "X_Air_1" )
            simFitData.fitData.flanges.back().X1 = spcGen->getGasConfigStruct().conc1xAir;
        else if( ui->comboBox_43->currentText() == "pur_conc_1" )
            simFitData.fitData.flanges.back().X1 = spcGen->getInputsStruct().purConc1;

        if( ui->comboBox_47->currentText() == "Air" )
        {
            simFitData.fitData.flanges.back().nameMatrX.push_back("N2");
            simFitData.fitData.flanges.back().matrX = spcGen->createMatrixConcentrations(false,false);
        }
        else if( ui->comboBox_47->currentText() == "custom" )
        {
            simFitData.fitData.flanges.back().nameMatrX.push_back(spcGen->getInputsStruct().purMatxGas1);
            simFitData.fitData.flanges.back().nameMatrX.push_back(spcGen->getInputsStruct().purMatxGas2);
            simFitData.fitData.flanges.back().matrX = spcGen->createMatrixConcentrations(false,false);
        }

        if( ui->comboBox_51->currentText() == "AT_min" )
            simFitData.fitData.flanges.back().T = spcGen->getInputsStruct().ATmin;
        else if( ui->comboBox_51->currentText() == "AT_max" )
            simFitData.fitData.flanges.back().T = spcGen->getInputsStruct().ATmax;
        else if( ui->comboBox_51->currentText() == "T_ins_typ" )
            simFitData.fitData.flanges.back().T = spcGen->getOutputsStruct().T_ins_tube_typ; // spcGen->getInputsStruct().ATtyp; // !!!! do zmiany

        if( ui->comboBox_55->currentText() == "P_atm" )
            simFitData.fitData.flanges.back().P = spcGen->getGasConfigStruct().PresAtm;
        else if( ui->comboBox_55->currentText() == "PP_min" )
            simFitData.fitData.flanges.back().P = spcGen->getInputsStruct().PPmin;
        else if( ui->comboBox_55->currentText() == "PP_max" )
            simFitData.fitData.flanges.back().P = spcGen->getInputsStruct().PPmax;
        else if( ui->comboBox_55->currentText() == "PP_typ" )
            simFitData.fitData.flanges.back().P = spcGen->getInputsStruct().PPtyp;

        if( ui->comboBox_59->currentText() == "L_ext_cell" )
            simFitData.fitData.flanges.back().L = spcGen->getOutputsStruct().L_ext_cell; // spcGen->getGasConfigStruct().extCellLenOpt;
        else if( ui->comboBox_59->currentText() == "L_ins_tube" )
        {
            simFitData.fitData.flanges.back().L = spcGen->getOutputInsTubeLen();
            spcGen->changeInsTubePurgeGas( ui->comboBox_47->currentText().toLocal8Bit().constData() );
        }
    }

    // Flange 3
    if( ui->comboBox_26->currentText() != "0" ) // flange defined
    {
        simFitData.fitData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
        simFitData.fitData.flanges.back().X2 = 0.0;

        if( ui->comboBox_26->currentText() == "conc_typ_1" )
            simFitData.fitData.flanges.back().X1 = spcGen->getInputsStruct().concTyp1;;

        if( ui->comboBox_27->currentText() == "Matrix" )
        {
            simFitData.fitData.flanges.back().nameMatrX.push_back("N2");
            simFitData.fitData.flanges.back().nameMatrX.push_back("CO2");

            simFitData.fitData.flanges.back().matrX = spcGen->createMatrixConcentrations(false,false);
        }

        if( ui->comboBox_28->currentText() == "PT_typ" )
            simFitData.fitData.flanges.back().T = spcGen->getInputsStruct().PTtyp;

        if( ui->comboBox_29->currentText() == "PP_typ" )
            simFitData.fitData.flanges.back().P = spcGen->getInputsStruct().PPtyp;

        if( ui->comboBox_30->currentText() == "L2-L_ins_tube" )
            simFitData.fitData.flanges.back().L = spcGen->getInputsStruct().L2 - spcGen->getOutputInsTubeLen();
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Widget::calculateAvailableOutput()
{
    simCase currentSimCase{None};

    if( ui->comboBox->currentText() == "Interior Tx Rx Space (LL)" )
        currentSimCase = TxRx_LL;
    else if( ui->comboBox->currentText() == "Stack (LL)" )
        currentSimCase = Stack_LL;
    else if( ui->comboBox->currentText() == "Flanges (LL)" )
        currentSimCase = Flange_LL;
    else if( ui->comboBox->currentText() == "External Cell (LL)" )
        currentSimCase = ExtCell_LL;
    else if( ui->comboBox->currentText() == "Max PW, Air purge, fixed PW (ICI)" )
        currentSimCase = ICI_1;
    else if( ui->comboBox->currentText() == "Max PW, N2 purge, fixed PW (ICI)" )
        currentSimCase = ICI_2;
    else if( ui->comboBox->currentText() == "Max PW, custom purge, fixed PW (ICI)" )
        currentSimCase = ICI_3;
    else if( ui->comboBox->currentText() == "Max PW, Air purge, floated PW (ICI)" )
        currentSimCase = ICI_4;
    else if( ui->comboBox->currentText() == "Max PW, N2 purge, floated PW (ICI)" )
        currentSimCase = ICI_5;
    else if( ui->comboBox->currentText() == "Max PW, custom purge, floated PW (ICI)" )
        currentSimCase = ICI_6;
    else if( ui->comboBox->currentText() == "Min PW, Air purge, fixed PW (ICI)" )
        currentSimCase = ICI_7;
    else if( ui->comboBox->currentText() == "Min PW, N2 purge, fixed PW (ICI)" )
        currentSimCase = ICI_8;
    else if( ui->comboBox->currentText() == "Min PW, custom purge, fixed PW (ICI)" )
        currentSimCase = ICI_9;
    else if( ui->comboBox->currentText() == "Min PW, Air purge, floated PW (ICI)" )
        currentSimCase = ICI_10;
    else if( ui->comboBox->currentText() == "Min PW, N2 purge, floated PW (ICI)" )
        currentSimCase = ICI_11;
    else if( ui->comboBox->currentText() == "Min PW, custom purge, floated PW (ICI)" )
        currentSimCase = ICI_12;
    else if( ui->comboBox->currentText() == "Pressure max, Air purge, fixed PW (PD)" )
        currentSimCase = PD_1;
    else if( ui->comboBox->currentText() == "Pressure max, N2 purge, fixed PW (PD)" )
        currentSimCase = PD_2;
    else if( ui->comboBox->currentText() == "Pressure max, custom purge, fixed PW (PD)" )
        currentSimCase = PD_3;
    else if( ui->comboBox->currentText() == "Pressure max, Air purge, floated PW (PD)" )
        currentSimCase = PD_4;
    else if( ui->comboBox->currentText() == "Pressure max, N2 purge, floated PW (PD)" )
        currentSimCase = PD_5;
    else if( ui->comboBox->currentText() == "Pressure max, custom purge, floated PW (PD)" )
        currentSimCase = PD_6;
    else if( ui->comboBox->currentText() == "Pressure min, Air purge, fixed PW (PD)" )
        currentSimCase = PD_7;
    else if( ui->comboBox->currentText() == "Pressure min, N2 purge, fixed PW (PD)" )
        currentSimCase = PD_8;
    else if( ui->comboBox->currentText() == "Pressure min, custom purge, fixed PW (PD)" )
        currentSimCase = PD_9;
    else if( ui->comboBox->currentText() == "Pressure min, Air purge, floated PW (PD)" )
        currentSimCase = PD_10;
    else if( ui->comboBox->currentText() == "Pressure min, N2 purge, floated PW (PD)" )
        currentSimCase = PD_11;
    else if( ui->comboBox->currentText() == "Pressure min, custom purge, floated PW (PD)" )
        currentSimCase = PD_12;
    else if( ui->comboBox->currentText() == "Temperature max, Air purge, fixed PW (TD)" )
        currentSimCase = TD_1;
    else if( ui->comboBox->currentText() == "Temperature max, N2 purge, fixed PW (TD)" )
        currentSimCase = TD_2;
    else if( ui->comboBox->currentText() == "Temperature max, custom purge, fixed PW (TD)" )
        currentSimCase = TD_3;
    else if( ui->comboBox->currentText() == "Temperature max, Air purge, floated PW (TD)" )
        currentSimCase = TD_4;
    else if( ui->comboBox->currentText() == "Temperature max, N2 purge, floated PW (TD)" )
        currentSimCase = TD_5;
    else if( ui->comboBox->currentText() == "Temperature max, custom purge, floated PW (TD)" )
        currentSimCase = TD_6;
    else if( ui->comboBox->currentText() == "Temperature min, Air purge, fixed PW (TD)" )
        currentSimCase = TD_7;
    else if( ui->comboBox->currentText() == "Temperature min, N2 purge, fixed PW (TD)" )
        currentSimCase = TD_8;
    else if( ui->comboBox->currentText() == "Temperature min, custom purge, fixed PW (TD)" )
        currentSimCase = TD_9;
    else if( ui->comboBox->currentText() == "Temperature min, Air purge, floated PW (TD)" )
        currentSimCase = TD_10;
    else if( ui->comboBox->currentText() == "Temperature min, N2 purge, floated PW (TD)" )
        currentSimCase = TD_11;
    else if( ui->comboBox->currentText() == "Temperature min, custom purge, floated PW (TD)" )
        currentSimCase = TD_12;
    else if( ui->comboBox->currentText() == "Air purge, fixed PW (Precision & LDL)" )
        currentSimCase = PrecLDL_1;
    else if( ui->comboBox->currentText() == "N2 purge, fixed PW (Precision & LDL)" )
        currentSimCase = PrecLDL_2;
    else if( ui->comboBox->currentText() == "Custom purge, fixed PW (Precision & LDL)" )
        currentSimCase = PrecLDL_3;
    else if( ui->comboBox->currentText() == "Air purge, floated PW (Precision & LDL)" )
        currentSimCase = PrecLDL_4;
    else if( ui->comboBox->currentText() == "N2 purge, floated PW (Precision & LDL)" )
        currentSimCase = PrecLDL_5;
    else if( ui->comboBox->currentText() == "Custom purge, floated PW (Precision & LDL)" )
        currentSimCase = PrecLDL_6;
    else if( ui->comboBox->currentText() == "Air purge, fixed PW (RAM)" )
        currentSimCase = RAM_1;
    else if( ui->comboBox->currentText() == "N2 purge, fixed PW (RAM)" )
        currentSimCase = RAM_2;
    else if( ui->comboBox->currentText() == "Custom purge, fixed PW (RAM)" )
        currentSimCase = RAM_3;
    else if( ui->comboBox->currentText() == "Air purge, floated PW (RAM)" )
        currentSimCase = RAM_4;
    else if( ui->comboBox->currentText() == "N2 purge, floated PW (RAM)" )
        currentSimCase = RAM_5;
    else if( ui->comboBox->currentText() == "Custom purge, floated PW (RAM)" )
        currentSimCase = RAM_6;
    else if( ui->comboBox->currentText() == "Air purge (VA)")
        currentSimCase = VA_1;
    else if( ui->comboBox->currentText() == "N2 purge (VA)")
        currentSimCase = VA_2;
    else if( ui->comboBox->currentText() == "Custom purge (VA)")
        currentSimCase = VA_3;

    bool green = spcGen->calculateSimOutputs(currentSimCase);

    switch( currentSimCase )
    {
    case TxRx_LL:
        if( green ) ui->label_79->setStyleSheet("QLabel { color : green; }"); else ui->label_79->setStyleSheet("QLabel { color : red; }");
    case Stack_LL:
        if( green ) ui->label_80->setStyleSheet("QLabel { color : green; }"); else ui->label_80->setStyleSheet("QLabel { color : red; }");
    case Flange_LL:
        if( green ) ui->label_81->setStyleSheet("QLabel { color : green; }"); else ui->label_81->setStyleSheet("QLabel { color : red; }");
    case ExtCell_LL:
        if( green ) ui->label_91->setStyleSheet("QLabel { color : green; }"); else ui->label_91->setStyleSheet("QLabel { color : red; }");
    default:
        break;
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Widget::if_comboBox_changed( int llOption )
{
    // Set dust loading to max
    ui->radioButton_6->setChecked(true);

    // Set doubled iteration OFF
    spcGen->setDoubledIteration(false);

    // Set Flange 3 to empty for all test cases except Validation/Adjustment
    ui->comboBox_21->setCurrentText("0");
    ui->comboBox_26->setCurrentText("0");

    // Set up for line-locking operations
    ui->checkBox_1->setChecked(true);
    ui->checkBox_2->setChecked(true);
    ui->checkBox_3->setChecked(true);
    ui->checkBox_4->setChecked(true);
    ui->checkBox_5->setChecked(false);
    ui->checkBox_6->setChecked(true);

    ui->comboBox_5->setCurrentText("Air");
    ui->comboBox_6->setCurrentText("Air");
    ui->comboBox_7->setCurrentText("Air");
    ui->comboBox_8->setCurrentText("Matrix");
    ui->comboBox_13->setCurrentText("P_atm");
    ui->comboBox_15->setCurrentText("P_atm");
    ui->comboBox_17->setCurrentText("L_Tx+L_Rx");
    ui->comboBox_18->setCurrentText("L1+L3");
    ui->comboBox_19->setCurrentText("L_ext_cell");
    ui->comboBox_20->setCurrentText("L2-L_ins_tube");
    ui->comboBox_41->setCurrentText("0");
    ui->comboBox_42->setCurrentText("0");
    ui->comboBox_43->setCurrentText("0");
    ui->comboBox_44->setCurrentText("X_meas");
    ui->comboBox_49->setCurrentText("AT_max");
    ui->comboBox_50->setCurrentText("AT_max");
    ui->comboBox_51->setCurrentText("AT_max");
    ui->comboBox_53->setCurrentText("P_atm");
    ui->comboBox_55->setCurrentText("P_atm");

    switch( llOption )
    {
    case 1: // "Interior Tx Rx Space (LL)"
        ui->comboBox_1->setCurrentText("X_Air_1");
        ui->comboBox_2->setCurrentText("0");
        ui->comboBox_3->setCurrentText("0");
        ui->comboBox_4->setCurrentText("0");
        ui->comboBox_9->setCurrentText("AT_max");
        ui->comboBox_12->setCurrentText("PT_max");
        ui->comboBox_14->setCurrentText("PP_max");
        ui->comboBox_16->setCurrentText("PP_max");
        ui->comboBox_20->setCurrentText("L2-L_ins_tube");
        ui->comboBox_48->setCurrentText("Air");
        ui->comboBox_52->setCurrentText("AT_max");
        ui->comboBox_54->setCurrentText("PP_max");
        ui->comboBox_56->setCurrentText("P_atm");
        ui->comboBox_60->setCurrentText("L_Tx+L_Rx");
        break;
    case 2: // "Stack (LL)"
        ui->comboBox_1->setCurrentText("0");
        ui->comboBox_2->setCurrentText("0");
        ui->comboBox_3->setCurrentText("0");
        ui->comboBox_4->setCurrentText("conc_min_1");
        ui->comboBox_12->setCurrentText("PT_max");
        ui->comboBox_14->setCurrentText("PP_max");
        ui->comboBox_16->setCurrentText("PP_max");
        ui->comboBox_48->setCurrentText("Matrix");
        ui->comboBox_52->setCurrentText("PT_max");
        ui->comboBox_56->setCurrentText("PP_max");
        ui->comboBox_60->setCurrentText("L2-L_ins_tube");
        break;
    case 3: // "Flanges (LL)"
        ui->comboBox_1->setCurrentText("0");
        ui->comboBox_2->setCurrentText("X_Air_1");
        ui->comboBox_3->setCurrentText("0");
        ui->comboBox_4->setCurrentText("0");
        ui->comboBox_10->setCurrentText("AT_max");
        ui->comboBox_14->setCurrentText("PP_max");
        ui->comboBox_16->setCurrentText("PP_max");
        ui->comboBox_48->setCurrentText("Air");
        ui->comboBox_52->setCurrentText("AT_max");
        ui->comboBox_54->setCurrentText("PP_max");
        ui->comboBox_56->setCurrentText("PP_max");
        ui->comboBox_60->setCurrentText("L1+L3");
        break;
    case 4: // "External Cell (LL)"
        ui->comboBox_1->setCurrentText("0");
        ui->comboBox_2->setCurrentText("0");
        ui->comboBox_3->setCurrentText("X_Air_1");
        ui->comboBox_4->setCurrentText("0");
        ui->comboBox_11->setCurrentText("AT_max");
        ui->comboBox_14->setCurrentText("PP_typ");
        ui->comboBox_16->setCurrentText("PP_max");
        ui->comboBox_48->setCurrentText("Air");
        ui->comboBox_52->setCurrentText("AT_max");
        ui->comboBox_54->setCurrentText("PP_typ");
        ui->comboBox_56->setCurrentText("P_atm");
        ui->comboBox_60->setCurrentText("L_ext_cell");
        break;
    default: // Indirect Cross Interference operations
        setICIcheckConfig(llOption);
    }

    ui->radioButton_2->setChecked(true);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Widget::setICIcheckConfig( int option )
{
    // Noise OFF
    ui->checkBox_1->setChecked(false);
    ui->checkBox_2->setChecked(false);
    ui->checkBox_3->setChecked(false);
    ui->checkBox_4->setChecked(false);
    ui->checkBox_5->setChecked(false);
    ui->checkBox_6->setChecked(false);

    ui->comboBox_1->setCurrentText("X_Air_1");
    ui->comboBox_4->setCurrentText("conc_typ_1");
    ui->comboBox_5->setCurrentText("Air");
    ui->comboBox_9->setCurrentText("AT_typ");
    ui->comboBox_10->setCurrentText("AT_typ");
    ui->comboBox_11->setCurrentText("T_ins_typ");
    ui->comboBox_12->setCurrentText("PT_typ");
    ui->comboBox_13->setCurrentText("P_atm");
    ui->comboBox_14->setCurrentText("PP_typ");
    ui->comboBox_15->setCurrentText("PP_typ");
    ui->comboBox_16->setCurrentText("PP_typ");
    ui->comboBox_17->setCurrentText("L_Tx+L_Rx+L_ext_cell");
    ui->comboBox_18->setCurrentText("L1+L3");
    ui->comboBox_19->setCurrentText("L_ins_tube");
    ui->comboBox_20->setCurrentText("L2-L_ins_tube");
    ui->comboBox_41->setCurrentText("X_Air_1");
    ui->comboBox_44->setCurrentText("X_meas");
    ui->comboBox_45->setCurrentText("Air");
    ui->comboBox_48->setCurrentText("Matrix");
    ui->comboBox_49->setCurrentText("AT_typ");
    ui->comboBox_50->setCurrentText("AT_typ");
    ui->comboBox_51->setCurrentText("T_ins_typ");
    ui->comboBox_52->setCurrentText("PT_typ");
    ui->comboBox_53->setCurrentText("P_atm");
    ui->comboBox_54->setCurrentText("PP_typ");
    ui->comboBox_55->setCurrentText("PP_typ");
    ui->comboBox_56->setCurrentText("PP_typ");
    ui->comboBox_57->setCurrentText("L_Tx+L_Rx+L_ext_cell");
    ui->comboBox_58->setCurrentText("L1+L3");
    ui->comboBox_59->setCurrentText("L_ins_tube");
    ui->comboBox_60->setCurrentText("L2-L_ins_tube");

    switch( option )
    {
    case 5: // Max PW, Air purge, fixed PW (ICI)
        ui->comboBox_2->setCurrentText("X_Air_1");
        ui->comboBox_3->setCurrentText("X_Air_1");
        ui->comboBox_6->setCurrentText("Air");
        ui->comboBox_7->setCurrentText("Air");
        ui->comboBox_8->setCurrentText("Matrix_maxPW");
        ui->comboBox_42->setCurrentText("X_Air_1");
        ui->comboBox_43->setCurrentText("X_Air_1");
        ui->comboBox_46->setCurrentText("Air");
        ui->comboBox_47->setCurrentText("Air");
        ui->checkBox->setChecked(false);
        break;
    case 6: // Max PW, N2 purge, fixed PW (ICI)
        ui->comboBox_2->setCurrentText("0");
        ui->comboBox_3->setCurrentText("0");
        ui->comboBox_6->setCurrentText("N2");
        ui->comboBox_7->setCurrentText("N2");
        ui->comboBox_8->setCurrentText("Matrix_maxPW");
        ui->comboBox_42->setCurrentText("0");
        ui->comboBox_43->setCurrentText("0");
        ui->comboBox_46->setCurrentText("N2");
        ui->comboBox_47->setCurrentText("N2");
        ui->checkBox->setChecked(false);
        break;
    case 7: // Max PW, Custom purge, fixed PW (ICI)
        ui->comboBox_2->setCurrentText("pur_conc_1");
        ui->comboBox_3->setCurrentText("pur_conc_1");
        ui->comboBox_6->setCurrentText("custom");
        ui->comboBox_7->setCurrentText("custom");
        ui->comboBox_8->setCurrentText("Matrix_maxPW");
        ui->comboBox_42->setCurrentText("pur_conc_1");
        ui->comboBox_43->setCurrentText("pur_conc_1");
        ui->comboBox_46->setCurrentText("custom");
        ui->comboBox_47->setCurrentText("custom");
        ui->checkBox->setChecked(false);
        break;
    case 8: // Max PW, Air purge, floated PW (ICI)
        ui->comboBox_2->setCurrentText("X_Air_1");
        ui->comboBox_3->setCurrentText("X_Air_1");
        ui->comboBox_6->setCurrentText("Air");
        ui->comboBox_7->setCurrentText("Air");
        ui->comboBox_8->setCurrentText("Matrix_maxPW");
        ui->comboBox_42->setCurrentText("X_Air_1");
        ui->comboBox_43->setCurrentText("X_Air_1");
        ui->comboBox_46->setCurrentText("Air");
        ui->comboBox_47->setCurrentText("Air");
        ui->checkBox->setChecked(true);
        break;
    case 9: // Max PW, N2 purge, floated PW (ICI)
        ui->comboBox_2->setCurrentText("0");
        ui->comboBox_3->setCurrentText("0");
        ui->comboBox_6->setCurrentText("N2");
        ui->comboBox_7->setCurrentText("N2");
        ui->comboBox_8->setCurrentText("Matrix_maxPW");
        ui->comboBox_42->setCurrentText("0");
        ui->comboBox_43->setCurrentText("0");
        ui->comboBox_46->setCurrentText("N2");
        ui->comboBox_47->setCurrentText("N2");
        ui->checkBox->setChecked(true);
        break;
    case 10: // Max PW, Custom purge, floated PW (ICI)
        ui->comboBox_2->setCurrentText("pur_conc_1");
        ui->comboBox_3->setCurrentText("pur_conc_1");
        ui->comboBox_6->setCurrentText("custom");
        ui->comboBox_7->setCurrentText("custom");
        ui->comboBox_8->setCurrentText("Matrix_maxPW");
        ui->comboBox_42->setCurrentText("pur_conc_1");
        ui->comboBox_43->setCurrentText("pur_conc_1");
        ui->comboBox_46->setCurrentText("custom");
        ui->comboBox_47->setCurrentText("custom");
        ui->checkBox->setChecked(true);
        break;
    case 11: // Min PW, Air purge, fixed PW (ICI)
        ui->comboBox_2->setCurrentText("X_Air_1");
        ui->comboBox_3->setCurrentText("X_Air_1");
        ui->comboBox_6->setCurrentText("Air");
        ui->comboBox_7->setCurrentText("Air");
        ui->comboBox_8->setCurrentText("Matrix_minPW");
        ui->comboBox_42->setCurrentText("X_Air_1");
        ui->comboBox_43->setCurrentText("X_Air_1");
        ui->comboBox_46->setCurrentText("Air");
        ui->comboBox_47->setCurrentText("Air");
        ui->checkBox->setChecked(false);
        break;
    case 12: // Min PW, N2 purge, fixed PW (ICI)
        ui->comboBox_2->setCurrentText("0");
        ui->comboBox_3->setCurrentText("0");
        ui->comboBox_6->setCurrentText("N2");
        ui->comboBox_7->setCurrentText("N2");
        ui->comboBox_8->setCurrentText("Matrix_minPW");
        ui->comboBox_42->setCurrentText("0");
        ui->comboBox_43->setCurrentText("0");
        ui->comboBox_46->setCurrentText("N2");
        ui->comboBox_47->setCurrentText("N2");
        ui->checkBox->setChecked(false);
        break;
    case 13: // Min PW, Custom purge, fixed PW (ICI)
        ui->comboBox_2->setCurrentText("pur_conc_1");
        ui->comboBox_3->setCurrentText("pur_conc_1");
        ui->comboBox_6->setCurrentText("custom");
        ui->comboBox_7->setCurrentText("custom");
        ui->comboBox_8->setCurrentText("Matrix_minPW");
        ui->comboBox_42->setCurrentText("pur_conc_1");
        ui->comboBox_43->setCurrentText("pur_conc_1");
        ui->comboBox_46->setCurrentText("custom");
        ui->comboBox_47->setCurrentText("custom");
        ui->checkBox->setChecked(false);
        break;
    case 14: // Min PW, Air purge, floated PW (ICI)
        ui->comboBox_2->setCurrentText("X_Air_1");
        ui->comboBox_3->setCurrentText("X_Air_1");
        ui->comboBox_6->setCurrentText("Air");
        ui->comboBox_7->setCurrentText("Air");
        ui->comboBox_8->setCurrentText("Matrix_minPW");
        ui->comboBox_42->setCurrentText("X_Air_1");
        ui->comboBox_43->setCurrentText("X_Air_1");
        ui->comboBox_46->setCurrentText("Air");
        ui->comboBox_47->setCurrentText("Air");
        ui->checkBox->setChecked(true);
        break;
    case 15: // Min PW, N2 purge, floated PW (ICI)
        ui->comboBox_2->setCurrentText("0");
        ui->comboBox_3->setCurrentText("0");
        ui->comboBox_6->setCurrentText("N2");
        ui->comboBox_7->setCurrentText("N2");
        ui->comboBox_8->setCurrentText("Matrix_minPW");
        ui->comboBox_42->setCurrentText("0");
        ui->comboBox_43->setCurrentText("0");
        ui->comboBox_46->setCurrentText("N2");
        ui->comboBox_47->setCurrentText("N2");
        ui->checkBox->setChecked(true);
        break;
    case 16: // Min PW, Custom purge, floated PW (ICI)
        ui->comboBox_2->setCurrentText("pur_conc_1");
        ui->comboBox_3->setCurrentText("pur_conc_1");
        ui->comboBox_6->setCurrentText("custom");
        ui->comboBox_7->setCurrentText("custom");
        ui->comboBox_8->setCurrentText("Matrix_minPW");
        ui->comboBox_42->setCurrentText("pur_conc_1");
        ui->comboBox_43->setCurrentText("pur_conc_1");
        ui->comboBox_46->setCurrentText("custom");
        ui->comboBox_47->setCurrentText("custom");
        ui->checkBox->setChecked(true);
        break;
    default:
        setPressureDepConfig( option );
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Widget::setPressureDepConfig( int option )
{
    ui->comboBox_1->setCurrentText("X_Air_1");
    ui->comboBox_4->setCurrentText("conc_typ_1");
    ui->comboBox_5->setCurrentText("Air");
    ui->comboBox_8->setCurrentText("Matrix");
    ui->comboBox_9->setCurrentText("AT_typ");
    ui->comboBox_10->setCurrentText("AT_typ");
    ui->comboBox_11->setCurrentText("T_ins_typ");
    ui->comboBox_12->setCurrentText("PT_typ");
    ui->comboBox_13->setCurrentText("P_atm");
    ui->comboBox_14->setCurrentText("PP_typ");
    ui->comboBox_15->setCurrentText("PP_typ");
    ui->comboBox_17->setCurrentText("L_Tx+L_Rx+L_ext_cell");
    ui->comboBox_18->setCurrentText("L1+L3");
    ui->comboBox_19->setCurrentText("L_ins_tube");
    ui->comboBox_20->setCurrentText("L2-L_ins_tube");
    ui->comboBox_41->setCurrentText("X_Air_1");
    ui->comboBox_44->setCurrentText("X_meas");
    ui->comboBox_45->setCurrentText("Air");
    ui->comboBox_48->setCurrentText("Matrix");
    ui->comboBox_49->setCurrentText("AT_typ");
    ui->comboBox_50->setCurrentText("AT_typ");
    ui->comboBox_51->setCurrentText("T_ins_typ");
    ui->comboBox_52->setCurrentText("PT_typ");
    ui->comboBox_53->setCurrentText("P_atm");
    ui->comboBox_54->setCurrentText("PP_typ");
    ui->comboBox_55->setCurrentText("PP_typ");
    ui->comboBox_56->setCurrentText("PP_typ");
    ui->comboBox_57->setCurrentText("L_Tx+L_Rx+L_ext_cell");
    ui->comboBox_58->setCurrentText("L1+L3");
    ui->comboBox_59->setCurrentText("L_ins_tube");
    ui->comboBox_60->setCurrentText("L2-L_ins_tube");

    switch( option )
    {
    case 17: // Pressure at max, Air purge, fixed PW (PD)
        ui->comboBox_2->setCurrentText("X_Air_1");
        ui->comboBox_3->setCurrentText("X_Air_1");
        ui->comboBox_6->setCurrentText("Air");
        ui->comboBox_7->setCurrentText("Air");
        ui->comboBox_16->setCurrentText("PP_max");
        ui->comboBox_42->setCurrentText("X_Air_1");
        ui->comboBox_43->setCurrentText("X_Air_1");
        ui->comboBox_46->setCurrentText("Air");
        ui->comboBox_47->setCurrentText("Air");
        ui->checkBox->setChecked(false);
        break;
    case 18: // Pressure at max, N2 purge, fixed PW (PD)
        ui->comboBox_2->setCurrentText("0");
        ui->comboBox_3->setCurrentText("0");
        ui->comboBox_6->setCurrentText("N2");
        ui->comboBox_7->setCurrentText("N2");
        ui->comboBox_16->setCurrentText("PP_max");
        ui->comboBox_42->setCurrentText("0");
        ui->comboBox_43->setCurrentText("0");
        ui->comboBox_46->setCurrentText("N2");
        ui->comboBox_47->setCurrentText("N2");
        ui->checkBox->setChecked(false);
        break;
    case 19: // Pressure at max, Custom purge, fixed PW (PD)
        ui->comboBox_2->setCurrentText("pur_conc_1");
        ui->comboBox_3->setCurrentText("pur_conc_1");
        ui->comboBox_6->setCurrentText("custom");
        ui->comboBox_7->setCurrentText("custom");
        ui->comboBox_16->setCurrentText("PP_max");
        ui->comboBox_42->setCurrentText("pur_conc_1");
        ui->comboBox_43->setCurrentText("pur_conc_1");
        ui->comboBox_46->setCurrentText("custom");
        ui->comboBox_47->setCurrentText("custom");
        ui->checkBox->setChecked(false);
        break;
    case 20: // Pressure at max, Air purge, floated PW (PD)
        ui->comboBox_2->setCurrentText("X_Air_1");
        ui->comboBox_3->setCurrentText("X_Air_1");
        ui->comboBox_6->setCurrentText("Air");
        ui->comboBox_7->setCurrentText("Air");
        ui->comboBox_16->setCurrentText("PP_max");
        ui->comboBox_42->setCurrentText("X_Air_1");
        ui->comboBox_43->setCurrentText("X_Air_1");
        ui->comboBox_46->setCurrentText("Air");
        ui->comboBox_47->setCurrentText("Air");
        ui->checkBox->setChecked(true);
        break;
    case 21: // Pressure at max, N2 purge, floated PW (PD)
        ui->comboBox_2->setCurrentText("0");
        ui->comboBox_3->setCurrentText("0");
        ui->comboBox_6->setCurrentText("N2");
        ui->comboBox_7->setCurrentText("N2");
        ui->comboBox_16->setCurrentText("PP_max");
        ui->comboBox_42->setCurrentText("0");
        ui->comboBox_43->setCurrentText("0");
        ui->comboBox_46->setCurrentText("N2");
        ui->comboBox_47->setCurrentText("N2");
        ui->checkBox->setChecked(true);
        break;
    case 22: // Pressure at max, Custom purge, floated PW (PD)
        ui->comboBox_2->setCurrentText("pur_conc_1");
        ui->comboBox_3->setCurrentText("pur_conc_1");
        ui->comboBox_6->setCurrentText("custom");
        ui->comboBox_7->setCurrentText("custom");
        ui->comboBox_16->setCurrentText("PP_max");
        ui->comboBox_42->setCurrentText("pur_conc_1");
        ui->comboBox_43->setCurrentText("pur_conc_1");
        ui->comboBox_46->setCurrentText("custom");
        ui->comboBox_47->setCurrentText("custom");
        ui->checkBox->setChecked(true);
        break;
    case 23: // Pressure at min, Air purge, fixed PW (PD)
        ui->comboBox_2->setCurrentText("X_Air_1");
        ui->comboBox_3->setCurrentText("X_Air_1");
        ui->comboBox_6->setCurrentText("Air");
        ui->comboBox_7->setCurrentText("Air");
        ui->comboBox_16->setCurrentText("PP_min");
        ui->comboBox_42->setCurrentText("X_Air_1");
        ui->comboBox_43->setCurrentText("X_Air_1");
        ui->comboBox_46->setCurrentText("Air");
        ui->comboBox_47->setCurrentText("Air");
        ui->checkBox->setChecked(false);
        break;
    case 24: // Pressure at min, N2 purge, fixed PW (PD)
        ui->comboBox_2->setCurrentText("0");
        ui->comboBox_3->setCurrentText("0");
        ui->comboBox_6->setCurrentText("N2");
        ui->comboBox_7->setCurrentText("N2");
        ui->comboBox_16->setCurrentText("PP_min");
        ui->comboBox_42->setCurrentText("0");
        ui->comboBox_43->setCurrentText("0");
        ui->comboBox_46->setCurrentText("N2");
        ui->comboBox_47->setCurrentText("N2");
        ui->checkBox->setChecked(false);
        break;
    case 25: // Pressure at min, Custom purge, fixed PW (PD)
        ui->comboBox_2->setCurrentText("pur_conc_1");
        ui->comboBox_3->setCurrentText("pur_conc_1");
        ui->comboBox_6->setCurrentText("custom");
        ui->comboBox_7->setCurrentText("custom");
        ui->comboBox_16->setCurrentText("PP_min");
        ui->comboBox_42->setCurrentText("pur_conc_1");
        ui->comboBox_43->setCurrentText("pur_conc_1");
        ui->comboBox_46->setCurrentText("custom");
        ui->comboBox_47->setCurrentText("custom");
        ui->checkBox->setChecked(false);
        break;
    case 26: // Pressure at min, Air purge, floated PW (PD)
        ui->comboBox_2->setCurrentText("X_Air_1");
        ui->comboBox_3->setCurrentText("X_Air_1");
        ui->comboBox_6->setCurrentText("Air");
        ui->comboBox_7->setCurrentText("Air");
        ui->comboBox_16->setCurrentText("PP_min");
        ui->comboBox_42->setCurrentText("X_Air_1");
        ui->comboBox_43->setCurrentText("X_Air_1");
        ui->comboBox_46->setCurrentText("Air");
        ui->comboBox_47->setCurrentText("Air");
        ui->checkBox->setChecked(true);
        break;
    case 27: // Pressure at min, N2 purge, floated PW (PD)
        ui->comboBox_2->setCurrentText("0");
        ui->comboBox_3->setCurrentText("0");
        ui->comboBox_6->setCurrentText("N2");
        ui->comboBox_7->setCurrentText("N2");
        ui->comboBox_16->setCurrentText("PP_min");
        ui->comboBox_42->setCurrentText("0");
        ui->comboBox_43->setCurrentText("0");
        ui->comboBox_46->setCurrentText("N2");
        ui->comboBox_47->setCurrentText("N2");
        ui->checkBox->setChecked(true);
        break;
    case 28: // Pressure at min, Custom purge, floated PW (PD)
        ui->comboBox_2->setCurrentText("pur_conc_1");
        ui->comboBox_3->setCurrentText("pur_conc_1");
        ui->comboBox_6->setCurrentText("custom");
        ui->comboBox_7->setCurrentText("custom");
        ui->comboBox_16->setCurrentText("PP_min");
        ui->comboBox_42->setCurrentText("pur_conc_1");
        ui->comboBox_43->setCurrentText("pur_conc_1");
        ui->comboBox_46->setCurrentText("custom");
        ui->comboBox_47->setCurrentText("custom");
        ui->checkBox->setChecked(true);
        break;
    default:
        setTemperatureDepConfig( option );
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Widget::setTemperatureDepConfig( int option )
{
    ui->comboBox_16->setCurrentText("PP_typ");

    switch( option )
    {
    case 29: // Temperature at max, Air purge, fixed PW (TD)
        ui->comboBox_2->setCurrentText("X_Air_1");
        ui->comboBox_3->setCurrentText("X_Air_1");
        ui->comboBox_6->setCurrentText("Air");
        ui->comboBox_7->setCurrentText("Air");
        ui->comboBox_12->setCurrentText("PT_max");
        ui->comboBox_42->setCurrentText("X_Air_1");
        ui->comboBox_43->setCurrentText("X_Air_1");
        ui->comboBox_46->setCurrentText("Air");
        ui->comboBox_47->setCurrentText("Air");
        ui->checkBox->setChecked(false);
        break;
    case 30: // Temperature at max, N2 purge, fixed PW (TD)
        ui->comboBox_2->setCurrentText("0");
        ui->comboBox_3->setCurrentText("0");
        ui->comboBox_6->setCurrentText("N2");
        ui->comboBox_7->setCurrentText("N2");
        ui->comboBox_12->setCurrentText("PT_max");
        ui->comboBox_42->setCurrentText("0");
        ui->comboBox_43->setCurrentText("0");
        ui->comboBox_46->setCurrentText("N2");
        ui->comboBox_47->setCurrentText("N2");
        ui->checkBox->setChecked(false);
        break;
    case 31: // Temperature at max, Custom purge, fixed PW (TD)
        ui->comboBox_2->setCurrentText("pur_conc_1");
        ui->comboBox_3->setCurrentText("pur_conc_1");
        ui->comboBox_6->setCurrentText("custom");
        ui->comboBox_7->setCurrentText("custom");
        ui->comboBox_12->setCurrentText("PT_max");
        ui->comboBox_42->setCurrentText("pur_conc_1");
        ui->comboBox_43->setCurrentText("pur_conc_1");
        ui->comboBox_46->setCurrentText("custom");
        ui->comboBox_47->setCurrentText("custom");
        ui->checkBox->setChecked(false);
        break;
    case 32: // Temperature at max, Air purge, floated PW (TD)
        ui->comboBox_2->setCurrentText("X_Air_1");
        ui->comboBox_3->setCurrentText("X_Air_1");
        ui->comboBox_6->setCurrentText("Air");
        ui->comboBox_7->setCurrentText("Air");
        ui->comboBox_12->setCurrentText("PT_max");
        ui->comboBox_42->setCurrentText("X_Air_1");
        ui->comboBox_43->setCurrentText("X_Air_1");
        ui->comboBox_46->setCurrentText("Air");
        ui->comboBox_47->setCurrentText("Air");
        ui->checkBox->setChecked(true);
        break;
    case 33: // Temperature at max, N2 purge, floated PW (TD)
        ui->comboBox_2->setCurrentText("0");
        ui->comboBox_3->setCurrentText("0");
        ui->comboBox_6->setCurrentText("N2");
        ui->comboBox_7->setCurrentText("N2");
        ui->comboBox_12->setCurrentText("PT_max");
        ui->comboBox_42->setCurrentText("0");
        ui->comboBox_43->setCurrentText("0");
        ui->comboBox_46->setCurrentText("N2");
        ui->comboBox_47->setCurrentText("N2");
        ui->checkBox->setChecked(true);
        break;
    case 34: // Temperature at max, Custom purge, floated PW (TD)
        ui->comboBox_2->setCurrentText("pur_conc_1");
        ui->comboBox_3->setCurrentText("pur_conc_1");
        ui->comboBox_6->setCurrentText("custom");
        ui->comboBox_7->setCurrentText("custom");
        ui->comboBox_12->setCurrentText("PT_max");
        ui->comboBox_42->setCurrentText("pur_conc_1");
        ui->comboBox_43->setCurrentText("pur_conc_1");
        ui->comboBox_46->setCurrentText("custom");
        ui->comboBox_47->setCurrentText("custom");
        ui->checkBox->setChecked(true);
        break;
    case 35: // Temperature at min, Air purge, fixed PW (TD)
        ui->comboBox_2->setCurrentText("X_Air_1");
        ui->comboBox_3->setCurrentText("X_Air_1");
        ui->comboBox_6->setCurrentText("Air");
        ui->comboBox_7->setCurrentText("Air");
        ui->comboBox_12->setCurrentText("PT_min");
        ui->comboBox_42->setCurrentText("X_Air_1");
        ui->comboBox_43->setCurrentText("X_Air_1");
        ui->comboBox_46->setCurrentText("Air");
        ui->comboBox_47->setCurrentText("Air");
        ui->checkBox->setChecked(false);
        break;
    case 36: // Temperature at min, N2 purge, fixed PW (TD)
        ui->comboBox_2->setCurrentText("0");
        ui->comboBox_3->setCurrentText("0");
        ui->comboBox_6->setCurrentText("N2");
        ui->comboBox_7->setCurrentText("N2");
        ui->comboBox_12->setCurrentText("PT_min");
        ui->comboBox_42->setCurrentText("0");
        ui->comboBox_43->setCurrentText("0");
        ui->comboBox_46->setCurrentText("N2");
        ui->comboBox_47->setCurrentText("N2");
        ui->checkBox->setChecked(false);
        break;
    case 37: // Temperature at min, Custom purge, fixed PW (TD)
        ui->comboBox_2->setCurrentText("pur_conc_1");
        ui->comboBox_3->setCurrentText("pur_conc_1");
        ui->comboBox_6->setCurrentText("custom");
        ui->comboBox_7->setCurrentText("custom");
        ui->comboBox_12->setCurrentText("PT_min");
        ui->comboBox_42->setCurrentText("pur_conc_1");
        ui->comboBox_43->setCurrentText("pur_conc_1");
        ui->comboBox_46->setCurrentText("custom");
        ui->comboBox_47->setCurrentText("custom");
        ui->checkBox->setChecked(false);
        break;
    case 38: // Temperature at min, Air purge, floated PW (TD)
        ui->comboBox_2->setCurrentText("X_Air_1");
        ui->comboBox_3->setCurrentText("X_Air_1");
        ui->comboBox_6->setCurrentText("Air");
        ui->comboBox_7->setCurrentText("Air");
        ui->comboBox_12->setCurrentText("PT_min");
        ui->comboBox_42->setCurrentText("X_Air_1");
        ui->comboBox_43->setCurrentText("X_Air_1");
        ui->comboBox_46->setCurrentText("Air");
        ui->comboBox_47->setCurrentText("Air");
        ui->checkBox->setChecked(true);
        break;
    case 39: // Temperature at min, N2 purge, floated PW (TD)
        ui->comboBox_2->setCurrentText("0");
        ui->comboBox_3->setCurrentText("0");
        ui->comboBox_6->setCurrentText("N2");
        ui->comboBox_7->setCurrentText("N2");
        ui->comboBox_12->setCurrentText("PT_min");
        ui->comboBox_42->setCurrentText("0");
        ui->comboBox_43->setCurrentText("0");
        ui->comboBox_46->setCurrentText("N2");
        ui->comboBox_47->setCurrentText("N2");
        ui->checkBox->setChecked(true);
        break;
    case 40: // Temperature at min, Custom purge, floated PW (TD)
        ui->comboBox_2->setCurrentText("pur_conc_1");
        ui->comboBox_3->setCurrentText("pur_conc_1");
        ui->comboBox_6->setCurrentText("custom");
        ui->comboBox_7->setCurrentText("custom");
        ui->comboBox_12->setCurrentText("PT_min");
        ui->comboBox_42->setCurrentText("pur_conc_1");
        ui->comboBox_43->setCurrentText("pur_conc_1");
        ui->comboBox_46->setCurrentText("custom");
        ui->comboBox_47->setCurrentText("custom");
        ui->checkBox->setChecked(true);
        break;
    default:
        setPrecisionLDLconfig(option);
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Widget::setPrecisionLDLconfig( int option )
{
    // Set Dust Loading to typical
    ui->radioButton_7->setChecked(true);

    // Noise ON
    ui->checkBox_1->setChecked(true);
    ui->checkBox_2->setChecked(true);
    ui->checkBox_3->setChecked(true);
    ui->checkBox_4->setChecked(true);
    ui->checkBox_6->setChecked(true);

    // Otherwise everything set to typical values
    ui->comboBox_12->setCurrentText("PT_typ");
    ui->comboBox_17->setCurrentText("L_Tx+L_Rx");
    ui->comboBox_57->setCurrentText("L_Tx+L_Rx");

    switch( option )
    {
    case 41: // Air purge, fixed PW (Precision & LDL)
        spcGen->setDoubledIteration(true);
        ui->comboBox_2->setCurrentText("X_Air_1");
        ui->comboBox_3->setCurrentText("X_Air_1");
        ui->comboBox_6->setCurrentText("Air");
        ui->comboBox_7->setCurrentText("Air");
        ui->comboBox_42->setCurrentText("X_Air_1");
        ui->comboBox_43->setCurrentText("X_Air_1");
        ui->comboBox_46->setCurrentText("Air");
        ui->comboBox_47->setCurrentText("Air");
        ui->checkBox->setChecked(false);
        break;
    case 42: // N2 purge, fixed PW (Precision & LDL)
        spcGen->setDoubledIteration(true);
        ui->comboBox_2->setCurrentText("0");
        ui->comboBox_3->setCurrentText("0");
        ui->comboBox_6->setCurrentText("N2");
        ui->comboBox_7->setCurrentText("N2");
        ui->comboBox_42->setCurrentText("0");
        ui->comboBox_43->setCurrentText("0");
        ui->comboBox_46->setCurrentText("N2");
        ui->comboBox_47->setCurrentText("N2");
        ui->checkBox->setChecked(false);
        break;
    case 43: // Custom purge, fixed PW (Precision & LDL)
        spcGen->setDoubledIteration(true);
        ui->comboBox_2->setCurrentText("pur_conc_1");
        ui->comboBox_3->setCurrentText("pur_conc_1");
        ui->comboBox_6->setCurrentText("custom");
        ui->comboBox_7->setCurrentText("custom");
        ui->comboBox_42->setCurrentText("pur_conc_1");
        ui->comboBox_43->setCurrentText("pur_conc_1");
        ui->comboBox_46->setCurrentText("custom");
        ui->comboBox_47->setCurrentText("custom");
        ui->checkBox->setChecked(false);
        break;
    case 44: // Air purge, floated PW (Precision & LDL)
        spcGen->setDoubledIteration(true);
        ui->comboBox_2->setCurrentText("X_Air_1");
        ui->comboBox_3->setCurrentText("X_Air_1");
        ui->comboBox_6->setCurrentText("Air");
        ui->comboBox_7->setCurrentText("Air");
        ui->comboBox_42->setCurrentText("X_Air_1");
        ui->comboBox_43->setCurrentText("X_Air_1");
        ui->comboBox_46->setCurrentText("Air");
        ui->comboBox_47->setCurrentText("Air");
        ui->checkBox->setChecked(true);
        break;
    case 45: // N2 purge, floated PW (Precision & LDL)
        spcGen->setDoubledIteration(true);
        ui->comboBox_2->setCurrentText("0");
        ui->comboBox_3->setCurrentText("0");
        ui->comboBox_6->setCurrentText("N2");
        ui->comboBox_7->setCurrentText("N2");
        ui->comboBox_42->setCurrentText("0");
        ui->comboBox_43->setCurrentText("0");
        ui->comboBox_46->setCurrentText("N2");
        ui->comboBox_47->setCurrentText("N2");
        ui->checkBox->setChecked(true);
        break;
    case 46: // Custom purge, floated PW (Precision & LDL)
        spcGen->setDoubledIteration(true);
        ui->comboBox_2->setCurrentText("pur_conc_1");
        ui->comboBox_3->setCurrentText("pur_conc_1");
        ui->comboBox_6->setCurrentText("custom");
        ui->comboBox_7->setCurrentText("custom");
        ui->comboBox_42->setCurrentText("pur_conc_1");
        ui->comboBox_43->setCurrentText("pur_conc_1");
        ui->comboBox_46->setCurrentText("custom");
        ui->comboBox_47->setCurrentText("custom");
        ui->checkBox->setChecked(true);
        break;
    default:
        setRAMconfig(option);
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Widget::setRAMconfig( int option )
{
    // Noise OFF
    ui->checkBox_1->setChecked(false);
    ui->checkBox_2->setChecked(false);
    ui->checkBox_3->setChecked(false);
    ui->checkBox_4->setChecked(false);
    ui->checkBox_6->setChecked(false);

    // RAM noise ON
    ui->checkBox_5->setChecked(true);

    // Otherwise everything set to typical values

    switch( option )
    {
    case 47: // Air purge, fixed PW
        ui->comboBox_2->setCurrentText("X_Air_1");
        ui->comboBox_3->setCurrentText("X_Air_1");
        ui->comboBox_6->setCurrentText("Air");
        ui->comboBox_7->setCurrentText("Air");
        ui->comboBox_42->setCurrentText("X_Air_1");
        ui->comboBox_43->setCurrentText("X_Air_1");
        ui->comboBox_46->setCurrentText("Air");
        ui->comboBox_47->setCurrentText("Air");
        ui->checkBox->setChecked(false);
        break;
    case 48: // N2 purge, fixed PW
        ui->comboBox_2->setCurrentText("0");
        ui->comboBox_3->setCurrentText("0");
        ui->comboBox_6->setCurrentText("N2");
        ui->comboBox_7->setCurrentText("N2");
        ui->comboBox_42->setCurrentText("0");
        ui->comboBox_43->setCurrentText("0");
        ui->comboBox_46->setCurrentText("N2");
        ui->comboBox_47->setCurrentText("N2");
        ui->checkBox->setChecked(false);
        break;
    case 49: // Custom purge, fixed PW
        ui->comboBox_2->setCurrentText("pur_conc_1");
        ui->comboBox_3->setCurrentText("pur_conc_1");
        ui->comboBox_6->setCurrentText("custom");
        ui->comboBox_7->setCurrentText("custom");
        ui->comboBox_42->setCurrentText("pur_conc_1");
        ui->comboBox_43->setCurrentText("pur_conc_1");
        ui->comboBox_46->setCurrentText("custom");
        ui->comboBox_47->setCurrentText("custom");
        ui->checkBox->setChecked(false);
        break;
    case 50: // Air purge, floated PW
        ui->comboBox_2->setCurrentText("X_Air_1");
        ui->comboBox_3->setCurrentText("X_Air_1");
        ui->comboBox_6->setCurrentText("Air");
        ui->comboBox_7->setCurrentText("Air");
        ui->comboBox_42->setCurrentText("X_Air_1");
        ui->comboBox_43->setCurrentText("X_Air_1");
        ui->comboBox_46->setCurrentText("Air");
        ui->comboBox_47->setCurrentText("Air");
        ui->checkBox->setChecked(true);
        break;
    case 51: // N2 purge, floated PW
        ui->comboBox_2->setCurrentText("0");
        ui->comboBox_3->setCurrentText("0");
        ui->comboBox_6->setCurrentText("N2");
        ui->comboBox_7->setCurrentText("N2");
        ui->comboBox_42->setCurrentText("0");
        ui->comboBox_43->setCurrentText("0");
        ui->comboBox_46->setCurrentText("N2");
        ui->comboBox_47->setCurrentText("N2");
        ui->checkBox->setChecked(true);
        break;
    case 52: // Custom purge, floated PW
        ui->comboBox_2->setCurrentText("pur_conc_1");
        ui->comboBox_3->setCurrentText("pur_conc_1");
        ui->comboBox_6->setCurrentText("custom");
        ui->comboBox_7->setCurrentText("custom");
        ui->comboBox_42->setCurrentText("pur_conc_1");
        ui->comboBox_43->setCurrentText("pur_conc_1");
        ui->comboBox_46->setCurrentText("custom");
        ui->comboBox_47->setCurrentText("custom");
        ui->checkBox->setChecked(true);
        break;
    default:
        setVAconfig(option);
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Widget::setVAconfig( int option )
{
    // Set Dust Loading to max
    ui->radioButton_6->setChecked(true);

    // Noise ON
    ui->checkBox_1->setChecked(true);
    ui->checkBox_2->setChecked(true);
    ui->checkBox_3->setChecked(true);
    ui->checkBox_4->setChecked(true);
    ui->checkBox_6->setChecked(true);

    // RAM noise OFF
    ui->checkBox_5->setChecked(false);

    // Add Flange no. 3
    ui->comboBox_21->setCurrentText("X_IntCell_1");
    ui->comboBox_26->setCurrentText("conc_typ_1");

    // Change against settings from previous functions:
    ui->comboBox_48->setCurrentText("Air");
    ui->comboBox_49->setCurrentText("AT_max");
    ui->comboBox_52->setCurrentText("AT_typ");
    ui->comboBox_56->setCurrentText("P_atm");
    ui->comboBox_60->setCurrentText("L_int_cell_opt");

    // These should be defaults, but just in case:
    ui->comboBox_22->setCurrentText("Air");
    ui->comboBox_23->setCurrentText("AT_typ");
    ui->comboBox_24->setCurrentText("P_atm");
    ui->comboBox_25->setCurrentText("L_int_cell_opt");
    ui->comboBox_27->setCurrentText("Matrix");
    ui->comboBox_28->setCurrentText("PT_typ");
    ui->comboBox_29->setCurrentText("PP_typ");
    ui->comboBox_30->setCurrentText("L2-L_ins_tube");

    switch( option )
    {
    case 53: // Air purge
        ui->comboBox_2->setCurrentText("X_Air_1");
        ui->comboBox_3->setCurrentText("X_Air_1");
        ui->comboBox_6->setCurrentText("Air");
        ui->comboBox_7->setCurrentText("Air");
        ui->comboBox_42->setCurrentText("X_Air_1");
        ui->comboBox_43->setCurrentText("X_Air_1");
        ui->comboBox_46->setCurrentText("Air");
        ui->comboBox_47->setCurrentText("Air");
        ui->checkBox->setChecked(false);
        break;
    case 54: // N2 purge, fixed PW
        ui->comboBox_2->setCurrentText("0");
        ui->comboBox_3->setCurrentText("0");
        ui->comboBox_6->setCurrentText("N2");
        ui->comboBox_7->setCurrentText("N2");
        ui->comboBox_42->setCurrentText("0");
        ui->comboBox_43->setCurrentText("0");
        ui->comboBox_46->setCurrentText("N2");
        ui->comboBox_47->setCurrentText("N2");
        ui->checkBox->setChecked(false);
        break;
    case 55: // Custom purge, fixed PW
        ui->comboBox_2->setCurrentText("pur_conc_1");
        ui->comboBox_3->setCurrentText("pur_conc_1");
        ui->comboBox_6->setCurrentText("custom");
        ui->comboBox_7->setCurrentText("custom");
        ui->comboBox_42->setCurrentText("pur_conc_1");
        ui->comboBox_43->setCurrentText("pur_conc_1");
        ui->comboBox_46->setCurrentText("custom");
        ui->comboBox_47->setCurrentText("custom");
        ui->checkBox->setChecked(false);
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Widget::turnSimConfigOff()
{
    ui->radioButton->setChecked(false);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Widget::setPsensorAirFixedOutput( QString val )
{
    ui->label_152->setText(val);

    if( val == "YES" )
        ui->label_152->setStyleSheet("QLabel { color : green; }");
    else
        ui->label_152->setStyleSheet("QLabel { color : red; }");
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Widget::setPsensorN2FixedOutput( QString val )
{
    ui->label_153->setText(val);

    if( val == "YES" )
        ui->label_153->setStyleSheet("QLabel { color : green; }");
    else
        ui->label_153->setStyleSheet("QLabel { color : red; }");
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Widget::setPsensorCustFixedOutput( QString val )
{
    ui->label_154->setText(val);

    if( val == "YES" )
        ui->label_154->setStyleSheet("QLabel { color : green; }");
    else
        ui->label_154->setStyleSheet("QLabel { color : red; }");
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Widget::setPsensorAirFloatedOutput( QString val )
{
    ui->label_155->setText(val);

    if( val == "YES" )
        ui->label_155->setStyleSheet("QLabel { color : green; }");
    else
        ui->label_155->setStyleSheet("QLabel { color : red; }");
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Widget::setPsensorN2FloatedOutput( QString val )
{
    ui->label_156->setText(val);

    if( val == "YES" )
        ui->label_156->setStyleSheet("QLabel { color : green; }");
    else
        ui->label_156->setStyleSheet("QLabel { color : red; }");
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Widget::setPsensorCustFloatedOutput( QString val )
{
    ui->label_157->setText(val);

    if( val == "YES" )
        ui->label_157->setStyleSheet("QLabel { color : green; }");
    else
        ui->label_157->setStyleSheet("QLabel { color : red; }");
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Widget::setTsensorAirFixedOutput( QString val )
{
    ui->label_219->setText(val);

    if( val == "YES" )
        ui->label_219->setStyleSheet("QLabel { color : green; }");
    else
        ui->label_219->setStyleSheet("QLabel { color : red; }");
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Widget::setTsensorN2FixedOutput( QString val )
{
    ui->label_221->setText(val);

    if( val == "YES" )
        ui->label_221->setStyleSheet("QLabel { color : green; }");
    else
        ui->label_221->setStyleSheet("QLabel { color : red; }");
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Widget::setTsensorCustFixedOutput( QString val )
{
    ui->label_223->setText(val);

    if( val == "YES" )
        ui->label_223->setStyleSheet("QLabel { color : green; }");
    else
        ui->label_223->setStyleSheet("QLabel { color : red; }");
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Widget::setTsensorAirFloatedOutput( QString val )
{
    ui->label_225->setText(val);

    if( val == "YES" )
        ui->label_225->setStyleSheet("QLabel { color : green; }");
    else
        ui->label_225->setStyleSheet("QLabel { color : red; }");
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Widget::setTsensorN2FloatedOutput( QString val )
{
    ui->label_227->setText(val);

    if( val == "YES" )
        ui->label_227->setStyleSheet("QLabel { color : green; }");
    else
        ui->label_227->setStyleSheet("QLabel { color : red; }");
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Widget::setTsensorCustFloatedOutput( QString val )
{
    ui->label_229->setText(val);

    if( val == "YES" )
        ui->label_229->setStyleSheet("QLabel { color : green; }");
    else
        ui->label_229->setStyleSheet("QLabel { color : red; }");
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Widget::on_pushButton_8_clicked()
{
    int curInd = ui->comboBox->currentIndex();

    if( curInd > 1 )
    {
        ui->comboBox->setCurrentIndex(--curInd);
        ui->radioButton->toggle();

        ui->pushButton_4->click();
        ui->pushButton_3->click();
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Widget::on_pushButton_9_clicked()
{
    int curInd = ui->comboBox->currentIndex();

    if( curInd < (counter-1) )
    {
        ui->comboBox->setCurrentIndex(++curInd);
        ui->radioButton->toggle();

        ui->pushButton_4->click();
        ui->pushButton_3->click();
    }
}
