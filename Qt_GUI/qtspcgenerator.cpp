#include "qtspcgenerator.h"
#include "noisegenerator.h"
#include "gasconfigstructs.h"
#include <iostream>
#include <memory>

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void qtSpcGenerator::calculateInitialGuessFit()
{
    spcGenerator::calculateInitialGuessFit();
    emit calculatedInitialEV( O2initial );
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void qtSpcGenerator::fitToLessPeaksINI()
{
    spcGenerator::fitToLessPeaksINI();
    O2stdDevValues.push_back(calculateConcentrationStdDev(0,O2concentrationsVector.size()));
    emit calculatedFittedEV( O2fitted );
    emit calculatedFittedMV( calculateConcentrationMeanVal(0,O2concentrationsVector.size()) );
    emit calculatedFittedSD( calculateConcentrationStdDev(0,O2concentrationsVector.size()) );
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void qtSpcGenerator::initializeMultipleCalcs( multipleCalcsData&& mulCdata )
{
    this->mulCdata = mulCdata;
    reinitNeeded = true;
    if( mulCdata.vecVarIndex == 0 )
        numOfIterations = 1;
    else
        numOfIterations = this->mulCdata.vecVariable.size();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

bool qtSpcGenerator::multipleCalcsPresent()
{
    if( mulCdata.vecVariable.size() > 0 )
        return true;
    else
        return false;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void qtSpcGenerator::iterativeCalculations()
{
    while( iterNo < numOfIterations )
    {
        calculateInitialGuessFit();
        fitToLessPeaksINI();
        iterNo++;
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void qtSpcGenerator::clearVectors()
{
    spcGenerator::clearVectors();
    O2stdDevValues.clear();
    iterNo = 0;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void qtSpcGenerator::removeMatrixGas( QString name )
{
    auto nameIter = inputs.matxGases.begin();
    auto typIter  = inputs.matxTyps.begin();
    auto minIter  = inputs.matxMins.begin();
    auto maxIter  = inputs.matxMaxs.begin();

    while( nameIter != inputs.matxGases.end() )
    {
        if( name.toLocal8Bit().constData() == *nameIter )
        {
            inputs.matxGases.erase(nameIter);
            inputs.matxTyps.erase(typIter);
            inputs.matxMins.erase(minIter);
            inputs.matxMaxs.erase(maxIter);
            break;
        }

        nameIter++;
        typIter++;
        minIter++;
        maxIter++;
    }

    emit gasMatrixChanged();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

bool qtSpcGenerator::calculateSimOutputs( simCase curCase )
{
    double SNR = calculateSNR();
    bool value;
    int  retVal;
    QString textValue;

    if( SNR >= gasConfig.SNRllMin )
    {
        value = true;
        textValue = "YES";
    }
    else
    {
        value = false;
        textValue = "NO";
    }

    if( simFitData.useSimFitStruct )
    {
        switch( curCase )
        {
        case ICI_1:
        case ICI_2:
        case ICI_3:
        case ICI_7:
        case ICI_8:
        case ICI_9:
            if( fabs(O2fitted - O2initial) > percent2ppm(gasConfig.threshICI * 1.0e-2 * inputs.range1) ) outputs.fixed_only = false;
        default:
            break;
        }

        switch( curCase )
        {
        case TxRx_LL:
            outputs.TxRx_LL = value;
            emit LLtxrxOutputCalculated(textValue);
            break;
        case Stack_LL:
            outputs.Stack_LL = value;
            emit LLstackOutputCalculated(textValue);
            break;
        case Flange_LL:
            outputs.Flange_LL = value;
            emit LLflangeOutputCalculated(textValue);
            break;
        case ExtCell_LL:
            outputs.ExtCell_LL = value;
            emit LLextCellOutputCalculated(textValue);
            break;
        case ICI_1:
            outputs.acc_matrix_max_air_fix = fabs(O2fitted - O2initial);
            emit ICI_1resultCalculated(QString::number(fabs(O2fitted - O2initial)));
            break;
        case ICI_2:
            outputs.acc_matrix_max_N2_fix = fabs(O2fitted - O2initial);
            emit ICI_2resultCalculated(QString::number(fabs(O2fitted - O2initial)));
            break;
        case ICI_3:
            outputs.acc_matrix_max_cust_fix = fabs(O2fitted - O2initial);
            emit ICI_3resultCalculated(QString::number(fabs(O2fitted - O2initial)));
            break;
        case ICI_4:
            outputs.acc_matrix_max_air_float = fabs(O2fitted - O2initial);
            emit ICI_4resultCalculated(QString::number(fabs(O2fitted - O2initial)));
            break;
        case ICI_5:
            outputs.acc_matrix_max_N2_float = fabs(O2fitted - O2initial);
            emit ICI_5resultCalculated(QString::number(fabs(O2fitted - O2initial)));
            break;
        case ICI_6:
            outputs.acc_matrix_max_cust_float = fabs(O2fitted - O2initial);
            emit ICI_6resultCalculated(QString::number(fabs(O2fitted - O2initial)));
            break;
        case ICI_7:
            outputs.acc_matrix_min_air_fix = fabs(O2fitted - O2initial);
            emit ICI_7resultCalculated(QString::number(fabs(O2fitted - O2initial)));
            break;
        case ICI_8:
            outputs.acc_matrix_min_N2_fix = fabs(O2fitted - O2initial);
            emit ICI_8resultCalculated(QString::number(fabs(O2fitted - O2initial)));
            break;
        case ICI_9:
            outputs.acc_matrix_min_cust_fix = fabs(O2fitted - O2initial);
            emit ICI_9resultCalculated(QString::number(fabs(O2fitted - O2initial)));
            break;
        case ICI_10:
            outputs.acc_matrix_min_air_float = fabs(O2fitted - O2initial);
            emit ICI_10resultCalculated(QString::number(fabs(O2fitted - O2initial)));
            break;
        case ICI_11:
            outputs.acc_matrix_min_N2_float = fabs(O2fitted - O2initial);
            emit ICI_11resultCalculated(QString::number(fabs(O2fitted - O2initial)));
            break;
        case ICI_12:
            outputs.acc_matrix_min_cust_float = fabs(O2fitted - O2initial);
            emit ICI_12resultCalculated(QString::number(fabs(O2fitted - O2initial)));
            break;
        case PD_1:
            outputs.acc_press_max_air_fix = fabs(O2fitted - O2initial);
            emit PD_1resultCalculated(QString::number(fabs(O2fitted - O2initial)));
            retVal = checkPsensor(outputs.acc_press_min_air_fix,outputs.acc_press_max_air_fix);
            if( retVal >= 0 ){ emit PsensorAirFixedChecked( bool2Qstring(retVal != 0) );
            outputs.P_sensor_air_fix = (retVal != 0); }
            break;
        case PD_2:
            outputs.acc_press_max_N2_fix = fabs(O2fitted - O2initial);
            emit PD_2resultCalculated(QString::number(fabs(O2fitted - O2initial)));
            retVal = checkPsensor(outputs.acc_press_min_N2_fix,outputs.acc_press_max_N2_fix);
            if( retVal >= 0 ){ emit PsensorN2FixedChecked( bool2Qstring(retVal != 0) );
            outputs.P_sensor_N2_fix = (retVal != 0); }
            break;
        case PD_3:
            outputs.acc_press_max_cust_fix = fabs(O2fitted - O2initial);
            emit PD_3resultCalculated(QString::number(fabs(O2fitted - O2initial)));
            retVal = checkPsensor(outputs.acc_press_min_cust_fix,outputs.acc_press_max_cust_fix);
            if( retVal >= 0 ) emit PsensorCustFixedChecked( bool2Qstring(retVal != 0) );
            if( retVal >= 0 ) outputs.P_sensor_cust_fix = (retVal != 0);
            break;
        case PD_4:
            outputs.acc_press_max_air_float = fabs(O2fitted - O2initial);
            emit PD_4resultCalculated(QString::number(fabs(O2fitted - O2initial)));
            retVal = checkPsensor(outputs.acc_press_min_air_float,outputs.acc_press_max_air_float);
            if( retVal >= 0 ){ emit PsensorAirFloatedChecked( bool2Qstring(retVal != 0) );
            outputs.P_sensor_air_float = (retVal != 0); }
            break;
        case PD_5:
            outputs.acc_press_max_N2_float = fabs(O2fitted - O2initial);
            emit PD_5resultCalculated(QString::number(fabs(O2fitted - O2initial)));
            retVal = checkPsensor(outputs.acc_press_min_N2_float,outputs.acc_press_max_N2_float);
            if( retVal >= 0 ){ emit PsensorN2FloatedChecked( bool2Qstring(retVal != 0) );
            outputs.P_sensor_N2_float = (retVal != 0); }
            break;
        case PD_6:
            outputs.acc_press_max_cust_float = fabs(O2fitted - O2initial);
            emit PD_6resultCalculated(QString::number(fabs(O2fitted - O2initial)));
            retVal = checkPsensor(outputs.acc_press_min_cust_float,outputs.acc_press_max_cust_float);
            if( retVal >= 0 ){ emit PsensorCustFloatedChecked( bool2Qstring(retVal != 0) );
            outputs.P_sensor_cust_float = (retVal != 0); }
            break;
        case PD_7:
            outputs.acc_press_min_air_fix = fabs(O2fitted - O2initial);
            emit PD_7resultCalculated(QString::number(fabs(O2fitted - O2initial)));
            retVal = checkPsensor(outputs.acc_press_min_air_fix,outputs.acc_press_max_air_fix);
            if( retVal >= 0 ){ emit PsensorAirFixedChecked( bool2Qstring(retVal != 0) );
            outputs.P_sensor_air_fix = (retVal != 0); }
            break;
        case PD_8:
            outputs.acc_press_min_N2_fix = fabs(O2fitted - O2initial);
            emit PD_8resultCalculated(QString::number(fabs(O2fitted - O2initial)));
            retVal = checkPsensor(outputs.acc_press_min_N2_fix,outputs.acc_press_max_N2_fix);
            if( retVal >= 0 ){ emit PsensorN2FixedChecked( bool2Qstring(retVal != 0) );
            outputs.P_sensor_N2_fix = (retVal != 0); }
            break;
        case PD_9:
            outputs.acc_press_min_cust_fix = fabs(O2fitted - O2initial);
            emit PD_9resultCalculated(QString::number(fabs(O2fitted - O2initial)));
            retVal = checkPsensor(outputs.acc_press_min_cust_fix,outputs.acc_press_max_cust_fix);
            if( retVal >= 0 ){ emit PsensorCustFixedChecked( bool2Qstring(retVal != 0) );
            outputs.P_sensor_cust_fix = (retVal != 0); }
            break;
        case PD_10:
            outputs.acc_press_min_air_float = fabs(O2fitted - O2initial);
            emit PD_10resultCalculated(QString::number(fabs(O2fitted - O2initial)));
            retVal = checkPsensor(outputs.acc_press_min_air_float,outputs.acc_press_max_air_float);
            if( retVal >= 0 ){ emit PsensorAirFloatedChecked( bool2Qstring(retVal != 0) );
            outputs.P_sensor_air_float = (retVal != 0); }
            break;
        case PD_11:
            outputs.acc_press_min_N2_float = fabs(O2fitted - O2initial);
            emit PD_11resultCalculated(QString::number(fabs(O2fitted - O2initial)));
            retVal = checkPsensor(outputs.acc_press_min_N2_float,outputs.acc_press_max_N2_float);
            if( retVal >= 0 ){ emit PsensorN2FloatedChecked( bool2Qstring(retVal != 0) );
            outputs.P_sensor_N2_float = (retVal != 0); }
            break;
        case PD_12:
            outputs.acc_press_min_cust_float = fabs(O2fitted - O2initial);
            emit PD_12resultCalculated(QString::number(fabs(O2fitted - O2initial)));
            retVal = checkPsensor(outputs.acc_press_min_cust_float,outputs.acc_press_max_cust_float);
            if( retVal >= 0 ){ emit PsensorCustFloatedChecked( bool2Qstring(retVal != 0) );
            outputs.P_sensor_cust_float = (retVal != 0); }
            break;
        case TD_1:
            outputs.acc_temp_max_air_fix = fabs(O2fitted - O2initial);
            emit TD_1resultCalculated(QString::number(fabs(O2fitted - O2initial)));
            retVal = checkTsensor(outputs.acc_temp_min_air_fix,outputs.acc_temp_max_air_fix);
            if( retVal >= 0 ){ emit TsensorAirFixedChecked( bool2Qstring(retVal != 0) );
            outputs.T_sensor_air_fix = (retVal != 0); }
            break;
        case TD_2:
            outputs.acc_temp_max_N2_fix = fabs(O2fitted - O2initial);
            emit TD_2resultCalculated(QString::number(fabs(O2fitted - O2initial)));
            retVal = checkTsensor(outputs.acc_temp_min_N2_fix,outputs.acc_temp_max_N2_fix);
            if( retVal >= 0 ){ emit TsensorN2FixedChecked( bool2Qstring(retVal != 0) );
            outputs.T_sensor_N2_fix = (retVal != 0); }
            break;
        case TD_3:
            outputs.acc_temp_max_cust_fix = fabs(O2fitted - O2initial);
            emit TD_3resultCalculated(QString::number(fabs(O2fitted - O2initial)));
            retVal = checkTsensor(outputs.acc_temp_min_cust_fix,outputs.acc_temp_max_cust_fix);
            if( retVal >= 0 ){ emit TsensorCustFixedChecked( bool2Qstring(retVal != 0) );
            outputs.T_sensor_cust_fix = (retVal != 0); }
            break;
        case TD_4:
            outputs.acc_temp_max_air_float = fabs(O2fitted - O2initial);
            emit TD_4resultCalculated(QString::number(fabs(O2fitted - O2initial)));
            retVal = checkTsensor(outputs.acc_temp_min_air_float,outputs.acc_temp_max_air_float);
            if( retVal >= 0 ){ emit TsensorAirFloatedChecked( bool2Qstring(retVal != 0) );
            outputs.T_sensor_air_float = (retVal != 0); }
            break;
        case TD_5:
            outputs.acc_temp_max_N2_float = fabs(O2fitted - O2initial);
            emit TD_5resultCalculated(QString::number(fabs(O2fitted - O2initial)));
            retVal = checkTsensor(outputs.acc_temp_min_N2_float,outputs.acc_temp_max_N2_float);
            if( retVal >= 0 ){ emit TsensorN2FloatedChecked( bool2Qstring(retVal != 0) );
            outputs.T_sensor_N2_float = (retVal != 0); }
            break;
        case TD_6:
            outputs.acc_temp_max_cust_float = fabs(O2fitted - O2initial);
            emit TD_6resultCalculated(QString::number(fabs(O2fitted - O2initial)));
            retVal = checkTsensor(outputs.acc_temp_min_cust_float,outputs.acc_temp_max_cust_float);
            if( retVal >= 0 ){ emit TsensorCustFloatedChecked( bool2Qstring(retVal != 0) );
            outputs.T_sensor_cust_float = (retVal != 0); }
            break;
        case TD_7:
            outputs.acc_temp_min_air_fix = fabs(O2fitted - O2initial);
            emit TD_7resultCalculated(QString::number(fabs(O2fitted - O2initial)));
            retVal = checkTsensor(outputs.acc_temp_min_air_fix,outputs.acc_temp_max_air_fix);
            if( retVal >= 0 ){ emit TsensorAirFixedChecked( bool2Qstring(retVal != 0) );
            outputs.T_sensor_air_fix = (retVal != 0); }
            break;
        case TD_8:
            outputs.acc_temp_min_N2_fix = fabs(O2fitted - O2initial);
            emit TD_8resultCalculated(QString::number(fabs(O2fitted - O2initial)));
            retVal = checkTsensor(outputs.acc_temp_min_N2_fix,outputs.acc_temp_max_N2_fix);
            if( retVal >= 0 ){ emit TsensorN2FixedChecked( bool2Qstring(retVal != 0) );
            outputs.T_sensor_N2_fix = (retVal != 0); }
            break;
        case TD_9:
            outputs.acc_temp_min_cust_fix = fabs(O2fitted - O2initial);
            emit TD_9resultCalculated(QString::number(fabs(O2fitted - O2initial)));
            retVal = checkTsensor(outputs.acc_temp_min_cust_fix,outputs.acc_temp_max_cust_fix);
            if( retVal >= 0 ){ emit TsensorCustFixedChecked( bool2Qstring(retVal != 0) );
            outputs.T_sensor_cust_fix = (retVal != 0); }
            break;
        case TD_10:
            outputs.acc_temp_min_air_float = fabs(O2fitted - O2initial);
            emit TD_10resultCalculated(QString::number(fabs(O2fitted - O2initial)));
            retVal = checkTsensor(outputs.acc_temp_min_air_float,outputs.acc_temp_max_air_float);
            if( retVal >= 0 ){ emit TsensorAirFloatedChecked( bool2Qstring(retVal != 0) );
            outputs.T_sensor_air_float = (retVal != 0); }
            break;
        case TD_11:
            outputs.acc_temp_min_N2_float = fabs(O2fitted - O2initial);
            emit TD_11resultCalculated(QString::number(fabs(O2fitted - O2initial)));
            retVal = checkTsensor(outputs.acc_temp_min_N2_float,outputs.acc_temp_max_N2_float);
            if( retVal >= 0 ){ emit TsensorN2FloatedChecked( bool2Qstring(retVal != 0) );
            outputs.T_sensor_N2_float = (retVal != 0); }
            break;
        case TD_12:
            outputs.acc_temp_min_cust_float = fabs(O2fitted - O2initial);
            emit TD_12resultCalculated(QString::number(fabs(O2fitted - O2initial)));
            retVal = checkTsensor(outputs.acc_temp_min_cust_float,outputs.acc_temp_max_cust_float);
            if( retVal >= 0 ){ emit TsensorCustFloatedChecked( bool2Qstring(retVal != 0) );
            outputs.T_sensor_cust_float = (retVal != 0); }
            break;
        case PrecLDL_1:
            outputs.prec_air_fix = calculateConcentrationStdDev(O2concentrationsVector.size()/2,O2concentrationsVector.size());
            emit Prec1resultCalculated(QString::number(outputs.prec_air_fix) + " P");
            outputs.LDL_air_fix = 4 * calculateConcentrationStdDev(0,O2concentrationsVector.size()/2);
            emit LDL_1resultCalculated(QString::number(outputs.LDL_air_fix) + " LDL");
            break;
        case PrecLDL_2:
            outputs.prec_N2_fix = calculateConcentrationStdDev(O2concentrationsVector.size()/2,O2concentrationsVector.size());
            emit Prec2resultCalculated(QString::number(outputs.prec_N2_fix) + " P");
            outputs.LDL_N2_fix = 4 * calculateConcentrationStdDev(0,O2concentrationsVector.size()/2);
            emit LDL_2resultCalculated(QString::number(outputs.LDL_N2_fix) + " LDL");
            break;
        case PrecLDL_3:
            outputs.prec_cust_fix = calculateConcentrationStdDev(O2concentrationsVector.size()/2,O2concentrationsVector.size());
            emit Prec3resultCalculated(QString::number(outputs.prec_cust_fix) + " P");
            outputs.LDL_cust_fix = 4 * calculateConcentrationStdDev(0,O2concentrationsVector.size()/2);
            emit LDL_3resultCalculated(QString::number(outputs.LDL_cust_fix) + " LDL");
            break;
        case PrecLDL_4:
            outputs.prec_air_float = calculateConcentrationStdDev(O2concentrationsVector.size()/2,O2concentrationsVector.size());
            emit Prec4resultCalculated(QString::number(outputs.prec_air_float) + " P");
            outputs.LDL_air_float = 4 * calculateConcentrationStdDev(0,O2concentrationsVector.size()/2);
            emit LDL_4resultCalculated(QString::number(outputs.LDL_air_float) + " LDL");
            break;
        case PrecLDL_5:
            outputs.prec_N2_float = calculateConcentrationStdDev(O2concentrationsVector.size()/2,O2concentrationsVector.size());
            emit Prec5resultCalculated(QString::number(outputs.prec_N2_float) + " P");
            outputs.LDL_N2_float = 4 * calculateConcentrationStdDev(0,O2concentrationsVector.size()/2);
            emit LDL_5resultCalculated(QString::number(outputs.LDL_N2_float) + " LDL");
            break;
        case PrecLDL_6:
            outputs.prec_cust_float = calculateConcentrationStdDev(O2concentrationsVector.size()/2,O2concentrationsVector.size());
            emit Prec6resultCalculated(QString::number(outputs.prec_cust_float) + " P");
            outputs.LDL_cust_float = 4 * calculateConcentrationStdDev(0,O2concentrationsVector.size()/2);
            emit LDL_6resultCalculated(QString::number(outputs.LDL_cust_float) + " LDL");
            break;
        case RAM_1:
            outputs.RAM_air_fix = calculateConcentrationStdDev(0,O2concentrationsVector.size());
            emit RAM_1resultCalculated(QString::number(outputs.RAM_air_fix));
            break;
        case RAM_2:
            outputs.RAM_N2_fix = calculateConcentrationStdDev(0,O2concentrationsVector.size());
            emit RAM_2resultCalculated(QString::number(outputs.RAM_N2_fix));
            break;
        case RAM_3:
            outputs.RAM_cust_fix = calculateConcentrationStdDev(0,O2concentrationsVector.size());
            emit RAM_3resultCalculated(QString::number(outputs.RAM_cust_fix));
            break;
        case RAM_4:
            outputs.RAM_air_float = calculateConcentrationStdDev(0,O2concentrationsVector.size());
            emit RAM_4resultCalculated(QString::number(outputs.RAM_air_float));
            break;
        case RAM_5:
            outputs.RAM_N2_float = calculateConcentrationStdDev(0,O2concentrationsVector.size());
            emit RAM_5resultCalculated(QString::number(outputs.RAM_N2_float));
            break;
        case RAM_6:
            outputs.RAM_cust_float = calculateConcentrationStdDev(0,O2concentrationsVector.size());
            emit RAM_6resultCalculated(QString::number(outputs.RAM_cust_float));
            break;
        case VA_1:
            outputs.IntCell_SNR_air_fix = SNR;
            emit VA_1resultCalculated(outputs.IntCell_SNR_air_fix);
            break;
        case VA_2:
            outputs.IntCell_SNR_N2_fix = SNR;
            emit VA_2resultCalculated(outputs.IntCell_SNR_N2_fix);
            break;
        case VA_3:
            outputs.IntCell_SNR_cust_fix = SNR;
            emit VA_3resultCalculated(outputs.IntCell_SNR_cust_fix);
            break;
        default:
            break;
        }
    }

    return value;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void qtSpcGenerator::checkIfLLmethodSettable()
{
    if( outputs.TxRx_LL )
    {
        outputs.LL_method  = "Tx Rx";
        outputs.L_ext_cell = 0.0;
    }
    else if( outputs.Stack_LL )
    {
        outputs.LL_method  = "Stack";
        outputs.L_ext_cell = 0.0;
    }
    else if( outputs.Flange_LL )
    {
        outputs.LL_method  = "Flange with AP";
        outputs.L_ext_cell = 0.0;
    }
    else if( outputs.ExtCell_LL )
    {
        outputs.LL_method  = "External Cell";
        outputs.L_ext_cell = gasConfig.extCellLenOpt;
        emit LLextCellLenCalculated(QString::number(outputs.L_ext_cell));
    }
    else
        outputs.LL_method = "Try external cell with larger length.";

    emit LLmethodSet( QString::fromStdString(outputs.LL_method) );
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void qtSpcGenerator::launchGasConfigOption1( bool launched )
{
    if( launched )
    {
        std::unique_ptr<O2CONFIG_COMMON_STRUCT> ptr = std::make_unique<O2CONFIG_OPTION1_STRUCT>();
        gasConfig = ptr.get();
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void qtSpcGenerator::launchGasConfigOption2( bool launched )
{
    if( launched )
    {
        std::unique_ptr<O2CONFIG_COMMON_STRUCT> ptr = std::make_unique<O2CONFIG_OPTION2_STRUCT>();
        gasConfig = ptr.get();
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void qtSpcGenerator::launchGasConfigOption3( bool launched )
{
    if( launched )
    {
        std::unique_ptr<O2CONFIG_COMMON_STRUCT> ptr = std::make_unique<O2CONFIG_OPTION3_STRUCT>();
        gasConfig = ptr.get();
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int qtSpcGenerator::checkPsensor( double accLimitMin, double accLimitMax )
{
    if( accLimitMax > 0.0 && accLimitMin > 0.0 )
    {
        if( fabs((accLimitMax/1.0e4/inputs.range1/(1.0e3*(inputs.PPmax-inputs.PPtyp)))*1.0e2) > gasConfig.specPdep
            || fabs((accLimitMin/1.0e4/inputs.range1/(1.0e3*(inputs.PPmin-inputs.PPtyp)))*1.0e2) > gasConfig.specPdep )
            return 1;
        else
            return 0;
    }
    return -1;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int qtSpcGenerator::checkTsensor( double accLimitMin, double accLimitMax )
{
    if( accLimitMax > 0.0 && accLimitMin > 0.0 )
    {
        if( fabs((accLimitMax/1.0e4/inputs.range1/(inputs.PTmax-inputs.PTtyp))*1.0e2) > gasConfig.specTdep
            || fabs((accLimitMin/1.0e4/inputs.range1/(inputs.PTmin-inputs.PTtyp))*1.0e2) > gasConfig.specTdep )
            return 1;
        else
            return 0;
    }
    return -1;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

QString qtSpcGenerator::bool2Qstring( bool boolean )
{
    if( boolean )
        return "YES";
    else
        return "NO";
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

std::array<bool,2> qtSpcGenerator::calculateNonSimOutputs()
{
    std::array<bool,2> retArray;

    insTubeConditions();
    emit trOrigMinSet(QString::number(outputs.Tr_orig_min));
    emit trOrigTypSet(QString::number(outputs.Tr_orig_typ));
    emit trOrigMaxSet(QString::number(outputs.Tr_orig_max));
    emit insTubeLengthSet(QString::number(outputs.L_ins_tube));

    if( outputs.L_ins_tube > 0.0 )
    {
        emit trITminSet(QString::number(outputs.Tr_IT_min));
        emit trITtypSet(QString::number(outputs.Tr_IT_typ));
        emit trITmaxSet(QString::number(outputs.Tr_IT_max));
    }

    conditionsWithinBounds();
    emit limitsWarningSet(QString::fromStdString(outputs.out_of_limits));

    if( outputs.L_ins_tube > 0.0 )
    {
        emit trITminSet(QString::number(outputs.Tr_IT_min));
        emit trITtypSet(QString::number(outputs.Tr_IT_typ));
        emit trITmaxSet(QString::number(outputs.Tr_IT_max));
    }

    if( outputs.within_limits )
        emit withinLimitsSet("within limits");
    else
        emit withinLimitsSet("out of limits");

    collimationCalcs();
    QString result = QString::fromStdString(outputs.collimation);
    emit collimationTypeSet(result);

    IRfilterPresent();
    emit IRfilterPresenceSet(bool2Qstring(outputs.Filter));

    spectralScanRate();
    emit spectralScanRateSet(QString::number(outputs.Scan_Rate));

    retArray[0] = outputs.within_limits;
    retArray[1] = outputs.Filter;

    return retArray;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void qtSpcGenerator::initializePTfromINI()
{
    spcGenerator::initializePTfromINI();
    emit simIterationsNumChanged(stackPT.noiseIterationsNum);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void qtSpcGenerator::checkConditionsForNewExtCellLen()
{
    spcGenerator::insTubeConditions();
    emit insTubeLengthSet(QString::number(outputs.L_ins_tube));
    emit trOrigMinSet(QString::number(outputs.Tr_orig_min));
    emit trOrigTypSet(QString::number(outputs.Tr_orig_typ));
    emit trOrigMaxSet(QString::number(outputs.Tr_orig_max));

    if( outputs.L_ins_tube > 0.0 )
    {
        emit trITminSet(QString::number(outputs.Tr_IT_min));
        emit trITtypSet(QString::number(outputs.Tr_IT_typ));
        emit trITmaxSet(QString::number(outputs.Tr_IT_max));
    }

    conditionsWithinBounds();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void qtSpcGenerator::proceedWithENTUmethod()
{
    spcGenerator::proceedWithENTUmethod();

    emit insTubeTminSet(QString::number(outputs.T_ins_tube_min) + " min");
    emit insTubeTtypSet(QString::number(outputs.T_ins_tube_typ) + " typ");
    emit insTubeTmaxSet(QString::number(outputs.T_ins_tube_max) + " max");
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void qtSpcGenerator::insTubeConditions()
{
    if( insTubeCallCounter < 1 )
    {
        spcGenerator::insTubeConditions();
        ++insTubeCallCounter;
    }
}
