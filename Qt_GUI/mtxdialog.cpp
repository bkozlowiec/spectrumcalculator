#include "mtxdialog.h"
#include "ui_mtxdialog.h"
#include "widget.h"
#include "qtspcgenerator.h"
#include <QMessageBox>

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

MtxDialog::MtxDialog( QWidget* parent ) : QDialog(parent),
    ui(new Ui::MtxDialog)
{
    ui->setupUi(this);
    setWindowTitle("Matrix Gases Dialog");

    layout()->setSizeConstraint(QLayout::SetFixedSize);
    importGasList();

    ui->label_7->setStyleSheet("QLabel { color : red; }");

    connect(this, SIGNAL(accepted()), this, SLOT(deleteLater()));
    connect(this, SIGNAL(accepted()), this, SLOT(deleteLater()));
    connect(ui->pushButton_6, SIGNAL(clicked(bool)), this, SLOT(addGasComponent()));
    connect(ui->pushButton_7, SIGNAL(clicked(bool)), this, SLOT(removeGasComponent()));
    connect(ui->comboBox_2, SIGNAL(activated(QString)), this, SLOT(possiblyEnableGasEditing()));
    connect(ui->comboBox_2, SIGNAL(activated(QString)), this, SLOT(loadGasProperties(QString)));

    Widget* widgetPtr = dynamic_cast<Widget*>( parent );
    connect(this, SIGNAL(newGasNameSet(QString)), widgetPtr->getSpcGenerator(), SLOT(addMatrixGasName(QString)));
    connect(this, SIGNAL(newGasConcTypSet(double)), widgetPtr->getSpcGenerator(), SLOT(addMatrixGasConcTyp(double)));
    connect(this, SIGNAL(newGasConcMinSet(double)), widgetPtr->getSpcGenerator(), SLOT(addMatrixGasConcMin(double)));
    connect(this, SIGNAL(newGasConcMaxSet(double)), widgetPtr->getSpcGenerator(), SLOT(addMatrixGasConcMax(double)));
    connect(this, SIGNAL(oldGasSet(QString)), widgetPtr->getSpcGenerator(), SLOT(removeMatrixGas(QString)));
    connect(this, SIGNAL(oldGasSet(QString)), ui->lineEdit_2, SLOT(clear()));
    connect(widgetPtr->getSpcGenerator(), SIGNAL(gasMatrixChanged()), this, SLOT(importGasList()));
    connect(ui->pushButton_8, SIGNAL(clicked(bool)), this, SLOT(checkConcentationConsistency()));
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

MtxDialog::~MtxDialog()
{
    delete ui;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void MtxDialog::addGasComponent()
{
    ui->label_7->clear();
    ui->comboBox_2->clear();

    Widget* widgetPtr = dynamic_cast<Widget*>( parent() );

    if( !ui->lineEdit_2->text().isEmpty() && ui->doubleSpinBox_2->value() > 0.0 )
    {
        for(auto iter = widgetPtr->getSpcGenerator()->getInputsStruct().matxGases.begin();
            iter != widgetPtr->getSpcGenerator()->getInputsStruct().matxGases.end(); ++iter)
        {
            if( ui->lineEdit_2->text().toLocal8Bit().constData() == *iter )
            {
                ui->label_7->setText("Gas component with this name already set!");
                return;
            }
        }

        emit newGasNameSet(ui->lineEdit_2->text());
        emit newGasConcTypSet(ui->doubleSpinBox_2->value());
        emit newGasConcMinSet(ui->doubleSpinBox_3->value());
        emit newGasConcMaxSet(ui->doubleSpinBox_4->value());

        ui->lineEdit_2->setDisabled(true);
        ui->doubleSpinBox_2->setDisabled(true);
        ui->doubleSpinBox_3->setDisabled(true);
        ui->doubleSpinBox_4->setDisabled(true);
        ui->pushButton_6->setDisabled(true);
    }
    else
        ui->label_7->setText("Gas component not set!");
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void MtxDialog::removeGasComponent()
{
    ui->label_7->clear();
    ui->comboBox_2->clear();
    ui->doubleSpinBox_2->clear();
    ui->doubleSpinBox_3->clear();
    ui->doubleSpinBox_4->clear();

    emit oldGasSet(ui->lineEdit_2->text());
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void MtxDialog::possiblyEnableGasEditing()
{
    if( ui->comboBox_2->currentText() == "Add Gas..." )
    {
        ui->lineEdit_2->setEnabled(true);
        ui->doubleSpinBox_2->setEnabled(true);
        ui->doubleSpinBox_3->setEnabled(true);
        ui->doubleSpinBox_4->setEnabled(true);
        ui->pushButton_6->setEnabled(true);
        ui->pushButton_7->setEnabled(false);
    }
    else
    {
        ui->lineEdit_2->setDisabled(true);
        ui->doubleSpinBox_2->setDisabled(true);
        ui->doubleSpinBox_3->setDisabled(true);
        ui->doubleSpinBox_4->setDisabled(true);
        ui->pushButton_6->setDisabled(true);
        ui->pushButton_7->setDisabled(false);
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void MtxDialog::importGasList()
{
    Widget* widgetPtr = dynamic_cast<Widget*>( parent() );
    const std::vector<std::string>& gasNamesList = widgetPtr->getSpcGenerator()->getInputsStruct().matxGases;

    ui->comboBox_2->addItem( "Add Gas..." );

    for(auto iterator = gasNamesList.begin(); iterator != gasNamesList.end(); ++iterator)
        ui->comboBox_2->addItem( QString::fromStdString(*iterator) );

    possiblyEnableGasEditing();
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void MtxDialog::loadGasProperties( QString curName )
{
    Widget* widgetPtr = dynamic_cast<Widget*>( parent() );

    const std::vector<std::string>& gasNamesList = widgetPtr->getSpcGenerator()->getInputsStruct().matxGases;
    const std::vector<double>& gasConcTypList = widgetPtr->getSpcGenerator()->getInputsStruct().matxTyps;
    const std::vector<double>& gasConcMinList = widgetPtr->getSpcGenerator()->getInputsStruct().matxMins;
    const std::vector<double>& gasConcMaxList = widgetPtr->getSpcGenerator()->getInputsStruct().matxMaxs;

    auto itTyp = gasConcTypList.begin();
    auto itMin = gasConcMinList.begin();
    auto itMax = gasConcMaxList.begin();

    for(auto iterator = gasNamesList.begin(); iterator != gasNamesList.end(); ++itTyp, ++itMin, ++itMax, ++iterator)
    {
        if( QString::fromStdString(*iterator) == curName )
        {
            ui->lineEdit_2->setText(curName);
            ui->doubleSpinBox_2->setValue( ppm2percent(*itTyp) );
            ui->doubleSpinBox_3->setValue( ppm2percent(*itMin) );
            ui->doubleSpinBox_4->setValue( ppm2percent(*itMax) );
            break;
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void MtxDialog::checkConcentationConsistency()
{
    Widget* widgetPtr = dynamic_cast<Widget*>( parent() );

    const std::vector<double>& gasConcTypList = widgetPtr->getSpcGenerator()->getInputsStruct().matxTyps;

    double concSum = 0.0;
    for(auto iterator = gasConcTypList.begin(); iterator != gasConcTypList.end(); ++iterator)
        concSum += *iterator;

    if( fabs(concSum - 1.0e6) < 0.1 )
        this->accept();
    else
    {
        ui->label_7->setText("Sum of component concentrations are not equal 1!");

        int ret = QMessageBox::warning(this,tr("Matrix Dialog"),tr("Do you want to proceed with these concentrations?"),
                                       QMessageBox::Yes | QMessageBox::No);
        if( ret == QMessageBox::Yes )
            this->accept();
    }
}
