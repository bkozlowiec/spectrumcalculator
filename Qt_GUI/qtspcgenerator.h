#ifndef QTSPCGENERATOR_H
#define QTSPCGENERATOR_H

#include "spcgenerator.h"
#include "dialog.h"
#include <QObject>
#include <QThread>
#include <QMutex>

/* This class is a wrapper class for spcGenerator class, designed to enable interaction with UI designed in Qt 5 */

class qtSpcGenerator : public QObject, public spcGenerator
{
    Q_OBJECT

    std::vector<double> O2stdDevValues;
    static QString bool2Qstring( bool );

    unsigned insTubeCallCounter;

public:
    explicit qtSpcGenerator() { resetITCcounter(); }
    void calculateInitialGuessFit();
    void fitToLessPeaksINI();
    NoiseGenerator& getNoiseGenerator() { return noiseGenerator; }
    const std::vector<double>& getO2stdDevValues() const { return O2stdDevValues; }
    const INPUT_VARIABLES_STRUCT& getInputsStruct() const { return inputs; }
    const OUTPUT_VARIABLES_STRUCT& getOutputsStruct() const { return outputs; }
    const GASCONFIG_VARIABLES_STRUCT& getGasConfigStruct() const { return gasConfig; }
    const multipleCalcsData& getMultipleCalcsData() const { return mulCdata; }
    GASCONFIG_SIM_FIT_STRUCT& getGCsimFitStruct() { return simFitData; }
    void updateStackPTiters( unsigned newIt ) { stackPT.noiseIterationsNum = newIt; }
    void initializeMultipleCalcs( multipleCalcsData&& );
    void iterativeCalculations();
    void clearVectors();
    void initializePTfromINI();
    void reinitializeOutputs() { outputs = OUTPUT_VARIABLES_STRUCT(); }
    void proceedWithENTUmethod();
    void resetITCcounter() { insTubeCallCounter = 0; }
    void insTubeConditions();
    bool multipleCalcsPresent();
    bool calculateSimOutputs( simCase );
    std::array<bool,2> calculateNonSimOutputs();
    int  checkPsensor(double, double);
    int  checkTsensor(double, double);
    void changeInsTubePurgeGas( std::string gasName ) { insTubePurgeGas = gasName; }
    void changeInsTubeMaterial( std::string matName ) { insTubeMaterial = matName; }

public slots:
    void turnWhiteNoiseOnOff( int val ) { noiseGenerator.getWhiteNoiseStruct().turnedOn = (val != 0); }
    void turnPressureNoiseOnOff( int val ) { noiseGenerator.getPressureNoiseStruct().turnedOn = (val != 0); }
    void turnTemperatureNoiseOnOff( int val ) { noiseGenerator.getTemperatureNoiseStruct().turnedOn = (val != 0); }
    void turnFreqShiftNoiseOnOff( int val ) { noiseGenerator.getFreqShiftNoiseStruct().turnedOn = (val != 0); }
    void turnRAMnoiseOnOff( int val ) { noiseGenerator.getRAMnoiseStruct().turnedOn = (val != 0); }
    void turnTransmissionNoiseOnOff( int val ) { noiseGenerator.getTransmissionNoiseStruct().turnedOn = (val != 0); }
    void setWhiteNoiseSD( double val ) { noiseGenerator.getWhiteNoiseStruct().stdDevNormalized = val; }
    void setPressureNoiseSD( double val ) { noiseGenerator.getPressureNoiseStruct().stdDev = val; }
    void setTemperatureNoiseSD( double val ) { noiseGenerator.getTemperatureNoiseStruct().stdDev = val; }
    void setFreqShiftNoiseSD( double val ) { noiseGenerator.getFreqShiftNoiseStruct().stdDev = val; }
    void setRAMnoiseAmplitude( double val ) { noiseGenerator.getRAMnoiseStruct().A = val; }
    void setRAMnoiseFbase( double val ) { noiseGenerator.getRAMnoiseStruct().fBase = val; }
    void setRAMnoiseFepsilon( double val ) { noiseGenerator.getRAMnoiseStruct().fEpsilon = val; }
    void setRAMnoisePhiEpsilon( double val ) { noiseGenerator.getRAMnoiseStruct().phiEpsilon = val; }
    void setTransmissionSF( double val ) { noiseGenerator.getTransmissionNoiseStruct().scalingFactor = val; }
    void changeNoiseIterationsNumber( int val ) { stackPT.noiseIterationsNum = val; }
    
    void changeInputConcTyp1( double val ) { inputs.concTyp1 = percent2ppm(val); }
    void changeInputConcMin1( double val ) { inputs.concMin1 = percent2ppm(val); }
    void changeInputConcMax1( double val ) { inputs.concMax1 = percent2ppm(val); }
    void changeInputRange1( double val )   { inputs.range1 = val; }
    void changeInputConcTyp2( double val ) { inputs.concTyp2 = percent2ppm(val); }
    void changeInputConcMin2( double val ) { inputs.concMin2 = percent2ppm(val); }
    void changeInputConcMax2( double val ) { inputs.concMax2 = percent2ppm(val); }
    void changeInputRange2( double val )   { inputs.range2 = val; }
    void changeInputDataOutRate( double val ) { inputs.dataOutputRate = val; }
    void changeInputPTtyp( double val ) { inputs.PTtyp = val; }
    void changeInputPTmin( double val ) { inputs.PTmin = val; }
    void changeInputPTmax( double val ) { inputs.PTmax = val; }
    void changeInputPPtyp( double val ) { inputs.PPtyp = val; }
    void changeInputPPmin( double val ) { inputs.PPmin = val; }
    void changeInputPPmax( double val ) { inputs.PPmax = val; }
    void changeInputH2OconcTyp( double val ) { inputs.H2OconcTyp = val; }
    void changeInputH2OconcMin( double val ) { inputs.H2OconcMin = val; }
    void changeInputH2OconcMax( double val ) { inputs.H2OconcMax = val; }
    void changeInputDustConcTyp( double val ) { inputs.dustConcTyp = val; }
    void changeInputDustConcMin( double val ) { inputs.dustConcMin = val; }
    void changeInputDustConcMax( double val ) { inputs.dustConcMax = val; }
    void changeInputVelTyp( double val ) { inputs.velTyp = val; }
    void changeInputVelMin( double val ) { inputs.velMin = val; }
    void changeInputVelMax( double val ) { inputs.velMax = val; }
    void changeInputL1( double val ) { inputs.L1 = val; }
    void changeInputL2( double val ) { inputs.L2 = val; }
    void changeInputL3( double val ) { inputs.L3 = val; }
    void changeInputAirPurge( int val )    { inputs.airPurge = (val != 0); }
    void changeInputNitroPurge( int val )  { inputs.nitPurge = (val != 0); }
    void changeInputCustomPurge( int val ) { inputs.custPurge = (val != 0); }
    void changeInputPurConc1( double val ) { inputs.purConc1 = val; }
    void changeInputPurConc2( double val ) { inputs.purConc2 = val; }
    void changeInputPurMatxConc1( double val ) { inputs.purMatxConc1 = val; }
    void changeInputPurMatxConc2( double val ) { inputs.purMatxConc2 = val; }
    void changeInputATtyp( double val ) { inputs.ATtyp = val; }
    void changeInputATmin( double val ) { inputs.ATmin = val; }
    void changeInputATmax( double val ) { inputs.ATmax = val; }
    void changeInputExtCell( int val )   { inputs.extCell = (val != 0); }
    void changeInputIntCell( int val )   { inputs.intCell = (val != 0); }
    void changeInputIsoFlange( int val ) { inputs.isoFlange = (val != 0); }
    void changeInputInsTube( int val )   { inputs.insTube = (val != 0); }
    void changeInputGas1( QString name ) { inputs.gas1 = name.toLocal8Bit().constData(); }
    void changeInputGas2( QString name ) { inputs.gas2 = name.toLocal8Bit().constData(); }
    void changeInputPurgeGas1( QString name ) { inputs.purMatxGas1 = name.toLocal8Bit().constData(); }
    void changeInputPurgeGas2( QString name ) { inputs.purMatxGas2 = name.toLocal8Bit().constData(); }
    void changeGasConfICIthresh( double val ) { gasConfig.threshICI = val; }
    void addMatrixGasName( QString name ) { inputs.matxGases.push_back(name.toLocal8Bit().constData()); }
    void addMatrixGasConcTyp( double val ) { inputs.matxTyps.push_back(percent2ppm(val)); emit gasMatrixChanged(); }
    void addMatrixGasConcMin( double val ) { inputs.matxMins.push_back(percent2ppm(val)); }
    void addMatrixGasConcMax( double val ) { inputs.matxMaxs.push_back(percent2ppm(val)); }
    void setDustLoadingToMin( bool checked ) { if( checked ) noiseGenerator.getTransmissionNoiseStruct().setDustType(0); }
    void setDustLoadingToMax( bool checked ) { if( checked ) noiseGenerator.getTransmissionNoiseStruct().setDustType(1); }
    void setDustLoadingToTyp( bool checked ) { if( checked ) noiseGenerator.getTransmissionNoiseStruct().setDustType(2); }
    void setFloatedPWmode( bool checked ) { PWfloating = checked; }
    void checkConditionsForNewExtCellLen();
    void removeMatrixGas( QString );
    void checkIfLLmethodSettable();
    void launchGasConfigOption1( bool );
    void launchGasConfigOption2( bool );
    void launchGasConfigOption3( bool );

signals:
    void calculatedInitialEV(double);
    void calculatedFittedEV(double);
    void calculatedFittedMV(double);
    void calculatedFittedSD(double);
    void calculatedTransmissionSF(double);
    void gasMatrixChanged();
    void LLtxrxOutputCalculated(QString);
    void LLstackOutputCalculated(QString);
    void LLflangeOutputCalculated(QString);
    void LLextCellOutputCalculated(QString);
    void LLmethodSet(QString);
    void LLextCellLenCalculated(QString);

    void ICI_1resultCalculated(QString);
    void ICI_2resultCalculated(QString);
    void ICI_3resultCalculated(QString);
    void ICI_4resultCalculated(QString);
    void ICI_5resultCalculated(QString);
    void ICI_6resultCalculated(QString);
    void ICI_7resultCalculated(QString);
    void ICI_8resultCalculated(QString);
    void ICI_9resultCalculated(QString);
    void ICI_10resultCalculated(QString);
    void ICI_11resultCalculated(QString);
    void ICI_12resultCalculated(QString);

    void PD_1resultCalculated(QString);
    void PD_2resultCalculated(QString);
    void PD_3resultCalculated(QString);
    void PD_4resultCalculated(QString);
    void PD_5resultCalculated(QString);
    void PD_6resultCalculated(QString);
    void PD_7resultCalculated(QString);
    void PD_8resultCalculated(QString);
    void PD_9resultCalculated(QString);
    void PD_10resultCalculated(QString);
    void PD_11resultCalculated(QString);
    void PD_12resultCalculated(QString);
    void PsensorAirFixedChecked(QString);
    void PsensorN2FixedChecked(QString);
    void PsensorCustFixedChecked(QString);
    void PsensorAirFloatedChecked(QString);
    void PsensorN2FloatedChecked(QString);
    void PsensorCustFloatedChecked(QString);

    void TD_1resultCalculated(QString);
    void TD_2resultCalculated(QString);
    void TD_3resultCalculated(QString);
    void TD_4resultCalculated(QString);
    void TD_5resultCalculated(QString);
    void TD_6resultCalculated(QString);
    void TD_7resultCalculated(QString);
    void TD_8resultCalculated(QString);
    void TD_9resultCalculated(QString);
    void TD_10resultCalculated(QString);
    void TD_11resultCalculated(QString);
    void TD_12resultCalculated(QString);
    void TsensorAirFixedChecked(QString);
    void TsensorN2FixedChecked(QString);
    void TsensorCustFixedChecked(QString);
    void TsensorAirFloatedChecked(QString);
    void TsensorN2FloatedChecked(QString);
    void TsensorCustFloatedChecked(QString);

    void Prec1resultCalculated(QString);
    void Prec2resultCalculated(QString);
    void Prec3resultCalculated(QString);
    void Prec4resultCalculated(QString);
    void Prec5resultCalculated(QString);
    void Prec6resultCalculated(QString);

    void LDL_1resultCalculated(QString);
    void LDL_2resultCalculated(QString);
    void LDL_3resultCalculated(QString);
    void LDL_4resultCalculated(QString);
    void LDL_5resultCalculated(QString);
    void LDL_6resultCalculated(QString);

    void RAM_1resultCalculated(QString);
    void RAM_2resultCalculated(QString);
    void RAM_3resultCalculated(QString);
    void RAM_4resultCalculated(QString);
    void RAM_5resultCalculated(QString);
    void RAM_6resultCalculated(QString);

    void VA_1resultCalculated(double);
    void VA_2resultCalculated(double);
    void VA_3resultCalculated(double);

    void withinLimitsSet(QString);
    void limitsWarningSet(QString);
    void collimationTypeSet(QString);
    void insTubeLengthSet(QString);
    void insTubeTminSet(QString);
    void insTubeTmaxSet(QString);
    void insTubeTtypSet(QString);
    void IRfilterPresenceSet(QString);
    void spectralScanRateSet(QString);

    void simIterationsNumChanged(int);

    void trOrigMinSet(QString);
    void trOrigTypSet(QString);
    void trOrigMaxSet(QString);
    void trITminSet(QString);
    void trITtypSet(QString);
    void trITmaxSet(QString);
};

/******************************* An additional class for Qt multithreading **************************************/

class qtGeneratorThread : public QThread
{
    Q_OBJECT

    qtSpcGenerator* spcPointer;
    QMutex mutex;

    void run() {
        QMutexLocker locker(&mutex);
        spcPointer->iterativeCalculations();
        emit resultReady();
    }

public:
    qtGeneratorThread( qtSpcGenerator* spcPtr ) : spcPointer(spcPtr) {}
    qtGeneratorThread( const qtGeneratorThread& ) = delete;
    qtGeneratorThread& operator=( const qtGeneratorThread& ) = delete;

signals:
    void resultReady();
};

#endif // QTSPCGENERATOR_H
