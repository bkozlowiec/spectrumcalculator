#ifndef DIALOG_H
#define DIALOG_H

#include "spcconfigurator.h"
#include <QDialog>

namespace Ui {
class Dialog;
}

//--------------------------------------------------------------------------------

class Dialog : public QDialog
{
    Q_OBJECT

    Ui::Dialog* ui;

    enum vectorVar { None, Concentration, PathLength, Pressure, Temperature };
    int currentVVindex;
    multipleCalcsData msgStruct;

public:
    explicit Dialog(QWidget* parent = 0);
    ~Dialog();

private slots:
    void setVectorVariable( int );
    void disableSpinBox( int );
    void fillMsgStruct();

signals:
    void vecVarChanged( int );
    void msgStructFilled( const multipleCalcsData& );
};

#endif // DIALOG_H
