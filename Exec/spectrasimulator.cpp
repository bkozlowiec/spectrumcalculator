#include <spcinterfaceimplementation.h>

#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <exception>

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// A small auxiliary POD struct to make information transfer clear without C-style arrays or containers
struct noiseConfigInput
{
    bool wnSetting, pnSetting, tnSetting, fsnSetting, rnSetting, trSetting;
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void readNoiseConfig( noiseConfigInput& );
void swanSong();
void plotDetailedOutput( spcInterface* );

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int main()
{
    std::set_terminate(swanSong);

    spcInterface* spcGen = spcImplementation::createInstance();
    noiseConfigInput noiseConfiguration;

    // Reading noise configuration from "noiseConfig.txt"
    readNoiseConfig(noiseConfiguration);

    // Setting up noise configuration
    spcGen->setWhiteNoise( noiseConfiguration.wnSetting );
    spcGen->setPressureNoise( noiseConfiguration.pnSetting );
    spcGen->setTemperatureNoise( noiseConfiguration.tnSetting );
    spcGen->setFreqShiftNoise( noiseConfiguration.fsnSetting );
    spcGen->setRAMnoise( noiseConfiguration.rnSetting );
    spcGen->setTransmissionNoise( noiseConfiguration.trSetting );

    // Setting up gas configuration
    spcGen->setGasConfigOption(O2_1);

    // The actual simulation phase
    spcGen->simulateSpectra();

    // Plot some basic output
    spcGen->basicOutput();

    // Plot detailed output
    plotDetailedOutput(spcGen);

    std::getchar();

    return 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void readNoiseConfig( noiseConfigInput& noiseConf )
{
    std::ifstream configTxt;
    configTxt.open("noiseConfig.txt", std::ios::in);

    if( !configTxt.is_open() )
    {
        std::cerr << "Error opening file. Check if noiseConfig.txt is present." << std::endl;
        throw 112;
    }

    std::string lineFromFile, key;

    int whiteNoiseOnOff, pressureNoiseOnOff, temperatureNoiseOnOff, freqShiftNoiseOnOff, RAMnoiseOnOff, transmissionNoiseOnOff;

    while(getline(configTxt, lineFromFile))
    {
        std::istringstream strstr(lineFromFile);
        key = "";
        strstr >> key;
        std::transform(key.begin(), key.end(), key.begin(), ::toupper);

        if( key == "WHITE_NOISE_PRESENT" )
            strstr >> whiteNoiseOnOff;
        else if( key == "PRESSURE_NOISE_PRESENT" )
            strstr >> pressureNoiseOnOff;
        else if( key == "TEMPERATURE_NOISE_PRESENT" )
            strstr >> temperatureNoiseOnOff;
        else if( key == "FREQUENCY_SHIFT_NOISE_PRESENT" )
            strstr >> freqShiftNoiseOnOff;
        else if( key == "RAM_NOISE_PRESENT" )
            strstr >> RAMnoiseOnOff;
        else if( key == "TRANSMISSION_NOISE_PRESENT" )
            strstr >> transmissionNoiseOnOff;
    }

    configTxt.close();

    noiseConf.wnSetting = whiteNoiseOnOff != 0;
    noiseConf.pnSetting = pressureNoiseOnOff != 0;
    noiseConf.tnSetting = temperatureNoiseOnOff != 0;
    noiseConf.fsnSetting = freqShiftNoiseOnOff != 0;
    noiseConf.rnSetting = RAMnoiseOnOff != 0;
    noiseConf.trSetting = transmissionNoiseOnOff != 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void swanSong()
{
    std::cerr << "Press any key..." << std::endl;
    std::getchar();
    exit(112);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void plotDetailedOutput( spcInterface* spcObj )
{
    std::cout << std::endl << "******************************** SIMULATION RESULTS - REPORT - START ********************************" << std::endl;

    std::cout << "CONDITIONS WITHIN LIMITS (bool): " << bool2string(spcObj->getOutputsStruct().within_limits) << std::endl
              << "ERROR MESSAGE: " << spcObj->getOutputsStruct().out_of_limits << std::endl << std::endl
              << "BEAM COLLIMATION: " << spcObj->getOutputsStruct().collimation << std::endl << std::endl
              << "INSERTION TUBE LENGTH: " << spcObj->getOutputsStruct().L_ins_tube << std::endl << std::endl
              << "INSERTION TUBE TEMPERATURES" << std::endl
              << "min: " << spcObj->getOutputsStruct().T_ins_tube_min << std::endl
              << "typ: " << spcObj->getOutputsStruct().T_ins_tube_typ << std::endl
              << "max: " << spcObj->getOutputsStruct().T_ins_tube_max << std::endl << std::endl
              << "HIGH TEMPERATURE FILTER REQUIRED: " << bool2string(spcObj->getOutputsStruct().Filter) << std::endl << std::endl
              << "SPECTRAL SCAN RATE: " << spcObj->getOutputsStruct().Scan_Rate << std::endl << std::endl
              << "SPACE FOR LINE LOCKING REQUIRED" << std::endl
              << "TX RX: " << bool2string(spcObj->getOutputsStruct().TxRx_LL) << std::endl
              << "STACK: " << bool2string(spcObj->getOutputsStruct().Stack_LL) << std::endl
              << "FLANGE: " << bool2string(spcObj->getOutputsStruct().Flange_LL) << std::endl
              << "EXTERNAL CELL: " << bool2string(spcObj->getOutputsStruct().ExtCell_LL) << std::endl
              << "RECOMMENDED OPTION FOR LINELOCKING: " << spcObj->getOutputsStruct().LL_method << std::endl << std::endl << std::endl
              << "PERFORMANCE ACCURACY" << std::endl << std::endl
              << "INFLUENCE OF INDIRECT CROSS INTERFERENCE" << std::endl
              << "Max PW, Air Purge, Fixed PW: " << spcObj->getOutputsStruct().acc_matrix_max_air_fix << std::endl
              << "Max PW, N2 Purge, Fixed PW: " << spcObj->getOutputsStruct().acc_matrix_max_N2_fix << std::endl
              << "Max PW, Custom Purge, Fixed PW: " << spcObj->getOutputsStruct().acc_matrix_max_cust_fix << std::endl
              << "Max PW, Air Purge, Floated PW: " << spcObj->getOutputsStruct().acc_matrix_max_air_float << std::endl
              << "Max PW, N2 Purge, Floated PW: " << spcObj->getOutputsStruct().acc_matrix_max_N2_float << std::endl
              << "Max PW, Custom Purge, Floated PW: " << spcObj->getOutputsStruct().acc_matrix_max_cust_float << std::endl
              << "Min PW, Air Purge, Fixed PW: " << spcObj->getOutputsStruct().acc_matrix_min_air_fix << std::endl
              << "Min PW, N2 Purge, Fixed PW: " << spcObj->getOutputsStruct().acc_matrix_min_N2_fix << std::endl
              << "Min PW, Custom Purge, Fixed PW: " << spcObj->getOutputsStruct().acc_matrix_min_cust_fix << std::endl
              << "Min PW, Air Purge, Floated PW: " << spcObj->getOutputsStruct().acc_matrix_min_air_float << std::endl
              << "Min PW, N2 Purge, Floated PW: " << spcObj->getOutputsStruct().acc_matrix_min_N2_float << std::endl
              << "Min PW, Custom Purge, Floated PW: " << spcObj->getOutputsStruct().acc_matrix_min_cust_float << std::endl << std::endl
              << "PRESSURE DEPENDENCE" << std::endl
              << "Pressure at max, Air Purge, Fixed PW: " << spcObj->getOutputsStruct().acc_press_max_air_fix << std::endl
              << "Pressure at max, N2 Purge, Fixed PW: " << spcObj->getOutputsStruct().acc_press_max_N2_fix << std::endl
              << "Pressure at max, Custom Purge, Fixed PW: " << spcObj->getOutputsStruct().acc_press_max_cust_fix << std::endl
              << "Pressure at max, Air Purge, Floated PW: " << spcObj->getOutputsStruct().acc_press_max_air_float << std::endl
              << "Pressure at max, N2 Purge, Floated PW: " << spcObj->getOutputsStruct().acc_press_max_N2_float << std::endl
              << "Pressure at max, Custom Purge, Floated PW: " << spcObj->getOutputsStruct().acc_press_max_cust_float << std::endl
              << "Pressure at min, Air Purge, Fixed PW: " << spcObj->getOutputsStruct().acc_press_min_air_fix << std::endl
              << "Pressure at min, N2 Purge, Fixed PW: " << spcObj->getOutputsStruct().acc_press_min_N2_fix << std::endl
              << "Pressure at min, Custom Purge, Fixed PW: " << spcObj->getOutputsStruct().acc_press_min_cust_fix << std::endl
              << "Pressure at min, Air Purge, Floated PW: " << spcObj->getOutputsStruct().acc_press_min_air_float << std::endl
              << "Pressure at min, N2 Purge, Floated PW: " << spcObj->getOutputsStruct().acc_press_min_N2_float << std::endl
              << "Pressure at min, Custom Purge, Floated PW: " << spcObj->getOutputsStruct().acc_press_min_cust_float << std::endl << std::endl
              << "PRESSURE SENSOR REQUIRED?" << std::endl
              << "Air Purge, Fixed PW: " << bool2string(spcObj->getOutputsStruct().P_sensor_air_fix) << std::endl
              << "N2 Purge, Fixed PW: " << bool2string(spcObj->getOutputsStruct().P_sensor_N2_fix) << std::endl
              << "Custom Purge, Fixed PW: " << bool2string(spcObj->getOutputsStruct().P_sensor_cust_fix) << std::endl
              << "Air Purge, Floated PW: " << bool2string(spcObj->getOutputsStruct().P_sensor_air_float) << std::endl
              << "N2 Purge, Floated PW: " << bool2string(spcObj->getOutputsStruct().P_sensor_N2_float) << std::endl
              << "Custom Purge, Floated PW: " << bool2string(spcObj->getOutputsStruct().P_sensor_cust_float) << std::endl << std::endl
              << "TEMPERATURE DEPENDENCE" << std::endl
              << "Temperature at max, Air Purge, Fixed PW: " << spcObj->getOutputsStruct().acc_temp_max_air_fix << std::endl
              << "Temperature at max, N2 Purge, Fixed PW: " << spcObj->getOutputsStruct().acc_temp_max_N2_fix << std::endl
              << "Temperature at max, Custom Purge, Fixed PW: " << spcObj->getOutputsStruct().acc_temp_max_cust_fix << std::endl
              << "Temperature at max, Air Purge, Floated PW: " << spcObj->getOutputsStruct().acc_temp_max_air_float << std::endl
              << "Temperature at max, N2 Purge, Floated PW: " << spcObj->getOutputsStruct().acc_temp_max_N2_float << std::endl
              << "Temperature at max, Custom Purge, Floated PW: " << spcObj->getOutputsStruct().acc_temp_max_cust_float << std::endl
              << "Temperature at min, Air Purge, Fixed PW: " << spcObj->getOutputsStruct().acc_temp_min_air_fix << std::endl
              << "Temperature at min, N2 Purge, Fixed PW: " << spcObj->getOutputsStruct().acc_temp_min_N2_fix << std::endl
              << "Temperature at min, Custom Purge, Fixed PW: " << spcObj->getOutputsStruct().acc_temp_min_cust_fix << std::endl
              << "Temperature at min, Air Purge, Floated PW: " << spcObj->getOutputsStruct().acc_temp_min_air_float << std::endl
              << "Temperature at min, N2 Purge, Floated PW: " << spcObj->getOutputsStruct().acc_temp_min_N2_float << std::endl
              << "Temperature at min, Custom Purge, Floated PW: " << spcObj->getOutputsStruct().acc_temp_min_cust_float << std::endl << std::endl
              << "TEMPERATURE SENSOR REQUIRED?" << std::endl
              << "Air Purge, Fixed PW: " << bool2string(spcObj->getOutputsStruct().T_sensor_air_fix) << std::endl
              << "N2 Purge, Fixed PW: " << bool2string(spcObj->getOutputsStruct().T_sensor_N2_fix) << std::endl
              << "Custom Purge, Fixed PW: " << bool2string(spcObj->getOutputsStruct().T_sensor_cust_fix) << std::endl
              << "Air Purge, Floated PW: " << bool2string(spcObj->getOutputsStruct().T_sensor_air_float) << std::endl
              << "N2 Purge, Floated PW: " << bool2string(spcObj->getOutputsStruct().T_sensor_N2_float) << std::endl
              << "Custom Purge, Floated PW: " << bool2string(spcObj->getOutputsStruct().T_sensor_cust_float) << std::endl << std::endl
              << "RAM ERROR" << std::endl
              << "Air Purge, Fixed PW: " << spcObj->getOutputsStruct().RAM_air_fix << std::endl
              << "N2 Purge, Fixed PW: " << spcObj->getOutputsStruct().RAM_N2_fix << std::endl
              << "Custom Purge, Fixed PW: " << spcObj->getOutputsStruct().RAM_cust_fix << std::endl
              << "Air Purge, Floated PW: " << spcObj->getOutputsStruct().RAM_air_float << std::endl
              << "N2 Purge, Floated PW: " << spcObj->getOutputsStruct().RAM_N2_float << std::endl
              << "Custom Purge, Floated PW: " << spcObj->getOutputsStruct().RAM_cust_float << std::endl << std::endl
              << "LOWEST DETECTION LIMIT" << std::endl
              << "Air Purge, Fixed PW: " << spcObj->getOutputsStruct().LDL_air_fix << std::endl
              << "N2 Purge, Fixed PW: " << spcObj->getOutputsStruct().LDL_N2_fix << std::endl
              << "Custom Purge, Fixed PW: " << spcObj->getOutputsStruct().LDL_cust_fix << std::endl
              << "Air Purge, Floated PW: " << spcObj->getOutputsStruct().LDL_air_float << std::endl
              << "N2 Purge, Floated PW: " << spcObj->getOutputsStruct().LDL_N2_float << std::endl
              << "Custom Purge, Floated PW: " << spcObj->getOutputsStruct().LDL_cust_float << std::endl << std::endl
              << "PRECISION" << std::endl
              << "Air Purge, Fixed PW: " << spcObj->getOutputsStruct().prec_air_fix << std::endl
              << "N2 Purge, Fixed PW: " << spcObj->getOutputsStruct().prec_N2_fix << std::endl
              << "Custom Purge, Fixed PW: " << spcObj->getOutputsStruct().prec_cust_fix << std::endl
              << "Air Purge, Floated PW: " << spcObj->getOutputsStruct().prec_air_float << std::endl
              << "N2 Purge, Floated PW: " << spcObj->getOutputsStruct().prec_N2_float << std::endl
              << "Custom Purge, Floated PW: " << spcObj->getOutputsStruct().prec_cust_float << std::endl << std::endl
              << "VALIDATION & ADJUSTMENT" << std::endl
              << "Air Purge (SNR): " << spcObj->getOutputsStruct().IntCell_SNR_air_fix << std::endl
              << "N2 Purge (SNR): " << spcObj->getOutputsStruct().IntCell_SNR_N2_fix << std::endl
              << "Custom Purge (SNR): " << spcObj->getOutputsStruct().IntCell_SNR_cust_fix << std::endl << std::endl
              << "TRANSMISSION" << std::endl
              << "Minimum Transmission (without insertion tube): " << spcObj->getOutputsStruct().Tr_orig_min << std::endl
              << "Typical Transmission (without insertion tube): " << spcObj->getOutputsStruct().Tr_orig_typ << std::endl
              << "Maximum Transmission (without insertion tube): " << spcObj->getOutputsStruct().Tr_orig_max << std::endl;

    if( spcObj->getOutputsStruct().L_ins_tube > 0.0 )
    {
        std::cout << "Minimum Transmission (with insertion tube): " << spcObj->getOutputsStruct().Tr_IT_min << std::endl
                  << "Typical Transmission (with insertion tube): " << spcObj->getOutputsStruct().Tr_IT_typ << std::endl
                  << "Maximum Transmission (with insertion tube): " << spcObj->getOutputsStruct().Tr_IT_max << std::endl << std::endl;
    }

    std::cout << std::endl << "******************************** SIMULATION RESULTS - REPORT - FINISH ********************************" << std::endl;
}
