#include "noisegenerator.h"
#include "spcgenerator.h"
#include "inputstructs.h"
#include "outputstructs.h"
#include "globals.h"
#include <algorithm>
#include <chrono>
#include <random>
#include <cstdio>

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void NoiseGenerator::WhiteNoise::createStdDevFromSig( const std::vector<double>& signal )
{
    double min = *std::min_element(signal.begin(), signal.end());
    double max = *std::max_element(signal.begin(), signal.end());

    if( min > 0.0 )
        stdDev = stdDevNormalized * ( max-min );
    else if( max < 0.0 )
        stdDev = stdDevNormalized * ( fabs(min)-fabs(max) );
    else
        stdDev = stdDevNormalized * ( fabs(max) + fabs(min) );
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void NoiseGenerator::WhiteNoise::createWhiteNoiseVector( const std::vector<double>& signal )
{
    double meanVal   = 0.0;
    double stdDevVal = stdDev;
    noiseVector.resize(signal.size());

    unsigned noiseSeed = std::chrono::system_clock::now().time_since_epoch().count();
    std::default_random_engine randGenerator = std::default_random_engine(noiseSeed);

    for(unsigned i=0; i<signal.size(); i++)
    {
        std::normal_distribution<double> distribution(meanVal,stdDevVal);
        double noiseVal  = distribution(randGenerator);
        //printf("noiseval[%i]: %f\n",i,noiseVal); fflush(stdout);
        //printf("noiseVec size: %i\n",noiseVector.size()); fflush(stdout);
        noiseVector[i] = noiseVal;
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void NoiseGenerator::computeWhiteNoise(const std::vector<double>& spectrum, const std::vector<double>& signal)
{
    if( whiteNoise.turnedOn )
    {
        whiteNoise.createStdDevFromSig(signal);
        whiteNoise.createWhiteNoiseVector(spectrum);
    }
    else
        whiteNoise.noiseVector = std::vector<double>(spectrum.size(),0.0);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void NoiseGenerator::computePressureNoise()
{
    if( pressureNoise.turnedOn )
    {
        double meanVal   = 0.0;
        double stdDevVal = bar2torr(pressureNoise.stdDev);

        unsigned noiseSeed = std::chrono::system_clock::now().time_since_epoch().count();
        std::default_random_engine randGenerator(noiseSeed);

        std::normal_distribution<double> distribution(meanVal,stdDevVal);
        pressureNoise.noiseValue = distribution(randGenerator);
    }
    else
        pressureNoise.noiseValue = 0.0;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void NoiseGenerator::computeTemperatureNoise()
{
    if( temperatureNoise.turnedOn )
    {
        double meanVal   = 0.0;
        double stdDevVal = temperatureNoise.stdDev;

        unsigned noiseSeed = std::chrono::system_clock::now().time_since_epoch().count();
        std::default_random_engine randGenerator(noiseSeed);

        std::normal_distribution<double> distribution(meanVal,stdDevVal);
        temperatureNoise.noiseValue = distribution(randGenerator);
    }
    else
        temperatureNoise.noiseValue = 0.0;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void NoiseGenerator::computeFreqShiftNoise()
{
    if( frequencyShiftNoise.turnedOn )
    {
        double meanVal   = 0.0;
        double stdDevVal = frequencyShiftNoise.stdDev;

        unsigned noiseSeed = std::chrono::system_clock::now().time_since_epoch().count();
        std::default_random_engine randGenerator(noiseSeed);

        std::normal_distribution<double> distribution(meanVal,stdDevVal);
        frequencyShiftNoise.noiseValue = distribution(randGenerator);
    }
    else
        frequencyShiftNoise.noiseValue = 0.0;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void NoiseGenerator::RAMnoise::createAlfaRAMvector( const std::vector<double>& frequency )
{
    unsigned noiseSeed = std::chrono::system_clock::now().time_since_epoch().count();
    std::default_random_engine randGenerator(noiseSeed);
    alfaRAM.resize(frequency.size());

    // random doubles from [0,1] range
    double double01no1 = randGenerator() / double(randGenerator.max());
    double double01no2 = randGenerator() / double(randGenerator.max());

    if( randGenerator() % 2 )
        f = fBase + fEpsilon * double01no1;
    else
        f = fBase - fEpsilon * double01no1;

    if( randGenerator() % 2 )
        phi = phiEpsilon * double01no2;
    else
        phi = -phiEpsilon * double01no2;

    for(unsigned i=0; i<frequency.size(); i++)
        alfaRAM[i] = A * sin( f * frequency[i] + phi );
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void NoiseGenerator::computeAlfaRAM( const std::vector<double>& frequency )
{
    if( ramNoise.turnedOn )
        ramNoise.createAlfaRAMvector(frequency);
    else
        ramNoise.alfaRAM = std::vector<double>(frequency.size(),0.0);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void NoiseGenerator::computeTransmissionNoise()
{
    if( !transmissionNoise.turnedOn )
        transmissionNoise.scalingFactor = 1.0;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void NoiseGenerator::TransmissionNoise::setDustType( unsigned whatDust )
{
    switch( whatDust )
    {
    case 0:
        dustType = min;
        break;
    case 1:
        dustType = max;
        break;
    case 2:
    default:
        dustType = typ;
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void NoiseGenerator::TransmissionNoise::calculateSF( const INPUT_VARIABLES_STRUCT& inpStr,
                                                     const OUTPUT_VARIABLES_STRUCT& outStr, bool initCalc )
{
    double Tr_d, Tr_L, L;

    if( initCalc )
        L        = inpStr.L2 / 1000.0;                                                     // [m]
    else
        L        = (inpStr.L2 - 2 * outStr.L_ins_tube + inpStr.L1 + inpStr.L3) / 1000.0;   // [m]
    double L_tot = (inpStr.L1 + inpStr.L2 + inpStr.L3 + outStr.L_ext_cell) / 1000.0;       // [m]
    double r     = 1.0e-5;                                                                 // [m]
    double Dlens = 50.0;                                                                   // [mm]
    double rho   = 2440.0 * 1.0e6;                                                         // [mg/m3]
    double phi;                                                                            // [mg/m3]

    switch( dustType )
    {
    case min:
        phi = inpStr.dustConcMin;
        printf("Min Dust\n"); fflush(stdout);
        break;
    case max:
        phi = inpStr.dustConcMax;
        printf("Max Dust\n"); fflush(stdout);
        break;
    case typ:
    default:
        phi = inpStr.dustConcTyp;
        printf("Typ Dust\n"); fflush(stdout);
    }

    Tr_d = exp(-(3 * phi * L)/(4 * r * rho));

    printf("phi = %f\n",phi); fflush(stdout);
    printf("L = %f\n",L); fflush(stdout);
    printf("r = %f\n",r); fflush(stdout);
    printf("rho = %f\n",rho); fflush(stdout);

    if( L_tot <= 6.0 )
        Tr_L = 1.0;
    else
        Tr_L = (Dlens / (13 + 10 * L_tot)) * (Dlens / (13 + 10 * L_tot));

    printf("Tr_d = %f, Tr_L = %f, Tr = %f\n",Tr_d,Tr_L,Tr_d * Tr_L); fflush(stdout);

    scalingFactor = std::min(Tr_d * Tr_L,1.0);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

bool NoiseGenerator::allNoiseSourcesOff()
{
    if( !whiteNoise.turnedOn )
        if( !pressureNoise.turnedOn )
            if( !temperatureNoise.turnedOn )
                if( !frequencyShiftNoise.turnedOn )
                    if( !ramNoise.turnedOn )
                        if( !transmissionNoise.turnedOn )
                            return true;
    return false;
}
