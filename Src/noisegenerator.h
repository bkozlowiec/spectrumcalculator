#ifndef NOISEGENERATOR_H
#define NOISEGENERATOR_H

#include <array>
#include <vector>
#include <cmath>

#ifndef M_PI
#define M_PI (3.14159265358979323846)
#endif

class INPUT_VARIABLES_STRUCT;
class OUTPUT_VARIABLES_STRUCT;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

struct SimpleNoise
{
    SimpleNoise(double sd) : stdDev(sd), turnedOn(true) {}
    /*const*/ double stdDev;
    double noiseValue;
    bool turnedOn;
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class NoiseGenerator
{
    struct WhiteNoise
    {
        /*const*/ double stdDevNormalized;
        double stdDev;
        std::vector<double> noiseVector;
        bool turnedOn;

        WhiteNoise(double sdn) : stdDevNormalized(sdn), turnedOn(true) {}
        void createStdDevFromSig( const std::vector<double>& );
        void createWhiteNoiseVector( const std::vector<double>& );
    };

    struct PressureNoise : public SimpleNoise
    {
        PressureNoise(double sd) : SimpleNoise(sd) {}
    };

    struct TemperatureNoise : public SimpleNoise
    {
        TemperatureNoise(double sd) : SimpleNoise(sd) {}
    };

    struct FrequencyShiftNoise : public SimpleNoise
    {
        FrequencyShiftNoise(double sd) : SimpleNoise(sd) {}
    };

    struct RAMnoise
    {
        /*const*/ double A, fBase, fEpsilon, phiEpsilon;
        double f, phi;
        std::vector<double> alfaRAM;
        bool turnedOn;

        RAMnoise(std::array<double,4> RAMcoeffs) : A(RAMcoeffs[0]), fBase(RAMcoeffs[1]),
            fEpsilon(RAMcoeffs[2]), phiEpsilon(RAMcoeffs[3]), turnedOn(true) {}
        void createAlfaRAMvector( const std::vector<double>& );
    };

    struct TransmissionNoise
    {
        double scalingFactor;
        bool   turnedOn;
        enum   dustTypes { min, max, typ } dustType;

        TransmissionNoise(double scale) : scalingFactor(scale), turnedOn(true), dustType(typ) {}
        void calculateSF( const INPUT_VARIABLES_STRUCT&, const OUTPUT_VARIABLES_STRUCT&, bool = false );
        void setDustType(unsigned);
    };

    WhiteNoise          whiteNoise;
    PressureNoise       pressureNoise;
    TemperatureNoise    temperatureNoise;
    FrequencyShiftNoise frequencyShiftNoise;
    RAMnoise            ramNoise;
    TransmissionNoise   transmissionNoise;

public:
    NoiseGenerator(double WNsdn = 1.41e-5, double Psd = 0.00199, double Tsd = 0.017, double FSsd = 0.012,
                   std::array<double,4> RAMcoeffs = { 6e-6, 0.2, 0.1, M_PI }, double Tsf = 1.0)
        : whiteNoise(WNsdn), pressureNoise(Psd), temperatureNoise(Tsd), frequencyShiftNoise(FSsd),
          ramNoise(RAMcoeffs), transmissionNoise(Tsf)
    {}

    void computeWhiteNoise( const std::vector<double>&, const std::vector<double>& );
    void computePressureNoise();
    void computeTemperatureNoise();
    void computeFreqShiftNoise();
    void computeAlfaRAM( const std::vector<double>& );
    void computeTransmissionNoise();
    bool allNoiseSourcesOff();
    const std::vector<double>& getWhiteNoise() const { return whiteNoise.noiseVector; }
    const double& getPressureNoise() const { return pressureNoise.noiseValue; }
    const double& getTemperatureNoise() const { return temperatureNoise.noiseValue; }
    const double& getFreqShiftNoise() const { return frequencyShiftNoise.noiseValue; }
    const std::vector<double>& getRAMnoise() const { return ramNoise.alfaRAM; }
    const double& getTransmissionNoise() const { return transmissionNoise.scalingFactor; }
    WhiteNoise& getWhiteNoiseStruct() { return whiteNoise; }
    SimpleNoise& getPressureNoiseStruct() { return pressureNoise; }
    SimpleNoise& getTemperatureNoiseStruct() { return temperatureNoise; }
    SimpleNoise& getFreqShiftNoiseStruct() { return frequencyShiftNoise; }
    RAMnoise& getRAMnoiseStruct() { return ramNoise; }
    TransmissionNoise& getTransmissionNoiseStruct() { return transmissionNoise; }
};

#endif // NOISEGENERATOR_H
