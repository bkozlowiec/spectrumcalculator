#ifndef SPCCONFIGURATOR_H
#define SPCCONFIGURATOR_H

#include "spcgenerator.h"

enum simCase { None, TxRx_LL, Stack_LL, Flange_LL, ExtCell_LL,
               ICI_1, ICI_2, ICI_3, ICI_4, ICI_5, ICI_6, ICI_7, ICI_8, ICI_9, ICI_10, ICI_11, ICI_12,
               PD_1, PD_2, PD_3, PD_4, PD_5, PD_6, PD_7, PD_8, PD_9, PD_10, PD_11, PD_12,
               TD_1, TD_2, TD_3, TD_4, TD_5, TD_6, TD_7, TD_8, TD_9, TD_10, TD_11, TD_12,
               PrecLDL_1, PrecLDL_2, PrecLDL_3, PrecLDL_4, PrecLDL_5, PrecLDL_6,
               RAM_1, RAM_2, RAM_3, RAM_4, RAM_5, RAM_6,
               VA_1, VA_2, VA_3,
               counter };

class spcConfigurator : private spcGenerator
{
    simCase currentCase;

    void setSimFitConfiguration();
    void setLineLockingConfig();
    void setIndirectCrossInterferenceConfig();
    void setPressureDependenceConfig();
    void setTemperatureDependenceConfig();
    void setPrecisionLDLconfig();
    void setRAMerrorConfig();
    void setValidationAdjustmentConfig();
    int checkPsensor( double, double );
    int checkTsensor( double, double );

protected:
    using spcGenerator::noiseGenerator;
    using spcGenerator::calculateInitialGuessFit;
    using spcGenerator::fitToLessPeaksINI;
    using spcGenerator::initializePTfromINI;
    using spcGenerator::O2initial;
    using spcGenerator::O2fitted;
    using spcGenerator::inputs;
    using spcGenerator::gasConfig;
    using spcGenerator::eNTUinputs;
    using spcGenerator::outputs;
    using spcGenerator::turnPWfloatingOn;

    spcConfigurator() : spcGenerator(), currentCase(None) {}
    void initializeSimCase( simCase curCase ) { currentCase = curCase; setSimFitConfiguration(); }
    void calculateSimOutputs();
    void checkIfLLmethodSettable();
    void calculateNonSimOutputs();
    void launchGasConfigOption1( bool );
    void launchGasConfigOption2( bool );
    void launchGasConfigOption3( bool );
};

// 1. check PW floating for flanges
// 2. make non-sim output calculations necessary

#endif // SPCCONFIGURATOR_H
