#include "spcconfigurator.h"
#include <memory>

void spcConfigurator::setSimFitConfiguration()
{
    if( currentCase == None )
        return;

    clearVectors();
    simFitData.clear();
    simFitData.useSimFitStruct = true;
    simFitData.nameX1 = inputs.gas1; // or insert from inputs?
    simFitData.nameX2 = inputs.gas2;

    switch( currentCase )
    {
    case TxRx_LL:
    case Stack_LL:
    case Flange_LL:
    case ExtCell_LL:
        setLineLockingConfig();
        return;
    case ICI_1:
        checkIfLLmethodSettable();
    case ICI_2:
    case ICI_3:
    case ICI_4:
    case ICI_5:
    case ICI_6:
    case ICI_7:
    case ICI_8:
    case ICI_9:
    case ICI_10:
    case ICI_11:
    case ICI_12:
        setIndirectCrossInterferenceConfig();
        break;
    case PD_1:
    case PD_2:
    case PD_3:
    case PD_4:
    case PD_5:
    case PD_6:
    case PD_7:
    case PD_8:
    case PD_9:
    case PD_10:
    case PD_11:
    case PD_12:
        setPressureDependenceConfig();
        break;
    case TD_1:
    case TD_2:
    case TD_3:
    case TD_4:
    case TD_5:
    case TD_6:
    case TD_7:
    case TD_8:
    case TD_9:
    case TD_10:
    case TD_11:
    case TD_12:
        setTemperatureDependenceConfig();
        break;
    case PrecLDL_1:
    case PrecLDL_2:
    case PrecLDL_3:
    case PrecLDL_4:
    case PrecLDL_5:
    case PrecLDL_6:
        setPrecisionLDLconfig();
        break;
    case RAM_1:
    case RAM_2:
    case RAM_3:
    case RAM_4:
    case RAM_5:
    case RAM_6:
        setRAMerrorConfig();
        break;
    case VA_1:
    case VA_2:
    case VA_3:
        setValidationAdjustmentConfig();
    }

    // For all non LL cases important info: simulated stack is not empty any more
    simFitData.simData.stack.isEmpty = false;

    // Reinitialization due to changed insTubeConditions
    //insTubeConditions();

    if( simFitData.simData.flanges.size() >= 2 && outputs.L_ins_tube > 0.0 )
    {
        simFitData.simData.flanges[2].isInsTube = true;
        proceedWithENTUmethod();
        simFitData.simData.stack.L  = inputs.L2 - outputs.L_ins_tube;
        simFitData.simData.flanges[2].T  = outputs.T_ins_tube_typ;
        simFitData.simData.flanges[2].P  = inputs.PPtyp;
        simFitData.simData.flanges[2].L  = outputs.L_ins_tube;
    }

    if( simFitData.fitData.flanges.size() >= 2 && outputs.L_ins_tube > 0.0 )
    {
        simFitData.fitData.stack.L  = inputs.L2 - outputs.L_ins_tube;
        simFitData.fitData.flanges[2].T  = outputs.T_ins_tube_typ;
        simFitData.fitData.flanges[2].P  = inputs.PPtyp;
        simFitData.fitData.flanges[2].L  = outputs.L_ins_tube;
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void spcConfigurator::setLineLockingConfig()
{
    doubledIteration = false;
    PWfloating = false;
    simFitData.stackFittingLater = true;

    noiseGenerator.getWhiteNoiseStruct().turnedOn = true;
    noiseGenerator.getPressureNoiseStruct().turnedOn = true;
    noiseGenerator.getTemperatureNoiseStruct().turnedOn = true;
    noiseGenerator.getFreqShiftNoiseStruct().turnedOn = true;
    noiseGenerator.getRAMnoiseStruct().turnedOn = false;
    noiseGenerator.getTransmissionNoiseStruct().turnedOn = true;

    recalcTransmissionSFaccToDust(1);

    simFitData.simData.stack.X2 = 0.0; // no second measurement component (for the time being)
    simFitData.fitData.stack.X2 = 0.0;

    switch( currentCase )
    {
    case TxRx_LL:
        simFitData.simData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
        simFitData.simData.flanges.back().X1    = gasConfig.conc1xAir;
        simFitData.simData.flanges.back().X2    = 0.0; // no second measurement component (for the time being)
        simFitData.simData.flanges.back().T     = inputs.ATmax;
        simFitData.simData.flanges.back().P     = gasConfig.PresAtm;
        simFitData.simData.flanges.back().L     = gasConfig.TxLen + gasConfig.RxLen;
        simFitData.simData.flanges.back().nameMatrX.push_back("N2");
        simFitData.simData.flanges.back().matrX = createMatrixConcentrations(false,true);

        simFitData.fitData.stack.T     = inputs.ATmax;
        simFitData.fitData.stack.P     = gasConfig.PresAtm;
        simFitData.fitData.stack.L     = gasConfig.TxLen + gasConfig.RxLen;
        simFitData.fitData.stack.nameMatrX.push_back("N2");
        simFitData.fitData.stack.matrX = createMatrixConcentrations(true,false);
        break;
    case Stack_LL:
        simFitData.simData.stack.X1      = inputs.concMin1;
        simFitData.simData.stack.T       = inputs.PTmax;
        simFitData.simData.stack.P       = inputs.PPmax;
        simFitData.simData.stack.L       = inputs.L2 - outputs.L_ins_tube; ////////// !! danger!
        simFitData.simData.stack.nameMatrX.push_back("N2");
        simFitData.simData.stack.nameMatrX.push_back("CO2");
        simFitData.simData.stack.matrX   = createMatrixConcentrations(true,true);
        simFitData.simData.stack.isEmpty = false;

        simFitData.fitData.stack.T     = inputs.PTmax;
        simFitData.fitData.stack.P     = inputs.PPmax;
        simFitData.fitData.stack.L     = inputs.L2 - outputs.L_ins_tube; ////////// !! danger!
        simFitData.fitData.stack.nameMatrX.push_back("N2");
        simFitData.fitData.stack.matrX = createMatrixConcentrations(true,false);
        break;
    case Flange_LL:
        simFitData.simData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
        simFitData.simData.flanges.back().X1    = gasConfig.conc1xAir;
        simFitData.simData.flanges.back().X2    = 0.0; // no second measurement component (for the time being)
        simFitData.simData.flanges.back().T     = inputs.ATmax;
        simFitData.simData.flanges.back().P     = inputs.PPmax;
        simFitData.simData.flanges.back().L     = inputs.L1 + inputs.L3;
        simFitData.simData.flanges.back().nameMatrX.push_back("N2");
        simFitData.simData.flanges.back().matrX = createMatrixConcentrations(false,true);

        simFitData.fitData.stack.T     = inputs.ATmax;
        simFitData.fitData.stack.P     = inputs.PPmax;
        simFitData.fitData.stack.L     = inputs.L1 + inputs.L3;
        simFitData.fitData.stack.nameMatrX.push_back("N2");
        simFitData.fitData.stack.matrX = createMatrixConcentrations(true,false);
        break;
    case ExtCell_LL:
        simFitData.simData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
        simFitData.simData.flanges.back().X1    = gasConfig.conc1xAir;
        simFitData.simData.flanges.back().X2    = 0.0; // no second measurement component (for the time being)
        simFitData.simData.flanges.back().T     = inputs.ATmax;
        simFitData.simData.flanges.back().P     = gasConfig.PresAtm;
        simFitData.simData.flanges.back().L     = gasConfig.extCellLenOpt;
        simFitData.simData.flanges.back().nameMatrX.push_back("N2");
        simFitData.simData.flanges.back().matrX = createMatrixConcentrations(false,true);

        simFitData.fitData.stack.T     = inputs.ATmax;
        simFitData.fitData.stack.P     = gasConfig.PresAtm;
        simFitData.fitData.stack.L     = gasConfig.extCellLenOpt;
        simFitData.fitData.stack.nameMatrX.push_back("N2");
        simFitData.fitData.stack.matrX = createMatrixConcentrations(true,false);
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void spcConfigurator::setIndirectCrossInterferenceConfig()
{
    // General warning: createMatrixConcentrations work only on the last chamber added! Don't add another before calling this function.

    doubledIteration = false;
    simFitData.stackFittingLater = true;

    noiseGenerator.getWhiteNoiseStruct().turnedOn = false;
    noiseGenerator.getPressureNoiseStruct().turnedOn = false;
    noiseGenerator.getTemperatureNoiseStruct().turnedOn = false;
    noiseGenerator.getFreqShiftNoiseStruct().turnedOn = false;
    noiseGenerator.getRAMnoiseStruct().turnedOn = false;
    noiseGenerator.getTransmissionNoiseStruct().turnedOn = false;

    simFitData.simData.stack.X2 = 0.0; // no second measurement component (for the time being)

    simFitData.simData.stack.X1 = inputs.concTyp1;
    simFitData.simData.stack.T  = inputs.PTtyp;
    simFitData.simData.stack.P  = inputs.PPtyp;
    simFitData.simData.stack.L  = inputs.L2 - outputs.L_ins_tube; ///// danger !!
    simFitData.simData.stack.nameMatrX.push_back("N2");
    simFitData.simData.stack.nameMatrX.push_back("CO2");

    // Flange 0 sim
    simFitData.simData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
    simFitData.simData.flanges.back().X1    = gasConfig.conc1xAir;
    simFitData.simData.flanges.back().X2    = 0.0; // no second measurement component (for the time being)
    simFitData.simData.flanges.back().T     = inputs.ATtyp;
    simFitData.simData.flanges.back().P     = gasConfig.PresAtm;
    simFitData.simData.flanges.back().L     = gasConfig.TxLen + gasConfig.RxLen + outputs.L_ext_cell; // this is fine, L_ext_cell should already be calculated
    simFitData.simData.flanges.back().nameMatrX.push_back("N2");
    simFitData.simData.flanges.back().matrX = createMatrixConcentrations(false,true);

    simFitData.fitData.stack.X2 = 0.0;

    simFitData.fitData.stack.T  = inputs.PTtyp;
    simFitData.fitData.stack.P  = inputs.PPtyp;
    simFitData.fitData.stack.L  = inputs.L2 - outputs.L_ins_tube; ///// danger !!
    simFitData.fitData.stack.nameMatrX.push_back("N2");
    simFitData.fitData.stack.nameMatrX.push_back("CO2");

    // Flange 0 fit
    simFitData.fitData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
    simFitData.fitData.flanges.back().X1 = gasConfig.conc1xAir;
    simFitData.fitData.flanges.back().X2    = 0.0; // no second measurement component (for the time being)
    simFitData.fitData.flanges.back().T  = inputs.ATtyp;
    simFitData.fitData.flanges.back().P  = gasConfig.PresAtm;
    simFitData.fitData.flanges.back().L  = gasConfig.TxLen + gasConfig.RxLen + outputs.L_ext_cell;
    simFitData.fitData.flanges.back().nameMatrX.push_back("N2");
    simFitData.fitData.flanges.back().matrX = createMatrixConcentrations(false,false);

    switch( currentCase )
    {
    case ICI_1:
    case ICI_4:
    case ICI_7:
    case ICI_10:
        // Flange 1 sim
        simFitData.simData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
        simFitData.simData.flanges[1].X1 = gasConfig.conc1xAir;
        simFitData.simData.flanges[1].X2 = 0.0; // no second measurement component (for the time being)
        simFitData.simData.flanges[1].nameMatrX.push_back("N2");
        simFitData.simData.flanges[1].matrX = createMatrixConcentrations(false,true);

        // Flange 1 fit
        simFitData.fitData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
        simFitData.fitData.flanges[1].X1 = gasConfig.conc1xAir;
        simFitData.fitData.flanges[1].X2 = 0.0; // no second measurement component (for the time being)
        simFitData.fitData.flanges[1].nameMatrX.push_back("N2");
        simFitData.fitData.flanges[1].matrX = createMatrixConcentrations(false,false);

        if( outputs.L_ins_tube > 0.0 )
        {
            // Flange 2 sim
            simFitData.simData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
            simFitData.simData.flanges[2].X1 = gasConfig.conc1xAir;
            simFitData.simData.flanges[2].X2 = 0.0; // no second measurement component (for the time being)
            simFitData.simData.flanges[2].nameMatrX.push_back("N2");
            simFitData.simData.flanges[2].matrX = createMatrixConcentrations(false,true);

            // Flange 2 fit
            simFitData.fitData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
            simFitData.fitData.flanges[2].X1 = gasConfig.conc1xAir;
            simFitData.fitData.flanges[2].X2 = 0.0; // no second measurement component (for the time being)
            simFitData.fitData.flanges[2].nameMatrX.push_back("N2");
            simFitData.fitData.flanges[2].matrX = createMatrixConcentrations(false,false);
        }

        insTubePurgeGas = "Air";
        break;
    case ICI_2:
    case ICI_5:
    case ICI_8:
    case ICI_11:
        // No chambers to be set
        //insTubePurgeGas = "N2";
        break;
    case ICI_3:
    case ICI_6:
    case ICI_9:
    case ICI_12:
        // Flange 1 sim
        simFitData.simData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
        simFitData.simData.flanges[1].X1 = inputs.purConc1;
        simFitData.simData.flanges[1].X2 = 0.0; // no second measurement component (for the time being)
        simFitData.simData.flanges[1].nameMatrX.push_back(inputs.purMatxGas1);
        simFitData.simData.flanges[1].nameMatrX.push_back(inputs.purMatxGas2);
        simFitData.simData.flanges[1].matrX = createMatrixConcentrations(false,true);

        // Flange 1 fit
        simFitData.fitData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
        simFitData.fitData.flanges[1].X1 = inputs.purConc1;
        simFitData.fitData.flanges[1].X2 = 0.0; // no second measurement component (for the time being)
        simFitData.fitData.flanges[1].nameMatrX.push_back(inputs.purMatxGas1);
        simFitData.fitData.flanges[1].nameMatrX.push_back(inputs.purMatxGas2);
        simFitData.fitData.flanges[1].matrX = createMatrixConcentrations(false,false);

        if( outputs.L_ins_tube )
        {
            // Flange 2 sim
            simFitData.simData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
            simFitData.simData.flanges[2].X1 = inputs.purConc1;
            simFitData.simData.flanges[2].X2 = 0.0; // no second measurement component (for the time being)
            simFitData.simData.flanges[2].nameMatrX.push_back(inputs.purMatxGas1);
            simFitData.simData.flanges[2].nameMatrX.push_back(inputs.purMatxGas2);
            simFitData.simData.flanges[2].matrX = createMatrixConcentrations(false,true);

            // Flange 2 fit
            simFitData.fitData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
            simFitData.fitData.flanges[2].X1 = inputs.purConc1;
            simFitData.fitData.flanges[2].X2 = 0.0; // no second measurement component (for the time being)
            simFitData.fitData.flanges[2].nameMatrX.push_back(inputs.purMatxGas1);
            simFitData.fitData.flanges[2].nameMatrX.push_back(inputs.purMatxGas2);
            simFitData.fitData.flanges[2].matrX = createMatrixConcentrations(false,false);
        }

        insTubePurgeGas = "custom";
    }

    switch( currentCase )
    {
    case ICI_1:
    case ICI_2:
    case ICI_3:
    case ICI_4:
    case ICI_5:
    case ICI_6:
        simFitData.simData.stack.mtxStyle = GASCONFIG_SIM_FIT_STRUCT::maxPW;
        simFitData.simData.stack.matrX = createMatrixConcentrations(true,true);

        simFitData.fitData.stack.mtxStyle = GASCONFIG_SIM_FIT_STRUCT::basic;
        break;
    case ICI_7:
    case ICI_8:
    case ICI_9:
    case ICI_10:
    case ICI_11:
    case ICI_12:
        simFitData.simData.stack.mtxStyle = GASCONFIG_SIM_FIT_STRUCT::minPW;
        simFitData.simData.stack.matrX = createMatrixConcentrations(true,true);

        simFitData.fitData.stack.mtxStyle = GASCONFIG_SIM_FIT_STRUCT::basic;
    }

    switch( currentCase )
    {
    case ICI_1:
    case ICI_2:
    case ICI_3:
    case ICI_7:
    case ICI_8:
    case ICI_9:
        PWfloating = false;
        break;
    case ICI_4:
    case ICI_5:
    case ICI_6:
    case ICI_10:
    case ICI_11:
    case ICI_12:
        PWfloating = true;
    }

    switch( currentCase )
    {
    case ICI_2:
    case ICI_5:
    case ICI_8:
    case ICI_11:
        return;
    }

    simFitData.simData.flanges[1].T  = inputs.ATtyp;
    simFitData.simData.flanges[1].P  = inputs.PPtyp;
    simFitData.simData.flanges[1].L  = inputs.L1 + inputs.L3;

    simFitData.fitData.flanges[1].T  = inputs.ATtyp;
    simFitData.fitData.flanges[1].P  = inputs.PPtyp;
    simFitData.fitData.flanges[1].L  = inputs.L1 + inputs.L3;

    /*if( outputs.L_ins_tube )
    {
        simFitData.simData.flanges[2].T  = outputs.T_ins_tube_typ; ///// danger !!
        simFitData.simData.flanges[2].P  = inputs.PPtyp;
        simFitData.simData.flanges[2].L  = outputs.L_ins_tube; ///// danger !!

        simFitData.fitData.flanges[2].T  = outputs.T_ins_tube_typ; ///// danger !!
        simFitData.fitData.flanges[2].P  = inputs.PPtyp;
        simFitData.fitData.flanges[2].L  = outputs.L_ins_tube; ///// danger !!
    }*/
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void spcConfigurator::setPressureDependenceConfig()
{
    // General warning: createMatrixConcentrations work only on the last chamber added! Don't add another before calling this function.

    doubledIteration = false;
    simFitData.stackFittingLater = true;

    noiseGenerator.getWhiteNoiseStruct().turnedOn = false;
    noiseGenerator.getPressureNoiseStruct().turnedOn = false;
    noiseGenerator.getTemperatureNoiseStruct().turnedOn = false;
    noiseGenerator.getFreqShiftNoiseStruct().turnedOn = false;
    noiseGenerator.getRAMnoiseStruct().turnedOn = false;
    noiseGenerator.getTransmissionNoiseStruct().turnedOn = false;

    simFitData.simData.stack.X2 = 0.0; // no second measurement component (for the time being)

    simFitData.simData.stack.X1 = inputs.concTyp1;
    simFitData.simData.stack.T  = inputs.PTtyp;
    simFitData.simData.stack.L  = inputs.L2 - outputs.L_ins_tube; ///// danger !!
    simFitData.simData.stack.nameMatrX.push_back("N2");
    simFitData.simData.stack.nameMatrX.push_back("CO2");
    simFitData.simData.stack.mtxStyle = GASCONFIG_SIM_FIT_STRUCT::basic;
    simFitData.simData.stack.matrX = createMatrixConcentrations(true,true);

    // Flange 0 sim
    simFitData.simData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
    simFitData.simData.flanges.back().X1 = gasConfig.conc1xAir;
    simFitData.simData.flanges.back().X2 = 0.0; // no second measurement component (for the time being)
    simFitData.simData.flanges.back().T  = inputs.ATtyp;
    simFitData.simData.flanges.back().P  = gasConfig.PresAtm;
    simFitData.simData.flanges.back().L  = gasConfig.TxLen + gasConfig.RxLen + outputs.L_ext_cell;
    simFitData.simData.flanges.back().nameMatrX.push_back("N2");
    simFitData.simData.flanges.back().matrX = createMatrixConcentrations(false,true);

    simFitData.fitData.stack.X2 = 0.0;

    simFitData.fitData.stack.T  = inputs.PTtyp;
    simFitData.fitData.stack.P  = inputs.PPtyp;
    simFitData.fitData.stack.L  = inputs.L2 - outputs.L_ins_tube; ///// danger !!
    simFitData.fitData.stack.nameMatrX.push_back("N2");
    simFitData.fitData.stack.nameMatrX.push_back("CO2");
    simFitData.fitData.stack.mtxStyle = GASCONFIG_SIM_FIT_STRUCT::basic;

    // Flange 0 fit
    simFitData.fitData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
    simFitData.fitData.flanges.back().X1 = gasConfig.conc1xAir;
    simFitData.fitData.flanges.back().X2 = 0.0; // no second measurement component (for the time being)
    simFitData.fitData.flanges.back().T  = inputs.ATtyp;
    simFitData.fitData.flanges.back().P  = gasConfig.PresAtm;
    simFitData.fitData.flanges.back().L  = gasConfig.TxLen + gasConfig.RxLen + outputs.L_ext_cell;
    simFitData.fitData.flanges.back().nameMatrX.push_back("N2");
    simFitData.fitData.flanges.back().matrX = createMatrixConcentrations(false,false);

    switch( currentCase )
    {
    case PD_1:
    case PD_4:
    case PD_7:
    case PD_10:
        // Flange 1 sim
        simFitData.simData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
        simFitData.simData.flanges[1].X1 = gasConfig.conc1xAir;
        simFitData.simData.flanges[1].X2 = 0.0; // no second measurement component (for the time being)
        simFitData.simData.flanges[1].nameMatrX.push_back("N2");
        simFitData.simData.flanges[1].matrX = createMatrixConcentrations(false,true);

        // Flange 1 fit
        simFitData.fitData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
        simFitData.fitData.flanges[1].X1 = gasConfig.conc1xAir;
        simFitData.fitData.flanges[1].X2 = 0.0; // no second measurement component (for the time being)
        simFitData.fitData.flanges[1].nameMatrX.push_back("N2");
        simFitData.fitData.flanges[1].matrX = createMatrixConcentrations(false,false);

        if( outputs.L_ins_tube > 0.0 )
        {
            // Flange 2 sim
            simFitData.simData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
            simFitData.simData.flanges[2].X1 = gasConfig.conc1xAir;
            simFitData.simData.flanges[2].X2 = 0.0; // no second measurement component (for the time being)
            simFitData.simData.flanges[2].nameMatrX.push_back("N2");
            simFitData.simData.flanges[2].matrX = createMatrixConcentrations(false,true);

            // Flange 2 fit
            simFitData.fitData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
            simFitData.fitData.flanges[2].X1 = gasConfig.conc1xAir;
            simFitData.fitData.flanges[2].X2 = 0.0; // no second measurement component (for the time being)
            simFitData.fitData.flanges[2].nameMatrX.push_back("N2");
            simFitData.fitData.flanges[2].matrX = createMatrixConcentrations(false,false);
        }

        insTubePurgeGas = "Air";
        break;
    case PD_2:
    case PD_5:
    case PD_8:
    case PD_11:
        // No flanges to be set
        //insTubePurgeGas = "N2";
        break;
    case PD_3:
    case PD_6:
    case PD_9:
    case PD_12:
        // Flange 1 sim
        simFitData.simData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
        simFitData.simData.flanges[1].X1 = inputs.purConc1;
        simFitData.simData.flanges[1].X2 = 0.0; // no second measurement component (for the time being)
        simFitData.simData.flanges[1].nameMatrX.push_back(inputs.purMatxGas1);
        simFitData.simData.flanges[1].nameMatrX.push_back(inputs.purMatxGas2);
        simFitData.simData.flanges[1].matrX = createMatrixConcentrations(false,true);

        // Flange 1 fit
        simFitData.fitData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
        simFitData.fitData.flanges[1].X1 = inputs.purConc1;
        simFitData.fitData.flanges[1].X2 = 0.0; // no second measurement component (for the time being)
        simFitData.fitData.flanges[1].nameMatrX.push_back(inputs.purMatxGas1);
        simFitData.fitData.flanges[1].nameMatrX.push_back(inputs.purMatxGas2);
        simFitData.fitData.flanges[1].matrX = createMatrixConcentrations(false,false);

        if( outputs.L_ins_tube > 0.0 )
        {
            // Flange 2 sim
            simFitData.simData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
            simFitData.simData.flanges[2].X1 = inputs.purConc1;
            simFitData.simData.flanges[2].X2 = 0.0; // no second measurement component (for the time being)
            simFitData.simData.flanges[2].nameMatrX.push_back(inputs.purMatxGas1);
            simFitData.simData.flanges[2].nameMatrX.push_back(inputs.purMatxGas2);
            simFitData.simData.flanges[2].matrX = createMatrixConcentrations(false,true);

            // Flange 2 fit
            simFitData.fitData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
            simFitData.fitData.flanges[2].X1 = inputs.purConc1;
            simFitData.fitData.flanges[2].X2 = 0.0; // no second measurement component (for the time being)
            simFitData.fitData.flanges[2].nameMatrX.push_back(inputs.purMatxGas1);
            simFitData.fitData.flanges[2].nameMatrX.push_back(inputs.purMatxGas2);
            simFitData.fitData.flanges[2].matrX = createMatrixConcentrations(false,false);
        }

        insTubePurgeGas = "custom";
    }

    switch( currentCase )
    {
    case PD_1:
    case PD_2:
    case PD_3:
    case PD_4:
    case PD_5:
    case PD_6:
        simFitData.simData.stack.P = inputs.PPmax;
        break;
    case PD_7:
    case PD_8:
    case PD_9:
    case PD_10:
    case PD_11:
    case PD_12:
        simFitData.simData.stack.P = inputs.PPmin;
    }

    switch( currentCase )
    {
    case PD_1:
    case PD_2:
    case PD_3:
    case PD_7:
    case PD_8:
    case PD_9:
        PWfloating = false;
        break;
    case PD_4:
    case PD_5:
    case PD_6:
    case PD_10:
    case PD_11:
    case PD_12:
        PWfloating = true;
    }

    switch( currentCase )
    {
    case PD_2:
    case PD_5:
    case PD_8:
    case PD_11:
        return;
    }

    simFitData.simData.flanges[1].T  = inputs.ATtyp;
    simFitData.simData.flanges[1].P  = inputs.PPtyp;
    simFitData.simData.flanges[1].L  = inputs.L1 + inputs.L3;

    simFitData.fitData.flanges[1].T  = inputs.ATtyp;
    simFitData.fitData.flanges[1].P  = inputs.PPtyp;
    simFitData.fitData.flanges[1].L  = inputs.L1 + inputs.L3;

    /*if( outputs.L_ins_tube > 0.0 )
    {
        simFitData.simData.flanges[2].T  = outputs.T_ins_tube_typ; ///// danger !!
        simFitData.simData.flanges[2].P  = inputs.PPtyp;
        simFitData.simData.flanges[2].L  = outputs.L_ins_tube; ///// danger !!

        simFitData.fitData.flanges[2].T  = outputs.T_ins_tube_typ; ///// danger !!
        simFitData.fitData.flanges[2].P  = inputs.PPtyp;
        simFitData.fitData.flanges[2].L  = outputs.L_ins_tube; ///// danger !!
    }*/
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void spcConfigurator::setTemperatureDependenceConfig()
{
    doubledIteration = false;
    simFitData.stackFittingLater = true;

    noiseGenerator.getWhiteNoiseStruct().turnedOn = false;
    noiseGenerator.getPressureNoiseStruct().turnedOn = false;
    noiseGenerator.getTemperatureNoiseStruct().turnedOn = false;
    noiseGenerator.getFreqShiftNoiseStruct().turnedOn = false;
    noiseGenerator.getRAMnoiseStruct().turnedOn = false;
    noiseGenerator.getTransmissionNoiseStruct().turnedOn = false;

    simFitData.simData.stack.X2 = 0.0; // no second measurement component (for the time being)

    simFitData.simData.stack.X1 = inputs.concTyp1;
    simFitData.simData.stack.P  = inputs.PPtyp;
    simFitData.simData.stack.L  = inputs.L2 - outputs.L_ins_tube; ///// danger !!
    simFitData.simData.stack.nameMatrX.push_back("N2");
    simFitData.simData.stack.nameMatrX.push_back("CO2");
    simFitData.simData.stack.mtxStyle = GASCONFIG_SIM_FIT_STRUCT::basic;
    simFitData.simData.stack.matrX = createMatrixConcentrations(true,true);

    // Flange 0 sim
    simFitData.simData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
    simFitData.simData.flanges.back().X1 = gasConfig.conc1xAir;
    simFitData.simData.flanges.back().X2 = 0.0; // no second measurement component (for the time being)
    simFitData.simData.flanges.back().T  = inputs.ATtyp;
    simFitData.simData.flanges.back().P  = gasConfig.PresAtm;
    simFitData.simData.flanges.back().L  = gasConfig.TxLen + gasConfig.RxLen + outputs.L_ext_cell;
    simFitData.simData.flanges.back().nameMatrX.push_back("N2");
    simFitData.simData.flanges.back().matrX = createMatrixConcentrations(false,true);

    simFitData.fitData.stack.X2 = 0.0;

    simFitData.fitData.stack.T  = inputs.PTtyp;
    simFitData.fitData.stack.P  = inputs.PPtyp;
    simFitData.fitData.stack.L  = inputs.L2 - outputs.L_ins_tube; ///// danger !!
    simFitData.fitData.stack.nameMatrX.push_back("N2");
    simFitData.fitData.stack.nameMatrX.push_back("CO2");
    simFitData.fitData.stack.mtxStyle = GASCONFIG_SIM_FIT_STRUCT::basic;

    // Flange 0 fit
    simFitData.fitData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
    simFitData.fitData.flanges.back().X1 = gasConfig.conc1xAir;
    simFitData.fitData.flanges.back().X2 = 0.0; // no second measurement component (for the time being)
    simFitData.fitData.flanges.back().T  = inputs.ATtyp;
    simFitData.fitData.flanges.back().P  = gasConfig.PresAtm;
    simFitData.fitData.flanges.back().L  = gasConfig.TxLen + gasConfig.RxLen + outputs.L_ext_cell;
    simFitData.fitData.flanges.back().nameMatrX.push_back("N2");
    simFitData.fitData.flanges.back().matrX = createMatrixConcentrations(false,false);

    switch( currentCase )
    {
    case TD_1:
    case TD_4:
    case TD_7:
    case TD_10:
        // Flange 1 sim
        simFitData.simData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
        simFitData.simData.flanges[1].X1 = gasConfig.conc1xAir;
        simFitData.simData.flanges[1].X2 = 0.0; // no second measurement component (for the time being)
        simFitData.simData.flanges[1].nameMatrX.push_back("N2");
        simFitData.simData.flanges[1].matrX = createMatrixConcentrations(false,true);

        // Flange 1 fit
        simFitData.fitData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
        simFitData.fitData.flanges[1].X1 = gasConfig.conc1xAir;
        simFitData.fitData.flanges[1].X2 = 0.0; // no second measurement component (for the time being)
        simFitData.fitData.flanges[1].nameMatrX.push_back("N2");
        simFitData.fitData.flanges[1].matrX = createMatrixConcentrations(false,false);

        if( outputs.L_ins_tube > 0.0 )
        {
            // Flange 2 sim
            simFitData.simData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
            simFitData.simData.flanges[2].X1 = gasConfig.conc1xAir;
            simFitData.simData.flanges[2].X2 = 0.0; // no second measurement component (for the time being)
            simFitData.simData.flanges[2].nameMatrX.push_back("N2");
            simFitData.simData.flanges[2].matrX = createMatrixConcentrations(false,true);

            // Flange 2 fit
            simFitData.fitData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
            simFitData.fitData.flanges[2].X1 = gasConfig.conc1xAir;
            simFitData.fitData.flanges[2].X2 = 0.0; // no second measurement component (for the time being)
            simFitData.fitData.flanges[2].nameMatrX.push_back("N2");
            simFitData.fitData.flanges[2].matrX = createMatrixConcentrations(false,false);
        }

        insTubePurgeGas = "Air";
        break;
    case TD_2:
    case TD_5:
    case TD_8:
    case TD_11:
        // No flanges to be set
        //insTubePurgeGas = "N2";
        break;
    case TD_3:
    case TD_6:
    case TD_9:
    case TD_12:
        // Flange 1 sim
        simFitData.simData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
        simFitData.simData.flanges[1].X1 = inputs.purConc1;
        simFitData.simData.flanges[1].X2 = 0.0; // no second measurement component (for the time being)
        simFitData.simData.flanges[1].nameMatrX.push_back(inputs.purMatxGas1);
        simFitData.simData.flanges[1].nameMatrX.push_back(inputs.purMatxGas2);
        simFitData.simData.flanges[1].matrX = createMatrixConcentrations(false,true);

        // Flange 1 fit
        simFitData.fitData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
        simFitData.fitData.flanges[1].X1 = inputs.purConc1;
        simFitData.fitData.flanges[1].X2 = 0.0; // no second measurement component (for the time being)
        simFitData.fitData.flanges[1].nameMatrX.push_back(inputs.purMatxGas1);
        simFitData.fitData.flanges[1].nameMatrX.push_back(inputs.purMatxGas2);
        simFitData.fitData.flanges[1].matrX = createMatrixConcentrations(false,false);

        if( outputs.L_ins_tube > 0.0 )
        {
            // Flange 2 sim
            simFitData.simData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
            simFitData.simData.flanges[2].X1 = inputs.purConc1;
            simFitData.simData.flanges[2].X2 = 0.0; // no second measurement component (for the time being)
            simFitData.simData.flanges[2].nameMatrX.push_back(inputs.purMatxGas1);
            simFitData.simData.flanges[2].nameMatrX.push_back(inputs.purMatxGas2);
            simFitData.simData.flanges[2].matrX = createMatrixConcentrations(false,true);

            // Flange 2 fit
            simFitData.fitData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
            simFitData.fitData.flanges[2].X1 = inputs.purConc1;
            simFitData.fitData.flanges[2].X2 = 0.0; // no second measurement component (for the time being)
            simFitData.fitData.flanges[2].nameMatrX.push_back(inputs.purMatxGas1);
            simFitData.fitData.flanges[2].nameMatrX.push_back(inputs.purMatxGas2);
            simFitData.fitData.flanges[2].matrX = createMatrixConcentrations(false,false);
        }

        insTubePurgeGas = "custom";
    }

    switch( currentCase )
    {
    case TD_1:
    case TD_2:
    case TD_3:
    case TD_4:
    case TD_5:
    case TD_6:
        simFitData.simData.stack.T = inputs.PTmax;
        break;
    case TD_7:
    case TD_8:
    case TD_9:
    case TD_10:
    case TD_11:
    case TD_12:
        simFitData.simData.stack.T = inputs.PTmin;
    }

    switch( currentCase )
    {
    case TD_1:
    case TD_2:
    case TD_3:
    case TD_7:
    case TD_8:
    case TD_9:
        PWfloating = false;
        break;
    case TD_4:
    case TD_5:
    case TD_6:
    case TD_10:
    case TD_11:
    case TD_12:
        PWfloating = true;
    }

    switch( currentCase )
    {
    case TD_2:
    case TD_5:
    case TD_8:
    case TD_11:
        return;
    }

    simFitData.simData.flanges[1].T  = inputs.ATtyp;
    simFitData.simData.flanges[1].P  = inputs.PPtyp;
    simFitData.simData.flanges[1].L  = inputs.L1 + inputs.L3;

    simFitData.fitData.flanges[1].T  = inputs.ATtyp;
    simFitData.fitData.flanges[1].P  = inputs.PPtyp;
    simFitData.fitData.flanges[1].L  = inputs.L1 + inputs.L3;

    /*if( outputs.L_ins_tube > 0.0 )
    {
        simFitData.simData.flanges[2].T  = outputs.T_ins_tube_typ; ///// danger !!
        simFitData.simData.flanges[2].P  = inputs.PPtyp;
        simFitData.simData.flanges[2].L  = outputs.L_ins_tube; ///// danger !!

        simFitData.fitData.flanges[2].T  = outputs.T_ins_tube_typ; ///// danger !!
        simFitData.fitData.flanges[2].P  = inputs.PPtyp;
        simFitData.fitData.flanges[2].L  = outputs.L_ins_tube; ///// danger !!
    }*/
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void spcConfigurator::setPrecisionLDLconfig()
{
    doubledIteration = true;
    simFitData.stackFittingLater = true;

    noiseGenerator.getWhiteNoiseStruct().turnedOn = true;
    noiseGenerator.getPressureNoiseStruct().turnedOn = true;
    noiseGenerator.getTemperatureNoiseStruct().turnedOn = true;
    noiseGenerator.getFreqShiftNoiseStruct().turnedOn = true;
    noiseGenerator.getRAMnoiseStruct().turnedOn = false;
    noiseGenerator.getTransmissionNoiseStruct().turnedOn = true;

    recalcTransmissionSFaccToDust(2);

    simFitData.simData.stack.X2 = 0.0; // no second measurement component (for the time being)

    simFitData.simData.stack.X1 = inputs.concTyp1;
    simFitData.simData.stack.P  = inputs.PPtyp;
    simFitData.simData.stack.T  = inputs.PTtyp;
    simFitData.simData.stack.L  = inputs.L2 - outputs.L_ins_tube; ///// danger !!
    simFitData.simData.stack.nameMatrX.push_back("N2");
    simFitData.simData.stack.nameMatrX.push_back("CO2");
    simFitData.simData.stack.mtxStyle = GASCONFIG_SIM_FIT_STRUCT::basic;
    simFitData.simData.stack.matrX = createMatrixConcentrations(true,true);

    // Flange 0 sim
    simFitData.simData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
    simFitData.simData.flanges.back().X1 = gasConfig.conc1xAir;
    simFitData.simData.flanges.back().X2 = 0.0; // no second measurement component (for the time being)
    simFitData.simData.flanges.back().T  = inputs.ATtyp;
    simFitData.simData.flanges.back().P  = gasConfig.PresAtm;
    simFitData.simData.flanges.back().L  = gasConfig.TxLen + gasConfig.RxLen;
    simFitData.simData.flanges.back().nameMatrX.push_back("N2");
    simFitData.simData.flanges.back().matrX = createMatrixConcentrations(false,true);

    simFitData.fitData.stack.X2 = 0.0;

    simFitData.fitData.stack.P  = inputs.PPtyp;
    simFitData.fitData.stack.T  = inputs.PTtyp;
    simFitData.fitData.stack.L  = inputs.L2 - outputs.L_ins_tube; ///// danger !!
    simFitData.fitData.stack.nameMatrX.push_back("N2");
    simFitData.fitData.stack.nameMatrX.push_back("CO2");
    simFitData.fitData.stack.mtxStyle = GASCONFIG_SIM_FIT_STRUCT::basic;

    // Flange 0 fit
    simFitData.fitData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
    simFitData.fitData.flanges.back().X1 = gasConfig.conc1xAir;
    simFitData.fitData.flanges.back().X2 = 0.0; // no second measurement component (for the time being)
    simFitData.fitData.flanges.back().T  = inputs.ATtyp;
    simFitData.fitData.flanges.back().P  = gasConfig.PresAtm;
    simFitData.fitData.flanges.back().L  = gasConfig.TxLen + gasConfig.RxLen;
    simFitData.fitData.flanges.back().nameMatrX.push_back("N2");
    simFitData.fitData.flanges.back().matrX = createMatrixConcentrations(false,false);

    switch( currentCase )
    {
    case PrecLDL_1:
    case PrecLDL_4:
        // Flange 1 sim
        simFitData.simData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
        simFitData.simData.flanges[1].X1 = gasConfig.conc1xAir;
        simFitData.simData.flanges[1].X2 = 0.0; // no second measurement component (for the time being)
        simFitData.simData.flanges[1].nameMatrX.push_back("N2");
        simFitData.simData.flanges[1].matrX = createMatrixConcentrations(false,true);

        // Flange 1 fit
        simFitData.fitData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
        simFitData.fitData.flanges[1].X1 = gasConfig.conc1xAir;
        simFitData.fitData.flanges[1].X2 = 0.0; // no second measurement component (for the time being)
        simFitData.fitData.flanges[1].nameMatrX.push_back("N2");
        simFitData.fitData.flanges[1].matrX = createMatrixConcentrations(false,false);

        if( outputs.L_ins_tube > 0.0 )
        {
            // Flange 2 sim
            simFitData.simData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
            simFitData.simData.flanges[2].X1 = gasConfig.conc1xAir;
            simFitData.simData.flanges[2].X2 = 0.0; // no second measurement component (for the time being)
            simFitData.simData.flanges[2].nameMatrX.push_back("N2");
            simFitData.simData.flanges[2].matrX = createMatrixConcentrations(false,true);

            // Flange 2 fit
            simFitData.fitData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
            simFitData.fitData.flanges[2].X1 = gasConfig.conc1xAir;
            simFitData.fitData.flanges[2].X2 = 0.0; // no second measurement component (for the time being)
            simFitData.fitData.flanges[2].nameMatrX.push_back("N2");
            simFitData.fitData.flanges[2].matrX = createMatrixConcentrations(false,false);
        }

        insTubePurgeGas = "Air";
        break;
    case PrecLDL_2:
    case PrecLDL_5:
        // No flanges to be set
        //insTubePurgeGas = "N2";
        break;
    case PrecLDL_3:
    case PrecLDL_6:
        // Flange 1 sim
        simFitData.simData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
        simFitData.simData.flanges[1].X1 = inputs.purConc1;
        simFitData.simData.flanges[1].X2 = 0.0; // no second measurement component (for the time being)
        simFitData.simData.flanges[1].nameMatrX.push_back(inputs.purMatxGas1);
        simFitData.simData.flanges[1].nameMatrX.push_back(inputs.purMatxGas2);
        simFitData.simData.flanges[1].matrX = createMatrixConcentrations(false,true);

        // Flange 1 fit
        simFitData.fitData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
        simFitData.fitData.flanges[1].X1 = inputs.purConc1;
        simFitData.fitData.flanges[1].X2 = 0.0; // no second measurement component (for the time being)
        simFitData.fitData.flanges[1].nameMatrX.push_back(inputs.purMatxGas1);
        simFitData.fitData.flanges[1].nameMatrX.push_back(inputs.purMatxGas2);
        simFitData.fitData.flanges[1].matrX = createMatrixConcentrations(false,false);

        if( outputs.L_ins_tube > 0.0 )
        {
            // Flange 2 sim
            simFitData.simData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
            simFitData.simData.flanges[2].X1 = inputs.purConc1;
            simFitData.simData.flanges[2].X2 = 0.0; // no second measurement component (for the time being)
            simFitData.simData.flanges[2].nameMatrX.push_back(inputs.purMatxGas1);
            simFitData.simData.flanges[2].nameMatrX.push_back(inputs.purMatxGas2);
            simFitData.simData.flanges[2].matrX = createMatrixConcentrations(false,true);

            // Flange 2 fit
            simFitData.fitData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
            simFitData.fitData.flanges[2].X1 = inputs.purConc1;
            simFitData.fitData.flanges[2].X2 = 0.0; // no second measurement component (for the time being)
            simFitData.fitData.flanges[2].nameMatrX.push_back(inputs.purMatxGas1);
            simFitData.fitData.flanges[2].nameMatrX.push_back(inputs.purMatxGas2);
            simFitData.fitData.flanges[2].matrX = createMatrixConcentrations(false,false);
        }

        insTubePurgeGas = "custom";
    }

    switch( currentCase )
    {
    case PrecLDL_1:
    case PrecLDL_2:
    case PrecLDL_3:
        PWfloating = false;
        break;
    case PrecLDL_4:
    case PrecLDL_5:
    case PrecLDL_6:
        PWfloating = true;
    }

    switch( currentCase )
    {
    case PrecLDL_2:
    case PrecLDL_5:
        return;
    }

    simFitData.simData.flanges[1].T  = inputs.ATtyp;
    simFitData.simData.flanges[1].P  = inputs.PPtyp;
    simFitData.simData.flanges[1].L  = inputs.L1 + inputs.L3;

    simFitData.fitData.flanges[1].T  = inputs.ATtyp;
    simFitData.fitData.flanges[1].P  = inputs.PPtyp;
    simFitData.fitData.flanges[1].L  = inputs.L1 + inputs.L3;

    /*if( outputs.L_ins_tube > 0.0 )
    {
        simFitData.simData.flanges[2].T  = outputs.T_ins_tube_typ; ///// danger !!
        simFitData.simData.flanges[2].P  = inputs.PPtyp;
        simFitData.simData.flanges[2].L  = outputs.L_ins_tube; ///// danger !!

        simFitData.fitData.flanges[2].T  = outputs.T_ins_tube_typ; ///// danger !!
        simFitData.fitData.flanges[2].P  = inputs.PPtyp;
        simFitData.fitData.flanges[2].L  = outputs.L_ins_tube; ///// danger !!
    }*/
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void spcConfigurator::setRAMerrorConfig()
{
    doubledIteration = false;
    simFitData.stackFittingLater = true;

    noiseGenerator.getWhiteNoiseStruct().turnedOn = false;
    noiseGenerator.getPressureNoiseStruct().turnedOn = false;
    noiseGenerator.getTemperatureNoiseStruct().turnedOn = false;
    noiseGenerator.getFreqShiftNoiseStruct().turnedOn = false;
    noiseGenerator.getRAMnoiseStruct().turnedOn = true;
    noiseGenerator.getTransmissionNoiseStruct().turnedOn = false;

    simFitData.simData.stack.X2 = 0.0; // no second measurement component (for the time being)

    simFitData.simData.stack.X1 = inputs.concTyp1;
    simFitData.simData.stack.P  = inputs.PPtyp;
    simFitData.simData.stack.T  = inputs.PTtyp;
    simFitData.simData.stack.L  = inputs.L2 - outputs.L_ins_tube; ///// danger !!
    simFitData.simData.stack.nameMatrX.push_back("N2");
    simFitData.simData.stack.nameMatrX.push_back("CO2");
    simFitData.simData.stack.mtxStyle = GASCONFIG_SIM_FIT_STRUCT::basic;
    simFitData.simData.stack.matrX = createMatrixConcentrations(true,true);

    // Flange 0 sim
    simFitData.simData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
    simFitData.simData.flanges.back().X1 = gasConfig.conc1xAir;
    simFitData.simData.flanges.back().X2 = 0.0; // no second measurement component (for the time being)
    simFitData.simData.flanges.back().T  = inputs.ATtyp;
    simFitData.simData.flanges.back().P  = gasConfig.PresAtm;
    simFitData.simData.flanges.back().L  = gasConfig.TxLen + gasConfig.RxLen;
    simFitData.simData.flanges.back().nameMatrX.push_back("N2");
    simFitData.simData.flanges.back().matrX = createMatrixConcentrations(false,true);

    simFitData.fitData.stack.X2 = 0.0;

    simFitData.fitData.stack.P  = inputs.PPtyp;
    simFitData.fitData.stack.T  = inputs.PTtyp;
    simFitData.fitData.stack.L  = inputs.L2 - outputs.L_ins_tube; ///// danger !!
    simFitData.fitData.stack.nameMatrX.push_back("N2");
    simFitData.fitData.stack.nameMatrX.push_back("CO2");
    simFitData.fitData.stack.mtxStyle = GASCONFIG_SIM_FIT_STRUCT::basic;

    // Flange 0 fit
    simFitData.fitData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
    simFitData.fitData.flanges.back().X1 = gasConfig.conc1xAir;
    simFitData.fitData.flanges.back().X2 = 0.0; // no second measurement component (for the time being)
    simFitData.fitData.flanges.back().T  = inputs.ATtyp;
    simFitData.fitData.flanges.back().P  = gasConfig.PresAtm;
    simFitData.fitData.flanges.back().L  = gasConfig.TxLen + gasConfig.RxLen;
    simFitData.fitData.flanges.back().nameMatrX.push_back("N2");
    simFitData.fitData.flanges.back().matrX = createMatrixConcentrations(false,false);

    switch( currentCase )
    {
    case RAM_1:
    case RAM_4:
        // Flange 1 sim
        simFitData.simData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
        simFitData.simData.flanges[1].X1 = gasConfig.conc1xAir;
        simFitData.simData.flanges[1].X2 = 0.0; // no second measurement component (for the time being)
        simFitData.simData.flanges[1].nameMatrX.push_back("N2");
        simFitData.simData.flanges[1].matrX = createMatrixConcentrations(false,true);

        // Flange 1 fit
        simFitData.fitData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
        simFitData.fitData.flanges[1].X1 = gasConfig.conc1xAir;
        simFitData.fitData.flanges[1].X2 = 0.0; // no second measurement component (for the time being)
        simFitData.fitData.flanges[1].nameMatrX.push_back("N2");
        simFitData.fitData.flanges[1].matrX = createMatrixConcentrations(false,false);

        if( outputs.L_ins_tube > 0.0 )
        {
            // Flange 2 sim
            simFitData.simData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
            simFitData.simData.flanges[2].X1 = gasConfig.conc1xAir;
            simFitData.simData.flanges[2].X2 = 0.0; // no second measurement component (for the time being)
            simFitData.simData.flanges[2].nameMatrX.push_back("N2");
            simFitData.simData.flanges[2].matrX = createMatrixConcentrations(false,true);

            // Flange 2 fit
            simFitData.fitData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
            simFitData.fitData.flanges[2].X1 = gasConfig.conc1xAir;
            simFitData.fitData.flanges[2].X2 = 0.0; // no second measurement component (for the time being)
            simFitData.fitData.flanges[2].nameMatrX.push_back("N2");
            simFitData.fitData.flanges[2].matrX = createMatrixConcentrations(false,false);
        }

        insTubePurgeGas = "Air";
        break;
    case RAM_2:
    case RAM_5:
        // No flanges to be set
        //insTubePurgeGas = "N2";
        break;
    case RAM_3:
    case RAM_6:
        // Flange 1 sim
        simFitData.simData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
        simFitData.simData.flanges[1].X1 = inputs.purConc1;
        simFitData.simData.flanges[1].X2 = 0.0; // no second measurement component (for the time being)
        simFitData.simData.flanges[1].nameMatrX.push_back(inputs.purMatxGas1);
        simFitData.simData.flanges[1].nameMatrX.push_back(inputs.purMatxGas2);
        simFitData.simData.flanges[1].matrX = createMatrixConcentrations(false,true);

        // Flange 1 fit
        simFitData.fitData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
        simFitData.fitData.flanges[1].X1 = inputs.purConc1;
        simFitData.fitData.flanges[1].X2 = 0.0; // no second measurement component (for the time being)
        simFitData.fitData.flanges[1].nameMatrX.push_back(inputs.purMatxGas1);
        simFitData.fitData.flanges[1].nameMatrX.push_back(inputs.purMatxGas2);
        simFitData.fitData.flanges[1].matrX = createMatrixConcentrations(false,false);

        if( outputs.L_ins_tube > 0.0 )
        {
            // Flange 2 sim
            simFitData.simData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
            simFitData.simData.flanges[2].X1 = inputs.purConc1;
            simFitData.simData.flanges[2].X2 = 0.0; // no second measurement component (for the time being)
            simFitData.simData.flanges[2].nameMatrX.push_back(inputs.purMatxGas1);
            simFitData.simData.flanges[2].nameMatrX.push_back(inputs.purMatxGas2);
            simFitData.simData.flanges[2].matrX = createMatrixConcentrations(false,true);

            // Flange 2 fit
            simFitData.fitData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
            simFitData.fitData.flanges[2].X1 = inputs.purConc1;
            simFitData.fitData.flanges[2].X2 = 0.0; // no second measurement component (for the time being)
            simFitData.fitData.flanges[2].nameMatrX.push_back(inputs.purMatxGas1);
            simFitData.fitData.flanges[2].nameMatrX.push_back(inputs.purMatxGas2);
            simFitData.fitData.flanges[2].matrX = createMatrixConcentrations(false,false);
        }

        insTubePurgeGas = "custom";
    }

    switch( currentCase )
    {
    case RAM_1:
    case RAM_2:
    case RAM_3:
        PWfloating = false;
        break;
    case RAM_4:
    case RAM_5:
    case RAM_6:
        PWfloating = true;
    }

    switch( currentCase )
    {
    case RAM_2:
    case RAM_5:
        return;
    }

    simFitData.simData.flanges[1].T  = inputs.ATtyp;
    simFitData.simData.flanges[1].P  = inputs.PPtyp;
    simFitData.simData.flanges[1].L  = inputs.L1 + inputs.L3;

    simFitData.fitData.flanges[1].T  = inputs.ATtyp;
    simFitData.fitData.flanges[1].P  = inputs.PPtyp;
    simFitData.fitData.flanges[1].L  = inputs.L1 + inputs.L3;

    /*if( outputs.L_ins_tube > 0.0 )
    {
        simFitData.simData.flanges[2].T  = outputs.T_ins_tube_typ; ///// danger !!
        simFitData.simData.flanges[2].P  = inputs.PPtyp;
        simFitData.simData.flanges[2].L  = outputs.L_ins_tube; ///// danger !!

        simFitData.fitData.flanges[2].T  = outputs.T_ins_tube_typ; ///// danger !!
        simFitData.fitData.flanges[2].P  = inputs.PPtyp;
        simFitData.fitData.flanges[2].L  = outputs.L_ins_tube; ///// danger !!
    }*/
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void spcConfigurator::setValidationAdjustmentConfig()
{
    doubledIteration = false;
    PWfloating = false;
    simFitData.stackFittingLater = true;

    noiseGenerator.getWhiteNoiseStruct().turnedOn = true;
    noiseGenerator.getPressureNoiseStruct().turnedOn = true;
    noiseGenerator.getTemperatureNoiseStruct().turnedOn = true;
    noiseGenerator.getFreqShiftNoiseStruct().turnedOn = true;
    noiseGenerator.getRAMnoiseStruct().turnedOn = false;
    noiseGenerator.getTransmissionNoiseStruct().turnedOn = true;

    recalcTransmissionSFaccToDust(1);

    simFitData.simData.stack.X2 = 0.0; // no second measurement component (for the time being)

    simFitData.simData.stack.X1 = inputs.concTyp1;
    simFitData.simData.stack.P  = inputs.PPtyp;
    simFitData.simData.stack.T  = inputs.PTtyp;
    simFitData.simData.stack.L  = inputs.L2 - outputs.L_ins_tube; ///// danger !!
    simFitData.simData.stack.nameMatrX.push_back("N2");
    simFitData.simData.stack.nameMatrX.push_back("CO2");
    simFitData.simData.stack.mtxStyle = GASCONFIG_SIM_FIT_STRUCT::basic;
    simFitData.simData.stack.matrX = createMatrixConcentrations(true,true);

    // Flange 0 sim
    simFitData.simData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
    simFitData.simData.flanges.back().X1 = gasConfig.conc1xAir;
    simFitData.simData.flanges.back().X2 = 0.0; // no second measurement component (for the time being)
    simFitData.simData.flanges.back().T  = inputs.ATtyp;
    simFitData.simData.flanges.back().P  = gasConfig.PresAtm;
    simFitData.simData.flanges.back().L  = gasConfig.TxLen + gasConfig.RxLen;
    simFitData.simData.flanges.back().nameMatrX.push_back("N2");
    simFitData.simData.flanges.back().matrX = createMatrixConcentrations(false,true);

    simFitData.fitData.stack.X2 = 0.0;

    simFitData.fitData.stack.P  = gasConfig.PresAtm;
    simFitData.fitData.stack.T  = inputs.ATtyp;
    simFitData.fitData.stack.L  = gasConfig.intCellLenOpt;
    simFitData.fitData.stack.nameMatrX.push_back("N2");
    //simFitData.fitData.stack.nameMatrX.push_back("CO2"); // because Air, not Matrix
    simFitData.fitData.stack.mtxStyle = GASCONFIG_SIM_FIT_STRUCT::basic;
    simFitData.fitData.stack.matrX = createMatrixConcentrations(true,false);

    // Flange 0 fit
    simFitData.fitData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
    simFitData.fitData.flanges.back().X1 = gasConfig.conc1xAir;
    simFitData.fitData.flanges.back().X2 = 0.0; // no second measurement component (for the time being)
    simFitData.fitData.flanges.back().T  = inputs.ATmax;
    simFitData.fitData.flanges.back().P  = gasConfig.PresAtm;
    simFitData.fitData.flanges.back().L  = gasConfig.TxLen + gasConfig.RxLen;
    simFitData.fitData.flanges.back().nameMatrX.push_back("N2");
    simFitData.fitData.flanges.back().matrX = createMatrixConcentrations(false,false);

    switch( currentCase )
    {
    case VA_1:
    //case VA_4:
        // Flange 1 sim
        simFitData.simData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
        simFitData.simData.flanges[1].X1 = gasConfig.conc1xAir;
        simFitData.simData.flanges[1].X2 = 0.0; // no second measurement component (for the time being)
        simFitData.simData.flanges[1].nameMatrX.push_back("N2");
        simFitData.simData.flanges[1].matrX = createMatrixConcentrations(false,true);

        // Flange 1 fit
        simFitData.fitData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
        simFitData.fitData.flanges[1].X1 = gasConfig.conc1xAir;
        simFitData.fitData.flanges[1].X2 = 0.0; // no second measurement component (for the time being)
        simFitData.fitData.flanges[1].nameMatrX.push_back("N2");
        simFitData.fitData.flanges[1].matrX = createMatrixConcentrations(false,false);

        if( outputs.L_ins_tube > 0.0 )
        {
            // Flange 2 sim
            simFitData.simData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
            simFitData.simData.flanges[2].X1 = gasConfig.conc1xAir;
            simFitData.simData.flanges[2].X2 = 0.0; // no second measurement component (for the time being)
            simFitData.simData.flanges[2].nameMatrX.push_back("N2");
            simFitData.simData.flanges[2].matrX = createMatrixConcentrations(false,true);

            // Flange 2 fit
            simFitData.fitData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
            simFitData.fitData.flanges[2].X1 = gasConfig.conc1xAir;
            simFitData.fitData.flanges[2].X2 = 0.0; // no second measurement component (for the time being)
            simFitData.fitData.flanges[2].nameMatrX.push_back("N2");
            simFitData.fitData.flanges[2].matrX = createMatrixConcentrations(false,false);
        }

        insTubePurgeGas = "Air";
        break;
    case VA_2:
    //case VA_5:
        // No flanges to be set
        //insTubePurgeGas = "N2";
        break;
    case VA_3:
    //case VA_6:
        // Flange 1 sim
        simFitData.simData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
        simFitData.simData.flanges[1].X1 = inputs.purConc1;
        simFitData.simData.flanges[1].X2 = 0.0; // no second measurement component (for the time being)
        simFitData.simData.flanges[1].nameMatrX.push_back(inputs.purMatxGas1);
        simFitData.simData.flanges[1].nameMatrX.push_back(inputs.purMatxGas2);
        simFitData.simData.flanges[1].matrX = createMatrixConcentrations(false,true);

        // Flange 1 fit
        simFitData.fitData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
        simFitData.fitData.flanges[1].X1 = inputs.purConc1;
        simFitData.fitData.flanges[1].X2 = 0.0; // no second measurement component (for the time being)
        simFitData.fitData.flanges[1].nameMatrX.push_back(inputs.purMatxGas1);
        simFitData.fitData.flanges[1].nameMatrX.push_back(inputs.purMatxGas2);
        simFitData.fitData.flanges[1].matrX = createMatrixConcentrations(false,false);

        if( outputs.L_ins_tube > 0.0 )
        {
            // Flange 2 sim
            simFitData.simData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
            simFitData.simData.flanges[2].X1 = inputs.purConc1;
            simFitData.simData.flanges[2].X2 = 0.0; // no second measurement component (for the time being)
            simFitData.simData.flanges[2].nameMatrX.push_back(inputs.purMatxGas1);
            simFitData.simData.flanges[2].nameMatrX.push_back(inputs.purMatxGas2);
            simFitData.simData.flanges[2].matrX = createMatrixConcentrations(false,true);

            // Flange 2 fit
            simFitData.fitData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
            simFitData.fitData.flanges[2].X1 = inputs.purConc1;
            simFitData.fitData.flanges[2].X2 = 0.0; // no second measurement component (for the time being)
            simFitData.fitData.flanges[2].nameMatrX.push_back(inputs.purMatxGas1);
            simFitData.fitData.flanges[2].nameMatrX.push_back(inputs.purMatxGas2);
            simFitData.fitData.flanges[2].matrX = createMatrixConcentrations(false,false);
        }

        insTubePurgeGas = "custom";
    }

    // Flange 3 sim
    simFitData.simData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
    simFitData.simData.flanges.back().X1 = gasConfig.conc1xIntCell;
    simFitData.simData.flanges.back().X2 = 0.0; // no second measurement component (for the time being)
    simFitData.simData.flanges.back().nameMatrX.push_back("N2");
    simFitData.simData.flanges.back().matrX = createMatrixConcentrations(false,true);
    simFitData.simData.flanges.back().T = inputs.ATtyp;
    simFitData.simData.flanges.back().P = gasConfig.PresAtm;
    simFitData.simData.flanges.back().L = gasConfig.intCellLenOpt;

    // Flange 3 fit
    simFitData.fitData.flanges.push_back(GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT::chamberInfo());
    simFitData.fitData.flanges.back().X1 = inputs.concTyp1;
    simFitData.fitData.flanges.back().X2 = 0.0; // no second measurement component (for the time being)
    simFitData.fitData.flanges.back().nameMatrX.push_back("N2");
    simFitData.fitData.flanges.back().nameMatrX.push_back("CO2");
    simFitData.fitData.flanges.back().matrX = createMatrixConcentrations(false,false);
    simFitData.fitData.flanges.back().T = inputs.PTtyp;
    simFitData.fitData.flanges.back().P = inputs.PPtyp;
    simFitData.fitData.flanges.back().L = inputs.L2 - outputs.L_ins_tube;

    switch( currentCase )
    {
    case VA_2:
    //case VA_5:
        return;
    }

    simFitData.simData.flanges[1].T  = inputs.ATtyp;
    simFitData.simData.flanges[1].P  = inputs.PPtyp;
    simFitData.simData.flanges[1].L  = inputs.L1 + inputs.L3;

    simFitData.fitData.flanges[1].T  = inputs.ATtyp;
    simFitData.fitData.flanges[1].P  = inputs.PPtyp;
    simFitData.fitData.flanges[1].L  = inputs.L1 + inputs.L3;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void spcConfigurator::calculateSimOutputs()
{
    double SNR = calculateSNR();
    bool value;
    int  retVal;

    if( SNR >= gasConfig.SNRllMin )
        value = true;
    else
        value = false;

    if( simFitData.useSimFitStruct )
    {
        switch( currentCase )
        {
        case ICI_1:
        case ICI_2:
        case ICI_3:
        case ICI_7:
        case ICI_8:
        case ICI_9:
            if( fabs(O2fitted - O2initial) > percent2ppm(gasConfig.threshICI * 1.0e-2 * inputs.range1) ) outputs.fixed_only = false;
        default:
            break;
        }

        switch( currentCase )
        {
        case TxRx_LL:
            outputs.TxRx_LL = value;
            break;
        case Stack_LL:
            outputs.Stack_LL = value;
            break;
        case Flange_LL:
            outputs.Flange_LL = value;
            break;
        case ExtCell_LL:
            outputs.ExtCell_LL = value;
            break;
        case ICI_1:
            outputs.acc_matrix_max_air_fix = fabs(O2fitted - O2initial);
            break;
        case ICI_2:
            outputs.acc_matrix_max_N2_fix = fabs(O2fitted - O2initial);
            break;
        case ICI_3:
            outputs.acc_matrix_max_cust_fix = fabs(O2fitted - O2initial);
            break;
        case ICI_4:
            outputs.acc_matrix_max_air_float = fabs(O2fitted - O2initial);
            break;
        case ICI_5:
            outputs.acc_matrix_max_N2_float = fabs(O2fitted - O2initial);
            break;
        case ICI_6:
            outputs.acc_matrix_max_cust_float = fabs(O2fitted - O2initial);
            break;
        case ICI_7:
            outputs.acc_matrix_min_air_fix = fabs(O2fitted - O2initial);
            break;
        case ICI_8:
            outputs.acc_matrix_min_N2_fix = fabs(O2fitted - O2initial);
            break;
        case ICI_9:
            outputs.acc_matrix_min_cust_fix = fabs(O2fitted - O2initial);
            break;
        case ICI_10:
            outputs.acc_matrix_min_air_float = fabs(O2fitted - O2initial);
            break;
        case ICI_11:
            outputs.acc_matrix_min_N2_float = fabs(O2fitted - O2initial);
            break;
        case ICI_12:
            outputs.acc_matrix_min_cust_float = fabs(O2fitted - O2initial);
            break;
        case PD_1:
            outputs.acc_press_max_air_fix = fabs(O2fitted - O2initial);
            retVal = checkPsensor(outputs.acc_press_min_air_fix,outputs.acc_press_max_air_fix);
            if( retVal >= 0 ) outputs.P_sensor_air_fix = (retVal != 0);
            break;
        case PD_2:
            outputs.acc_press_max_N2_fix = fabs(O2fitted - O2initial);
            retVal = checkPsensor(outputs.acc_press_min_N2_fix,outputs.acc_press_max_N2_fix);
            if( retVal >= 0 ) outputs.P_sensor_N2_fix = (retVal != 0);
            break;
        case PD_3:
            outputs.acc_press_max_cust_fix = fabs(O2fitted - O2initial);
            retVal = checkPsensor(outputs.acc_press_min_cust_fix,outputs.acc_press_max_cust_fix);
            if( retVal >= 0 ) outputs.P_sensor_cust_fix = (retVal != 0);
            break;
        case PD_4:
            outputs.acc_press_max_air_float = fabs(O2fitted - O2initial);
            retVal = checkPsensor(outputs.acc_press_min_air_float,outputs.acc_press_max_air_float);
            if( retVal >= 0 ) outputs.P_sensor_air_float = (retVal != 0);
            break;
        case PD_5:
            outputs.acc_press_max_N2_float = fabs(O2fitted - O2initial);
            retVal = checkPsensor(outputs.acc_press_min_N2_float,outputs.acc_press_max_N2_float);
            if( retVal >= 0 ) outputs.P_sensor_N2_float = (retVal != 0);
            break;
        case PD_6:
            outputs.acc_press_max_cust_float = fabs(O2fitted - O2initial);
            retVal = checkPsensor(outputs.acc_press_min_cust_float,outputs.acc_press_max_cust_float);
            if( retVal >= 0 ) outputs.P_sensor_cust_float = (retVal != 0);
            break;
        case PD_7:
            outputs.acc_press_min_air_fix = fabs(O2fitted - O2initial);
            retVal = checkPsensor(outputs.acc_press_min_air_fix,outputs.acc_press_max_air_fix);
            if( retVal >= 0 ) outputs.P_sensor_air_fix = (retVal != 0);
            break;
        case PD_8:
            outputs.acc_press_min_N2_fix = fabs(O2fitted - O2initial);
            retVal = checkPsensor(outputs.acc_press_min_N2_fix,outputs.acc_press_max_N2_fix);
            if( retVal >= 0 ) outputs.P_sensor_N2_fix = (retVal != 0);
            break;
        case PD_9:
            outputs.acc_press_min_cust_fix = fabs(O2fitted - O2initial);
            retVal = checkPsensor(outputs.acc_press_min_cust_fix,outputs.acc_press_max_cust_fix);
            if( retVal >= 0 ) outputs.P_sensor_cust_fix = (retVal != 0);
            break;
        case PD_10:
            outputs.acc_press_min_air_float = fabs(O2fitted - O2initial);
            retVal = checkPsensor(outputs.acc_press_min_air_float,outputs.acc_press_max_air_float);
            if( retVal >= 0 ) outputs.P_sensor_air_float = (retVal != 0);
            break;
        case PD_11:
            outputs.acc_press_min_N2_float = fabs(O2fitted - O2initial);
            retVal = checkPsensor(outputs.acc_press_min_N2_float,outputs.acc_press_max_N2_float);
            if( retVal >= 0 ) outputs.P_sensor_N2_float = (retVal != 0);
            break;
        case PD_12:
            outputs.acc_press_min_cust_float = fabs(O2fitted - O2initial);
            retVal = checkPsensor(outputs.acc_press_min_cust_float,outputs.acc_press_max_cust_float);
            if( retVal >= 0 ) outputs.P_sensor_cust_float = (retVal != 0);
            break;
        case TD_1:
            outputs.acc_temp_max_air_fix = fabs(O2fitted - O2initial);
            retVal = checkTsensor(outputs.acc_temp_min_air_fix,outputs.acc_temp_max_air_fix);
            if( retVal >= 0 ) outputs.T_sensor_air_fix = (retVal != 0);
            break;
        case TD_2:
            outputs.acc_temp_max_N2_fix = fabs(O2fitted - O2initial);
            retVal = checkTsensor(outputs.acc_temp_min_N2_fix,outputs.acc_temp_max_N2_fix);
            if( retVal >= 0 ) outputs.T_sensor_N2_fix = (retVal != 0);
            break;
        case TD_3:
            outputs.acc_temp_max_cust_fix = fabs(O2fitted - O2initial);
            retVal = checkTsensor(outputs.acc_temp_min_cust_fix,outputs.acc_temp_max_cust_fix);
            if( retVal >= 0 ) outputs.T_sensor_cust_fix = (retVal != 0);
            break;
        case TD_4:
            outputs.acc_temp_max_air_float = fabs(O2fitted - O2initial);
            retVal = checkTsensor(outputs.acc_temp_min_air_float,outputs.acc_temp_max_air_float);
            if( retVal >= 0 ) outputs.T_sensor_air_float = (retVal != 0);
            break;
        case TD_5:
            outputs.acc_temp_max_N2_float = fabs(O2fitted - O2initial);
            retVal = checkTsensor(outputs.acc_temp_min_N2_float,outputs.acc_temp_max_N2_float);
            if( retVal >= 0 ) outputs.T_sensor_N2_float = (retVal != 0);
            break;
        case TD_6:
            outputs.acc_temp_max_cust_float = fabs(O2fitted - O2initial);
            retVal = checkTsensor(outputs.acc_temp_min_cust_float,outputs.acc_temp_max_cust_float);
            if( retVal >= 0 ) outputs.T_sensor_cust_float = (retVal != 0);
            break;
        case TD_7:
            outputs.acc_temp_min_air_fix = fabs(O2fitted - O2initial);
            retVal = checkTsensor(outputs.acc_temp_min_air_fix,outputs.acc_temp_max_air_fix);
            if( retVal >= 0 ) outputs.T_sensor_air_fix = (retVal != 0);
            break;
        case TD_8:
            outputs.acc_temp_min_N2_fix = fabs(O2fitted - O2initial);
            retVal = checkTsensor(outputs.acc_temp_min_N2_fix,outputs.acc_temp_max_N2_fix);
            if( retVal >= 0 ) outputs.T_sensor_N2_fix = (retVal != 0);
            break;
        case TD_9:
            outputs.acc_temp_min_cust_fix = fabs(O2fitted - O2initial);
            retVal = checkTsensor(outputs.acc_temp_min_cust_fix,outputs.acc_temp_max_cust_fix);
            if( retVal >= 0 ) outputs.T_sensor_cust_fix = (retVal != 0);
            break;
        case TD_10:
            outputs.acc_temp_min_air_float = fabs(O2fitted - O2initial);
            retVal = checkTsensor(outputs.acc_temp_min_air_float,outputs.acc_temp_max_air_float);
            if( retVal >= 0 ) outputs.T_sensor_air_float = (retVal != 0);
            break;
        case TD_11:
            outputs.acc_temp_min_N2_float = fabs(O2fitted - O2initial);
            retVal = checkTsensor(outputs.acc_temp_min_N2_float,outputs.acc_temp_max_N2_float);
            if( retVal >= 0 ) outputs.T_sensor_N2_float = (retVal != 0);
            break;
        case TD_12:
            outputs.acc_temp_min_cust_float = fabs(O2fitted - O2initial);
            retVal = checkTsensor(outputs.acc_temp_min_cust_float,outputs.acc_temp_max_cust_float);
            if( retVal >= 0 ) outputs.T_sensor_cust_float = (retVal != 0);
            break;
        case PrecLDL_1:
            outputs.prec_air_fix = calculateConcentrationStdDev(O2concentrationsVector.size()/2,O2concentrationsVector.size());
            outputs.LDL_air_fix  = 4 * calculateConcentrationStdDev(0,O2concentrationsVector.size()/2);
            break;
        case PrecLDL_2:
            outputs.prec_N2_fix = calculateConcentrationStdDev(O2concentrationsVector.size()/2,O2concentrationsVector.size());
            outputs.LDL_N2_fix  = 4 * calculateConcentrationStdDev(0,O2concentrationsVector.size()/2);
            break;
        case PrecLDL_3:
            outputs.prec_cust_fix = calculateConcentrationStdDev(O2concentrationsVector.size()/2,O2concentrationsVector.size());
            outputs.LDL_cust_fix  = 4 * calculateConcentrationStdDev(0,O2concentrationsVector.size()/2);
            break;
        case PrecLDL_4:
            outputs.prec_air_float = calculateConcentrationStdDev(O2concentrationsVector.size()/2,O2concentrationsVector.size());
            outputs.LDL_air_float  = 4 * calculateConcentrationStdDev(0,O2concentrationsVector.size()/2);
            break;
        case PrecLDL_5:
            outputs.prec_N2_float = calculateConcentrationStdDev(O2concentrationsVector.size()/2,O2concentrationsVector.size());
            outputs.LDL_N2_float  = 4 * calculateConcentrationStdDev(0,O2concentrationsVector.size()/2);
            break;
        case PrecLDL_6:
            outputs.prec_cust_float = calculateConcentrationStdDev(O2concentrationsVector.size()/2,O2concentrationsVector.size());
            outputs.LDL_cust_float  = 4 * calculateConcentrationStdDev(0,O2concentrationsVector.size()/2);
            break;
        case RAM_1:
            outputs.RAM_air_fix = calculateConcentrationStdDev(0,O2concentrationsVector.size());
            break;
        case RAM_2:
            outputs.RAM_N2_fix = calculateConcentrationStdDev(0,O2concentrationsVector.size());
            break;
        case RAM_3:
            outputs.RAM_cust_fix = calculateConcentrationStdDev(0,O2concentrationsVector.size());
            break;
        case RAM_4:
            outputs.RAM_air_float = calculateConcentrationStdDev(0,O2concentrationsVector.size());
            break;
        case RAM_5:
            outputs.RAM_N2_float = calculateConcentrationStdDev(0,O2concentrationsVector.size());
            break;
        case RAM_6:
            outputs.RAM_cust_float = calculateConcentrationStdDev(0,O2concentrationsVector.size());
            break;
        case VA_1:
            outputs.IntCell_SNR_air_fix = SNR;
            break;
        case VA_2:
            outputs.IntCell_SNR_N2_fix = SNR;
            break;
        case VA_3:
            outputs.IntCell_SNR_cust_fix = SNR;
            break;
        default:
            break;
        }
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void spcConfigurator::checkIfLLmethodSettable()
{
    if( outputs.TxRx_LL )
    {
        outputs.LL_method  = "Tx Rx";
        outputs.L_ext_cell = 0.0;
    }
    else if( outputs.Stack_LL )
    {
        outputs.LL_method  = "Stack";
        outputs.L_ext_cell = 0.0;
    }
    else if( outputs.Flange_LL )
    {
        outputs.LL_method  = "Flange";
        outputs.L_ext_cell = 0.0;
    }
    else if( outputs.ExtCell_LL )
    {
        outputs.LL_method  = "External Cell";
        outputs.L_ext_cell = gasConfig.extCellLenOpt;
        insTubeConditions();
        conditionsWithinBounds();
    }
    else
        outputs.LL_method = "Try external cell with larger length.";
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void spcConfigurator::launchGasConfigOption1( bool launched )
{
    if( launched )
    {
        std::unique_ptr<O2CONFIG_COMMON_STRUCT> ptr = std::make_unique<O2CONFIG_OPTION1_STRUCT>();
        gasConfig = ptr.get();
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void spcConfigurator::launchGasConfigOption2( bool launched )
{
    if( launched )
    {
        std::unique_ptr<O2CONFIG_COMMON_STRUCT> ptr = std::make_unique<O2CONFIG_OPTION2_STRUCT>();
        gasConfig = ptr.get();
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void spcConfigurator::launchGasConfigOption3( bool launched )
{
    if( launched )
    {
        std::unique_ptr<O2CONFIG_COMMON_STRUCT> ptr = std::make_unique<O2CONFIG_OPTION3_STRUCT>();
        gasConfig = ptr.get();
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int spcConfigurator::checkPsensor( double accLimitMin, double accLimitMax )
{
    if( accLimitMax > 0.0 && accLimitMin > 0.0 )
    {
        if( fabs((accLimitMax/1.0e4/inputs.range1/(1.0e3*(inputs.PPmax-inputs.PPtyp)))*1.0e2) > gasConfig.specPdep
            || fabs((accLimitMin/1.0e4/inputs.range1/(1.0e3*(inputs.PPmin-inputs.PPtyp)))*1.0e2) > gasConfig.specPdep )
            return 1;
        else
            return 0;
    }
    return -1;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int spcConfigurator::checkTsensor( double accLimitMin, double accLimitMax )
{
    if( accLimitMax > 0.0 && accLimitMin > 0.0 )
    {
        if( fabs((accLimitMax/1.0e4/inputs.range1/(inputs.PTmax-inputs.PTtyp))*1.0e2) > gasConfig.specTdep
            || fabs((accLimitMin*1.0e4/inputs.range1/(inputs.PTmin-inputs.PTtyp))*1.0e2) > gasConfig.specTdep )
            return 1;
        else
            return 0;
    }
    return -1;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void spcConfigurator::calculateNonSimOutputs()
{
    insTubeConditions();
    conditionsWithinBounds();
    collimationCalcs();
    IRfilterPresent();
    spectralScanRate();
}
