#include "spcgenerator.h"
#include "globals.h"   // to have access to readGoofyINI function
#include "libMASSConfigManager.h"
#include <float.h>

#include <cstdlib>
#include <numeric>
#include <cstdio>
#include <thread>
#include <chrono>

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

std::vector<double> spcGenerator::createRandomVector(unsigned size)
{
    double min = 0.0;
    double max = 2e6;
    std::vector<double> returnVec;

    for(unsigned i=0; i<size; i++)
        returnVec.push_back((((double) rand() / (double) RAND_MAX) * (max - min)) + min);

    return returnVec;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void spcGenerator::padVectorWith0s(std::vector<double>& vec, unsigned upTo, unsigned size)
{
    vec.insert(vec.begin(),upTo,0);
    vec.resize(size,0);
    //for(int i=0; i<vec.size(); i++) std::cout << "vec[" << i << "] = " << vec[i] << std:: endl;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void spcGenerator::unpadVector(std::vector<double>& vec, unsigned upTo, unsigned size)
{
    vec.erase(vec.begin(),vec.begin()+upTo);
    vec.resize(size);
    //for(int i=0; i<vec.size(); i++) std::cout << "vec[" << i << "] = " << vec[i] << std:: endl;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void spcGenerator::calculateInitialGuessFit()
{
    // Setting the directory of the crossstack.ini
    std::string confDirectory = "../MasterINI/";

    // Reading Goofy INI (crossstack.ini)
    StructLaserRampArguments eeprom;
    StructStackConfig stack;
    std::vector<StructStackConfig> flanges;
    std::vector<StructError> lmErrors;
    std::string sVersion;

    if( reinitNeeded )
        reinitializeStack(stack);

    // Defining some additional parameters
    std::vector<double> flangeT_C;
    std::vector<double> flangeP_torr;
    double Temp_C = stackPT.stackTemp;
    double P_torr = stackPT.stackPres;
    uint64_t fitTime(0);

    // Initializing libMASS manager
    manager = SPCFactory::createManager(confDirectory, "O2_760NM");

    if( simFitData.useSimFitStruct )
    {
        readGoofyINI(confDirectory + "crossstack.ini", eeprom);
        readStackInputs(broadeningCoeffsINI, stack, Temp_C, P_torr, simFitData, 1);
        readFlangesInputs(broadeningCoeffsINI, flanges, flangeT_C, flangeP_torr, simFitData, 1);

        if( simFitData.stackFittingLater && !flanges.empty() && simFitData.simData.stack.isEmpty )
        {
            stack = flanges.back();
            Temp_C = flangeT_C.back();
            P_torr = flangeP_torr.back();
            flanges.clear();
            flangeT_C.clear();
            flangeP_torr.clear();
        }
    }
    else
        readGoofyINI(confDirectory + "crossstack.ini", stack, flanges, eeprom, flangeT_C, flangeP_torr);

    sVersion = manager->getVersion();
    lmErrors = manager->getErrors();

    // Initializing necessary interface members
    manager->initStack(stack);
    manager->initFlanges(flanges);
    manager->initLaserRampArguments(eeprom);
    manager->setMode(SPCInterface::kDefaultMode);

    // Enabling calculation of initial guess spectrum
    manager->getInterface()->setDoCalculateInitialY(0,true);

    // Some additional parameters
    std::vector<int> ringdownCount;

    ringdownCount.push_back(manager->getInterface()->getRDFitCnt());

    unsigned int iRampStart = manager->getInterface()->getLaserRampStartIdx();
    unsigned int iRampEnd = iRampStart + manager->getInterface()->getLaserRampLength();

    unsigned int iRingdownStart = manager->getInterface()->getLaserOFFStartIdx();
    unsigned int iRingdownEnd = iRingdownStart + manager->getInterface()->getLaserOFFLength();

    /*std::vector<double> laserRampData(&(spectrum.at(iRampStart)),
        &(spectrum.at(iRampEnd-1)) + 1);
    std::vector<double> ringdownData(&spectrum.at(iRingdownStart),
        &spectrum.at(iRingdownEnd-1) + 1);*/
    std::vector<double> laserRampData = createRandomVector();
    std::vector<double> ringdownData  = createRandomVector();

    // Enabling linelock
    manager->getInterface()->setEnableLinelock(0, true);

    manager->getInterface()->computeSpectrum(
        Temp_C,
        P_torr,
        flangeT_C,
        flangeP_torr,
        std::vector<std::vector<double> >(1, laserRampData),
        std::vector<std::vector<double> >(1, ringdownData),
        ringdownCount);

    // Retrieving initial guess spectra (light intensity)
    startingVector = manager->getInterface()->getInitialGuessFit(0);

    // Retrieving frequency domain
    ghzPoints = manager->getInterface()->getGHzPts(0);

    // Retrieving baseline vector
    double BLoffset = manager->getInterface()->getBaselineParameters(0)[0];
    double BLslope  = manager->getInterface()->getBaselineParameters(0)[1];

    for(unsigned i=0; i<ghzPoints.size(); ++i)
        noisedBaseLine.push_back(BLoffset + BLslope * ghzPoints[i]);

    /************************ Adding noise to baseline ************************/
    // 0. Adding transmission noise
    addTransmissionNoiseToBL(noisedBaseLine);
    /**************************************************************************/

    // Simulating raw transmission signal
    rawSignal = manager->getInterface()->getRawSignal(0);

    // Padding 0s
    padVectorWith0s(startingVector, eeprom.fitWindow[0], spectrumWidth);
    padVectorWith0s(noisedBaseLine, eeprom.fitWindow[0], spectrumWidth);
    padVectorWith0s(ghzPoints, eeprom.fitWindow[0], spectrumWidth);
    padVectorWith0s(rawSignal, eeprom.fitWindow[0], spectrumWidth);

    calculateSimulatedRS(BLslope);

    // Calculating initial concentration value
    O2initial = stack.absorbers[0].concentration_ppm;

    if( simFitData.useSimFitStruct && simFitData.stackFittingLater )
    {
        simFitData.fitData.stack.X1 = (manager->getInterface()->getEVs())["O2"];
        simFitData.fitData.stack.matrX = createMatrixConcentrations(true,false);
        //simFitData.fitData.stack.matrX = createNonTypicalMatrixConcentrations(true,simFitData.fitData.stack.mtxStyle,simFitData.fitData.stack.X1,
        //                                                                      0.0,simFitData.fitData.stack.nameMatrX);
    }

    delete manager;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void spcGenerator::fitToLessPeaksINI()
{
    auto start = std::chrono::high_resolution_clock::now();

    // Setting the directory of the crossstack.ini
    std::string confDirectory = "../FitINI/";

    // Reading Goofy INI (crossstack.ini)
    StructLaserRampArguments eeprom;
    StructStackConfig stack;
    std::vector<StructStackConfig> flanges;
    std::vector<StructError> lmErrors;
    std::string sVersion;

    if( reinitNeeded )
        reinitializeStack(stack);

    // Defining some additional parameters
    std::vector<double> flangeT_C;
    std::vector<double> flangeP_torr;
    double Temp_C = stackPT.stackTemp;
    double P_torr = stackPT.stackPres;

    // Initializing libMASS manager
    manager = SPCFactory::createManager(confDirectory, "O2_760NM");

    if( simFitData.useSimFitStruct )
    {
        readGoofyINI(confDirectory + "crossstack.ini", eeprom);
        // Insert dummy stackTemp and stackPres if reinitializing (output plot in vector space) needed or make sure sim and fit T,P are configured as same
        readStackInputs(broadeningCoeffsINI, stack, Temp_C, P_torr, simFitData, 0);
        readFlangesInputs(broadeningCoeffsINI, flanges, flangeT_C, flangeP_torr, simFitData, 0);
    }
    else
        readGoofyINI(confDirectory + "crossstack.ini", stack, flanges, eeprom, flangeT_C, flangeP_torr);

    sVersion = manager->getVersion();
    lmErrors = manager->getErrors();

    // Initializing necessary interface members
    manager->initStack(stack);
    manager->initFlanges(flanges);

    // Setting floating or fixed PW mode
    if( PWfloating )
        manager->setMode(SPCInterface::kPressureWidthMode);
    else
        manager->setMode(SPCInterface::kDefaultMode);

    std::vector<int> ringdownCount;

    O2concentrationsVector.clear();

    if( noiseGenerator.allNoiseSourcesOff() )
        stackPT.noiseIterationsNum = 1;

    const unsigned long hardware_threads = std::thread::hardware_concurrency();
    unsigned long num_threads = hardware_threads != 0 ? hardware_threads-1 : 1;

    // Loop for fitting with additional threads
    if( !doubledIteration )
    {
        for(unsigned iter = 0; iter < stackPT.noiseIterationsNum; iter+=(num_threads+1) )
        {
            if( (stackPT.noiseIterationsNum-iter) < (num_threads+1) )
                num_threads = stackPT.noiseIterationsNum-iter-1;

            std::vector<std::thread> threadVec;
            threadVec.resize(num_threads);

            for(unsigned j = 0; j < num_threads; ++j)
                threadVec[j] = std::thread(&spcGenerator::singleFitIteration,this,std::ref(flangeT_C),std::ref(flangeP_torr),std::ref(ringdownCount),eeprom,Temp_C,P_torr);

            singleFitIteration(flangeT_C,flangeP_torr,ringdownCount,eeprom,Temp_C,P_torr);

            for(unsigned j = 0; j < num_threads; ++j)
                threadVec[j].join();
        }
    }
    else
    {
        unsigned long addit_threads = num_threads % 2 != 0 ? num_threads : num_threads-1;

        std::vector<double> notAveraged;

        /*for(unsigned iter = 0; iter < stackPT.noiseIterationsNum; iter+=(addit_threads+1)/2)
        {
            if( (stackPT.noiseIterationsNum-iter) < (addit_threads+1)/2 )
                addit_threads = stackPT.noiseIterationsNum-iter-1;

            std::vector<std::thread> available_threads;
            available_threads.resize(addit_threads);

            for(unsigned thr = 0; thr < addit_threads; ++thr)
                available_threads[thr] = std::thread(&spcGenerator::singleFitIteration,this,std::ref(flangeT_C),std::ref(flangeP_torr),std::ref(ringdownCount),eeprom,Temp_C,P_torr);
            singleFitIteration(flangeT_C,flangeP_torr,ringdownCount,eeprom,Temp_C,P_torr);

            for(unsigned thr = 0; thr < addit_threads; ++thr)
                available_threads[thr].join();

            for(unsigned thr = 0; thr < (addit_threads+1)/2; ++thr)
            {
                notAveraged.push_back((O2concentrationsVector[iter+addit_threads-thr]));
                O2concentrationsVector[iter+thr] = (O2concentrationsVector[iter+thr] + O2concentrationsVector[iter+addit_threads-thr])/2;
                O2concentrationsVector.pop_back();
            }
        }*/
        for(unsigned iter = 0; iter < stackPT.noiseIterationsNum; ++iter)
        {
            std::thread t1(&spcGenerator::singleFitIteration,this,std::ref(flangeT_C),std::ref(flangeP_torr),std::ref(ringdownCount),eeprom,Temp_C,P_torr);
            singleFitIteration(flangeT_C,flangeP_torr,ringdownCount,eeprom,Temp_C,P_torr);
            t1.join();

            O2concentrationsVector[iter] = (O2concentrationsVector[iter] + O2concentrationsVector[iter+1])/2;
            notAveraged.push_back(O2concentrationsVector[iter+1]);
            O2concentrationsVector.pop_back();
        }

        O2concentrationsVector.insert(O2concentrationsVector.end(),notAveraged.begin(),notAveraged.end());
    }

    // Producing the fitted light intensity spectrum
    fittedVectorI = manager->getInterface()->getPeakFitWithBL(0);

    // Calculating concentration value after fitting
    O2fitted = (manager->getInterface()->getEVs())["O2"];

    // Unpadding padded vectors
    unpadVector(noisedVector, eeprom.fitWindow[0], eeprom.fitWindow[1]);
    unpadVector(noisedBaseLine, eeprom.fitWindow[0], eeprom.fitWindow[1]);
    unpadVector(ghzPoints, eeprom.fitWindow[0], eeprom.fitWindow[1]);

    // Retrieving simulated and fitted absorbance spectra
    simAbsorbanceVec = manager->getInterface()->getMeasuredData(0); //calculateAbsorbanceVec(fittedVectorI,fittedVectorI0);
    fitAbsorbanceVec = manager->getInterface()->getPeakFit(0); // == -log(fittedVectorI0[i] / noisedBaseLine[i]);

    if( numOfIterations != 1 ) clearVectors();

    delete manager;

    auto end = std::chrono::high_resolution_clock::now();
    printf("****************** SIM TIME: %i sec. ******************\n\n",
           std::chrono::duration_cast<std::chrono::milliseconds>(end - start)); fflush(stdout);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void spcGenerator::singleFitIteration( const std::vector<double>& flangeT_C, const std::vector<double>& flangeP_torr,
                                       std::vector<int>& ringdownCount, StructLaserRampArguments eeprom, double Temp_C, double P_torr )
{
    std::vector<double> workingVector = startingVector;

    /************************ Adding noise in one iteration step ************************/
    // 1. Adding shift noise to the domain
    eeprom.shift_GHz += calculatedFreqShiftNoise();

    // 2. Adding RAM noise
    addRAMnoiseToVecBasedOnBL(workingVector,noisedBaseLine,ghzPoints);

    // 3. Adding white noise to vector based on simulated "raw" signal
    addWhiteNoiseToVecBasedOnRS(workingVector,rawSignal);

    // 4. Adding pressure and temperature noises
    double temperature = Temp_C + calculatedTemperatureNoise();
    double pressure    = P_torr + calculatedPressureNoise();

    std::lock_guard<std::mutex> lck(muti);
    manager->initLaserRampArguments(eeprom);

    ringdownCount.push_back(manager->getInterface()->getRDFitCnt());
    /************************** End of noise addition procedure *************************/

    unsigned int iRampStart = manager->getInterface()->getLaserRampStartIdx();
    unsigned int iRampEnd = iRampStart + manager->getInterface()->getLaserRampLength();

    unsigned int iRingdownStart = manager->getInterface()->getLaserOFFStartIdx();
    unsigned int iRingdownEnd = iRingdownStart + manager->getInterface()->getLaserOFFLength();

    std::vector<double> laserRampData(&(workingVector.at(iRampStart)),
        &(workingVector.at(iRampEnd-1)) + 1);
    std::vector<double> ringdownData(&workingVector.at(iRingdownStart),
        &workingVector.at(iRingdownEnd-1) + 1);

    // Enabling linelock
    manager->getInterface()->setEnableLinelock(0, true);

    manager->getInterface()->computeSpectrum(
        temperature,
        pressure,
        flangeT_C,
        flangeP_torr,
        std::vector<std::vector<double> >(1, laserRampData),
        std::vector<std::vector<double> >(1, ringdownData),
        ringdownCount);

    O2concentrationsVector.push_back( (manager->getInterface()->getEVs())["O2"] );

    noisedVector = std::move(workingVector);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void spcGenerator::calculateSimulatedRS(double A1)
{
    // Vector extrapolation
    unsigned i = 0;
    for_each( rawSignal.begin(), rawSignal.end(), [A1, &i, this]( double& RSval )
    {
        RSval += A1 * ghzPoints[i];
        i++;
    } );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

bool spcGenerator::concentrationsDiffer()
{
    if( abs(O2initial - O2fitted)/O2initial < 0.01 )
        return false;
    else
        return true;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void spcGenerator::addWhiteNoiseToVecBasedOnRS(std::vector<double>& spectrum, const std::vector<double>& RSignal)
{
    noiseGenerator.computeWhiteNoise(spectrum, RSignal);

    for(unsigned i=0; i<RSignal.size(); i++)
        spectrum[i] += noiseGenerator.getWhiteNoise()[i];
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void spcGenerator::addRAMnoiseToVecBasedOnBL(std::vector<double>& spectrum, const std::vector<double>& BLine,
                                             const std::vector<double>& freqDomain)
{
    noiseGenerator.computeAlfaRAM(freqDomain);

    for(unsigned i=0; i<BLine.size(); i++)
        spectrum[i] = BLine[i] * (1.0 - (spectrum[i] + noiseGenerator.getRAMnoise()[i]));
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void spcGenerator::addTransmissionNoiseToBL(std::vector<double>& spectrum)
{
    noiseGenerator.computeTransmissionNoise();

    for_each(spectrum.begin(), spectrum.end(), [this]( double& val ){
        val *= noiseGenerator.getTransmissionNoise();
    });
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

double spcGenerator::calculatedPressureNoise()
{
    noiseGenerator.computePressureNoise();
    return noiseGenerator.getPressureNoise();
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

double spcGenerator::calculatedTemperatureNoise()
{
    noiseGenerator.computeTemperatureNoise();
    return noiseGenerator.getTemperatureNoise();
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

double spcGenerator::calculatedFreqShiftNoise()
{
    noiseGenerator.computeFreqShiftNoise();
    return noiseGenerator.getFreqShiftNoise();
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void spcGenerator::initializePTfromINI()
{
    const std::string inputINIpath = "../MasterINI";
    std::ifstream inputINI;
    inputINI.open(inputINIpath + "/input_stackPT.ini", std::ios::in);

    std::string lineFromFile, key;
    while(getline(inputINI, lineFromFile))
    {
        std::istringstream strstr(lineFromFile);
        key = "";
        strstr >> key;
        std::transform(key.begin(), key.end(), key.begin(), ::toupper);

        if(key == "INPUT_STACK_TEMPERATURE")
            strstr >> stackPT.stackTemp;
        else if(key == "INPUT_STACK_PRESSURE")
            strstr >> stackPT.stackPres;
        else if(key == "NUMBER_OF_NOISE_ITERATIONS")
            strstr >> stackPT.noiseIterationsNum;
    }

    inputINI.close();
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void spcGenerator::initializeENTUinputsFromINI()
{
    std::ifstream inputINI;
    inputINI.open(entuInputsINI, std::ios::in);

    if( !inputINI.is_open() )
    {
        std::cerr << "eNTUinputs.ini not found!" << std::endl;
        return;
    }

    std::string lineFromFile, key;
    while(getline(inputINI, lineFromFile))
    {
        if (lineFromFile.length() < 1 || lineFromFile.at(0) == '#')
        {
            // Skip blank lines or comments (lines starting with #)
        }
        else
        {
            std::istringstream strstr(lineFromFile);
            // Read a key and convert to uppercase
            key = "";
            strstr >> key;
            std::transform(key.begin(), key.end(), key.begin(), ::toupper);

            if(key == "VOLUME_FLOW_RATE_IT")
                strstr >> eNTUinputs.VdotIT;
            else if(key == "VELOCITY_IT")
                strstr >> eNTUinputs.VelIT;
            else if(key == "MASS_FLOW_RATE_IT")
                strstr >> eNTUinputs.mdotIT;
            else if(key == "VELOCITY_S")
                strstr >> eNTUinputs.VelS;
            else if(key == "MASS_FLOW_RATE_S")
                strstr >> eNTUinputs.mdotS;
            else if(key == "OUTER_DIAMETER_IT")
                strstr >> eNTUinputs.Do;
            else if(key == "INNER_DIAMETER_IT")
                strstr >> eNTUinputs.Di;
            else if(key == "FOULING_RESISTANCE_INSIDE_IT")
                strstr >> eNTUinputs.RfiBis;
            else if(key == "FOULING_RESISTANCE_OUTSIDE_IT")
                strstr >> eNTUinputs.RfoBis;
            else if(key == "INLET_TEMPERATURE")
            {
                std::vector<double> inletTemps, varValues;
                double number;
                while( strstr >> number )
                    inletTemps.push_back( K2degC(number) );

                getline(inputINI, lineFromFile);
                std::istringstream sstr(lineFromFile);
                // Read a key and convert to uppercase
                key = "";
                sstr >> key;
                std::transform(key.begin(), key.end(), key.begin(), ::toupper);

                while( sstr >> number )
                    varValues.push_back(number);

                if(key == "IT_SPEC_HEAT_COEF_AIR" && insTubePurgeGas == "Air")
                    eNTUinputs.cpIT = interpolate1dVector(inputs.ATtyp,std::move(inletTemps),std::move(varValues));
                else if(key == "IT_SPEC_HEAT_COEF_CUS" && insTubePurgeGas == "custom")
                    eNTUinputs.cpIT = interpolate1dVector(inputs.ATtyp,std::move(inletTemps),std::move(varValues));
                else if(key == "IT_VISCOSITY_AIR" && insTubePurgeGas == "Air")
                    eNTUinputs.muIT = interpolate1dVector(inputs.ATtyp,std::move(inletTemps),std::move(varValues));
                else if(key == "IT_VISCOSITY_CUS" && insTubePurgeGas == "custom")
                    eNTUinputs.muIT = interpolate1dVector(inputs.ATtyp,std::move(inletTemps),std::move(varValues));
                else if(key == "IT_THERMAL_COND_AIR" && insTubePurgeGas == "Air")
                    eNTUinputs.kIT = interpolate1dVector(inputs.ATtyp,std::move(inletTemps),std::move(varValues));
                else if(key == "IT_THERMAL_COND_CUS" && insTubePurgeGas == "custom")
                    eNTUinputs.kIT = interpolate1dVector(inputs.ATtyp,std::move(inletTemps),std::move(varValues));
                else if(key == "S_SPEC_HEAT_COEF_AIR" && insTubePurgeGas == "Air")
                    eNTUinputs.cpS = interpolate1dVector(inputs.PTtyp,std::move(inletTemps),std::move(varValues));
                else if(key == "S_SPEC_HEAT_COEF_CUS" && insTubePurgeGas == "custom")
                    eNTUinputs.cpS = interpolate1dVector(inputs.PTtyp,std::move(inletTemps),std::move(varValues));
                else if(key == "S_VISCOSITY_AIR" && insTubePurgeGas == "Air")
                    eNTUinputs.muS = interpolate1dVector(inputs.PTtyp,std::move(inletTemps),std::move(varValues));
                else if(key == "S_VISCOSITY_CUS" && insTubePurgeGas == "custom")
                    eNTUinputs.muS = interpolate1dVector(inputs.PTtyp,std::move(inletTemps),std::move(varValues));
                else if(key == "S_THERMAL_COND_AIR" && insTubePurgeGas == "Air")
                    eNTUinputs.kS = interpolate1dVector(inputs.PTtyp,std::move(inletTemps),std::move(varValues));
                else if(key == "S_THERMAL_COND_CUS" && insTubePurgeGas == "custom")
                    eNTUinputs.kS = interpolate1dVector(inputs.PTtyp,std::move(inletTemps),std::move(varValues));
                else if(key == "THERMAL_COND_STEEL" && insTubeMaterial == "steel")
                    eNTUinputs.k = interpolate1dVector(0.5*(inputs.ATtyp+inputs.PTtyp),std::move(inletTemps),std::move(varValues));
                else if(key == "THERMAL_COND_BRONZE" && insTubeMaterial == "bronze")
                    eNTUinputs.k = interpolate1dVector(0.5*(inputs.ATtyp+inputs.PTtyp),std::move(inletTemps),std::move(varValues));
            }
            else if(key == "INLET_TEMPERATURE_VEC")
            {
                std::vector<double> inletTemps, processPress;
                std::vector<std::vector<double> > varValues;
                double number;
                while( strstr >> number )
                    inletTemps.push_back( K2degC(number) );

                getline(inputINI, lineFromFile);
                std::istringstream sstr(lineFromFile);
                // Read a key and convert to uppercase
                key = "";
                sstr >> key;
                std::transform(key.begin(), key.end(), key.begin(), ::toupper);

                if(key != "PROCESS_PRESSURE_VEC")
                    throw std::runtime_error("Error! eNTUinputs.ini badly set!");

                while( sstr >> number )
                    processPress.push_back(number);

                for(unsigned i=0; i<processPress.size(); ++i)
                {
                    getline(inputINI, lineFromFile);
                    std::istringstream sstr(lineFromFile);

                    if(i == 0)
                    {
                        // Read a key and convert to uppercase
                        key = "";
                        sstr >> key;
                        std::transform(key.begin(), key.end(), key.begin(), ::toupper);
                    }

                    varValues.push_back(std::vector<double>());
                    while( sstr >> number )
                        varValues.back().push_back(number);
                }

                /*if(key == "IT_DENISTY_AIR" && insTubePurgeGas == "Air")
                    eNTUinputs.rhoIT = interpolate2dVector(inputs.PPtyp,inputs.ATtyp,std::move(processPress),std::move(inletTemps),std::move(varValues));
                else if(key == "IT_DENISTY_CUS" && insTubePurgeGas == "custom")
                    eNTUinputs.rhoIT = interpolate2dVector(inputs.PPtyp,inputs.ATtyp,std::move(processPress),std::move(inletTemps),std::move(varValues));
                else if(key == "S_DENISTY_AIR" && insTubePurgeGas == "Air")
                    eNTUinputs.rhoS = interpolate2dVector(inputs.PPtyp,inputs.ATtyp,std::move(processPress),std::move(inletTemps),std::move(varValues));
                else if(key == "S_DENISTY_CUS" && insTubePurgeGas == "custom")
                    eNTUinputs.rhoS = interpolate2dVector(inputs.PPtyp,inputs.ATtyp,std::move(processPress),std::move(inletTemps),std::move(varValues));*/
            }
        }
    }

    inputINI.close();
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void spcGenerator::readMolecularWeights()
{
    std::ifstream inputINI;
    inputINI.open(molWeightsINI, std::ios::in);

    if( !inputINI.is_open() )
    {
        std::cerr << "eNTUinputs.ini not found!" << std::endl;
        return;
    }

    unsigned insTubeIndex = 0;
    std::vector<double> stackMolWeights, insTubeMolWeights;
    stackMolWeights.resize(simFitData.simData.stack.matrX.size()+2);

    for(unsigned i=0; i<simFitData.simData.flanges.size(); ++i)
        if( simFitData.simData.flanges[i].isInsTube )
            insTubeIndex = i+1;

    if( insTubeIndex )
        insTubeMolWeights.resize(simFitData.simData.flanges[insTubeIndex-1].matrX.size()+2);

    std::string lineFromFile, key;
    while(getline(inputINI, lineFromFile))
    {
        if (lineFromFile.length() < 1 || lineFromFile.at(0) == '#')
        {
            // Skip blank lines or comments (lines starting with #)
        }
        else
        {
            std::istringstream strstr(lineFromFile);
            // Read a key and convert to uppercase
            key = "";
            strstr >> key;

            double flagValue = -1.0;
            if( key.compare(0, inputs.gas1.length(), inputs.gas1) == 0 )
            {
                strstr >> stackMolWeights[0];
                if( insTubeIndex ) insTubeMolWeights[0] = stackMolWeights[0];
            }
            if( key.compare(0, inputs.gas2.length(), inputs.gas2) == 0 )
            {
                strstr >> stackMolWeights[1];
                if( insTubeIndex ) insTubeMolWeights[1] = stackMolWeights[1];
            }
            for(unsigned i=0; i<simFitData.simData.stack.matrX.size(); ++i)
                if( key.compare(0, simFitData.simData.stack.nameMatrX[i].length(), simFitData.simData.stack.nameMatrX[i]) == 0 )
                {
                    strstr >> stackMolWeights[i+2];
                    flagValue = stackMolWeights[i+2];
                }
            if( insTubeIndex )
                for(unsigned i=0; i<simFitData.simData.flanges[insTubeIndex-1].matrX.size(); ++i)
                    if( key.compare(0, simFitData.simData.flanges[insTubeIndex-1].nameMatrX[i].length(),
                                    simFitData.simData.flanges[insTubeIndex-1].nameMatrX[i]) == 0 )
                    {
                        if( flagValue >= 0.0 )
                            insTubeMolWeights[i+2] = flagValue;
                        else
                            strstr >> insTubeMolWeights[i+2];
                    }
        }
    }

    eNTUinputs.MwS = stackMolWeights[0]*simFitData.simData.stack.X1 + stackMolWeights[1]*simFitData.simData.stack.X2;

    for(unsigned i=0; i<simFitData.simData.stack.matrX.size(); ++i)
        eNTUinputs.MwS += (stackMolWeights[i+2] * simFitData.simData.stack.matrX[i]);

    eNTUinputs.MwS /= ( simFitData.simData.stack.X1 + simFitData.simData.stack.X2
                        + std::accumulate(simFitData.simData.stack.matrX.begin(),simFitData.simData.stack.matrX.end(),0.0) );

    if( insTubeIndex )
    {
        eNTUinputs.MwIT = insTubeMolWeights[0]*simFitData.simData.flanges[insTubeIndex-1].X1 +
                          insTubeMolWeights[1]*simFitData.simData.flanges[insTubeIndex-1].X2;

        for(unsigned i=0; i<simFitData.simData.flanges[insTubeIndex-1].matrX.size(); ++i)
            eNTUinputs.MwIT += (insTubeMolWeights[i+2] * simFitData.simData.flanges[insTubeIndex-1].matrX[i]);

        eNTUinputs.MwIT /= ( simFitData.simData.flanges[insTubeIndex-1].X1 + simFitData.simData.flanges[insTubeIndex-1].X2
                            + std::accumulate(simFitData.simData.flanges[insTubeIndex-1].matrX.begin(),
                                              simFitData.simData.flanges[insTubeIndex-1].matrX.end(),0.0) );
    }

    inputINI.close();
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void spcGenerator::reinitializeStack( StructStackConfig& outStack )
{
    switch( mulCdata.vecVarIndex )
    {
    case 0:
        for_each(outStack.absorbers.begin(), outStack.absorbers.end(), [this](StructAbsorber& absorb)
        {
            if( absorb.moleculeName == "O2" )
                absorb.concentration_ppm = mulCdata.O2concen;
            else if( absorb.moleculeName == "N2" )
                absorb.concentration_ppm = 1e6 /*- std::accumulate(inputs.matxTyps.begin(),inputs.matxTyps.end(),0.0)*/ - mulCdata.O2concen; //
        });

        outStack.length_m = mulCdata.pathLen;
        stackPT.stackPres = mulCdata.pres;
        stackPT.stackTemp = mulCdata.temp;
        break;
    case 1:
        for_each(outStack.absorbers.begin(), outStack.absorbers.end(), [this](StructAbsorber& absorb)
        {
            if( absorb.moleculeName == "O2" )
                absorb.concentration_ppm = mulCdata.vecVariable[iterNo];
            else if( absorb.moleculeName == "N2" )
                absorb.concentration_ppm = 1e6 /*- std::accumulate(inputs.matxTyps.begin(),inputs.matxTyps.end(),0.0)*/ - mulCdata.vecVariable[iterNo]; //
        });

        outStack.length_m = mulCdata.pathLen;
        stackPT.stackPres = mulCdata.pres;
        stackPT.stackTemp = mulCdata.temp;
        break;
    case 2:
        for_each(outStack.absorbers.begin(), outStack.absorbers.end(), [this](StructAbsorber& absorb)
        {
            if( absorb.moleculeName == "O2" )
                absorb.concentration_ppm = mulCdata.O2concen;
            else if( absorb.moleculeName == "N2" )
                absorb.concentration_ppm = 1e6 /*- std::accumulate(inputs.matxTyps.begin(),inputs.matxTyps.end(),0.0)*/ - mulCdata.O2concen; //
        });

        outStack.length_m = mulCdata.vecVariable[iterNo];
        stackPT.stackPres = mulCdata.pres;
        stackPT.stackTemp = mulCdata.temp;
        break;
    case 3:
        for_each(outStack.absorbers.begin(), outStack.absorbers.end(), [this](StructAbsorber& absorb)
        {
            if( absorb.moleculeName == "O2" )
                absorb.concentration_ppm = mulCdata.O2concen;
            else if( absorb.moleculeName == "N2" )
                absorb.concentration_ppm = 1e6 /*- std::accumulate(inputs.matxTyps.begin(),inputs.matxTyps.end(),0.0)*/ - mulCdata.O2concen; //
        });

        outStack.length_m = mulCdata.pathLen;
        stackPT.stackPres = mulCdata.vecVariable[iterNo];
        stackPT.stackTemp = mulCdata.temp;
        break;
    case 4:
        for_each(outStack.absorbers.begin(), outStack.absorbers.end(), [this](StructAbsorber& absorb)
        {
            if( absorb.moleculeName == "O2" )
                absorb.concentration_ppm = mulCdata.O2concen;
            else if( absorb.moleculeName == "N2" )
                absorb.concentration_ppm = 1e6 /*- std::accumulate(inputs.matxTyps.begin(),inputs.matxTyps.end(),0.0)*/ - mulCdata.O2concen; //
        });

        outStack.length_m = mulCdata.pathLen;
        stackPT.stackPres = mulCdata.pres;
        stackPT.stackTemp = mulCdata.vecVariable[iterNo];
        break;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

const spcGenerator::fromInputINI* spcGenerator::getStackPTptr() const
{
    if( stackPT.noiseIterationsNum != 0 )
        return &stackPT;
    else
        return 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void spcGenerator::clearVectors()
{
    rawSignal.clear();
    noisedBaseLine.clear();
    startingVector.clear();
    ghzPoints.clear();
    //O2concentrationsVector.clear();
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

double spcGenerator::calculateConcentrationMeanVal( unsigned start, unsigned finish ) const
{
    double startVal = 0.0;

    for_each( O2concentrationsVector.begin()+start, O2concentrationsVector.begin()+finish, [&startVal](double vecVal){ startVal += vecVal; } );

    return ( startVal / (finish - start) );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

double spcGenerator::calculateConcentrationStdDev( unsigned start, unsigned finish ) const
{
    double startVal = 0.0;
    double meanVal = calculateConcentrationMeanVal(start,finish);

    for_each( O2concentrationsVector.begin()+start, O2concentrationsVector.begin()+finish, [&](double vecVal)
    {
        startVal += (vecVal - meanVal)*(vecVal - meanVal);
    });

    return sqrt( startVal / (finish - start - 1) );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

std::vector<double> spcGenerator::createMatrixConcentrations( bool forStack, bool forSim )
{
    double mainGas1Conc, mainGas2Conc;
    std::vector<std::string> matxGasNames;
    GASCONFIG_SIM_FIT_STRUCT::mtxCreationStyles mtxCreationStyle;

    if( forStack )
    {
        if( forSim )
        {
            mainGas1Conc     = simFitData.simData.stack.X1;
            mainGas2Conc     = simFitData.simData.stack.X2;
            matxGasNames     = simFitData.simData.stack.nameMatrX;
            mtxCreationStyle = simFitData.simData.stack.mtxStyle;
        }
        else // == forFit
        {
            mainGas1Conc     = simFitData.fitData.stack.X1;
            mainGas2Conc     = simFitData.fitData.stack.X2;
            matxGasNames     = simFitData.fitData.stack.nameMatrX;
            mtxCreationStyle = simFitData.fitData.stack.mtxStyle;
        }
    }
    else // == forFlange
    {
        if( forSim )
        {
            mainGas1Conc     = simFitData.simData.flanges.back().X1;
            mainGas2Conc     = simFitData.simData.flanges.back().X2;
            matxGasNames     = simFitData.simData.flanges.back().nameMatrX;
            mtxCreationStyle = simFitData.simData.flanges.back().mtxStyle;
        }
        else // == forFit
        {
            mainGas1Conc     = simFitData.fitData.flanges.back().X1;
            mainGas2Conc     = simFitData.fitData.flanges.back().X2;
            matxGasNames     = simFitData.fitData.flanges.back().nameMatrX;
            mtxCreationStyle = simFitData.fitData.flanges.back().mtxStyle;
        }
    }

    if( mtxCreationStyle )
        return createNonTypicalMatrixConcentrations(forStack,mtxCreationStyle,mainGas1Conc,mainGas2Conc,matxGasNames);
    else
        return createTypicalMatrixConcentrations(forStack,mainGas1Conc,mainGas2Conc,matxGasNames);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

std::vector<double> spcGenerator::createTypicalMatrixConcentrations( bool forStack, double mainGas1Conc, double mainGas2Conc,
                                                                     const std::vector<string>& matxGasNames )
{
    std::vector<double> workVec( matxGasNames.size() );
    double X1, X2;

    if( mainGas1Conc + mainGas2Conc == 0.0 )
    {
        X1 = inputs.concTyp1;
        X2 = inputs.concTyp2;
    }
    else
    {
        X1 = mainGas1Conc;
        X2 = mainGas2Conc;
    }

    bool searchError = true;

    // Check for matrix gases' typical concentrations read from inputs
    if( forStack )
    {
        for(unsigned i=0; i<matxGasNames.size(); i++)
        {
            for(unsigned j=0; j<inputs.matxGases.size(); j++)
            {
                if( matxGasNames[i] == inputs.matxGases[j] )
                {
                    workVec[i] = inputs.matxTyps[j];
                    searchError = false;
                    break;
                }
            }
            if( searchError ) std::cerr << "Error! Gas name not found in input data." << std::endl;
        }
    }
    else // == forFlange
    {
        for(unsigned i=0; i<matxGasNames.size(); i++)
        {
            if( matxGasNames[i] == inputs.purMatxGas1 )
                workVec[i] = inputs.purMatxConc1;
            else if( matxGasNames[i] == inputs.purMatxGas2 )
                workVec[i] = inputs.purMatxConc2;
            else
                std::cerr << "Error! Gas name not found in input data." << std::endl;
        }
    }

    double matxConcSum = std::accumulate(workVec.begin(),workVec.end(),0.0);
    if( doubleEquals(X1 + X2 + matxConcSum, 1.0e6) )
        return workVec;
    else
    {
        std::vector<double> retVec = workVec;

        for(unsigned i=0; i<workVec.size(); i++)
            retVec[i] = workVec[i] / matxConcSum * (1.0e6 - mainGas1Conc - mainGas2Conc);

        return retVec;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

std::vector<double> spcGenerator::createNonTypicalMatrixConcentrations( bool forStack, GASCONFIG_SIM_FIT_STRUCT::mtxCreationStyles mtxCreationStyle,
                                                                        double mainGas1Conc, double mainGas2Conc,
                                                                        const std::vector<std::string>& matxGasNames )
{
    std::vector<double> broadCoeffs( matxGasNames.size() );

    std::string lineFromFile, gasName1, gasName2;

    // Opening AllBroadeningCoeffs.ini
    std::ifstream broadeningCoeffsFile;
    broadeningCoeffsFile.open(broadeningCoeffsINI.c_str(), std::ios::in);

    if( !broadeningCoeffsFile.is_open() )
    {
        std::cerr << "Program terminated, AllBroadeningCoefficients.ini not found!" << std::endl;
        exit(112);
    }

    // Reading broadening coefficients for matrix gases
    for(unsigned i=0; i<matxGasNames.size(); i++)
    {
        if( matxGasNames[i] == inputs.gas1 )
        {
            std::cerr << "Error! Duplication of main gas component by one of matrix gases!" << std::endl;
            exit(113);
        }

        broadeningCoeffsFile.clear();
        broadeningCoeffsFile.seekg(0, ios::beg);

        // Ignore first two lines of INI file
        getline(broadeningCoeffsFile, lineFromFile);
        getline(broadeningCoeffsFile, lineFromFile);

        while( getline(broadeningCoeffsFile, lineFromFile) )
        {
            std::istringstream ss(lineFromFile);
            gasName1.clear();
            gasName2.clear();

            ss >> gasName1;

            if( gasName1 == matxGasNames[i] )
            {
                ss >> gasName2;
                if( gasName2 == inputs.gas1 )
                {
                    ss >> broadCoeffs[i];
                    break;
                }
                else
                {
                    ss.ignore(128,'\t');
                    ss >> gasName2;
                    if( gasName2 == matxGasNames[i] )
                    {
                        ss >> broadCoeffs[i];
                        break;
                    }
                    else
                    {
                        std::cerr << "Error! Broadening coefficient for gases " << matxGasNames[i]
                                  << " and " << inputs.gas1 << " not found." << std::endl;
                        exit(114);
                    }
                }
            }
        }
    }

    // Some auxiliary structs
    struct gasStruct
    {
        unsigned    origPos;
        double      gasConc, maxGasConc, gasBroadeningCoeff;
        std::string gasName;
    };

    struct byBroadeningCoeff
    {
        bool operator()( const gasStruct& gas1, const gasStruct& gas2 ) { return gas1.gasBroadeningCoeff < gas2.gasBroadeningCoeff; }
    };

    struct byOriginalPos
    {
        bool operator()( const gasStruct& gas1, const gasStruct& gas2 ) { return gas1.origPos < gas2.origPos; }
    };

    double availablePPMs = 1.0e6 - mainGas1Conc - mainGas2Conc;

    std::vector<gasStruct> gases( matxGasNames.size() );

    for(unsigned i=0; i<gases.size(); i++)
        gases[i] = { i, 0.0, availablePPMs, broadCoeffs[i], matxGasNames[i] };

    // Sorting vector of gases from lowest broadening coefficient to highest
    std::sort(gases.begin(), gases.end(), byBroadeningCoeff() );

    if( mtxCreationStyle == GASCONFIG_SIM_FIT_STRUCT::basic )
    {
        std::cerr << "Error! Basic style of gas matrix set for non-typical matrix creation!\n";
        exit(115);
    }

    if( mtxCreationStyle == GASCONFIG_SIM_FIT_STRUCT::maxPW ) // == from highest to lowest
        std::reverse(gases.begin(),gases.end());

    bool searchError = true;

    // Check for matrix gases' minimum concentrations read from inputs
    if( forStack )
    {
        for(unsigned i=0; i<gases.size(); ++i)
        {
            for(unsigned j=0; j<inputs.matxGases.size(); ++j)
            {
                if( gases[i].gasName == inputs.matxGases[j] )
                {
                    gases[i].gasConc    = inputs.matxMins[j];
                    gases[i].maxGasConc = inputs.matxMaxs[j];
                    searchError = false;
                    break;
                }
            }
            if( searchError ) std::cerr << "Error! Gas name " << gases[i].gasName << " not found in input data." << std::endl;
        }
    }
    else // == forFlange
    {
        for(unsigned i=0; i<gases.size(); i++)
        {
            if( gases[i].gasName == inputs.purMatxGas1 )
                gases[i].gasConc = inputs.purMatxConc1;
            else if( gases[i].gasName == inputs.purMatxGas2 )
                gases[i].gasConc = inputs.purMatxConc2;
            else
                std::cerr << "Error! Gas name not found in input data." << std::endl;
        }
    }

    // Creating return vector
    std::vector<double> retVec;

    // Algorithm for assigning maximum gas concentration in ascending broadening coefficients' order
    double matxConcSum = 0.0;
    for(unsigned i=0; i<gases.size(); i++)
    {
        gases[i].gasConc = gases[i].maxGasConc;

        matxConcSum = 0.0;
        for(unsigned j=0; j<gases.size(); j++)
            matxConcSum += gases[j].gasConc;

        if( (matxConcSum - availablePPMs) > 0.1 )
        {
            gases[i].gasConc -= (matxConcSum - availablePPMs);

            // Moving back to original order
            std::sort(gases.begin(), gases.end(), byOriginalPos() );

            for(unsigned j=0; j<gases.size(); j++)
                retVec.push_back(gases[j].gasConc);

            return retVec;
        }
    }

    // Moving back to original order
    std::sort(gases.begin(), gases.end(), byOriginalPos() );

    // If sum of all gases is still less than 1
    for(unsigned i=0; i<gases.size(); i++)
        retVec.push_back(gases[i].gasConc * (availablePPMs/matxConcSum));

    return retVec;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

std::vector<double> spcGenerator::calculateResidualsVec( const std::vector<double>& I, const std::vector<double>& I0 )
{
    std::vector<double> transmissionVec, absorbanceVec;
    unsigned length = I.size();

    if( I.size() != I0.size() )
        std::cerr << "Warning! I and I0 vectors are of different lengths." << std::endl;

    for(unsigned i=0; i<length; i++)
        transmissionVec.push_back( I[i]/I0[i] );

    for(unsigned i=0; i<length; i++)
        absorbanceVec.push_back( -log(transmissionVec[i]) );

    return absorbanceVec;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void spcGenerator::collimationCalcs()
{
    /*double totalFlangeL = 0.0;

    for( auto itr = simFitData.simData.flanges.begin(), end = simFitData.simData.flanges.end(); itr != end; itr++ )
        totalFlangeL += (*itr).L;

    totalPL = simFitData.simData.stack.L + totalFlangeL;

    if( totalPL >= 300.0 && totalPL <= 2.0e5 )
    {
        if( totalPL > gasConfig.collimXlen )
            outputs.collimation = "diverging";
        else
            outputs.collimation = "collimated";
    }
    else
        outputs.collimation = "PL out of range";*/

    if( inputs.L1 + inputs.L2 + inputs.L3 <= gasConfig.collimXlen )
        outputs.collimation = "collimated";
    else
        outputs.collimation = "diverging";
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void spcGenerator::setConfigsToDefaults()
{
    simFitData = GASCONFIG_SIM_FIT_STRUCT();
    gasConfig  = GASCONFIG_VARIABLES_STRUCT();

    noiseGenerator   = NoiseGenerator();
    numOfIterations  = 1;
    iterNo           = 0;
    reinitNeeded     = false;
    doubledIteration = false;
    PWfloating       = false;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void spcGenerator::recalculateTrMin()
{
    auto prev = noiseGenerator.getTransmissionNoiseStruct().dustType;

    noiseGenerator.getTransmissionNoiseStruct().setDustType(0);
    calculateTransmissionSF();
    outputs.Tr_IT_max = noiseGenerator.getTransmissionNoise();

    noiseGenerator.getTransmissionNoiseStruct().setDustType(2);
    calculateTransmissionSF();
    outputs.Tr_IT_typ = noiseGenerator.getTransmissionNoise();

    noiseGenerator.getTransmissionNoiseStruct().setDustType(1);
    calculateTransmissionSF();
    Tr_min = noiseGenerator.getTransmissionNoise();
    outputs.Tr_IT_min = Tr_min;

    // Back to previous
    noiseGenerator.getTransmissionNoiseStruct().setDustType(prev);

    calculateTransmissionSF();
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void spcGenerator::calculateInitialTrMin()
{
    noiseGenerator.getTransmissionNoiseStruct().setDustType(0);
    noiseGenerator.getTransmissionNoiseStruct().calculateSF(inputs,outputs,true);
    outputs.Tr_orig_max = noiseGenerator.getTransmissionNoise();

    noiseGenerator.getTransmissionNoiseStruct().setDustType(2);
    noiseGenerator.getTransmissionNoiseStruct().calculateSF(inputs,outputs,true);
    outputs.Tr_orig_typ = noiseGenerator.getTransmissionNoise();

    noiseGenerator.getTransmissionNoiseStruct().setDustType(1);
    noiseGenerator.getTransmissionNoiseStruct().calculateSF(inputs,outputs,true);
    Tr_min = noiseGenerator.getTransmissionNoise();
    outputs.Tr_orig_min = Tr_min;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void spcGenerator::recalcTransmissionSFaccToDust( unsigned dust )
{
    noiseGenerator.getTransmissionNoiseStruct().setDustType(dust);
    calculateTransmissionSF();
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void spcGenerator::conditionsWithinBounds()
{
    outputs.within_limits = false;

    if( !(inputs.ATmin < gasConfig.ATLimMin ) )
    {
        if( !(inputs.ATmax > gasConfig.ATLimMax) )
        {
            if( !(inputs.range1 < gasConfig.rangeLimMin) )
            {
                if( !(inputs.range1 > gasConfig.rangeLimMax) )
                {
                    if( !(inputs.PPmin < gasConfig.PPLimMin) )
                    {
                        if( !(inputs.PPmax > gasConfig.PPLimMax) )
                        {
                            if( !(inputs.PTmin < gasConfig.PTLimMin) )
                            {
                                if( !(inputs.PTmax > gasConfig.PTLimMax) )
                                {
                                    if( !(inputs.L2 < gasConfig.OPLenMin) )
                                    {
                                        if( !((inputs.L1 + inputs.L2 + inputs.L3) > gasConfig.OPLenMax) )
                                        {
                                            if( !(Tr_min < 0.01*gasConfig.transmMin) )
                                                outputs.within_limits = true;
                                            else outputs.out_of_limits = "Transmission below acceptable range";
                                        }
                                        else outputs.out_of_limits = "Overall path length too long";
                                    }
                                    else outputs.out_of_limits = "Stack path length too short";
                                }
                                else outputs.out_of_limits = "Temperature above acceptable range";
                            }
                            else outputs.out_of_limits = "Temperature below acceptable range";
                        }
                        else outputs.out_of_limits = "Pressure above acceptable range";
                    }
                    else outputs.out_of_limits = "Pressure below acceptable range";
                }
                else outputs.out_of_limits = "Range above acceptable range";
            }
            else outputs.out_of_limits = "Range below acceptable range";
        }
        else outputs.out_of_limits = "Ambient Temperature above acceptable range";
    }
    else outputs.out_of_limits = "Ambient Temperature below acceptable range";
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void spcGenerator::insTubeConditions()
{
    calculateInitialTrMin();

    outputs.L_ins_tube = 0.0;
    outputs.T_ins_tube_typ = outputs.T_ins_tube_max = outputs.T_ins_tube_min = 0.0;

    // Is insertion tube physically feasible?
    if( inputs.L2 - (2*gasConfig.insTubeLenOpt - inputs.L1 - inputs.L3) >= gasConfig.OPLenMin )
        outputs.fit_ins_tube = true;
    else
        outputs.fit_ins_tube = false;

    outputs.ins_tube = true;
    if( outputs.fit_ins_tube )
    {
        // Is insertion tube needed or requested by the user?
        if( Tr_min >= 0.01*gasConfig.transmMin && !inputs.insTube )
            outputs.ins_tube = false;

        // Decide on insertion tube length
        if( outputs.ins_tube )
        {
            outputs.L_ins_tube = 2*gasConfig.insTubeLenOpt - inputs.L1 - inputs.L3;
            if( outputs.L_ins_tube < 0.0 ) outputs.L_ins_tube = 0.0;
            recalculateTrMin();
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void spcGenerator::IRfilterPresent()
{
    if( inputs.PTmax >= gasConfig.filterXtemp )
        outputs.Filter = true;
    else
        outputs.Filter = false;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void spcGenerator::spectralScanRate()
{
    if( inputs.PPmin < gasConfig.scanXpres )
        outputs.Scan_Rate = 1.0;
    else
        outputs.Scan_Rate = 4.0;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

double spcGenerator::calculateInsTubeTempViaENTU( double TITinDegC, double TSinDegC )
{
    double TITinK = degC2K(TITinDegC);
    double TSinK  = degC2K(TSinDegC);
    double Lhalf  = 1.0e-3 * outputs.L_ins_tube / 2;

    // Calculating necessary introduction variables
    eNTUinputs.rhoIT  = inputs.PPtyp * eNTUinputs.MwIT / (eNTUinputs.R * TITinK);
    eNTUinputs.rhoS   = inputs.PPtyp * eNTUinputs.MwS / (eNTUinputs.R * TSinK);
    eNTUinputs.VelIT  = eNTUinputs.VdotIT * 1.e-3 * 4.0 / ( M_PI * eNTUinputs.Di * eNTUinputs.Di * 60.0 );
    eNTUinputs.mdotIT = eNTUinputs.VdotIT * 1.e-3 * eNTUinputs.rhoIT / 60.0;
    eNTUinputs.mdotS  = eNTUinputs.VelS * eNTUinputs.rhoS * M_PI * inputs.L2 * inputs.L2 * 1.0e-6 / 4.0;

    // Calculating convection coefficient inside the insertion tube
    double ReD = 4 * eNTUinputs.mdotIT / (M_PI * eNTUinputs.Di * eNTUinputs.muIT);
    double Pr  = eNTUinputs.cpIT * eNTUinputs.muIT / eNTUinputs.kIT;

    double n;
    if( TSinK < TITinK )
        n = 0.3;
    else if( TSinK > TITinK )
        n = 0.4;
    else
        return K2degC(TITinK);

    double NuD = 0.023 * std::pow(ReD,0.8) * std::pow(Pr,n);
    double hi  = NuD * eNTUinputs.kIT / eNTUinputs.Di;

    // Calculating convection coefficient outside the insertion tube
    ReD = eNTUinputs.rhoS * eNTUinputs.VelS * eNTUinputs.Do / eNTUinputs.muS;
    Pr  = eNTUinputs.cpS * eNTUinputs.muS / eNTUinputs.kS;

    double C, m;
    if( /*ReD >= 0.4 &&*/ ReD < 4.0 )
    {
        C = 0.989;
        m = 0.330;
    }
    else if( ReD >= 4.0 && ReD < 40.0 )
    {
        C = 0.911;
        m = 0.385;
    }
    else if( ReD >= 40.0 && ReD < 4.0e3 )
    {
        C = 0.683;
        m = 0.466;
    }
    else if( ReD >= 4.0e3 && ReD < 4.0e4 )
    {
        C = 0.196;
        m = 0.618;
    }
    else //if( ReD >= 4.0e4 && ReD < 4.0e5 )
    {
        C = 0.027;
        m = 0.805;
    }
    //else
    //    throw std::runtime_error("Reynolds number outside the limits!");

    NuD = C * std::pow(ReD,m) * std::pow(Pr,1.0/3);
    double ho = NuD * eNTUinputs.kS / eNTUinputs.Do;

    // Calculating the overall heat transfer coefficient
    double Ai = Lhalf * M_PI * eNTUinputs.Di;
    double Ao = Lhalf * M_PI * eNTUinputs.Do;
    double UA = 1/( 1/(hi*Ai) + eNTUinputs.RfiBis/Ai + log(eNTUinputs.Do/eNTUinputs.Di)/(2*M_PI*eNTUinputs.k*Lhalf)
                    + eNTUinputs.RfoBis/Ao + 1/(ho*Ao) );
    double Uo = UA/Ao;

    // Determining Cmin, Cmax and Cr
    double CIT = eNTUinputs.mdotIT * eNTUinputs.cpIT;
    double CS  = eNTUinputs.mdotS * eNTUinputs.cpS;

    double Cmin, Cmax;
    if( CIT < CS )
    {
        Cmin = CIT;
        Cmax = CS;
    }
    else // to be reviewed
    {
        Cmin = CS;
        Cmax = CIT;
    }

    double Cr = Cmin/Cmax;

    // Calculating NTU as a function of length along the insertion tube (x)
    unsigned N = 10;

    std::vector<double> x;
    for(unsigned i=0; i<=N; ++i)
        x.push_back(i * Lhalf/N);

    std::vector<double> NTU;
    for(unsigned i=0; i<=N; ++i)
        NTU.push_back( (Uo*M_PI*eNTUinputs.Do/Cmin)*x[i] );

    // Calculating epsilon as a function of length along the insertion tube (x)
    std::vector<double> epsilon;
    for(unsigned i=0; i<=N; ++i)
    {
        if( CIT < CS )
            epsilon.push_back( (1/Cr)*(1-exp(-Cr*(1-exp(-NTU[i])))) );
        else // to be reviewed
            epsilon.push_back( 1-exp(-(1/Cr)*(1-exp(-Cr*NTU[i]))) );
    }

    // Calculating the rate of heat transfer Q as a function of length along the insertion tube (x)
    std::vector<double> Q;
    for(unsigned i=0; i<=N; ++i)
        Q.push_back( epsilon[i]*Cmin*(TSinK - TITinK) );

    // Calculating the outlet temperature as a function of length
    std::vector<double> TIToutK;
    for(unsigned i=0; i<=N; ++i)
        TIToutK.push_back( TITinK + Q[i]/CIT );

    // Calculating the average temperature in the insertion tube
    double TITavgK = std::accumulate(TIToutK.begin(),TIToutK.end(),0.0) / TIToutK.size();

    return K2degC(TITavgK);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void spcGenerator::proceedWithENTUmethod()
{
    readMolecularWeights();

    outputs.T_ins_tube_min = calculateInsTubeTempViaENTU(inputs.ATmin,inputs.PTmin); // 0.5*(inputs.PTmin + inputs.ATmin);
    outputs.T_ins_tube_typ = calculateInsTubeTempViaENTU(inputs.ATtyp,inputs.PTtyp); // 0.5*(inputs.PTtyp + inputs.ATtyp);
    outputs.T_ins_tube_max = calculateInsTubeTempViaENTU(inputs.ATmax,inputs.PTmax); // 0.5*(inputs.PTmax + inputs.ATmax);
}
