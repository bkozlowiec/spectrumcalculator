#ifndef SPCINTERFACEIMPLEMENTATION_H
#define SPCINTERFACEIMPLEMENTATION_H

#include "spcconfigurator.h"

enum gasOption { O2_1, O2_2, O2_3 };

struct spcInterface
{
    virtual void simulateSpectra() = 0;
    virtual void setWhiteNoise( bool ) = 0;
    virtual void setPressureNoise( bool ) = 0;
    virtual void setTemperatureNoise( bool ) = 0;
    virtual void setFreqShiftNoise( bool ) = 0;
    virtual void setRAMnoise( bool ) = 0;
    virtual void setTransmissionNoise( bool ) = 0;
    virtual void setGasConfigOption( gasOption ) = 0;
    virtual void basicOutput() = 0;
    virtual OUTPUT_VARIABLES_STRUCT getOutputsStruct() = 0;
    virtual INPUT_VARIABLES_STRUCT& setInputsStruct() = 0;
    virtual GASCONFIG_VARIABLES_STRUCT& setGasConfigStruct() = 0;
	virtual ENTU_METHOD_VARIABLES_STRUCT& setENTUinputsStruct() = 0;
};

class spcImplementation : public spcInterface, private spcConfigurator
{
    spcImplementation() : spcConfigurator() {}
    spcImplementation( const spcImplementation& ) = delete;
    spcImplementation& operator=( const spcImplementation& ) = delete;

public:
    static spcImplementation* createInstance() { static spcImplementation instance; return &instance; }

protected:
    void simulateSpectra();
    void setWhiteNoise( bool onOff ) { noiseGenerator.getWhiteNoiseStruct().turnedOn = onOff; }
    void setPressureNoise( bool onOff ) { noiseGenerator.getPressureNoiseStruct().turnedOn = onOff; }
    void setTemperatureNoise( bool onOff ) { noiseGenerator.getTemperatureNoiseStruct().turnedOn = onOff; }
    void setFreqShiftNoise( bool onOff ) { noiseGenerator.getFreqShiftNoiseStruct().turnedOn = onOff; }
    void setRAMnoise( bool onOff ) { noiseGenerator.getRAMnoiseStruct().turnedOn = onOff; }
    void setTransmissionNoise( bool onOff ) { noiseGenerator.getTransmissionNoiseStruct().turnedOn = onOff; }
    void basicOutput();
    void setGasConfigOption( gasOption );
    OUTPUT_VARIABLES_STRUCT getOutputsStruct() { return outputs; }
    INPUT_VARIABLES_STRUCT& setInputsStruct() { return inputs; }
    GASCONFIG_VARIABLES_STRUCT& setGasConfigStruct() { return gasConfig; }
	ENTU_METHOD_VARIABLES_STRUCT& setENTUinputsStruct() { return eNTUinputs; }
};

#endif // SPCINTERFACEIMPLEMENTATION_H
