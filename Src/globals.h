#ifndef GLOBALS_H
#define GLOBALS_H

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <algorithm>
#include <stdexcept>
#include <cmath>

#include "libMASSConfigManager.h"
#include "spectra/baseline.h"
#include "gasconfigstructs.h"

/////////////////////////////////////////////// Global functions for units converstion ////////////////////////////////////////////////

inline double bar2torr(double bar)  { return bar * 750.062; }
inline double torr2bar(double torr) { return torr / 750.062; }
inline double ppm2percent(double ppm)     { return ppm / 1.0e4; }
inline double percent2ppm(double percent) { return percent * 1.0e4; }
inline double degC2K(double degC) { return degC + 273.15; }
inline double K2degC(double K) { return K - 273.15; }

inline std::string bool2string(bool b) { if(b) return "YES"; else return "NO"; }

//////////////////////////////////////////////// Global function for comparing doubles ////////////////////////////////////////////////

inline bool doubleEquals(double d1, double d2, double epsilon = 1.0e-3) { if( fabs(d1-d2) < epsilon ) return true; else return false; }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

template<class T>
std::ostream & operator<<(std::ostream & str, const std::vector<T> & v)
{
    str << "[";
    for (size_t nn = 0; nn < v.size()-1; nn++)
        str << v[nn] << ", ";
    str << v[v.size()-1] << "]";
    return str;
}

//void convertSpectraFile(std::string currDir);
int paramsAreCorrect(int argc, const std::array<std::string,3>& argv);//(int argc, const char** argv);
void parseCrossStackSpectrumLine(const std::string & line,
        std::vector<double> & outSpectrum,
        double & outTemperature,
        double & outPressure,
        std::string & outDatestamp,
        std::string & outTimestamp);

/**
 * Read stack, flange and EEPROM info from its temporary home in the crossstack
 * INI file.  This should be short-term development stuff.  PCH170301
 */
void readGoofyINI(const std::string & crossStackIniPath,
    StructStackConfig & outStack,
    std::vector<StructStackConfig> & outFlanges,
    StructLaserRampArguments & outEEPROM,
    std::vector<double> & outFlangeT_C,
    std::vector<double> & outFlangeP_torr);

void readGoofyINI(const std::string & crossStackIniPath,
    StructLaserRampArguments & outEEPROM);

void readStackInputs(const std::string& broadeningCoeffsIniPath,
    StructStackConfig & outStack,
    double& temperature,
    double& P_torr,
    const GASCONFIG_SIM_FIT_STRUCT& appInputs,
    bool simOrFit );

void readFlangesInputs(const std::string& broadeningCoeffsIniPath,
    std::vector<StructStackConfig> & outFlanges,
    std::vector<double> & outFlangeT_C,
    std::vector<double> & outFlangeP_torr,
    const GASCONFIG_SIM_FIT_STRUCT& appInputs,
    bool simOrFit);

std::string makeOutFileName(const std::string & spectraFilePath, const std::string & suffix);

void printSpectralThings(const SPCInterface * spcInterface);
void printPostFitThings(const SPCInterface* spcInterface);

double interpolate1dVector( double, std::vector<double>&&, std::vector<double>&& );
double interpolate2dVector( double, double, std::vector<double>&&, std::vector<double>&&, std::vector<std::vector<double> >&& );

#endif // GLOBALS_H
