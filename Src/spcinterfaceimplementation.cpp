#include "spcinterfaceimplementation.h"
#include <iostream>

void spcImplementation::simulateSpectra()
{
    auto start = std::chrono::high_resolution_clock::now();

    calculateNonSimOutputs();

    for(unsigned i=1; i<counter; ++i)
    {
        if( i > 4 )
        {
            if( !inputs.airPurge )
                if( i % 3 == 2 ) continue;

            if( !inputs.nitPurge )
                if( i % 3 == 0 ) continue;

            if( !inputs.custPurge )
                if( i % 3 == 1 ) continue;
        }

        if( i > 16 )
        {
            if( outputs.fixed_only )
            {
                if( (i % 6 == 2) || (i % 6 == 3) || (i % 6 == 4) )
                    continue;
            }
            else
            {
                if( (i % 6 == 5) || (i % 6 == 0) || (i % 6 == 1) )
                    continue;
            }
        }

        initializePTfromINI();
        initializeSimCase(static_cast<simCase>(i));
        calculateInitialGuessFit();
        fitToLessPeaksINI();
        calculateSimOutputs();
    }

    if( !outputs.fixed_only && (outputs.LL_method == "Stack") )
    {
        initializePTfromINI();
        initializeSimCase(static_cast<simCase>(2));
        turnPWfloatingOn();
        calculateInitialGuessFit();
        fitToLessPeaksINI();
        calculateSimOutputs();
        checkIfLLmethodSettable();
    }

    auto end = std::chrono::high_resolution_clock::now();

    printf("\n****************** TOTAL SIMULATION TIME: %i sec. ******************\n\n",
           std::chrono::duration_cast<std::chrono::seconds>(end - start)); fflush(stdout);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void spcImplementation::setGasConfigOption( gasOption gasConf )
{
    switch( gasConf )
    {
    case O2_1:
        launchGasConfigOption1(true);
        break;
    case O2_2:
        launchGasConfigOption2(true);
        break;
    case O2_3:
        launchGasConfigOption3(true);
        break;
    default:
        break;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void spcImplementation::basicOutput()
{
    std::cout << std::endl << "********** Basic simulation results **********" << std::endl
              << "O2 concentration value (read from INI): " << O2initial << std::endl
              << "O2 concentration value (after fitting): " << O2fitted  << std::endl;
}
