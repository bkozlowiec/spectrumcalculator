#ifndef INPUTSTRUCTS_H
#define INPUTSTRUCTS_H

#include "globals.h"

#include <string>

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

struct INPUT_DEFAULTS_STRUCT
{
    // Measurement gas components
    const std::string gas1;
    const double      concTyp1;           // typical [ppm]
    const double      concMin1;           // minimum [ppm]
    const double      concMax1;           // maximum [ppm]
    const double      range1;             // maximum [%]

    const std::string gas2;
    const double      concTyp2;           // typical [ppm]
    const double      concMin2;           // minimum [ppm]
    const double      concMax2;           // maximum [ppm]
    const double      range2;             // maximum [%]

    const double      dataOutputRate;     // [s]

    // Gas matrix (additional components)
    const std::vector<std::string> matxGases;
    const std::vector<double>      matxTyps;      // typical vals [ppm]
    const std::vector<double>      matxMins;      // minimum vals [ppm]
    const std::vector<double>      matxMaxs;      // maximum vals [ppm]

    // Process temperature
    const double PTtyp;   // typical [deg C]
    const double PTmin;   // minimum [deg C]
    const double PTmax;   // maximum [deg C]

    // Process pressure
    const double PPtyp;   // typical [bar]
    const double PPmin;   // minimum [bar]
    const double PPmax;   // maximum [bar]

    // Water vapor
    const double H2OconcTyp;  // typical [ppm]
    const double H2OconcMin;  // minimum [ppm]
    const double H2OconcMax;  // maximum [ppm]

    // Dust
    const double dustConcTyp; // typical [mg/m3]
    const double dustConcMin; // minimum [mg/m3]
    const double dustConcMax; // maximum [mg/m3]

    // Process flow velocity
    const double velTyp;      // typical [m/s]
    const double velMin;      // minimum [m/s]
    const double velMax;      // maximum [m/s]

    // Optical path lengths
    const double L1, L2, L3;  // [mm]

    // Instrument air availability
    const bool airPurge;

    // Nitrogen availability
    const bool nitPurge;

    // Custom purging medium
    const bool custPurge;

    // Measurement components in purging medium
    const double purConc1, purConc2;  // [ppm]

    // Purging medium components matrix
    const std::string purMatxGas1;
    const double      purMatxConc1;   // [ppm]

    const std::string purMatxGas2;
    const double      purMatxConc2;   // [ppm]

    // Ambient temperature
    const double ATtyp;       // typical [deg C]
    const double ATmin;       // minimum [deg C]
    const double ATmax;       // maximum [deg C]

    // External cell presence
    const bool extCell;

    // Internal cell presence
    const bool intCell;

    // Isolation flange presence
    const bool isoFlange;

    // Insertion tubes presence
    const bool insTube;

    // The default and only constructor
    INPUT_DEFAULTS_STRUCT() :
        gas1("O2"),
        concTyp1(2e5),
        concMin1(1e5),
        concMax1(3e5),
        range1(30.0),
        gas2("H2O"),
        concTyp2(0e5),
        concMin2(0e5),
        concMax2(0e5),
        range2(0.0),
        dataOutputRate(1.0),
        matxGases( {"N2","CO2"} ),
        matxTyps( {7e5,1e5} ),
        matxMins( {6e5,0.0} ),
        matxMaxs( {9e5,3e5} ),
        PTtyp(250.0),
        PTmin(20.0),
        PTmax(400.0),
        PPtyp(2.0),
        PPmin(1.0),
        PPmax(4.0),
        H2OconcTyp(1e5),
        H2OconcMin(0.5e5),
        H2OconcMax(1.5e5),
        dustConcTyp(500.0),
        dustConcMin(0.0),
        dustConcMax(3000.0),
        velTyp(10.0),
        velMin(5.0),
        velMax(12.0),
        L1(100.0),
        L2(1000.0),
        L3(100.0),
        airPurge(true),
        nitPurge(true),
        custPurge(true),
        purConc1(0.5e5),
        purConc2(0.0e5),
        purMatxGas1("N2"),
        purMatxConc1(5.0e5),
        purMatxGas2("CO2"),
        purMatxConc2(4.5e5),
        ATtyp(35.0),
        ATmin(25.0),
        ATmax(45.0),
        extCell(false),
        intCell(false),
        isoFlange(false),
        insTube(true)
    {}
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

struct INPUT_VARIABLES_STRUCT
{
    // Measurement gas components
    std::string gas1;
    double      concTyp1;           // typical [ppm]
    double      concMin1;           // minimum [ppm]
    double      concMax1;           // maximum [ppm]
    double      range1;             // maximum [%]

    std::string gas2;
    double      concTyp2;           // typical [ppm]
    double      concMin2;           // minimum [ppm]
    double      concMax2;           // maximum [ppm]
    double      range2;             // maximum [%]

    double      dataOutputRate;     // [s]

    // Gas matrix (additional components)
    std::vector<std::string> matxGases;
    std::vector<double>      matxTyps;      // typical vals [ppm]
    std::vector<double>      matxMins;      // minimum vals [ppm]
    std::vector<double>      matxMaxs;      // maximum vals [ppm]

    // Process temperature
    double PTtyp;   // typical [deg C]
    double PTmin;   // minimum [deg C]
    double PTmax;   // maximum [deg C]

    // Process pressure
    double PPtyp;   // typical [bar]
    double PPmin;   // minimum [bar]
    double PPmax;   // maximum [bar]

    // Water vapor
    double H2OconcTyp;  // typical [ppm]
    double H2OconcMin;  // minimum [ppm]
    double H2OconcMax;  // maximum [ppm]

    // Dust
    double dustConcTyp; // typical [mg/m3]
    double dustConcMin; // minimum [mg/m3]
    double dustConcMax; // maximum [mg/m3]

    // Process flow velocity
    double velTyp;      // typical [m/s]
    double velMin;      // minimum [m/s]
    double velMax;      // maximum [m/s]

    // Optical path lengths
    double L1, L2, L3;  // [mm]

    // Instrument air availability
    bool airPurge;

    // Nitrogen availability
    bool nitPurge;

    // Custom purging medium
    bool custPurge;

    // Measurement components in purging medium
    double purConc1, purConc2;  // [ppm]

    // Purging medium components matrix
    std::string purMatxGas1;
    double      purMatxConc1;   // [ppm]

    std::string purMatxGas2;
    double      purMatxConc2;   // [ppm]

    // Ambient temperature
    double ATtyp;       // typical [deg C]
    double ATmin;       // minimum [deg C]
    double ATmax;       // maximum [deg C]

    // External cell presence
    bool extCell;

    // Internal cell presence
    bool intCell;

    // Isolation flange presence
    bool isoFlange;

    // Insertion tubes presence
    bool insTube;

    // The default constructor
    INPUT_VARIABLES_STRUCT( INPUT_DEFAULTS_STRUCT defaults = INPUT_DEFAULTS_STRUCT() ) :
        gas1( defaults.gas1 ),
        concTyp1( defaults.concTyp1 ),
        concMin1( defaults.concMin1 ),
        concMax1( defaults.concMax1 ),
        range1( defaults.range1 ),
        gas2( defaults.gas2 ),
        concTyp2( defaults.concTyp2 ),
        concMin2( defaults.concMin2 ),
        concMax2( defaults.concMax2 ),
        range2( defaults.range2 ),
        dataOutputRate( defaults.dataOutputRate ),
        matxGases( defaults.matxGases ),
        matxTyps( defaults.matxTyps ),
        matxMins( defaults.matxMins ),
        matxMaxs( defaults.matxMaxs ),
        PTtyp( defaults.PTtyp ),
        PTmin( defaults.PTmin ),
        PTmax( defaults.PTmax ),
        PPtyp( defaults.PPtyp ),
        PPmin( defaults.PPmin ),
        PPmax( defaults.PPmax ),
        H2OconcTyp( defaults.H2OconcTyp ),
        H2OconcMin( defaults.H2OconcMin ),
        H2OconcMax( defaults.H2OconcMax ),
        dustConcTyp( defaults.dustConcTyp ),
        dustConcMin( defaults.dustConcMin ),
        dustConcMax( defaults.dustConcMax ),
        velTyp( defaults.velTyp ),
        velMin( defaults.velMin ),
        velMax( defaults.velMax ),
        L1( defaults.L1 ),
        L2( defaults.L2 ),
        L3( defaults.L3 ),
        airPurge( defaults.airPurge ),
        nitPurge( defaults.nitPurge ),
        custPurge( defaults.custPurge ),
        purConc1( defaults.purConc1 ),
        purConc2( defaults.purConc2 ),
        purMatxGas1( defaults.purMatxGas1 ),
        purMatxConc1( defaults.purMatxConc1 ),
        purMatxGas2( defaults.purMatxGas2 ),
        purMatxConc2( defaults.purMatxConc2 ),
        ATtyp( defaults.ATtyp ),
        ATmin( defaults.ATmin ),
        ATmax( defaults.ATmax ),
        extCell( defaults.extCell ),
        intCell( defaults.intCell ),
        isoFlange( defaults.isoFlange ),
        insTube( defaults.insTube )
    {}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

struct ENTU_METHOD_DEFAULTS_STRUCT
{
    const double VelIT;                // velocity of gas in insertion tube [m/s]
    const double mdotIT;               // mass flow rate of gas in insertion tube [kg/s]

    const double VelS;                 // velocity of gas in stack [m/s]
    const double mdotS;                // mass flow rate of gas in stack [kg/s]

    const double Do;                   // outer diameter of insertion tube [m]
    const double Di;                   // inner diameter of insertion tube [m]

    const double RfiBis;               // fouling resistance on the inside of insertion tube [m^2*K/W]
    const double RfoBis;               // fouling resistance on the outside of insertion tube [m^2*K/W]

    const double MwIT;                 // molecular weight of gas in insertion tube [kg/mol]
    const double MwS;                  // molecular weight of gas in stack [kg/mol]

    const double R;                    // molar gas constant [m^3*bar/K*mol]

    ENTU_METHOD_DEFAULTS_STRUCT() :
        VelIT(10.0),
        mdotIT(0.1),
        VelS(3.0),
        mdotS(0.2),
        Do(0.045),
        Di(0.035),
        RfiBis(3.5e-4),
        RfoBis(1.8e-4),
        MwIT(0.02897),
        MwS(0.02897),
        R(8.3144598e-5)
    {}

private:
    class singleParamDependent
    {
    protected:
        std::vector<double> AT_Air;
        std::vector<double> values_Air;
        std::vector<double> AT_N2;
        std::vector<double> values_N2;
        std::vector<double> AT_Custom;
        std::vector<double> values_Custom;

    public:
        double getValue( double ATtyp, std::string gas )
        {
            std::cout << "ATtemp = " << ATtyp << std::endl;
            if( gas == "Air" )
                return interpolate1dVector(ATtyp,std::move(AT_Air),std::move(values_Air));
            else if( gas == "N2" )
                return interpolate1dVector(ATtyp,std::move(AT_N2),std::move(values_N2));
            else if( gas == "custom" )
                return interpolate1dVector(ATtyp,std::move(AT_Custom),std::move(values_Custom));
            else
                throw std::runtime_error( "Error! Gas type not defined for 1D interpolation." );
        }
    };

    class cpIT : public singleParamDependent // specific heat capacities of gases in insertion tube [J/(kg*K)]
    {
    public:
        cpIT() { AT_Air = { 0.0,10.0,20.0,30.0,40.0,50.0,60.0,70.0,80.0,90.0 },
                 values_Air = { 0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0 },
                 AT_N2 = { 0.0,11.0,22.0,33.0,44.0,55.0,66.0,77.0,88.0,99.0 },
                 values_N2 = { 0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0 },
                 AT_Custom = { 0.0,12.0,24.0,36.0,48.0,60.0,72.0,84.0,96.0,108.0 },
                 values_Custom = { 0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0 }; }
    };

    class muIT : public singleParamDependent // viscosity of gas in insertion tube [kg/(s*m)]
    {
    public:
        muIT() { AT_Air = { 0.0,10.0,20.0,30.0,40.0,50.0,60.0,70.0,80.0,90.0 },
                 values_Air = { 0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0 },
                 AT_N2 = { 0.0,11.0,22.0,33.0,44.0,55.0,66.0,77.0,88.0,99.0 },
                 values_N2 = { 0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0 },
                 AT_Custom = { 0.0,12.0,24.0,36.0,48.0,60.0,72.0,84.0,96.0,108.0 },
                 values_Custom = { 0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0 }; }
    };

    class kIT : public singleParamDependent // thermal conductivity of gas in insertion tube [W/(m*K)]
    {
    public:
        kIT() { AT_Air = { 0.0,10.0,20.0,30.0,40.0,50.0,60.0,70.0,80.0,90.0 },
                values_Air = { 0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0 },
                AT_N2 = { 0.0,11.0,22.0,33.0,44.0,55.0,66.0,77.0,88.0,99.0 },
                values_N2 = { 0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0 },
                AT_Custom = { 0.0,12.0,24.0,36.0,48.0,60.0,72.0,84.0,96.0,108.0 },
                values_Custom = { 0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0 }; }
    };

    class cpS : public singleParamDependent // specific heat capacities of gases in stack [J/(kg*K)]
    {
    public:
        cpS() { AT_Air = { 0.0,10.0,20.0,30.0,40.0,50.0,60.0,70.0,80.0,90.0 },
                values_Air = { 0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0 },
                AT_N2 = { 0.0,11.0,22.0,33.0,44.0,55.0,66.0,77.0,88.0,99.0 },
                values_N2 = { 0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0 },
                AT_Custom = { 0.0,12.0,24.0,36.0,48.0,60.0,72.0,84.0,96.0,108.0 },
                values_Custom = { 0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0 }; }
    };

    class muS : public singleParamDependent // viscosity of gas in stack [kg/(s*m)]
    {
    public:
        muS() { AT_Air = { 0.0,10.0,20.0,30.0,40.0,50.0,60.0,70.0,80.0,90.0 },
                values_Air = { 0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0 },
                AT_N2 = { 0.0,11.0,22.0,33.0,44.0,55.0,66.0,77.0,88.0,99.0 },
                values_N2 = { 0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0 },
                AT_Custom = { 0.0,12.0,24.0,36.0,48.0,60.0,72.0,84.0,96.0,108.0 },
                values_Custom = { 0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0 }; }
    };

    class kS : public singleParamDependent // thermal conductivity of gas in stack [W/(m*K)]
    {
    public:
        kS() { AT_Air = { 0.0,10.0,20.0,30.0,40.0,50.0,60.0,70.0,80.0,90.0 },
               values_Air = { 0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0 },
               AT_N2 = { 0.0,11.0,22.0,33.0,44.0,55.0,66.0,77.0,88.0,99.0 },
               values_N2 = { 0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0 },
               AT_Custom = { 0.0,12.0,24.0,36.0,48.0,60.0,72.0,84.0,96.0,108.0 },
               values_Custom = { 0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0 }; }
    };

    class doubleParamDependent
    {
    protected:
        std::vector<double> AT_Air, PP_Air;
        std::vector<std::vector<double> > values_Air;
        std::vector<double> AT_N2, PP_N2;
        std::vector<std::vector<double> > values_N2;
        std::vector<double> AT_Custom, PP_Custom;
        std::vector<std::vector<double> > values_Custom;

    public:
        double getValue( double PPtyp, double ATtyp, std::string gas )
        {
            if( gas == "Air" )
                return interpolate2dVector(PPtyp,ATtyp,std::move(PP_Air),std::move(AT_Air),std::move(values_Air));
            else if( gas == "N2" )
                return interpolate2dVector(PPtyp,ATtyp,std::move(PP_N2),std::move(AT_N2),std::move(values_N2));
            else if( gas == "custom" )
                return interpolate2dVector(PPtyp,ATtyp,std::move(PP_Custom),std::move(AT_Custom),std::move(values_Custom));
            else
                throw std::runtime_error( "Error! Gas type not defined for 2D interpolation." );
        }
    };

    class rhoIT : public doubleParamDependent // density of gas in insertion tube [kg/m^3]
    {
    public:
        rhoIT() { PP_Air = { 0.0,10.0,20.0 },
                  AT_Air = { 0.0,10.0,20.0,30.0,40.0,50.0,60.0,70.0,80.0,90.0 },
                  values_Air = { { 0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0 },
                                 { 0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0 },
                                 { 0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0 } },
                  PP_N2 = { 0.0,11.0,22.0 },
                  AT_N2 = { 0.0,11.0,22.0,33.0,44.0,55.0,66.0,77.0,88.0,99.0 },
                  values_N2 = { { 0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0 },
                                { 0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0 },
                                { 0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0 } },
                  PP_Custom = { 0.0,12.0,24.0, 36.0 },
                  AT_Custom = { 0.0,12.0,24.0,36.0,48.0,60.0,72.0,84.0,96.0,108.0 },
                  values_Custom = { { 0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0 },
                                    { 0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0 },
                                    { 0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0 },
                                    { 0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0 } }; }
    };

    class rhoS : public doubleParamDependent // density of gas in stack [kg/m^3]
    {
    public:
        rhoS() { PP_Air = { 0.0,10.0,20.0 },
                 AT_Air = { 0.0,10.0,20.0,30.0,40.0,50.0,60.0,70.0,80.0,90.0 },
                 values_Air = { { 0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0 },
                                { 0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0 },
                                { 0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0 } },
                 PP_N2 = { 0.0,11.0,22.0 },
                 AT_N2 = { 0.0,11.0,22.0,33.0,44.0,55.0,66.0,77.0,88.0,99.0 },
                 values_N2 = { { 0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0 },
                               { 0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0 },
                               { 0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0 } },
                 PP_Custom = { 0.0,12.0,24.0, 36.0 },
                 AT_Custom = { 0.0,12.0,24.0,36.0,48.0,60.0,72.0,84.0,96.0,108.0 },
                 values_Custom = { { 0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0 },
                                   { 0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0 },
                                   { 0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0 },
                                   { 0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0 } }; }
    };

    class k // thermal conductivity of insertion tube [W/(m*K)]
    {
        std::vector<double> avgTemp_steel;
        std::vector<double> k_steel;
        std::vector<double> avgTemp_bronze;
        std::vector<double> k_bronze;

    public:
        k() { avgTemp_steel = { 0.0,10.0,20.0,30.0,40.0,50.0,60.0,70.0,80.0,90.0 },
              k_steel = { 0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0 },
              avgTemp_bronze = { 0.0,11.0,22.0,33.0,44.0,55.0,66.0,77.0,88.0,99.0 },
              k_bronze = { 0.0,1.1,2.2,3.3,4.4,5.5,6.6,7.7,8.8,9.9 }; }

        double getValue( double ATtyp, double PTtyp, std::string insTubeMaterial )
        {
            double avgTemp = 0.5*(ATtyp + PTtyp);

            if( insTubeMaterial == "steel" )
                return interpolate1dVector(avgTemp,std::move(avgTemp_steel),std::move(k_steel));
            else if( insTubeMaterial == "bronze" )
                return interpolate1dVector(avgTemp,std::move(avgTemp_bronze),std::move(k_bronze));
            else
                throw std::runtime_error( "Error! Material type not defined for 1D interpolation." );
        }
    };

    friend class ENTU_METHOD_VARIABLES_STRUCT;
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

struct ENTU_METHOD_VARIABLES_STRUCT
{
    double cpIT;                 // specific heat capacities of gases in insertion tube [J/(kg*K)]
    double muIT;                 // viscosity of gas in insertion tube [kg/(s*m)]
    double kIT;                  // thermal conductivity of gas in insertion tube [W/(m*K)]
    double rhoIT;                // density of gas in insertion tube [kg/m^3]
    double VelIT;                // velocity of gas in insertion tube [m/s]
    double mdotIT;               // mass flow rate of gas in insertion tube [kg/s]
    double VdotIT;               // volumetric flow rate of gas in insertion tube [L/min]

    double cpS;                  // specific heat capacity of gas in stack [J/(kg*K)]
    double muS;                  // viscosity of gas in stack [kg/(s*m)]
    double kS;                   // thermal conductivity of gas in stack [W/(m*K)]
    double rhoS;                 // density of gas in stack [kg/m^3]
    double VelS;                 // velocity of gas in stack [m/s]
    double mdotS;                // mass flow rate of gas in stack [kg/s]

    double Do;                   // outer diameter of insertion tube [m]
    double Di;                   // inner diameter of insertion tube [m]

    double k;                    // thermal conductivity of insertion tube [W/(m*K)]
    double RfiBis;               // fouling resistance on the inside of insertion tube [m^2*K/W]
    double RfoBis;               // fouling resistance on the outside of insertion tube [m^2*K/W]

    double MwIT;                 // molecular weight of gas in insertion tube [kg/mol]
    double MwS;                  // molecular weight of gas in stack [kg/mol]

    double R;                    // molar gas constant [m^3*bar/K*mol]

    ENTU_METHOD_VARIABLES_STRUCT( std::string purgeGas = "Air",
                                  std::string insTubeMaterial = "steel",
                                  ENTU_METHOD_DEFAULTS_STRUCT defaults = ENTU_METHOD_DEFAULTS_STRUCT(),
                                  INPUT_DEFAULTS_STRUCT defs = INPUT_DEFAULTS_STRUCT() ) :
        cpIT( ENTU_METHOD_DEFAULTS_STRUCT::cpIT().getValue(defs.ATtyp,purgeGas) ),
        muIT( ENTU_METHOD_DEFAULTS_STRUCT::muIT().getValue(defs.ATtyp,purgeGas) ),
        kIT( ENTU_METHOD_DEFAULTS_STRUCT::kIT().getValue(defs.ATtyp,purgeGas) ),
        rhoIT( ENTU_METHOD_DEFAULTS_STRUCT::rhoIT().getValue(defs.PPtyp,defs.PTtyp,purgeGas) ),
        VelIT( defaults.VelIT ),
        mdotIT( defaults.mdotIT ),
        cpS( ENTU_METHOD_DEFAULTS_STRUCT::cpS().getValue(defs.PTtyp,purgeGas) ),
        muS( ENTU_METHOD_DEFAULTS_STRUCT::muS().getValue(defs.PTtyp,purgeGas) ),
        kS( ENTU_METHOD_DEFAULTS_STRUCT::kS().getValue(defs.PTtyp,purgeGas) ),
        rhoS( ENTU_METHOD_DEFAULTS_STRUCT::rhoS().getValue(defs.PPtyp,defs.PTtyp,purgeGas) ),
        VelS( defaults.VelS ),
        mdotS( defaults.mdotS ),
        Do( defaults.Do ),
        Di( defaults.Di ),
        k( ENTU_METHOD_DEFAULTS_STRUCT::k().getValue(defs.ATtyp,defs.PPtyp,insTubeMaterial) ),
        RfiBis( defaults.RfiBis ),
        RfoBis( defaults.RfoBis ),
        MwIT( defaults.MwIT ),
        MwS( defaults.MwS ),
        R( defaults.R )
    {}

    ENTU_METHOD_VARIABLES_STRUCT& operator=( const ENTU_METHOD_VARIABLES_STRUCT& ) = default;
};

#endif // INPUTSTRUCTS_H
