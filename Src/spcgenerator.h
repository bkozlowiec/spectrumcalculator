#ifndef SPCGENERATOR_H
#define SPCGENERATOR_H

#include "noisegenerator.h"
#include "inputstructs.h"
#include "outputstructs.h"
#include "gasconfigstructs.h"
#include "spc_interface.h"
#include <vector>
#include <mutex>

class StructStackConfig;

/////////////////////// Main simulator class ///////////////////////

class spcGenerator
{
protected:
    struct fromInputINI
    {
        double stackPres;
        double stackTemp;
        unsigned noiseIterationsNum;
    };

    enum { spectrumWidth = 950 };
    std::vector<double> rawSignal;
    std::vector<double> noisedBaseLine;
    std::vector<double> startingVector;
    std::vector<double> noisedVector;
    std::vector<double> fittedVectorI;
    std::vector<double> ghzPoints;
    std::vector<double> O2concentrationsVector;
    std::vector<double> simAbsorbanceVec, fitAbsorbanceVec;
    double O2initial, O2fitted;
    NoiseGenerator noiseGenerator;
    fromInputINI   stackPT;
    unsigned numOfIterations, iterNo;
    multipleCalcsData mulCdata;
    bool reinitNeeded;
    bool doubledIteration;

    INPUT_VARIABLES_STRUCT       inputs;
    OUTPUT_VARIABLES_STRUCT      outputs;
    GASCONFIG_VARIABLES_STRUCT   gasConfig;
    GASCONFIG_SIM_FIT_STRUCT     simFitData;
    ENTU_METHOD_VARIABLES_STRUCT eNTUinputs;

    SPCManager* manager;
    double      totalPL; // [mm]
    double      Tr_min;  // [-]
    bool        PWfloating;
    std::mutex  muti;
    const std::string broadeningCoeffsINI;
    const std::string entuInputsINI;
    const std::string molWeightsINI;
    std::string insTubePurgeGas;
    std::string insTubeMaterial;

public:
    spcGenerator() : noiseGenerator(NoiseGenerator()), numOfIterations(1), iterNo(0), reinitNeeded(false), doubledIteration(false), PWfloating(false),
        broadeningCoeffsINI("../MasterINI/AllBroadeningCoefficients.ini"), entuInputsINI("../MasterINI/eNTUinputs.ini"), molWeightsINI("../MasterINI/molWeights.ini")
    { initializePTfromINI(); initializeENTUinputsFromINI(); }
    ~spcGenerator() {}

    void createGoofyStartingVector(unsigned);
    void calculateInitialGuessFit();
    void fitToLessPeaksINI();
    void singleFitIteration(const std::vector<double>&, const std::vector<double>&, std::vector<int>&, StructLaserRampArguments, double, double);
    void calculateSimulatedRS(double);
    void initializePTfromINI();
    void initializeENTUinputsFromINI();
    void readMolecularWeights();
    void proceedWithENTUmethod();
    void reinitializeStack( StructStackConfig& );
    void addWhiteNoiseToVecBasedOnRS(std::vector<double>&, const std::vector<double>&);
    void addRAMnoiseToVecBasedOnBL(std::vector<double>&, const std::vector<double>&, const std::vector<double>&);
    void addTransmissionNoiseToBL(std::vector<double>&);
    void clearVectors();
    void collimationCalcs();
    void conditionsWithinBounds();
    void insTubeConditions();
    void IRfilterPresent();
    void spectralScanRate();
    void calculateTransmissionSF() { noiseGenerator.getTransmissionNoiseStruct().calculateSF(inputs,outputs); }
    void setDoubledIteration( bool launchDoubled ) { doubledIteration = launchDoubled; }
    void setConfigsToDefaults();
    void recalcTransmissionSFaccToDust( unsigned );
    void calculateInitialTrMin();
    void recalculateTrMin();
    void turnPWfloatingOn() { PWfloating = true; }
    double calculatedPressureNoise();
    double calculatedTemperatureNoise();
    double calculatedFreqShiftNoise();
    double calculateConcentrationMeanVal( unsigned, unsigned ) const;
    double calculateConcentrationStdDev( unsigned, unsigned ) const;
    double calculateSNR() const { return calculateConcentrationMeanVal(0,O2concentrationsVector.size()) / calculateConcentrationStdDev(0,O2concentrationsVector.size()); }
    double calculateInsTubeTempViaENTU( double, double );
    double getOutputInsTubeLen() const { return outputs.L_ins_tube; }
    const std::vector<double>& getRawSignal() const { return rawSignal; }
    const std::vector<double>& getGhzPoints() const { return ghzPoints; }
    const std::vector<double>& getBaseLine() const { return noisedBaseLine; }
    const std::vector<double>& getFittedVector() const { return fittedVectorI; }
    const std::vector<double>& getNoisedVector() const { return noisedVector; }
    const std::vector<double>& getSimAbsorbance() const { return simAbsorbanceVec; }
    const std::vector<double>& getFitAbsorbance() const { return fitAbsorbanceVec; }
    const fromInputINI* getStackPTptr() const;
    std::vector<double> createMatrixConcentrations( bool, bool );
    std::vector<double> createTypicalMatrixConcentrations( bool, double, double, const std::vector<std::string>& );
    std::vector<double> createNonTypicalMatrixConcentrations( bool, GASCONFIG_SIM_FIT_STRUCT::mtxCreationStyles,
                                                              double, double, const std::vector<std::string>& );
    std::vector<double> createRandomVector(unsigned size = spectrumWidth);
    bool concentrationsDiffer();

    static void padVectorWith0s(std::vector<double>&, unsigned, unsigned);
    static void unpadVector(std::vector<double>&, unsigned, unsigned);
    static std::vector<double> calculateResidualsVec( const std::vector<double>&, const std::vector<double>& );
};

#endif // SPCGENERATOR_H
