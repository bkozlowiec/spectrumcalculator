#include "globals.h"

/*********************************************** Global functions from spctool.cpp ***************************************************/

int paramsAreCorrect(int argc, const std::array<std::string,3>& argv) //(int argc, const char** argv)
{
    std::string path1, path2;
    int exitCode = -1;
    if (argc == 3)
    {
        //check if arguments are valid
        //if configuration directory is valid
        std::string path = argv[1];
        path1 = path + "/" + "spc_config.ini";
        std::ifstream fileCheck1(path1.c_str());
        path2 = path + "/" + "crossstack.ini";
        std::ifstream fileCheck2(path2.c_str());
        if (fileCheck1.good())
        {
            // Old mode with peaks.ini, spc_config.ini, etc...
            exitCode = 0;
            fileCheck1.close();
        }
        else if (fileCheck2.good())
        {
            // New mode with crossstack.ini
            exitCode = 1;
            fileCheck2.close();
        }
        else
        {
            exitCode = -1;
            std::cout << "Configuration directory: "
                << argv[1]
                << " is either not valid or it does not contain valid .INI file(s)."
                << std::endl;
        }

        //if spectra file is valid
        path1 = argv[2];
        fileCheck1.open(path1.c_str());
        if (fileCheck1.good())
        {
            fileCheck1.close();
        }
        else
        {
            exitCode = -1;
            std::cout << "Spectra File: " << argv[2] << " does not exist" << std::endl;
        }
    }
    else
    {
        std::cout << "Usage " << std::endl;
        std::cout << "  ./spctool <configuration directory> <spectra file>" << std::endl;
        std::cout << "  e.g. " << std::endl;
        std::cout << "  ./spctool ./conf_O2 ./spectra/spectra_O2.txt" << std::endl;
        exitCode = -1;
    }

    return exitCode;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void parseCrossStackSpectrumLine(const std::string & line,
    std::vector<double> & outSpectrum,
    double & outTemperature,
    double & outPressure,
    std::string & outDatestamp,
    std::string & outTimestamp)
{
    int nOfPoints;
    outSpectrum.clear();
    outSpectrum.reserve(4000);

    std::stringstream dataStream(line);
    std::string dataPoint(""); //string to store a data point from logline

    //extract individual points from the line
    /* Log file format with column name as below
     *  Acq_Num
     *  Date
     *  Timestamp
     *  Pressure
     *  Temperature
     *  TEC_Temp
     *  16 Values
     *  EV1
     *  EV2
     *  EV3
     *  EV4
     *  EV5
     *  EV6
     *  EV7
     *  EV8
     *  Auxiliary 1
     *  Auxiliary 2
     *  Data_len
     *  DP1
     *  <column with heading DP<n>, where n is integer, will continue for Data_len
    */
    std::string value("");
    dataStream >> value; // acquisition number
    if (value == "Acq_Num")
        throw std::runtime_error("Trying to parse header line, stop that!");

    dataStream >> outDatestamp; // Datestamp
    dataStream >> outTimestamp; // Timestamp

    dataStream >> outPressure;

    outPressure *= 750.062; // convert pressure from BAR to TORR

    dataStream >> outTemperature;

    dataStream >> value; // TEC temperature

    for(unsigned int diagIdx = 0; diagIdx < 16; diagIdx++)
    {
        dataStream >> value;
    }

    // advance the stream past the engineering values (EVs)
    for(unsigned int evIdx = 0; evIdx < 8; evIdx++)
        dataStream >> value;

    //logs with device status does not has Auxiliary
    dataStream >> value;
    dataStream >> value;
    // Number of data points
    dataStream >> nOfPoints;

    //now get all data points of the spectrum stored in a line
    double spectrumDataPoint;
    for (int i = 0; i< nOfPoints; i++)
    {
        dataStream >> spectrumDataPoint;
        outSpectrum.push_back(spectrumDataPoint);
    }

    /*
        Cross Stack log support different number of data point
        per spectrum which is configured by the TX settings of Laser
        This code currently required 4000 data points which is the
        max number of data points supported by Cross Stack.
        This code disect Laser Ramp and Ringdown region from spectrum
        based on the index value of LASER_RAMP_START and LASER_OFF_START
        variables in spc_config.ini file respectively.

        TODO code needs to modify based on the section ratio of
        laser Ramp and ringdown in a spectrum and not on fixed index value
        So as to accomodate different number of data points in spectrum.

        Since we have fixed start index so tempIntVec needs to be filled with
        0 in case data points are less than 4000
    */

    // shouldn't need this line though!
    if (outSpectrum.size() < 4000)
        outSpectrum.resize(4000, 0.0);

//    std::vector<double> dataPts(&spectrum[lrStartIdx], &spectrum[lrStartIdx + lrLength - 1]);
//    std::vector<double> dataPts2(&spectrum[rdStartIdx], &spectrum[rdStartIdx + rdLength - 1]);
//
//    std::cout << std::endl <<"dataPts.size() " << dataPts.size();
//    std::cout << std::endl <<"dataPts2.size() " << dataPts2.size();
//
//    laserRamp.push_back(dataPts);
//    rdData.push_back(dataPts2);
//    spectraID++;

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void readGoofyINI(const std::string & crossStackIniPath,
    StructStackConfig & outStack,
    std::vector<StructStackConfig> & outFlanges,
    StructLaserRampArguments & outEEPROM,
    std::vector<double> & outFlangeT_C,
    std::vector<double> & outFlangeP_torr)
{
    std::string lineFromFile, key, token; // A line has a key often followed by some tokens

    outStack.absorbers.clear();
    outFlanges.clear();

    std::stringstream sstr("");
    std::ifstream crossstackFile;
    crossstackFile.open(crossStackIniPath.c_str(), std::ios::in);

    if( !crossstackFile.is_open() )
    {
        std::cerr << "Error! crossstack.ini not found!" << std::endl;
        return;
    }

    while (getline(crossstackFile, lineFromFile))
    {
        if (lineFromFile.length() < 1 || lineFromFile.at(0) == '#')
        {
            // Skip blank lines or comments (lines starting with #)
        }
        else
        {
            std::istringstream ss(lineFromFile);
            // Read a key and convert to uppercase
            key = "";
            ss >> key;
            std::transform(key.begin(), key.end(), key.begin(), ::toupper);

            // Has to come from EEPROM
            if (key == "ETALON_COEFFICIENTS")
            {
                outEEPROM.etalonCoeffients.assign(5, 0.0);
                ss >> outEEPROM.etalonCoeffients[0];
                ss >> outEEPROM.etalonCoeffients[1];
                ss >> outEEPROM.etalonCoeffients[2];
                ss >> outEEPROM.etalonCoeffients[3];
                ss >> outEEPROM.etalonCoeffients[4];
            }
            // Has to come from EEPROM
            if (key == "SHIFT")
            {
                ss >> outEEPROM.shift_GHz;
            }
            // Has to come from EEPROM
            if (key == "BL_SAMPLE")
            {
                outEEPROM.baselineSample.assign(4, 0);
                ss >> outEEPROM.baselineSample[0];
                ss >> outEEPROM.baselineSample[1];
                ss >> outEEPROM.baselineSample[2];
                ss >> outEEPROM.baselineSample[3];
            }
            // Has to come from EEPROM
            if (key == "FIT_WINDOW")
            {
                outEEPROM.fitWindow.assign(2, 0);
                ss >> outEEPROM.fitWindow[0];
                ss >> outEEPROM.fitWindow[1];
            }
            if (key == "LASER_RAMP_START")
            {
                ss >> outEEPROM.laserRampStart;
            }
            if (key == "LASER_RAMP_LENGTH")
            {
                ss >> outEEPROM.laserRampLength;
            }
            if (key == "LASER_OFF_START")
            {
                ss >> outEEPROM.laserOffStart;
            }
            if (key == "LASER_OFF_LENGTH")
            {
                ss >> outEEPROM.laserOffLength;
            }
            // Has to come from APPS
            if (key == "STACK_PROPERTIES")
            {
                double length_m;
                ss >> length_m;
                outStack.length_m = length_m;
            }
            if (key == "FLANGE_PROPERTIES")
            {
                double length_m, flangeT_C, flangeP_bar;
                int flangeIndex;
                ss >> flangeIndex >> length_m >> flangeT_C >> flangeP_bar;

                if (flangeIndex >= (int)outFlanges.size())
                {
                    // this is why flanges should be in order.
                    // Need to push a new flange, so keep the flange params and T and P and all that
                    StructStackConfig strFlangeParams;
                    strFlangeParams.length_m = length_m;
                    outFlanges.push_back(strFlangeParams);

                    outFlangeT_C.push_back(flangeT_C);
                    outFlangeP_torr.push_back(flangeP_bar*750.062);
                }
                else
                {
                    outFlanges.at(flangeIndex).length_m = length_m;
                    outFlangeT_C.at(flangeIndex) = flangeT_C;
                    outFlangeP_torr.at(flangeIndex) = flangeP_bar*750.062;
                }
            }
            if (key == "STACK_GAS")
            {
                std::string gas, ind_gas;
                double init_conc, ind_coeff;
                ss >> gas >> init_conc;
                //outStack.length_m = length;

                StructAbsorber strAbsorber;
                strAbsorber.moleculeName = gas;
                strAbsorber.concentration_ppm = init_conc;
                while (ss >> ind_gas >> ind_coeff)
                {
                    strAbsorber.broadeningCoefficents.push_back({ind_gas, ind_coeff});
                }
                outStack.absorbers.push_back(strAbsorber);
            }
            if (key == "FLANGE_GAS")
            {
                std::string gas, ind_gas;
                int flangeIndex;
                double init_conc, ind_coeff;
                ss >> flangeIndex >> gas >> init_conc;

                StructAbsorber strAbsorber;
                strAbsorber.moleculeName = gas;
                strAbsorber.concentration_ppm = init_conc;
                while (ss >> ind_gas >> ind_coeff)
                {
                    strAbsorber.broadeningCoefficents.push_back({ ind_gas, ind_coeff });
                }

                if (flangeIndex >= (int)outFlanges.size())
                {
                    // Need to push a new flange, so keep the flange params and T and P and all that
                    StructStackConfig strFlangeParams;
                    strFlangeParams.length_m = -1.0;
                    strFlangeParams.absorbers.push_back(strAbsorber);

                    outFlanges.push_back(strFlangeParams);
                    outFlangeT_C.push_back(0); // just... because... this works
                    outFlangeP_torr.push_back(0);
                }
                else
                {
                    // Just need to push the flange gas's affected gases and broadening factor.
                    outFlanges[flangeIndex].absorbers.push_back(strAbsorber);
                }
            }
            /*
            if (key == "CELSIUS")
            {
                ss >> safeT_C;
            }
            if (key == "TORR")
            {
                ss >> safeP_torr;
            }
            */
        }
    }
    crossstackFile.close();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void readGoofyINI(const std::string & crossStackIniPath,
    StructLaserRampArguments & outEEPROM)
{
    std::string lineFromFile, key, token; // A line has a key often followed by some tokens

    std::stringstream sstr("");
    std::ifstream crossstackFile;
    crossstackFile.open(crossStackIniPath.c_str(), std::ios::in);

    if( !crossstackFile.is_open() )
    {
        std::cerr << "Error! crossstack.ini not found!" << std::endl;
        return;
    }

    while (getline(crossstackFile, lineFromFile))
    {
        if (lineFromFile.length() < 1 || lineFromFile.at(0) == '#')
        {
            // Skip blank lines or comments (lines starting with #)
        }
        else
        {
            std::istringstream ss(lineFromFile);
            // Read a key and convert to uppercase
            key = "";
            ss >> key;
            std::transform(key.begin(), key.end(), key.begin(), ::toupper);

            // Has to come from EEPROM
            if (key == "ETALON_COEFFICIENTS")
            {
                outEEPROM.etalonCoeffients.assign(5, 0.0);
                ss >> outEEPROM.etalonCoeffients[0];
                ss >> outEEPROM.etalonCoeffients[1];
                ss >> outEEPROM.etalonCoeffients[2];
                ss >> outEEPROM.etalonCoeffients[3];
                ss >> outEEPROM.etalonCoeffients[4];
            }
            // Has to come from EEPROM
            if (key == "SHIFT")
            {
                ss >> outEEPROM.shift_GHz;
            }
            // Has to come from EEPROM
            if (key == "BL_SAMPLE")
            {
                outEEPROM.baselineSample.assign(4, 0);
                ss >> outEEPROM.baselineSample[0];
                ss >> outEEPROM.baselineSample[1];
                ss >> outEEPROM.baselineSample[2];
                ss >> outEEPROM.baselineSample[3];
            }
            // Has to come from EEPROM
            if (key == "FIT_WINDOW")
            {
                outEEPROM.fitWindow.assign(2, 0);
                ss >> outEEPROM.fitWindow[0];
                ss >> outEEPROM.fitWindow[1];
            }
            if (key == "LASER_RAMP_START")
            {
                ss >> outEEPROM.laserRampStart;
            }
            if (key == "LASER_RAMP_LENGTH")
            {
                ss >> outEEPROM.laserRampLength;
            }
            if (key == "LASER_OFF_START")
            {
                ss >> outEEPROM.laserOffStart;
            }
            if (key == "LASER_OFF_LENGTH")
            {
                ss >> outEEPROM.laserOffLength;
            }
        }
    }
    crossstackFile.close();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void readStackInputs(const std::string& broadeningCoeffsIniPath,
    StructStackConfig & outStack,
    double& temperature,
    double& P_torr,
    const GASCONFIG_SIM_FIT_STRUCT& appInputs,
    bool simOrFit )
{
    std::string lineFromFile;

    outStack.absorbers.clear();

    // read from inputsstruct and AllBroadeningCoeffs.ini
    std::ifstream broadeningCoeffsFile;
    broadeningCoeffsFile.open(broadeningCoeffsIniPath.c_str(), std::ios::in);
    double ind_coeff12, ind_coeff21, ind_coeff31, ind_coeff32;
    std::string gasName1, gasName2, gasName3;

    if( !broadeningCoeffsFile.is_open() )
    {
        std::cerr << "AllBroadeningCoefficients.ini path: " << broadeningCoeffsIniPath << std::endl;
        std::cerr << "AllBroadeningCoefficients.ini not found!" << std::endl;
        return;
    }

    // Read data for simulation or for fitting?
    const GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT* appInputsPtr;
    if( simOrFit == true ) // == sim
        appInputsPtr = &appInputs.simData;
    else // == fit
        appInputsPtr = &appInputs.fitData;

    // STACK part
    outStack.length_m = appInputsPtr->stack.L / 1000;
    temperature = appInputsPtr->stack.T;
    P_torr      = bar2torr( appInputsPtr->stack.P );

    StructAbsorber strAbsorber1;
    strAbsorber1.moleculeName = appInputs.nameX1;
    strAbsorber1.concentration_ppm = appInputsPtr->stack.X1;

    StructAbsorber strAbsorber2;
    strAbsorber2.moleculeName = appInputs.nameX2;
    strAbsorber2.concentration_ppm = appInputsPtr->stack.X2;

    // Ignore first two lines of INI file
    getline(broadeningCoeffsFile, lineFromFile);
    getline(broadeningCoeffsFile, lineFromFile);

    bool loopErrorStatus = true;
    while( getline(broadeningCoeffsFile, lineFromFile) )
    {
        std::istringstream ss(lineFromFile);
        gasName1.clear();
        gasName2.clear();

        ss >> gasName1;

        if( gasName1 == strAbsorber1.moleculeName )
        {
            ss >> gasName2;
            if( gasName2 == strAbsorber2.moleculeName )
            {
                ss >> ind_coeff12;
                continue;
            }
            else
            {
                ss.ignore(128,'\t');
                ss >> gasName2;
                if( gasName2 == strAbsorber2.moleculeName )
                {
                    ss >> ind_coeff12;
                    continue;
                }
                else
                    std::cerr << "Error! Broadening coefficient for gases " << strAbsorber1.moleculeName
                              << " and " << strAbsorber2.moleculeName << " not found." << std::endl;
            }
        }
        else if( gasName1 == strAbsorber2.moleculeName )
        {
            ss >> gasName2;
            if( gasName2 == strAbsorber1.moleculeName )
            {
                ss >> ind_coeff21;
                continue;
            }
            else
            {
                ss.ignore(128,'\t');
                ss >> gasName2;
                if( gasName2 == strAbsorber1.moleculeName )
                {
                    ss >> ind_coeff21;
                    continue;
                }
                else
                    std::cerr << "Error! Broadening coefficient for gases " << strAbsorber2.moleculeName
                              << " and " << strAbsorber1.moleculeName << " not found." << std::endl;
            }
        }
        loopErrorStatus = false;
        break;
    }
    if( loopErrorStatus )
        std::cerr << "Error! Name of one of gases: " << strAbsorber1.moleculeName << ", " << strAbsorber2.moleculeName
                  << " not found in AllBroadeningCoefficients.ini!" << std::endl;

    strAbsorber1.broadeningCoefficents.push_back({appInputs.nameX1, ind_coeff12}); // To be created from broadening coeffs struct (?)
    outStack.absorbers.push_back(strAbsorber1);
    strAbsorber2.broadeningCoefficents.push_back({appInputs.nameX2, ind_coeff21});
    outStack.absorbers.push_back(strAbsorber2);

    // Input for additional gases (matrix components)
    if( appInputsPtr->stack.matrX.size() )
    {
        std::vector<StructAbsorber> strAbsorbersVec(appInputsPtr->stack.matrX.size());

        for(unsigned i=0; i<strAbsorbersVec.size(); i++)
        {
            bool loopErrorStatus = false;
            strAbsorbersVec[i].moleculeName = appInputsPtr->stack.nameMatrX[i];
            strAbsorbersVec[i].concentration_ppm = appInputsPtr->stack.matrX[i];

            broadeningCoeffsFile.clear();
            broadeningCoeffsFile.seekg(0, ios::beg);

            // Ignore first two lines of INI file
            getline(broadeningCoeffsFile, lineFromFile);
            getline(broadeningCoeffsFile, lineFromFile);

            // broadening...
            while( getline(broadeningCoeffsFile, lineFromFile) )
            {
                loopErrorStatus = false;

                std::istringstream ss(lineFromFile);
                gasName1.clear();
                gasName2.clear();
                gasName3.clear();

                ss >> gasName3;

                if( gasName3 == strAbsorbersVec[i].moleculeName )
                {
                    ss >> gasName1;

                    if( gasName1 == strAbsorber1.moleculeName )
                        ss >> ind_coeff31;
                    else if( gasName1 == strAbsorber2.moleculeName )
                        ss >> ind_coeff32;
                    else
                        std::cerr << "Error! Broadening coefficient for mixture of " << strAbsorbersVec[i].moleculeName
                                  << " with " << strAbsorber1.moleculeName << " and " << strAbsorber2.moleculeName
                                  << " not found." << std::endl;

                    ss.ignore(128,'\t');
                    ss >> gasName2;

                    if( gasName2 == strAbsorber1.moleculeName )
                        ss >> ind_coeff31;
                    else if( gasName2 == strAbsorber2.moleculeName )
                        ss >> ind_coeff32;
                    else
                        std::cerr << "Error! Broadening coefficient for mixture of " << strAbsorbersVec[i].moleculeName
                                  << " with " << strAbsorber1.moleculeName << " and " << strAbsorber2.moleculeName
                                  << " not found." << std::endl;

                    break;
                }

                loopErrorStatus = true;
            }

            if( loopErrorStatus )
                std::cerr << "Error! Name of gas: " << strAbsorbersVec[i].moleculeName
                          << " not found in AllBroadeningCoefficients.ini!" << std::endl;

            strAbsorbersVec[i].broadeningCoefficents.push_back({strAbsorber1.moleculeName, ind_coeff31}); // To be created from broadening coeffs struct (?)
            strAbsorbersVec[i].broadeningCoefficents.push_back({strAbsorber2.moleculeName, ind_coeff32});
            outStack.absorbers.push_back(strAbsorbersVec[i]);
        }
    }
    broadeningCoeffsFile.close();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void readFlangesInputs(const std::string& broadeningCoeffsIniPath,
    std::vector<StructStackConfig> & outFlanges,
    std::vector<double> & outFlangeT_C,
    std::vector<double> & outFlangeP_torr,
    const GASCONFIG_SIM_FIT_STRUCT& appInputs,
    bool simOrFit)
{
    std::string lineFromFile;

    outFlanges.clear();

    // read from inputsstruct and AllBroadeningCoeffs.ini
    std::ifstream broadeningCoeffsFile;
    broadeningCoeffsFile.open(broadeningCoeffsIniPath.c_str(), std::ios::in);
    double ind_coeff12, ind_coeff21, ind_coeff31, ind_coeff32;
    std::string gasName1, gasName2, gasName3;

    if( !broadeningCoeffsFile.is_open() )
    {
        std::cerr << "AllBroadeningCoefficients.ini not found!" << std::endl;
        return;
    }

    // Read data for simulation or for fitting?
    const GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT* appInputsPtr;
    if( simOrFit == true ) // == sim
        appInputsPtr = &appInputs.simData;
    else // == fit
        appInputsPtr = &appInputs.fitData;

    for(unsigned i=0; i<appInputsPtr->flanges.size(); i++)
    {
        outFlanges.push_back(StructStackConfig());
        outFlangeT_C.push_back(appInputsPtr->flanges[i].T);
        outFlangeP_torr.push_back( bar2torr(appInputsPtr->flanges[i].P) );
    }

    for( unsigned i=0; i<outFlanges.size(); i++ )
    {
        StructAbsorber flStrAbsorber1;
        StructAbsorber flStrAbsorber2;

        std::vector<std::string> flMatxGases;
        std::vector<double>      flMatxConcs;

        outFlanges[i].length_m = appInputsPtr->flanges[i].L / 1000;

        flStrAbsorber1.moleculeName = appInputs.nameX1;
        flStrAbsorber1.concentration_ppm = appInputsPtr->flanges[i].X1;

        flStrAbsorber2.moleculeName = appInputs.nameX2;
        flStrAbsorber2.concentration_ppm = appInputsPtr->flanges[i].X2;

        // Input for additional flange gases (matrix components - set to 2 in INPUT_VARIABLES_STRUCT)
        // Creating artificial vectors for consistency and in case of input structures modifications
        flMatxGases = appInputsPtr->flanges[i].nameMatrX;
        flMatxConcs = appInputsPtr->flanges[i].matrX;

        broadeningCoeffsFile.clear();
        broadeningCoeffsFile.seekg(0, ios::beg);

        // Ignore first two lines of INI file
        getline(broadeningCoeffsFile, lineFromFile);
        getline(broadeningCoeffsFile, lineFromFile);

        bool flLoopErrorStatus = true;
        while( getline(broadeningCoeffsFile, lineFromFile) )
        {
            std::istringstream ss(lineFromFile);
            gasName1.clear();
            gasName2.clear();

            ss >> gasName1;

            if( gasName1 == flStrAbsorber1.moleculeName )
            {
                ss >> gasName2;
                if( gasName2 == flStrAbsorber2.moleculeName )
                {
                    ss >> ind_coeff12;
                    continue;
                }
                else
                {
                    ss.ignore(128,'\t');
                    ss >> gasName2;
                    if( gasName2 == flStrAbsorber2.moleculeName )
                    {
                        ss >> ind_coeff12;
                        continue;
                    }
                    else
                        std::cerr << "Error! Broadening coefficient for gases " << flStrAbsorber1.moleculeName
                                  << " and " << flStrAbsorber2.moleculeName << " not found." << std::endl;
                }
            }
            else if( gasName1 == flStrAbsorber2.moleculeName )
            {
                ss >> gasName2;
                if( gasName2 == flStrAbsorber1.moleculeName )
                {
                    ss >> ind_coeff21;
                    continue;
                }
                else
                {
                    ss.ignore(128,'\t');
                    ss >> gasName2;
                    if( gasName2 == flStrAbsorber1.moleculeName )
                    {
                        ss >> ind_coeff21;
                        continue;
                    }
                    else
                        std::cerr << "Error! Broadening coefficient for gases " << flStrAbsorber2.moleculeName
                                  << " and " << flStrAbsorber1.moleculeName << " not found." << std::endl;
                }
            }
            flLoopErrorStatus = false;
            break;
        }
        if( flLoopErrorStatus )
            std::cerr << "Error! Name of one of gases: " << flStrAbsorber1.moleculeName << ", " << flStrAbsorber2.moleculeName
                      << " not found in AllBroadeningCoefficients.ini!" << std::endl;

        flStrAbsorber1.broadeningCoefficents.push_back({flStrAbsorber2.moleculeName, ind_coeff12}); // To be created from broadening coeffs struct (?)
        outFlanges[i].absorbers.push_back(flStrAbsorber1);
        flStrAbsorber2.broadeningCoefficents.push_back({flStrAbsorber1.moleculeName, ind_coeff21});
        outFlanges[i].absorbers.push_back(flStrAbsorber2);

        if( flMatxGases.size() )
        {
            std::vector<StructAbsorber> flStrAbsorbersVec(flMatxGases.size());

            for(unsigned j=0; j<flStrAbsorbersVec.size(); j++)
            {
                bool loopErrorStatus = false;
                broadeningCoeffsFile.clear();
                broadeningCoeffsFile.seekg(0, ios::beg);

                // Ignore first two lines of INI file
                getline(broadeningCoeffsFile, lineFromFile);
                getline(broadeningCoeffsFile, lineFromFile);

                flStrAbsorbersVec[j].moleculeName = flMatxGases[j];
                flStrAbsorbersVec[j].concentration_ppm = flMatxConcs[j];

                // broadening...
                while( getline(broadeningCoeffsFile, lineFromFile) )
                {
                    loopErrorStatus = false;

                    std::istringstream ss(lineFromFile);
                    gasName1.clear();
                    gasName2.clear();
                    gasName3.clear();

                    ss >> gasName3;

                    if( gasName3 == flStrAbsorbersVec[j].moleculeName )
                    {
                        ss >> gasName1;

                        if( gasName1 == flStrAbsorber1.moleculeName )
                            ss >> ind_coeff31;
                        else if( gasName1 == flStrAbsorber2.moleculeName )
                            ss >> ind_coeff32;
                        else
                            std::cerr << "Error! Broadening coefficient for mixture of " << flStrAbsorbersVec[j].moleculeName
                                      << " with " << flStrAbsorber1.moleculeName << " and " << flStrAbsorber2.moleculeName
                                      << " not found." << std::endl;

                        ss.ignore(128,'\t');
                        ss >> gasName2;

                        if( gasName2 == flStrAbsorber1.moleculeName )
                            ss >> ind_coeff31;
                        else if( gasName2 == flStrAbsorber2.moleculeName )
                            ss >> ind_coeff32;
                        else
                            std::cerr << "Error! Broadening coefficient for mixture of " << flStrAbsorbersVec[j].moleculeName
                                      << " with " << flStrAbsorber1.moleculeName << " and " << flStrAbsorber2.moleculeName
                                      << " not found." << std::endl;

                        break;
                    }

                    loopErrorStatus = true;
                }

                if( loopErrorStatus )
                    std::cerr << "Error! Name of gas: " << flStrAbsorbersVec[j].moleculeName
                              << " not found in AllBroadeningCoefficients.ini!" << std::endl;

                flStrAbsorbersVec[j].broadeningCoefficents.push_back({flStrAbsorber1.moleculeName, ind_coeff31}); // To be created from broadening coeffs struct (?)
                flStrAbsorbersVec[j].broadeningCoefficents.push_back({flStrAbsorber2.moleculeName, ind_coeff32});
                outFlanges[i].absorbers.push_back(flStrAbsorbersVec[j]);
            }
        }
    }

    broadeningCoeffsFile.close();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

std::string makeOutFileName(const std::string & spectraFilePath, const std::string & suffix)
{
    std::size_t indxStart = spectraFilePath.find_last_of('/');
    std::size_t indxStop = spectraFilePath.find_last_of('.');
    std::string fname = spectraFilePath.substr(indxStart + 1, indxStop - indxStart - 1) + "_" + suffix;
    return fname;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void printSpectralThings(const SPCInterface * spcInterface)
{

    std::cout << "Raw signal size " << spcInterface->getRawSignal(0).size()
        << " samplePts size " << spcInterface->getSamplePts(0).size() << "\n";
//    rawSignal is the ringdown appended to the data.  If they were not
//    contiguous then you will really get two separate data chunks
//    slapped together!
    std::cout << "rawSignal = " << spcInterface->getRawSignal(0) << "\n";
    std::cout << "samplePts = " << spcInterface->getSamplePts(0) << "\n";
    std::cout << "\n";

    std::vector<double> baselineFit = spcInterface->getBaselineFit(0);
    unsigned int ibl0 = spcInterface->getBaselineFitStart(0);

    std::cout << "baselineFit = " << baselineFit << "\n";
    std::cout << "ibl0 = range(" << ibl0 << "," << ibl0 + baselineFit.size() << ")\n";
    std::cout << "\n";
    std::cout << "measured = " << spcInterface->getMeasuredData(0) << "\n";
    std::cout << "peakFit = " << spcInterface->getPeakFit(0) << "\n";
    std::cout << "ilr0 = " << spcInterface->getLaserRampStartIdx() << "\n";
    std::cout << "nlr = " << spcInterface->getLaserRampLength() << "\n";
    std::cout << "\n";
    std::cout << "ghz = " << spcInterface->getGHzPts(0) << "\n";
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void printPostFitThings(const SPCInterface* spcInterface)
{
        std::vector<double> rawSignal = spcInterface->getRawSignal(0);
//        std::cout << "rawSignal.size(): " << rawSignal.size() << std::endl;
        std::cout << "rawSignal = " << rawSignal << ";\n";

        std::vector<double> baselineFit = spcInterface->getBaselineFit(0);
//        std::cout << "baselineFit.size(): " << baselineFit.size() << std::endl;
        std::cout << "baselineFit = " << baselineFit << ";\n";

        std::vector<double> ringdownFit = spcInterface->getRingdownFit(0);
//        std::cout << "ringdownFit.size(): " << ringdownFit.size() << std::endl;

        std::vector<unsigned int> samplePts = spcInterface->getSamplePts(0);
//        std::cout << "samplePts.size(): " << samplePts.size() << std::endl;

        unsigned int baselineFitStart = spcInterface->getBaselineFitStart(0);
        std::cout << "baselineFitStart = " << baselineFitStart << ";\n";

        unsigned int ringDownFitStart = spcInterface->getRingdownFitStart(0);
        std::cout << "ringDownFitStart = " << ringDownFitStart << ";\n";

        std::vector<double> measuredData = spcInterface->getMeasuredData(0);
//        std::cout << "measuredData.size(): " << measuredData.size() << std::endl;

        std::vector<double> peakFit = spcInterface->getPeakFit(0);
//        std::cout << "peakFit.size(): " << peakFit.size() << std::endl;
        std::cout << "peakFit = " << peakFit << ";\n";

        std::vector<double> ghzPts = spcInterface->getGHzPts(0);
//        std::cout << "ghzPts.size(): " << ghzPts.size() << std::endl;
        std::cout << "ghzPts = " << ghzPts << ";\n";
}

///************************************************ Some global functions for vector interpolation ****************************************************///

double interpolate1dVector( double xPoint, std::vector<double>&& xArray, std::vector<double>&& yArray )
{
    if( xArray.size() != yArray.size() )
        throw std::runtime_error("Error! Vectors of different lengths passed!");

    const unsigned size = xArray.size();

    if( size == 0 )
        throw std::runtime_error("Error! Array of 0 length passed!");
    else if( size == 1 )
        return xArray[0];
    else
    {
        // Sorting algorithm
        struct pairXY { double X, Y; };
        struct byXval { bool operator()(const pairXY& a, const pairXY& b) { return a.X < b.X; } };
        std::vector<pairXY> xyArray(size);

        for(unsigned i=0; i<size; ++i)
        {
            xyArray[i].X = std::move(xArray[i]);
            xyArray[i].Y = std::move(yArray[i]);
        }

        std::sort(xyArray.begin(), xyArray.end(), byXval());

        double tangent;

        if( xPoint < xyArray[0].X )
        {
            tangent = (xyArray[1].Y - xyArray[0].Y)/(xyArray[1].X - xyArray[0].X);
            return xyArray[0].Y - tangent*(xyArray[0].X - xPoint);
        }
        else if( xPoint >= xyArray[size-1].X )
        {
            tangent = (xyArray[size-1].Y - xyArray[size-2].Y)/(xyArray[size-1].X - xyArray[size-2].X);
            return xyArray[size-1].Y + tangent*(xPoint - xyArray[size-1].X);
        }
        else
        {
            unsigned curi = 0;

            for(unsigned i=0; i<(size-2); ++i)
            {
                if( xPoint >= xyArray[i].X && xPoint < xyArray[i+1].X )
                {
                    curi = i;
                    break;
                }
            }

            tangent = (xyArray[curi+1].Y - xyArray[curi].Y)/(xyArray[curi+1].X - xyArray[curi].X);
            return xyArray[curi].Y + tangent*(xPoint - xyArray[curi].X);
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

double interpolate2dVector( double xPoint, double yPoint, std::vector<double>&& xArray,
                            std::vector<double>&& yArray, std::vector<std::vector<double> >&& zArray )
{
    const unsigned sizeX = xArray.size(), sizeY = yArray.size();

    if( sizeX * sizeY < 3 )
        throw std::runtime_error("Error! Array of too small size passed!");
    else if( sizeX != zArray.size() || sizeY != zArray[0].size() )
        throw std::runtime_error("Error! Arrays' dimensions do not match!");
    else
    {
        // Calculating "flat" distances of map points to the interpolated point
        struct dist { unsigned i, j; double distance; };
        struct byDistance { bool operator()(const dist& a, const dist& b){ return a.distance < b.distance; } };
        std::vector<dist> distances;

        for(unsigned i=0; i<sizeX; ++i)
            for(unsigned j=0; j<sizeY; ++j)
                distances.push_back({i, j, (xArray[i]-xPoint)*(xArray[i]-xPoint)+(yArray[j]-yPoint)*(yArray[j]-yPoint)});

        std::sort(distances.begin(), distances.end(), byDistance());

        // Get three points closest to the interpolated point and pass a plane through them
        struct Vector { double X, Y, Z;
                        Vector(double X = 0.0, double Y = 0.0, double Z = 0.0) : X(X), Y(Y), Z(Z) {}
                        Vector operator-() { return Vector(-X,-Y,-Z); }
                        Vector operator/( double num ) { return Vector(X/num,Y/num,Z/num); }
                        bool operator==(const Vector& V) { return (X == V.X && Y == V.Y && Z == V.Z); }
                        double length() { return sqrt(X*X + Y*Y + Z*Z); } };
        struct Point  { double x, y, z; };

        Point P0 = { xArray[distances[0].i], yArray[distances[0].j], zArray[distances[0].i][distances[0].j] };
        Point P1 = { xArray[distances[1].i], yArray[distances[1].j], zArray[distances[1].i][distances[1].j] };
        Vector P01, P02;

        bool colinear = true;
        unsigned soughtIdx = 2;

        while( colinear )
        {
            Point P2 = { xArray[distances[soughtIdx].i], yArray[distances[soughtIdx].j],
                         zArray[distances[soughtIdx].i][distances[soughtIdx].j] };

            // Check if next closest point is colinear with the first two
            P01.X = P1.x - P0.x, P01.Y = P1.y - P0.y, P01.Z = P1.z - P0.z;
            Vector n01 = P01 / P01.length();
            P02.X = P2.x - P0.x, P02.Y = P2.y - P0.y, P02.Z = P2.z - P0.z;
            Vector n02 = P02 / P02.length();

            if( n01 == n02 || n01 == -n02 )
                ++soughtIdx;
            else
                colinear = false;
        }

        std::cout << "P01.x = " << P01.X << ", P01.y = " << P01.Y << ", P01.z = " << P01.Z << std::endl;
        std::cout << "P02.x = " << P02.X << ", P02.y = " << P02.Y << ", P02.z = " << P02.Z << std::endl;

        double A, B, C, D;
        A = P01.Y * P02.Z - P02.Y * P01.Z;
        B = P01.Z * P02.X - P02.Z * P01.X;
        C = P01.X * P02.Y - P02.X * P01.Y;
        D = A * xArray[distances[0].i] + B * yArray[distances[0].j] + C * zArray[distances[0].i][distances[0].j];

        std::cout << "RETVAL = " << ( D - A * xPoint - B * yPoint )/C << std::endl;
        std::cout << "A = " << A << ", B = " << B << ", C = " << C << ", D = " << D << std::endl;
        return ( D - A * xPoint - B * yPoint )/C;
    }
}
